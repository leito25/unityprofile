﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditorInternal;
using UnityEditor;
using System;
using UnityEngine.Rendering;

using System.Reflection;
using Object = UnityEngine.Object;

public class GameManagerS : MonoBehaviour {

    public GameObject[] objetosScena;
    public String[] objetosScenaString;
    public GameObject objetPourTeste;
    public GameObject objetPourTesteDos;

    public long startMemoryAmount;

    public long initMemory;
    public long currentMemory;

    public long objMemory;

    public Scene currentScene;

    public Renderer[] rendercitos;

    public Texture[] texturecitos;
    public Texture2D[] texturecitos2D;
    public Texture3D[] texturecitos3D;

    public List<Texture> textureList;

    public Material[] materialsRenderer;

    public List<Texture> allTexture = new List<Texture>();
    public Shader shader;

    // Use this for initialization
    void Start () {

        currentScene = SceneManager.GetActiveScene();

        //Debug.Log(currentScene.GetRootGameObjects());

        objetosScena = currentScene.GetRootGameObjects();

        //amount = 1020;
        initMemory = System.GC.GetTotalMemory(false);
        startMemoryAmount = initMemory;
        Debug.Log(System.GC.GetTotalMemory(false));
        //var structs = new Quaternion[amount];
        //Debug.Log((System.GC.GetTotalMemory(false)-mem)/amount);

        

        


    }

    public void LoadTree()
    {


        GameObject clone = Instantiate(objetPourTeste, transform.position, transform.rotation);
        Debug.Log("Init_Mem"+initMemory/8);
        currentMemory = System.GC.GetTotalMemory(false);
        Debug.Log("Current_Mem" + currentMemory);
        objMemory = initMemory - currentMemory;
        Debug.Log("OBJ_Mem" + objMemory);

        objetosScena = currentScene.GetRootGameObjects();
        initMemory = currentMemory;
        Debug.Log(startMemoryAmount);


    }

    public void LoadTree2()
    {


        GameObject clone2 = Instantiate(objetPourTesteDos, transform.position, transform.rotation);
        Debug.Log("Init_Mem" + initMemory / 8);
        currentMemory = System.GC.GetTotalMemory(false);
        Debug.Log("Current_Mem" + currentMemory);
        objMemory = initMemory - currentMemory;
        Debug.Log("OBJ_Mem" + objMemory);

        objetosScena = currentScene.GetRootGameObjects();
        initMemory = currentMemory;
        Debug.Log(startMemoryAmount);


    }

    public void CheckType()
    {

        rendercitos = FindObjects<Renderer>();

        texturecitos = FindObjectsOfType<Texture>();
        texturecitos2D = FindObjectsOfType<Texture2D>();
        texturecitos3D = FindObjectsOfType<Texture3D>();


        foreach (Renderer rendercito in rendercitos)
        {
            //
            materialsRenderer = rendercito.materials.ToArray<Material>();

            allTexture = new List<Texture>();
            shader = rendercito.sharedMaterial.shader;
            for (int i = 0; i < ShaderUtil.GetPropertyCount(shader); i++)
            {
                if (ShaderUtil.GetPropertyType(shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                {
                    Texture texture = rendercito.sharedMaterial.GetTexture(ShaderUtil.GetPropertyName(shader, i));
                    allTexture.Add(texture);
                }
            }

            foreach (Material material in rendercito.sharedMaterials)
            {
                //

                

            }
            //
        }

    }

    public T[] FindObjects<T>() where T : Object
    {
        List<T> meshfilters = new List<T>();
        GameObject[] allGo = currentScene.GetRootGameObjects();
        foreach (GameObject go in allGo)
        {
            Transform[] tgo = go.GetComponentsInChildren<Transform>(true).ToArray();
            foreach (Transform tr in tgo)
            {
                if (tr.GetComponent<T>())
                    meshfilters.Add(tr.GetComponent<T>());
            }
        }
        return (T[])meshfilters.ToArray();

    }



    // Update is called once per frame
    void Update () {
		
	}
}
