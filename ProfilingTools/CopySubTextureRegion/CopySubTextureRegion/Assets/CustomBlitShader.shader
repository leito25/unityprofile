﻿Shader "CopySubTextureRegion/CustomBlitShader"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			uniform sampler2D _MainTex;
            uniform float4 _MainTex_TexelSize;
            uniform float4 _SourceRect;

			v2f vert (appdata v)
			{
				v2f o;

                float2 topLeftCornerUv = _SourceRect.xy * _MainTex_TexelSize.xy;
                float2 bottomRightCornerUv = (_SourceRect.xy + _SourceRect.zw) * _MainTex_TexelSize.xy;
				o.uv = lerp(topLeftCornerUv, bottomRightCornerUv, v.uv);

                o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
