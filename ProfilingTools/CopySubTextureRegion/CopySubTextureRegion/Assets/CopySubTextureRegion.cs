﻿

[UnityEngine.ExecuteInEditMode]
public class CopySubTextureRegion : UnityEngine.MonoBehaviour
{
    [UnityEngine.SerializeField]
    private UnityEngine.Camera renderCamera = null;

    [UnityEngine.SerializeField]
    private UnityEngine.RectInt grabRect = new UnityEngine.RectInt(100, 100, 400, 400);

    [UnityEngine.SerializeField]
    private TestMode test = TestMode.CopyTexture;

    [UnityEngine.SerializeField]
    private UnityEngine.Material blitMaterial = null;

    private enum TestMode
    {
        CopyTexture,
        Blit
    };

    private UnityEngine.Rendering.CommandBuffer commandBuffer = null;
    private int grabRenderTargetId = -1;

    protected void OnEnable()
    {
        this.commandBuffer = new UnityEngine.Rendering.CommandBuffer();
        this.renderCamera.AddCommandBuffer(UnityEngine.Rendering.CameraEvent.AfterEverything, this.commandBuffer);

        this.grabRenderTargetId = UnityEngine.Shader.PropertyToID("_GrabRenderTarget");
    }

    protected void OnDisable()
    {
        if (this.commandBuffer != null)
        {
            if (this.renderCamera != null)
            {
                this.renderCamera.RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent.AfterEverything, this.commandBuffer);
            }

            this.commandBuffer.Release();
        }
    }

    protected void Update()
    {
        this.commandBuffer.Clear();

        this.commandBuffer.GetTemporaryRT(this.grabRenderTargetId, grabRect.width, grabRect.height, 0, UnityEngine.FilterMode.Point, UnityEngine.RenderTextureFormat.ARGB32, UnityEngine.RenderTextureReadWrite.sRGB);        

        if (this.test == TestMode.CopyTexture)
        {
            this.commandBuffer.CopyTexture(UnityEngine.Rendering.BuiltinRenderTextureType.CurrentActive, 0, 0, grabRect.x, grabRect.y, grabRect.width, grabRect.height, this.grabRenderTargetId, 0, 0, 0, 0);
        }
        else if (this.test == TestMode.Blit)
        {
            this.blitMaterial.SetVector("_SourceRect", new UnityEngine.Vector4(this.grabRect.x, this.grabRect.y, this.grabRect.width, this.grabRect.height));
            this.commandBuffer.Blit(UnityEngine.Rendering.BuiltinRenderTextureType.CurrentActive, this.grabRenderTargetId, this.blitMaterial);
        }
    }

#if UNITY_EDITOR

    protected void OnGUI()
    {
        UnityEngine.Texture grabRenderTarget = UnityEngine.Shader.GetGlobalTexture(this.grabRenderTargetId);
        if (grabRenderTarget != null)
        {
            UnityEngine.GUI.DrawTexture(new UnityEngine.Rect(UnityEngine.Screen.width - grabRect.width, UnityEngine.Screen.height - grabRect.height, grabRect.width, grabRect.height), grabRenderTarget);
        }
    }

#endif
}