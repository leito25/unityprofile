﻿package
{
	/**
	 * ...
	 * @author ...
	 */
	public class GlobalTexts
	{
		private static var _instance:GlobalTexts;
		
        private var _xmlData:XML;

		public static function getInstance():GlobalTexts
		{
			if( _instance == null ) _instance = new GlobalTexts( new SingletonEnforcer() );
			return _instance;
		}
		
		public function GlobalTexts( singletonEnforcer:SingletonEnforcer )
		{
		}

        public static function get xmlData():XML
        {
            return(getInstance()._xmlData);
        }

        public static function set xmlData(value:XML):void
        {
            getInstance()._xmlData = value;
        }
    }
}

internal class SingletonEnforcer{}