﻿package owncode {

	//modulos flash
	import flash.display.*;
	import flash.events.*;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.text.TextField;

	//newgrounds api
	import com.newgrounds.*;
	import com.newgrounds.components.*;

	//clase carta
	import owncode.cardClass;
	import flash.ui.Mouse;
	//import com.adobe.tvsdk.mediacore.events.TimedEvent;
	import owncode.main;
	
	import owncode.GlobalVars;

	public dynamic
	class matchingGame extends MovieClip {
		
		trace("idio: " + GlobalVars.getNW_user() );
		trace(" - : " + GlobalVars.getNW_score());

		private var theLevel: String;

		//variable del juego
		//espacio entre
		private var space: uint = 5;

		//constantes para la repartición de cartas
		private var columnas: uint;
		private var filas: uint;
		private var espacioHorizontal: Number;
		private var espacioVertical: Number;
		private var offSetX: Number;
		private var offSetY: Number;

		//constantes para el score
		
		private var point_Match: int = 20;
		private var point_Miss: int = -5;

		//Textfield Score
		private var gameScoreField: TextField = new TextField();
		var myScoreIcon: mc_StatsIcon = new mc_StatsIcon();
		//Score variable
		private var gameScore: int;//first bonus

		//Game Timer
		private var gameStartTime: uint;
		private var gameTime: uint;
		//Game Timer Field
		var myTimerIcon: mc_StatsIcon = new mc_StatsIcon();
		
		//User Data
		private var user_name:String;
		public var myUserName: mc_StatsLabel = new mc_StatsLabel();

		//FLIP-Timer
		private var flipBackTimer: Timer;

		//newgrounds event


		//Arreglo logico para el mazo de cartas
		public var theCardList: Array = new Array;

		//Objeto gráfico para el mazo de cartas
		public var cardList_MovieClips: MovieClip;

		//variables para las cartas que se enfrentan
		public var firstCard: cardClass;
		public var secondCard: cardClass;

		//variable para las cartas restantes
		//-cuando son adivinadas
		private var cardsLeft: uint;
		
		//

		//starstamp for the game
		//getTimer inicia un contador en tiempo real

		//matchin game --> mg

		//función para repartir cartas
		public function mgSpreadCards(_columnas: uint,
			_filas: uint, _espacioHorizontal: uint, _espacioVertical: uint,
			_boardOffSetX: uint, _boardOffSetY: uint /*, _level:String*/ ) {
			//
			//trace(_columnas + "-" + _filas + "-" + _espacioHorizontal + "-" + _espacioVertical);
			this.columnas = _columnas;
			this.filas = _filas;
			this.espacioHorizontal = _espacioHorizontal;
			this.espacioVertical = _espacioVertical;
			this.offSetX = _boardOffSetX;
			this.offSetY = _boardOffSetY;

			//this.theLevel = _level;

			//this.mgBuildLevel(theLevel);

		}

		public function mgBuildLevel(_level: String) {
			//building level
			//trace("Building level: " + this.theLevel + "param: " + _level);

			//Game time start
			gameStartTime = getTimer();

			//initialize gameScore and gameTime vars
			gameScore = 0;
			gameTime = 0;

			//adding timer to the STAGE
			addEventListener(Event.ENTER_FRAME, showTime);



			//btn de rest -- eliminado por ahora

			//construcción del array de pares
			//este es un array abstracto - no grafico
			//CUENTA LA MITAD DE CARTAS DEL MAZO
			//EL MAZO ES LA SUMA DE FILAS Y COLUMNAS
			for (var i: uint = 0; i < (this.columnas * this.filas / 2); i++) {

				//se agregan de a pares al array
				theCardList.push(i);
				theCardList.push(i);

			}

			//cartas restantes - cardsLeft
			//--Para chequear cuando termine
			cardsLeft = 0;
			//repartir las cartas
			for (var j: uint = 0; j < this.columnas; j++) {
				for (var k: uint = 0; k < this.filas; k++) {
					//objeto carta
					var theCard;

					//asiganación del numero de nivel
					//a la variable 
					theLevel = _level;

					//SELECCIÓN DE NIVEL
					switch (theLevel) {
						case "00":
							trace("Level 1 ");
							var theCardMZ01: mazoCardDeck01 = new mazoCardDeck01();
							theCard = theCardMZ01;
							break;
						case "10":
							trace("Level 2");
							var theCardMZ02: mazoCardDeck02 = new mazoCardDeck02();
							theCard = theCardMZ02;
							break;
						case "20":
							trace("Level 3");
							var theCardMZ03: mazoCardDeck03 = new mazoCardDeck03();
							theCard = theCardMZ03;
							break;
						case "30":
							trace("Level 4");
							var theCardMZ04: mazoCardDeck04 = new mazoCardDeck04();
							theCard = theCardMZ04;
							break;
						case "01":
							trace("Level 5");
							//var theCardMZ01: mazoCardDeck01 = new mazoCardDeck01();
							//theCard = theCardMZ01;
							break;
						case "11":
							trace("Level 6");
							//var theCardMZ01: mazoCardDeck01 = new mazoCardDeck01();
							//theCard = theCardMZ01;
							break;
						case "21":
							trace("Level 6");
							//var theCardMZ01: mazoCardDeck01 = new mazoCardDeck01();
							//theCard = theCardMZ01;
							break;
						case "31":
							trace("Level 8");
							//var theCardMZ01: mazoCardDeck01 = new mazoCardDeck01();
							//theCard = theCardMZ01;
							break;
					}

					//stop the card
					//La carta es un clip con fotogramas
					//La carta backFace es el frame 1
					//La carta al inicio esta en Stop
					theCard.stop();

					//ubicación en la cuadricula
					theCard.x = j * espacioHorizontal + offSetX; //Spread X
					theCard.y = k * espacioVertical + offSetY; //Spread Y
					//trace("x: " + theCard.x + "" + "y: " + theCard.y);

					//render de las cartas pero en una ubicación random
					// se randomiza los valores del arreglo
					var r: uint = Math.floor(Math.random() * theCardList.length);

					//propiedad agregada al objeto CARD que deriva del arreglo logico
					theCard.cardface = theCardList[r]; //esto numera las cartas graficas

					//splice elimina cada uno de los elementos del array que se utilizan
					//reammplazando
					theCardList.splice(r, 1);

					//Evento click para la carta
					theCard.addEventListener(MouseEvent.CLICK, touchCard);

					addChild(theCard);

					cardsLeft++;

				}
			}

			//Building....

			//trace(this.root.width);
			
			//LABEL -Username
			user_name = API.username;
			myUserName.statsTextField.text = String(user_name);
			myUserName.x = this.width + space * 12;
			myUserName.y = 10;
			addChild(myUserName);
			

			//TIMER Icon
			myTimerIcon.x = myUserName.x;
			myTimerIcon.y = myUserName.y + 35;
			//myTimerIcon.statsTextField.text = String(gameTime);
			addChild(myTimerIcon);

			//SCORE Icon
			myScoreIcon.x = myTimerIcon.x;
			myScoreIcon.y = myTimerIcon.y + 60;
			myScoreIcon.statsTextField.text = String(gameScore);
			addChild(myScoreIcon);
			
			


		}
		
		

		public function touchCard(event: MouseEvent) {

			//local var for the card
			var thisCard: cardClass = (event.currentTarget as cardClass);

			if (firstCard == null) //-primer toque
			{
				//from clicked to firstCard var
				firstCard = thisCard;

				//flipping the card
				thisCard.startFlip(thisCard.cardface + 2);

			} else if (firstCard == thisCard) {
				//flipBack
				thisCard.startFlip(1);
				firstCard = null;

			} else if (secondCard == null) {
				//click para la second card
				secondCard = thisCard;
				thisCard.startFlip(thisCard.cardface + 2);

				if (firstCard.cardface == secondCard.cardface) {
					//
					removeChild(firstCard);
					removeChild(secondCard);
					//firstCard.enabled = false;
					//secondCard.enabled = false;

					firstCard = null;
					secondCard = null;

					gameScore += point_Match;
					trace(gameScore);
					myScoreIcon.statsTextField.text = String(gameScore);

					cardsLeft -= 2;
					
					
					
					if (cardsLeft == 0) {
						///GAME OVER
						trace("GAME OVER");
						
						//congrats screen - stars
						//-user name
						//-point
						//-menu
						
						//build a good screen
						
						/*menu.x = stage.width / 2;
						menu.y = stage.height / 2;
						addChild(menu);


						btn.x = stage.width / 2; //menu.width / 2;
						btn.y = stage.height / 2; //menu.height / 2;
						btn.gotoAndStop(1);
						addChild(btn);

						btn.b_label.text = "START";*/
						
						
						
						
					}

				} else {
					//
					gameScore += point_Miss;

					//-
					flipBackTimer = new Timer(500, 1);
					flipBackTimer.addEventListener(TimerEvent.TIMER_COMPLETE, returnCards);
					flipBackTimer.start();

					myScoreIcon.statsTextField.text = String(gameScore);


				}

			} else {

				trace("si la segunda no esta vacia");

				returnCards(null);

				// select first card in next pair
				firstCard = thisCard;
				firstCard.startFlip(thisCard.cardface + 2);

			}


		}

		//turnBack the cards - trigered by a timer
		//dar vuelta a las cartas de nuevo- cerrar partida
		public function returnCards(event: TimerEvent) {
			//backFlip the cards when time finished
			if (firstCard != null) firstCard.startFlip(1);
			if (secondCard != null) secondCard.startFlip(1);

			//poner cartas en null - disponibles
			firstCard = null;
			secondCard = null;

			//remove event
			flipBackTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, returnCards);
		}

		public function clockTime(ms: int) {
			//
			var seconds: int = Math.floor(ms / 1000);
			var minutes: int = Math.floor(seconds / 60);
			seconds -= minutes * 60;

			var timeString: String = minutes + ":" + String(seconds + 100).substr(1, 2);

			return timeString;
		}

		//timer function - Show Time
		public function showTime(event: Event) {

			//
			gameTime = getTimer() - gameStartTime;
			myTimerIcon.statsTextField.text = clockTime(gameTime);
			//trace(gameTime + " - - " + getTimer() +" - "+ gameStartTime);

		}




	}


}