﻿package owncode{

	import flash.display.MovieClip;
	import flash.display.*;
	import flash.events.TouchEvent;
	import flash.events.*;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.events.MouseEvent;
	
	//kongregate API
	import flash.display.LoaderInfo;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.system.Security;

	//newgrounds API
	import com.newgrounds.*;
	import com.newgrounds.components.*;
	import flash.text.TextField;
	
	//own classes
	import owncode.cardClass;
	import owncode.matchingGame;
	
	//
	import owncode.GlobalVars;

	//API.connect(root, appid, encryptKey);
	//API.addEventListener(APIEvent.API_CONNECTED, onAPIConnected);
	//API.connect([root], "46114:02MQPmRq", "iuLpxNfI1vE4uMArYrP2M050q5PXRf5X");

	public class main extends MovieClip {
		
		//nueva instancia del juego de MAtching
		public var MyGame:MovieClip;

		//Space
		private static const space = 5;

		//constantes para la construcción cartas
		private static const columnss: uint = 6; //número de cartas horizontales
		private static const rowss: uint = 4; //número de cartas verticales
		private static const cardHorizontalSpace: Number = 85; //
		private static const cardVerticalSpace: Number = 110;
		private static const boardOffSetX: Number = 160;
		public static const boardOffSetY: Number = 70;

		//constantes para la construcción menu
		private static const m_columnss: uint = 4; //número de cartas horizontales
		private static const m_rowss: uint = 2; //número de cartas verticales
		private static const m_cardHorizontalSpace: Number = 160; //
		private static const m_cardVerticalSpace: Number = 160;
		private static const m_boardOffSetX: Number = 100;
		private static const m_boardOffSetY: Number = 80;

		//constantes para el score
		private static const pointForMatch: int = 50;
		private static const pointForMiss: int = -5;

		//Timer flip
		private var flipBackTimer_: Timer;

		//--Newgrounds Objects
		//var NewgroundsObj:API = new API();
		var flashADs: FlashAd = new FlashAd();
		var flashADs_01:FlashAd = new FlashAd();
		
		//--Kongregate API
		private var kongregate:*;

		//objeto paraa splash
		var mySplash: Splash = new Splash();
		
		//objeto para cinematica
		var myCinematica: mc_Cinematica = new mc_Cinematica();

		//objeto para el fondo
		var mybg: BgRef = new BgRef();

		//objetos para menu
		var menu: Menu = new Menu();
		var btn: Buton = new Buton();

		//arreglo de clips para los botones de los niveles
		var levelList: MovieClip = new MovieClip();
		var levelBtnClip: MovieClip = new MovieClip();

		//mazo de las cartas dado en un arreglo logico
		var cardList: Array = new Array();

		//mazo grafico en un clip
		var cardList_MC: MovieClip = new MovieClip();

		//variables for the two card states
		private var firstCard_: cardClass;
		private var secondCard_: cardClass;

		//check the cards left
		private var cardsLeft: uint;
		
		//vars kongregate
		public var k_user;
		
		//Arreglo logico para el menu
		public var levelArray:Array = new Array();
		
		//Boton de inicio
		var startBtnB: Buton = new Buton();
		
		//Start Screen
		var startScreen:mc_StartScreen = new mc_StartScreen();
		
		//MAIN FUNCTION
		public function main() {
			
			//funcion para el loading
			//---
			//TODO
			
			//main function - para la carga del stage
			if (stage) init();
            else addEventListener(Event.ADDED_TO_STAGE, init);

		}
		
		public static function home():void
		{
			//
			trace("hello-reset");
		}
		
		
		private function init(e:Event = null):void {
			
            removeEventListener(Event.ADDED_TO_STAGE, init);
			
			if (CONFIG::PLATFORM == "Kongregate"){
				//init Kongragate API
				iniKongregateAPI();
			} else {
				//Init NewGrounds
				initNewGrounds();
			}
			
            // entry point
			showSplash(); //showing splash
        }
		
		private function initNewGrounds():void{
			//conexión al API
			API.connect(stage, "46114:02MQPmRq", "iuLpxNfI1vE4uMArYrP2M050q5PXRf5X");

			if (API.isNewgrounds) {
				//
				trace("conectado a newGrounds");
				//API.unlockMedal("Begin_medal");
			}
		}
		
		//
		private function iniKongregateAPI():void {
			//
			//Pull del api de las flashVars
			var paramsObj:Object = LoaderInfo(root.loaderInfo).parameters;
			
			//Loacal testing with the shadow API
			var apiPath:String = paramsObj.kongregate_api_path || 
			"http://www.kongregate.com/flash/API_AS3_Local.swf";
			
			// Allow the API access to this SWF
			Security.allowDomain(apiPath);
			
			// Load the API
			var request:URLRequest = new URLRequest(apiPath);
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadComplete);
			loader.load(request);
			this.addChild(loader);  
		}
		
		// This function is called when loading is complete
		private function loadComplete(event:Event):void {
			// Save Kongregate API reference
			kongregate = event.target.content;
         
			// Connect to the back-end
			kongregate.services.connect();
             
			// You can now access the API via:
			// kongregate.services
			// kongregate.user
			// kongregate.scores
			// kongregate.stats
			// etc...
			//
			k_user = kongregate.user;
			GlobalVars.setNW_user(k_user);
			GlobalVars.setNW_score(kongregate.scores);

			//trace(k_userF());
			
		}
		
		/*public function k_userF(){
			var k = k_user;
			return k;
		}*/


		//--funcion para la carga del splash, ?how dinamicaly
		public function showSplash(): void {

			//tamaño y cordenadas del splash
			mySplash.x = 400;
			mySplash.y = 240;
			mySplash.width = 800;
			mySplash.height = 480;
			addChild(mySplash);

			//timer del splash
			var myTimer: Timer = new Timer(5000, 1); // 1 second
			myTimer.addEventListener(TimerEvent.TIMER, runCinematica);
			myTimer.start()
			

		}

		//función inicial para la carga del fondo
		public function runCinematica(event: TimerEvent): void {
			trace("runOnce() called @ " + getTimer() + " ms");
			removeChild(mySplash);
			
			myCinematica.x = 0;
			myCinematica.y = 0;
			myCinematica.width = 800;
			myCinematica.height = 480;
			
			myCinematica.gotoAndStop(0);
			
			var myCinematic_totalFrames:int = myCinematica.totalFrames;
			//trace("Total Frames" + myCinematic_totalFrames);
			
			//timer del splash
			var myTimerFrames: Timer = new Timer(500, myCinematic_totalFrames); // 1 second
			myTimerFrames.addEventListener(TimerEvent.TIMER, runCinematicaFrames);
			myTimerFrames.start();
			
			addChild(myCinematica);
			
			//loadBackground();
		}
		
		//--
		public function runCinematicaFrames(event: TimerEvent):void {
			
			//running cinematic
			/*if (myCinematica.currentFrame > myCinematica.getTimeline().frameCount){
				myCinematica.nextFrame();
			} else {
				loadBackground();
			}*/
			
			myCinematica.nextFrame();
			
			if (myCinematica.currentFrame == myCinematica.totalFrames)
			{
				//
				
				trace("A: "+ myCinematica.currentFrame + "B: " + myCinematica.totalFrames);
				//cargar boton siguiente
				//myCinematica.stop();
				cargarBtnStart();
				
			}
			
		}
		
		public function cargarBtnStart():void
		{
			
			
			
			//carga de boton de decisión
			startBtnB.x = (stage.width/2);
			startBtnB.y = (stage.height/2);
			//nombre
			startBtnB.stop();
			startBtnB.b_label.text = "Chooose";
			
			startBtnB.addEventListener(MouseEvent.CLICK, clickStart);
			
			addChild(startBtnB);
		}
		
		public function clickStart(event:MouseEvent):void
		{
			//
			startBtnB.removeEventListener(MouseEvent.CLICK, clickStart);
			trigger();
		}
		
		public function trigger():void {
			//
			removeChild(startBtnB);
			removeChild(myCinematica);
			
			touchStartScreen();
		}
		
		public function touchStartScreen():void{
			//mostrar pantalla de inicio
			startScreen.x = 0;
			startScreen.y = 0;
			startScreen.addEventListener(MouseEvent.CLICK, clickStartLevel);
			
			addChild(startScreen);
			
		}
		
		public function clickStartLevel(event:MouseEvent):void
		{
			//
			//startBtn.removeEventListener(MouseEvent.CLICK, clickStart);
			startScreen.removeEventListener(MouseEvent.CLICK, clickStartLevel);
			//removeChild(startScreen);
			
			loadBackground();
		}
		
		

		//-->función para la carga del background
		public function loadBackground(): void {
			trace("HH");
			//remover la funcion
			//removeEventListener(TimerEvent.TIMER, runCinematicaFrames)
			
			//remove Cinematica
			removeChild(startScreen);
			
			//remover la funcion de temporizador

			//coodenadas y add to stage(render)
			mybg.x = 0;
			mybg.y = 0;
			addChild(mybg);

			//carga función de los levels
			levelsMenu();

		}

		public function levelsMenu(): void {
			
			//construcci{on arreglo par los niveles
			var fotograma:int = 0;

			//arreglo de niveles
			for (var x: uint = 0; x < m_columnss; x++) //horizontal
			{
				for (var y: uint = 0; y < m_rowss; y++) //vertical
				{

					//objeto boton nivel
					var thelvlBtn: lvlBtn = new lvlBtn();

					//detener clip si tiene animación
					thelvlBtn.stop();

					//--acomodación de botones
					//formula para hacer la cuadricula
					thelvlBtn.x = x * m_cardHorizontalSpace + m_boardOffSetX; //spread x
					//trace( theCard.x + "and" + theCard.y);
					thelvlBtn.y = y * m_cardVerticalSpace + m_boardOffSetY; //spread

					thelvlBtn.numberOfLevel = x + "" + y;
					
					
					//fotograma
					fotograma += 1;
					thelvlBtn.gotoAndStop(fotograma);
					trace("Foto" + fotograma);
					

					//adding the click event
					thelvlBtn.addEventListener(MouseEvent.CLICK, touchLevel);

					//render each card packed in another clip
					levelList.addChild(thelvlBtn);


				}

			}
			//main List Clip
			
			//prueba con for each
			//for (var x: uint = 0; x < m_columnss; x++) //horizontal
			
			trace("booom");
			addChild(levelList);

		}

		public function touchLevel(event: MouseEvent): void {


			var thislvl: lvlBtn = (event.currentTarget as lvlBtn); //What Card?
			trace("COOL" + thislvl.numberOfLevel);

			//asignación a clip logico temporal
			levelBtnClip = thislvl;

			//remover botones de menu de level
			removeChild(levelList);
			startMenu();

		}

		public function startMenu(): void {

			menu.x = stage.width / 2;
			menu.y = stage.height / 2;
			addChild(menu);


			btn.x = stage.width / 2; //menu.width / 2;
			btn.y = stage.height / 2; //menu.height / 2;
			btn.gotoAndStop(1);
			addChild(btn);

			btn.b_label.text = "START";

			btn.addEventListener(MouseEvent.CLICK, startBtn);
			//btn-like Events
			btn.addEventListener(MouseEvent.ROLL_OVER, btn_RollOver);
			btn.addEventListener(MouseEvent.ROLL_OUT, btn_RollOut);
		}

		public function startBtn(event: MouseEvent) {
			
			//ADs
			//Load Ads
			flashADs.width = 150;
			flashADs.height = 150;
			flashADs.x = stage.width - flashADs.width-10;
			flashADs.y = stage.height - (stage.height / 3);
			
			flashADs_01.width = 150;
			flashADs_01.height = 150;
			flashADs_01.x = flashADs.x;
			flashADs_01.y = flashADs.y - 160;
			
			addChild(flashADs);
			addChild(flashADs_01);

			removeChild(menu);
			removeChild(btn);

			//spreadCards();
			
			MyGame = new matchingGame();
			
			MyGame.x = 0;
			MyGame.y = 0;
			
			MyGame.mgSpreadCards(7, 4, 85, 110, 60, 70/*, this.levelBtnClip.numberOfLevel*/);
			MyGame.mgBuildLevel(levelBtnClip.numberOfLevel);
			addChild(MyGame);
		}

		public function btn_RollOver(event: MouseEvent) {

			btn.gotoAndStop(2); //

		}

		public function btn_RollOut(event: MouseEvent) {

			btn.gotoAndStop(1); //

		}

		public function resetTouch(event: MouseEvent): void {
			//

			removeChild(cardList_MC);
			levelsMenu();

		}

		
		

		


	}

}