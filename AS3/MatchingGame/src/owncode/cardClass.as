﻿package owncode {
	import flash.display.*;
	import flash.events.*;
	
	public dynamic class cardClass extends MovieClip {
		private var flipStep:uint;
		private var isFlipping:Boolean = false;
		private var flipToFrame:uint;
		trace("hello");
		// begin the flip, remember which frame to jump to
		public function startFlip(flipToWhichFrame:uint) {
			isFlipping = true;
			flipStep = 10;
			flipToFrame = flipToWhichFrame;
			addEventListener(Event.ENTER_FRAME, flip);
		}
		
		// take 10 steps to flip
		public function flip(event:Event) {
			flipStep--; // next step
			
			if (flipStep > 5) { // first half of flip
				this.scaleX = .20*(flipStep-6);
			} else { // second half of flip
				this.scaleX = .20*(5-flipStep);
			}
			
			// when it is the middle of the flip, go to new frame
			if (flipStep == 5) {
				gotoAndStop(flipToFrame);
			}
			
			// at the end of the flip, stop the animation
			if (flipStep == 0) {
				removeEventListener(Event.ENTER_FRAME, flip);
			}
		}
	}
}