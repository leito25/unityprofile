﻿package owncode
{
	import flash.display.Bitmap;

	/**
	 * ...
	 * @author ...
	 */
	public class GlobalVars
	{
		private static var _instance:GlobalVars;

		private var _verbtn:Boolean = false;
		private var _Swidth:int = 0;
		private var _Sheigth:int = 0;
		private var _Sactualwidth:int = 0;
		private var _Sactualheigth:int = 0;
		private var _Idioma:String = CONFIG::LANGUAGE;
		private var _BtnInfoX:Number = 0;
		private var _clickLan:Boolean = false;
		
		private var _CalcAncho:Number = 0;
		private var _CalcAlto:Number = 0;
		
		private var _OS:String = "";		
		private var _tipo_dispositivo:String = "";
		private var _userName:String = "";
		private var _idCurso:String = "";
		private var _idInstitucion:String = "";
		private var _curso:String = "";
		private var _situacionSolicitada:uint = 0;
        private var _xmlPreguntas:XML;
        private var _tituloReporte:String = "";
        private var _nombreCurso:String = "";
        private var _nombrePractica:String = "";
		private var _xmlDiagnostico:XML;
		private var _imageLogo:Bitmap;
		
		public static var totalScore:Number;
		public static var recordTime:Number;
		//variables por nivel
		
		public static var NG_user:String = "";
		
		public static function getNW_user():String
		{
			return (NG_user);
		}
		
		public static function setNW_user(value:String):void
		{
			NG_user = value;
		}
		
		public static var NG_score:int;
		
		public static function getNW_score():int
		{
			return (NG_score);
		}
		
		public static function setNW_score(value:int):void
		{
			NG_score = value;
		}

		public static function getInstance():GlobalVars
		{
			if( _instance == null ) _instance = new GlobalVars( new SingletonEnforcer() );
			return _instance;
		}
		
		public function GlobalVars( singletonEnforcer:SingletonEnforcer )
		{
		}
		


		public static function get CalcAlto():Number 
		{
			return(getInstance()._CalcAlto);
		}
		
		public static function set CalcAlto(value:Number):void 
		{
			getInstance()._CalcAlto = value;
		}
		public static function get CalcAncho():Number 
		{
			return(getInstance()._CalcAncho);
		}
		
		public static function set CalcAncho(value:Number):void 
		{
			getInstance()._CalcAncho = value;
		}


		public static function get Idioma():String 
		{
			return(getInstance()._Idioma);
		}
		
		public static function set verbtn(value:Boolean):void 
		{
			getInstance()._verbtn = value;
		}
		

		public static function get verbtn():Boolean 
		{
			return(getInstance()._verbtn);
		}
		public static function set clickLan(value:Boolean):void 
		{
			getInstance()._clickLan = value;
		}
		

		public static function get clickLan():Boolean 
		{
			return(getInstance()._clickLan);
		}
		
		public static function set Idioma(value:String):void 
		{
			getInstance()._Idioma = value;
		}
		
		public static function get BtnInfoX():Number
        {
            return(getInstance()._BtnInfoX);
        }

        public static function set BtnInfoX(value:Number):void
        {
            getInstance()._BtnInfoX = value;
        }
		
		
		public static function get Sactualwidth():int
        {
            return(getInstance()._Sactualwidth);
        }

        public static function set Sactualwidth(value:int):void
        {
            getInstance()._Sactualwidth = value;
        }
		
		
		public static function get Sactualheigth():int
        {
            return(getInstance()._Sactualheigth);
        }

        public static function set Sactualheigth(value:int):void
        {
            getInstance()._Sactualheigth = value;
        }
		
		public static function get Swidth():int
        {
            return(getInstance()._Swidth);
        }

        public static function set Swidth(value:int):void
        {
            getInstance()._Swidth = value;
        }
		
		
		public static function get Sheigth():int
        {
            return(getInstance()._Sheigth);
        }

        public static function set Sheigth(value:int):void
        {
            getInstance()._Sheigth = value;
        }
		

		public static function get OS():String 
		{
			return(getInstance()._OS);
		}

		
		
		public static function set OS(value:String):void 
		{
			getInstance()._OS = value;
		}
		
		public static function get tipo_dispositivo():String 
		{
			return(getInstance()._tipo_dispositivo);
		}
		
		public static function set tipo_dispositivo(value:String):void 
		{
			getInstance()._tipo_dispositivo = value;
		}
		
		public static function get userName():String 
		{
			return(getInstance()._userName);
		}
		
		public static function set userName(value:String):void 
		{
			getInstance()._userName = value;
		}
		
		public static function get idCurso():String 
		{
			return(getInstance()._idCurso);
		}
		
		public static function set idCurso(value:String):void 
		{
			getInstance()._idCurso = value;
		}
		
		public static function get idInstitucion():String 
		{
			return(getInstance()._idInstitucion);
		}
		
		public static function set idInstitucion(value:String):void 
		{
			getInstance()._idInstitucion = value;
		}
		
		public static function get curso():String 
		{
			return(getInstance()._curso);
		}
		
		public static function set curso(value:String):void 
		{
			getInstance()._curso = value;
		}
		
		public static function get situacionSolicitada():uint 
		{
			return(getInstance()._situacionSolicitada);
		}
		
		public static function set situacionSolicitada(value:uint):void 
		{
			getInstance()._situacionSolicitada = value;
		}

        public static function get xmlPreguntas():XML
        {
            return(getInstance()._xmlPreguntas);
        }

        public static function set xmlPreguntas(value:XML):void
        {
            getInstance()._xmlPreguntas = value;
        }

        public static function get tituloReporte():String
        {
            return(getInstance()._tituloReporte);
        }

        public static function set tituloReporte(value:String):void
        {
            getInstance()._tituloReporte = value;
        }

        public static function get nombreCurso():String
        {
            return(getInstance()._nombreCurso);
        }

        public static function set nombreCurso(value:String):void
        {
            getInstance()._nombreCurso = value;
        }

        public static function get nombrePractica():String
        {
            return(getInstance()._nombrePractica);
        }

        public static function set nombrePractica(value:String):void
        {
            getInstance()._nombrePractica = value;
        }
		
		public static function get xmlDiagnostico():XML
        {
            return(getInstance()._xmlDiagnostico);
        }

        public static function set xmlDiagnostico(value:XML):void
        {
            getInstance()._xmlDiagnostico = value;
        }
		
		public static function get imageLogo():Bitmap
        {
            return(getInstance()._imageLogo);
        }

        public static function set imageLogo(value:Bitmap):void
        {
            getInstance()._imageLogo = value;
        }
    }
}

internal class SingletonEnforcer{}