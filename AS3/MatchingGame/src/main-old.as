﻿package {

	import flash.display.MovieClip;
	import flash.display.*;
	import flash.events.TouchEvent;
	import flash.events.*;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;

	//newgrounds store
	import com.newgrounds.*;
	import com.newgrounds.components.*;
	import flash.text.TextField;
	import owncode.cardClass;

	//API.connect(root, appid, encryptKey);
	//API.addEventListener(APIEvent.API_CONNECTED, onAPIConnected);
	//API.connect([root], "46114:02MQPmRq", "iuLpxNfI1vE4uMArYrP2M050q5PXRf5X");




	public class main extends MovieClip {

		//Space
		private static const space = 5;

		//constantes para la construcción cartas
		private static const columnss: uint = 6; //número de cartas horizontales
		private static const rowss: uint = 4; //número de cartas verticales
		private static const cardHorizontalSpace: Number = 85; //
		private static const cardVerticalSpace: Number = 110;
		private static const boardOffSetX: Number = 160;
		public static const boardOffSetY: Number = 70;

		//constantes para la construcción menu
		private static const m_columnss: uint = 4; //número de cartas horizontales
		private static const m_rowss: uint = 2; //número de cartas verticales
		private static const m_cardHorizontalSpace: Number = 160; //
		private static const m_cardVerticalSpace: Number = 160;
		private static const m_boardOffSetX: Number = 100;
		private static const m_boardOffSetY: Number = 80;

		//constantes para el score
		private static const pointForMatch: int = 100;
		private static const pointForMiss: int = -5;

		//Textfield Score
		private var gameScoreField: TextField = new TextField();
		var myScoreIcon: mc_StatsIcon = new mc_StatsIcon();
		//Score variable
		private var gameScore: int;

		//Game Timer
		private var gameStartTime: uint;
		private var gameTime: uint;
		//Game Timer Field
		var myTimerIcon: mc_StatsIcon = new mc_StatsIcon();

		//Timer flip
		private var flipBackTimer: Timer;

		//Newgrounds Objects
		//var NewgroundsObj:API = new API();
		var flashADs: FlashAd = new FlashAd();

		//objeto paraa splash
		var mySplash: Splash = new Splash();

		//objeto para el fondo
		var mybg: BgRef = new BgRef();

		//objetos para menu
		var menu: Menu = new Menu();
		var btn: Buton = new Buton();

		//arreglo de clips para los botones de los niveles
		var levelList: MovieClip = new MovieClip();
		var levelBtnClip: MovieClip = new MovieClip();

		//mazo de las cartas dado en un arreglo logico
		var cardList: Array = new Array();

		//mazo grafico en un clip
		var cardList_MC: MovieClip = new MovieClip();

		//variables for the two card states
		private var firstCard: cardClass;
		private var secondCard: cardClass;

		//check the cards left
		private var cardsLeft: uint;

		//MAIN FUNCTION
		public function main() {

			showSplash(); //showing splash

		}


		//--funcion para la carga del splash, ?how dinamicaly
		public function showSplash(): void {

			//tamaño y cordenadas del splash
			mySplash.x = 400;
			mySplash.y = 240;
			mySplash.width = 800;
			mySplash.height = 480;
			addChild(mySplash);
			trace(stage.width);

			//timer del splash
			var myTimer: Timer = new Timer(5000, 1); // 1 second
			myTimer.addEventListener(TimerEvent.TIMER, runOnce);
			myTimer.start()

		}

		//función inicial para la carga del fondo
		public function runOnce(event: TimerEvent): void {
			trace("runOnce() called @ " + getTimer() + " ms");
			removeChild(mySplash);
			loadBackground();
		}

		//-->función para la carga del background
		public function loadBackground(): void {
			//conexión al API
			API.connect(stage, "46114:02MQPmRq", "iuLpxNfI1vE4uMArYrP2M050q5PXRf5X");

			if (API.isNewgrounds) {
				//
				trace("conectado a newGrounds");
				API.unlockMedal("Begin_medal");
			}

			//coodenadas y add to stage(render)
			mybg.x = 0;
			mybg.y = 0;
			addChild(mybg);

			//Load Ads
			flashADs.width = 100;
			flashADs.height = 100;
			flashADs.x = stage.width - flashADs.width;
			flashADs.y = stage.height - (stage.height / 5);
			addChild(flashADs);

			//-

			//carga función de los levels
			levelsMenu();

		}

		public function levelsMenu(): void {

			//arreglo de niveles
			for (var x: uint = 0; x < m_columnss; x++) //horizontal
			{
				for (var y: uint = 0; y < m_rowss; y++) //vertical
				{

					//objeto boton nivel
					var thelvlBtn: lvlBtn = new lvlBtn();

					//detener clip si tiene animación
					thelvlBtn.stop();

					//--acomodación de botones
					//formula para hacer la cuadricula
					thelvlBtn.x = x * m_cardHorizontalSpace + m_boardOffSetX; //spread x
					//trace( theCard.x + "and" + theCard.y);
					thelvlBtn.y = y * m_cardVerticalSpace + m_boardOffSetY; //spread

					thelvlBtn.numberOfLevel = x + "" + y;

					//adding the click event
					thelvlBtn.addEventListener(MouseEvent.CLICK, touchLevel);

					//render each card packed in another clip
					levelList.addChild(thelvlBtn);


				}

			}
			//main List Clip
			addChild(levelList);

		}

		public function touchLevel(event: MouseEvent): void {


			var thislvl: lvlBtn = (event.currentTarget as lvlBtn); //What Card?
			trace(thislvl.numberOfLevel);

			//asignación a clip logico temporal
			levelBtnClip = thislvl;

			//remover botones de menu de level
			removeChild(levelList);
			startMenu();

		}

		public function startMenu(): void {

			menu.x = stage.width / 2;
			menu.y = stage.height / 2;
			addChild(menu);


			btn.x = stage.width / 2; //menu.width / 2;
			btn.y = stage.height / 2; //menu.height / 2;
			btn.gotoAndStop(1);
			addChild(btn);

			btn.b_label.text = "START";

			btn.addEventListener(MouseEvent.CLICK, startBtn);
			//btn-like Events
			btn.addEventListener(MouseEvent.ROLL_OVER, btn_RollOver);
			btn.addEventListener(MouseEvent.ROLL_OUT, btn_RollOut);
		}

		public function startBtn(event: MouseEvent) {

			removeChild(menu);
			removeChild(btn);

			spreadCards();

		}

		public function btn_RollOver(event: MouseEvent) {

			btn.gotoAndStop(2); //

		}

		public function btn_RollOut(event: MouseEvent) {

			btn.gotoAndStop(1); //

		}

		public function resetTouch(event: MouseEvent): void {
			//

			removeChild(cardList_MC);
			levelsMenu();

		}

		public function spreadCards(): void {

			//start stamp for the game time
			this.gameStartTime = getTimer();

			//TIMER --adding a timer to the stage
			// will be exute every 60 frmaes
			stage.addEventListener(Event.ENTER_FRAME, showTime);

			gameScore = 0;
			gameTime = 0;

			//botn de rest
			var resetBtn: lvlBtn = new lvlBtn();
			addChild(resetBtn);
			resetBtn.x = 780;
			resetBtn.y = 460;
			resetBtn.width = 20;
			resetBtn.height = 20;
			resetBtn.addEventListener(MouseEvent.CLICK, resetTouch);

			//--building the array of pairs - este array no es visual
			for (var i: uint = 0; i < (columnss * rowss / 2); i++) {
				cardList.push(i);
				cardList.push(i);
			}

			//cardLeft initialized
			this.cardsLeft = 0;



			//spread the cards
			for (var x: uint = 0; x < columnss; x++) //horizontal
			{
				for (var y: uint = 0; y < rowss; y++) //vertical
				{
					var theCard;
					//--LEVEL SELECTION
					switch (levelBtnClip.numberOfLevel) {
						case "00":
							trace("Level 1");
							var theCard00: cardClass = new cardClass();
							theCard = theCard00;
							break;
						case "10":
							trace("Level 2");
							var theCard01: CardDeck_01 = new CardDeck_01();
							theCard = theCard01;
							break;
						case "20":
							trace("Level 3");
							var theCard02: CardDeck_02 = new CardDeck_02();
							theCard = theCard02;
							break;
						case "30":
							trace("Level 4");
							var theCard02: CardDeck_02 = new CardDeck_02();
							theCard = theCard02;
							break;
						case "01":
							trace("Level 5");
							var theCard03: CardDeck_03 = new CardDeck_03();
							theCard = theCard03;
							break;
						case "11":
							trace("Level 6");
							var theCard04: CardDeck_04 = new CardDeck_04();
							theCard = theCard04;
							break;
						case "21":
							trace("Level 7");
							var theCard05: CardDeck_05 = new CardDeck_05();
							theCard = theCard05;
							break;
						case "31":
							trace("Level 8");
							var theCard05: CardDeck_05 = new CardDeck_05();
							theCard = theCard05;
							break;
					}


					//-1 creación de un objeto carta, el cual tiene frames
					// en caso de utilizar sprites, se hace un array de sprites u objetos


					//theCard es la instancia de la clase Card en la biblioteca
					theCard.stop(); //Card is a movieclip so we have to stop it;

					//formula para hacer la cuadricula
					theCard.x = x * cardHorizontalSpace + boardOffSetX; //spread x
					//trace( theCard.x + "and" + theCard.y);
					theCard.y = y * cardVerticalSpace + boardOffSetY; //spread y

					//render the cards in a random way
					// r es el tamaño del arreglo
					var r: uint = Math.floor(Math.random() * cardList.length); //randome face - number

					//se crea una propiedad logica, no visible y en tiempo de ejecución
					//que enlaza el arreglo de cardList al moviclip theCard base.
					theCard.cardFace = cardList[r]; // assign a face to the card

					//se eliminan los elementos del array cada que se utilizan
					cardList.splice(r, 1); //remove the face after used;

					//JUST TO TEST
					//theCard.gotoAndStop(theCard.cardFace+2); 
					// +2 cause we avoid frame 0 that doesnt exist in flash videoclips
					// and the frame with the card back

					//render each card
					//cardList_MC.addChild(theCard);


					//adding the click event
					theCard.addEventListener(MouseEvent.CLICK, touchCard);


					addChild(theCard);
					//addChild(cardList_MC);

					cardsLeft++;
				}

			}

			addChild(cardList_MC);

			//Timer
			myTimerIcon.x = stage.width - myScoreIcon.width - space;
			myTimerIcon.y = stage.x + space;
			myTimerIcon.statsTextField.text = String(gameScore);
			addChild(myTimerIcon);

			//Score
			myScoreIcon.x = stage.width - myScoreIcon.width - space;
			myScoreIcon.y = (stage.x + space) + this.myTimerIcon.height + 5;
			myScoreIcon.statsTextField.text = String(gameScore);
			addChild(myScoreIcon);

		}

		public function touchCard(event: MouseEvent) {
			//trace(event.target);
			var thisCard: cardClass = (event.currentTarget as cardClass); //What Card?
			//cardList_MC.addChild(thisCard);
			trace(thisCard.cardFace);

			if (firstCard == null) //first guess is not clicked yet
			{

				//passing all data from clicked card to first Card
				firstCard = thisCard;
				
				thisCard.startFlip(thisCard.cardFace+2);
				
				//show card face
				//firstCard.gotoAndStop(thisCard.cardFace + 2);
				//firstCard.startFlip(thisCard.cardFace + 2); ///now flip ---------

			} else if (firstCard == thisCard) //clicked again in the same first card
			{

				//just send to frame 0
				//firstCard.gotoAndStop(1); //turn Back Over de card
				firstCard.startFlip(1); //-now it flips -------------

				firstCard = null; // come back to null

			} else if (secondCard == null) //after, we gonna check the secondcard
			{
				//assign thisCard- ClickCard  to secondCard
				secondCard = thisCard;
				//secondCard.gotoAndStop(thisCard.cardFace + 2); //show card face
				thisCard.startFlip(thisCard.cardFace + 2); ///now flip ---------

				//compare two cards
				if (firstCard.cardFace == secondCard.cardFace) { // -- MATCH
					//remove match cards from the scene
					//trace(firstCard);
					//trace(secondCard);
					removeChild(firstCard);
					removeChild(secondCard);

					//reset selection
					firstCard = null;
					secondCard = null;



					gameScore += pointForMatch;
					myScoreIcon.statsTextField.text = String(gameScore);

					//cards left counter
					cardsLeft -= 2;

					if (cardsLeft == 0) {
						//
						trace("GAME OVER");
						//close game - show victory and start over
						addChild(menu);

						addChild(btn);

						btn.b_label.text = "PLAY AGAIN";
						btn.width = btn.width * 2;
						btn.height = btn.height * 2;

					}

				} else {

					//
					gameScore += pointForMiss;

					//-
					flipBackTimer = new Timer(2000, 1);
					flipBackTimer.addEventListener(TimerEvent.TIMER_COMPLETE, returnCards);
					flipBackTimer.start();

					myScoreIcon.statsTextField.text = String(gameScore);


					//---NOW THE FUNCTION OF THE TIMER MAKE THE MOVEMENT
					// reset previous pair - resetear el par anterior
					//firstCard.gotoAndStop(1);
					//secondCard.gotoAndStop(1);

					//secondCard = null; //dar al segundo click el null

					//firstCard = thisCard; //asigna el segundo pero fallido intento al primer intento
					//firstCard.gotoAndStop(thisCard.cardFace + 2); //dar vuelta a la carta
				}
			} else {
				trace("si la segunda no esta vacia");

				returnCards(null);

				// select first card in next pair
				firstCard = thisCard;
				firstCard.startFlip(thisCard.cardFace + 2);

			}

		}

		//return back fuction - triggered by timer
		public function returnCards(event: TimerEvent) {

			if (firstCard != null) firstCard.startFlip(1);
			if (secondCard != null) secondCard.startFlip(1);



			firstCard = null;
			secondCard = null;
			flipBackTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, returnCards);

		}

		//timer function - Show Time
		public function showTime(event: Event) {

			//
			gameTime = getTimer() - gameStartTime;
			myTimerIcon.statsTextField.text = clockTime(gameTime);
			//trace(gameTime + " - - " + getTimer() +" - "+ gameStartTime);

		}

		public function clockTime(ms: int) {
			//
			var seconds: int = Math.floor(ms / 1000);
			var minutes: int = Math.floor(seconds / 60);
			seconds -= minutes * 60;

			var timeString: String = minutes + ":" + String(seconds + 100).substr(1, 2);

			return timeString;
		}


	}

}