﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour
{
    float mov_velocity = 0.1f;

    //velocity
    [SerializeField, Range(0f, 100f)]
    float maxSpeed = 10f;
    //accel
    [SerializeField, Range(0f, 100f)]
    float maxAccel = 10f;

    Vector3 velocity;

    //limites del cuadro
    [SerializeField]
    Rect areaPermitida = new Rect(-5f, -5f, 10f, 10f);

    //evitar la saturación
    Vector3 newPosition = new Vector3();

    //posicion 2D topview de la sphera
    Vector2 topPosition = new Vector2();

    //setting bounciness
    [SerializeField, Range(0f, 1f)]
    float bounciness = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //--Tomar la posición de player
        Vector2 playerInput;//Vector 2D
        playerInput.x = Input.GetAxis("Horizontal");
        playerInput.y = Input.GetAxis("Vertical");
        //Debug.Log("playerInput: " + playerInput);

        //--Se normaliza el vertor de input para que en los extremos de un valor de 1
        //--se utiliza normalize() pero en terminos matemáticos es pitagoras RaizCuadrada(x^2 + y^2)
        //playerInput.Normalize();

        //
        playerInput = Vector2.ClampMagnitude(playerInput, 1f);

        //--Transofrmación del this object
        //transform.localPosition = new Vector3(playerInput.x, 0f, playerInput.y);

        //--Nuevo componente distancia que es relativo al movimiento para no generar un movimiento de
        //--teletrasportación cuando el movimiento del input se mueve inmediatamente con el objeto
        //>> given i = input // p = position // d = desplazamiento
        //el posicionamiento restringida de i = p genera un movimiento sobre el mismo espacip
        //el dezplazamiento va sumando cada posición en cada uno de los usos del input d = i + p
        //_oldCode // Vector3 desplazamiento = new Vector3(playerInput.x, 0f, playerInput.y);
        //_oldCode // transform.localPosition += desplazamiento * mov_velocity; //va sumando los desplazamientos


        //d = i+p puede ser reemplazado por  d = vt donde v = velocidad y t = tiempo
        //_oldCode // Vector3 velocity = new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        //_oldCode // Vector3 desplazamiento = velocity * Time.deltaTime;
        //_oldCode // transform.localPosition += desplazamiento;

        //d = i+p puede ser reemplazado por  d = vt donde v = velocidad y t = tiempo
        //_oldCode // Vector3 accel = new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        //_oldCode // velocity += accel * Time.deltaTime;
        //_oldCode // Vector3 desplazamiento = velocity * Time.deltaTime;
        //_oldCode // transform.localPosition += desplazamiento;

        //d = i+p puede ser reemplazado por  d = vt donde v = velocidad y t = tiempo
        Vector3 desiredVelocity = new Vector3(playerInput.x, 0f, playerInput.y) * maxSpeed;
        float maxSpeedChange = maxAccel * Time.deltaTime;
        velocity.x = Mathf.MoveTowards(velocity.x, desiredVelocity.x, maxSpeedChange);
        velocity.z = Mathf.MoveTowards(velocity.z, desiredVelocity.z, maxSpeedChange);
        Vector3 desplazamiento = velocity * Time.deltaTime;
        //transform.localPosition += desplazamiento;


        //Encerrando la sphera en una región
        /* oldCODE newPosition = transform.localPosition + desplazamiento;
        transform.localPosition = newPosition;*/

        //Encerrando la sphera en una región - restricción usanto contains
        //asi se validad si el valor esta dentro del area del rect areaPermitida
        /* oldCODE newPosition = transform.localPosition + desplazamiento;
        if(!areaPermitida.Contains(newPosition))
        {
            newPosition = transform.localPosition;
        }
        transform.localPosition = newPosition;*/



        //Encerrando la sphera en una región - restricción con el uso de un vector2
        /*newPosition = transform.localPosition + desplazamiento;
        topPosition.x = newPosition.x;
        topPosition.y = newPosition.z;
        if (!areaPermitida.Contains(topPosition))
        {
            newPosition = transform.localPosition;//al ser exacto se genera un bloqueo atasco
        }
        transform.localPosition = newPosition;*/



        //Encerrando la sphera en una región - restricción con el uso de un vector2
        /*newPosition = transform.localPosition + desplazamiento;
        topPosition.x = newPosition.x;
        topPosition.y = newPosition.z;
        if (!areaPermitida.Contains(topPosition))
        {
            //se utiliza clamp para suavizar el movimiento en los limites
            //Clamp redondea a alguno de los extremos
            newPosition.x = Mathf.Clamp(newPosition.x, areaPermitida.xMin, areaPermitida.xMax);
            newPosition.z = Mathf.Clamp(newPosition.z, areaPermitida.yMin, areaPermitida.yMax);
            Debug.Log(areaPermitida.xMin + " - " + areaPermitida.xMax + "\n");
            Debug.Log(areaPermitida.yMin + " - " + areaPermitida.yMax + "\n");
        }
        transform.localPosition = newPosition;*/


        //Encerrando la sphera en una región - usando nuestro propio Clamp
        newPosition = transform.localPosition + desplazamiento;
        topPosition.x = newPosition.x;
        topPosition.y = newPosition.z;
        //Manualente, sin utilizar ni contain ni clamp se puede resetear la velocidad para que
        //no pierda dinamismo cuando la esphera choca el limite
        if(newPosition.x < areaPermitida.xMin)
        {
            newPosition.x = areaPermitida.xMin;
            //velocity.x = 0f;
            //velocity.x = -velocity.x;//Bouncing
            velocity.x = -velocity.x * bounciness; //utilizando un factor de rebote
        }
        else if(newPosition.x > areaPermitida.xMax)
        {
            newPosition.x = areaPermitida.xMax;
            //velocity.x = 0f;
            //velocity.x = -velocity.x;//Bouncing
            velocity.x = -velocity.x * bounciness; //utilizando un factor de rebote
        }
        if (newPosition.z < areaPermitida.yMin)
        {
            newPosition.z = areaPermitida.yMin;
            //velocity.z = 0f;
            //velocity.z = -velocity.z;//Bouncing
            velocity.z = -velocity.z * bounciness; //utilizando un factor de rebote
        }
        else if (newPosition.z > areaPermitida.yMax)
        {
            newPosition.z = areaPermitida.yMax;
            //velocity.z = 0f;
            //velocity.z = -velocity.z;//Bouncing
            velocity.z = -velocity.z * bounciness; //utilizando un factor de rebote
        }

        transform.localPosition = newPosition;


        //Debug.Log("playerInput After: " + playerInput);
    }
}
