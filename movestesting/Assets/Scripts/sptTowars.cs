﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sptTowars : MonoBehaviour
{
    //Ajustar la velocidad
    public float speed = 1.0f;

    //el target
    private Transform target;

    private void Awake()
    {
        //posiciñon del cubo en el origen
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        //crear y posicionar el cilindro target
        GameObject cylindro = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        Camera.main.transform.position = new Vector3(0.85f, 1.0f, -3.0f);

        //Traer os datos del cilindro y ponerlos en el target
        target = cylindro.transform;
        target.transform.localScale = new Vector3(0.15f, 1.0f, 0.15f);
        target.transform.position = new Vector3(0.8f, 0.0f, 0.8f);

        //Position the camera
        Camera.main.transform.position = new Vector3(0.85f, 1.0f, -3.0f);
        Camera.main.transform.localEulerAngles = new Vector3(15.0f, -20.0f, -0.5f);

        //creación y posicionaiento del suelo
        GameObject suelo = GameObject.CreatePrimitive(PrimitiveType.Plane);
        suelo.transform.position = new Vector3(0.0f, -1.0f, 0.0f);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //move la posiciñon del cubo un paso adelante hacia el target
        float paso = speed * Time.deltaTime; // calculo de la distancia
        //moverse paso a paso hacia al target, no de manera inmediata
        transform.position = Vector3.MoveTowards(transform.position, target.position, paso);

        //verificar si la pocisión del cubo es casi la misma a la del target
        if (Vector3.Distance(transform.position, target.position) < 0.001f)
        {
            //cambiar la posicion del cilindro
            target.position *= -1.0f;
        }
    }
}
