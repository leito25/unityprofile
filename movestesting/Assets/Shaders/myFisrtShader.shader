﻿Shader "Custom/myFisrtShader"
{
	Properties
	{
		//like global properties
		//Color, textures, maps, etc
		_GlobalTinta ("Tinta", Color) = (1,1,1,1)//Variable tinta queda expuesta en el editor
	}


	//Esto es de unity - shaderLab
	SubShader
	{
		/*passes*/
		Pass
		{
			/*opengl program*/
			CGPROGRAM

			//programa para los vertices
			#pragma vertex MyVertexProgram
			//programa para los fragmentos / pixeles
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"
			//this library is compound by
			//UnityShaderVariables: hlsl support
			//UintyInstancing
			//This library Manage the camera,
			//the transformations and all this kind of things

			//Las propiedades globales, toca traerlas de nuevo al programa
			//del shader
			float4 _GlobalTinta;//Propiedad gloaba de tinta

			float4 MyVertexProgram(
				float4 pos:POSITION,
				out float3 localPosition : TEXCOORD0/*manda la local position como output*/
			) : SV_POSITION //metodo posición vertex
			{
				//tira todos los vertices a 0
				//return pos;//posición del vertice(vertices en paralelo) sin proyectar a la camara
				//return UNITY_MATRIX_MVP * pos;//posición del vertice por la matrix de proyection
				//return mul(UNITY_MATRIX_MVP, pos);//old fashion matrix multiplication

				//pasar la local position a la variable localPosition
				localPosition = pos.xyz;

				//return mul(UNITY_MATRIX_MVP, pos);//old fashion matrix multiplication
				return UnityObjectToClipPos(pos);//unity built in function
			}

			float4 MyFragmentProgram(
				float4 pos:SV_POSITION/*pos del vertex*/,
				float3 localPosition:TEXCOORD0/*colorear a partir color/position */
				/*pasando la posición como color se crea un efecto 3D con toda la color scale*/
			):SV_TARGET //pass imagen pasa el color
			{
				//return 0;//retorna el color a 0
				//coloring
				//return _GlobalTinta;//returning a rbga color - en este caso amarillo

				return float4(localPosition, 1);
			}


			ENDCG

		}
	}
}
