﻿Shader "Custom/myFisrtShader_one"
{
	Properties
	{
		//like global properties
		//Color, textures, maps, etc
		_GlobalTinta ("Tinta", Color) = (1,1,1,1)//Variable tinta queda expuesta en el editor
		_GlobalTextura ("Texura", 2D) = "white" {}
	}


	//Esto es de unity - shaderLab
	SubShader
	{
		/*passes*/
		Pass
		{
			/*opengl program*/
			CGPROGRAM

			//programa para los vertices
			#pragma vertex MyVertexProgram
			//programa para los fragmentos / pixeles
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"

			//GLOBALES
			float4 _GlobalTinta;
			sampler2D _GlobalTextura;//propiedad de textura
			float4 _GlobalTextura_ST;//esta propiedad tiene las coordenadas del mapeado

			//estructura para organizar los datos
			struct Interpoladores
			{
				//data
				float4 position : SV_POSITION;
				//float3 localPosition : TEXCOORD0;
				float2 uv : TEXCOORD0; //la uv es un mapeo 2d sobre una superficie 3d
			};

			//se crea una struc para pasarle datos al vertex program
			struct VertexData
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};

			Interpoladores MyVertexProgram(VertexData v)
			{
				//se crea un instancia de la estructura
				//Interpoladores ipl;

				//se pasa el local position del objeto a la estructura
				//ipl.localPosition = position.xyz;

				//se pasan los datos de la proyección de los vertices a la pos del obj
				//ipl.position = UnityObjectToClipPos(position);
				//return ipl;//retornar los datos para dibujar

				//utilizando la nueva structura que pasa los datos de verticd
				Interpoladores itp;
				itp.position = UnityObjectToClipPos(v.position);
				//itp.uv = v.uv * _GlobalTextura_ST.xy;//en xy va el scalamiento
				//el offset es la suma del tiling con el desfase
				//itp.uv = v.uv * _GlobalTextura_ST.xy + _GlobalTextura_ST.zw;

				//con la ayuda de la libreria de unity
				itp.uv = TRANSFORM_TEX(v.uv, _GlobalTextura);
				return itp;
			}

			float4 MyFragmentProgram(Interpoladores i) : SV_TARGET //as argument struct data ipl
			{
				//retorno de el color como una conversión de la posición
				//return float4(i.localPosition, 1);

				//se retorna la posición como color y se mult por 0.5 para dar más efecto 3D
				//return float4(i.localPosition + 0.5, 1);

				//se retorna la posición como color y se mult por 0.5 para dar más efecto 3D
				//para fusionar the basecolor con la tinta se multiplica el resultado by globaltint
				//return float4(i.localPosition + 0.8, 1) * _GlobalTinta;

				//Con los uv habilitados lo que se hace es pasar uv como xy
				//y los valores restantes quedan en 1
				//return float4(i.uv, 1, 1);

				//se aplica el mapeo con la función tex2D arg - la textura, los uv
				//return tex2D(_GlobalTextura, i.uv);

				//la textura se puede multiplicar con la tinta
				return tex2D(_GlobalTextura, i.uv) * _GlobalTinta;
			}

			ENDCG

		}
	}
}
