//Maya ASCII 2016 scene
//Name: ams_kk_mainCharKnight-animations_states.ma
//Last modified: Sun, Aug 13, 2017 11:59:17 PM
//Codeset: 1252
requires maya "2016";
requires -nodeType "gameFbxExporter" "gameFbxExporter" "1.0";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201603180400-990260";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "D6348E9D-4ECB-6351-8697-3EA873C5E830";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -452.03435067426108 151.47242231062461 588.61928838970357 ;
	setAttr ".r" -type "double3" -0.33835270309105675 2479.7999999992489 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5B622D3D-4BC7-02A8-9018-F492F0216A23";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 10;
	setAttr ".fcp" 1000000;
	setAttr ".coi" 713.68650058462617;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -82.337253269689441 136.54491433196418 14.362535020765231 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "61285E53-4A6B-74E6-80B5-64BD47723263";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -167.02161003765923 -171.08026806476607 189.35295374825122 ;
	setAttr ".r" -type "double3" -89.999999999999986 -3.1805546814635176e-015 -224.31224041788798 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "7ADAE271-4640-1891-4CE6-2F80B5B51C34";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100000;
	setAttr ".coi" 100.1;
	setAttr ".ow" 653.32882961379676;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "4C3B03C8-41AE-4E06-4E73-0C9DDC7C18F3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 52.986433481997182 143.97913699548664 326.0973644609561 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "E01593A7-4B02-7DF5-26E7-38936FE873DA";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 253.83592001982001;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "DFE4DAAF-4499-E9E5-9B05-5EBC66631F72";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.09999999999998 171.88103723164605 33.465639137831104 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "C519B6D5-4905-5A9E-9B44-E0846BD48AB3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 662.69722038846487;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "ground";
	rename -uid "E9A24292-4312-047D-5907-D898A22A5225";
	setAttr ".t" -type "double3" 0 -0.5 0 ;
	setAttr ".s" -type "double3" 10000 1 10000 ;
createNode mesh -n "groundShape" -p "ground";
	rename -uid "804E4363-4B38-F6FB-2792-80BFBB7ADE30";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "left";
	rename -uid "D81E48D2-434C-40CC-A4A9-C198EAFFF6C5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -5011.7964843866803 49.300206168872876 7.6179918711892984 ;
	setAttr ".r" -type "double3" 0 -89.999999999999986 0 ;
createNode camera -n "leftShape" -p "left";
	rename -uid "44122792-4952-0F42-5842-559700067173";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 5000.1000000000004;
	setAttr ".ow" 376.23887324876591;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
createNode ikHandle -n "ik_R_wrist";
	rename -uid "DC0288DD-49A4-9CDF-CEB5-E29905681AA3";
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "ik_R_wrist_poleVectorConstraint1" -p "ik_R_wrist";
	rename -uid "7D000606-4938-0AE6-AA56-E2BD6DFF8C04";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "locator2W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -46.966297048384746 0.51055445633909358 -1.7114400000000014 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ik_R_wrist_parentConstraint1" -p "ik_R_wrist";
	rename -uid "48C50A07-4401-55B2-498C-18989A3626CF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ct_R_HandW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.019370676355274341 -0.0065293448389009967 
		-2.7755575615628914e-017 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".lr" -type "double3" -92.838282372393337 34.585433224920848 -91.612000551212901 ;
	setAttr ".rst" -type "double3" -151.10876637902746 196.82426690888192 -3.8022200000000024 ;
	setAttr -k on ".w0";
createNode ikHandle -n "ik_L_wrist";
	rename -uid "8BC7B4BB-45B8-F41D-EA4C-558B8500741A";
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "ikHandle1_poleVectorConstraint1" -p "ik_L_wrist";
	rename -uid "13FD6B95-498E-7EBC-91E4-56A00DE8E159";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "locator1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 46.968372756629307 0.2072063916142497 -1.711439775626447 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ikHandle1_parentConstraint1" -p "ik_L_wrist";
	rename -uid "5FACEB82-4825-A79F-5A70-70BA9CCACFBB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ct_L_HandW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -0.019316756529191181 -0.0064669839433584286 
		0 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".lr" -type "double3" 61.779083735270198 19.642661376416356 -94.008566550247437 ;
	setAttr ".rst" -type "double3" 148.6230794448345 195.85753015640105 -3.8022185033605176 ;
	setAttr ".rsrr" -type "double3" 0 0 1.2722218725854067e-014 ;
	setAttr -k on ".w0";
createNode transform -n "loc_L_ElbowPoleV";
	rename -uid "9A25477A-4C0F-6C59-83C3-8ABACAA1E5FD";
createNode locator -n "loc_L_ElbowPoleVShape" -p "loc_L_ElbowPoleV";
	rename -uid "A0014398-40DC-4D62-165A-7EB07EC3A0E6";
	setAttr -k off ".v";
createNode parentConstraint -n "loc_L_ElbowPoleV_parentConstraint1" -p "loc_L_ElbowPoleV";
	rename -uid "3FDFA493-4E49-B4BA-07E3-FB893E09783A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_ElbowPoleVW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -7.1054273576010019e-015 0 0.038242793266426989 ;
	setAttr ".rst" -type "double3" 101.63557788552905 195.65023937586545 -37.014019351649154 ;
	setAttr -k on ".w0";
createNode transform -n "loc_R_ElbowPoleV";
	rename -uid "241EAF3E-43C4-EB59-D73F-339A0E837FAE";
createNode locator -n "loc_R_ElbowPoleVShape" -p "loc_R_ElbowPoleV";
	rename -uid "00B14214-44C9-E7F8-1A4B-788C09CA2C60";
	setAttr -k off ".v";
createNode parentConstraint -n "loc_R_ElbowPoleV_parentConstraint1" -p "loc_R_ElbowPoleV";
	rename -uid "BC8592DF-4F77-C01D-355C-2DB6850011A1";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_ElbowPoleVW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -2.2204460492503131e-016 0.004279687447221292 
		0.38231874528507531 ;
	setAttr ".rst" -type "double3" -104.12416619909597 196.31465339652601 -4.8660500000000013 ;
	setAttr -k on ".w0";
createNode transform -n "L_ikGroup";
	rename -uid "2579A976-4197-4CB0-7E92-D09C93794605";
createNode transform -n "L_HeelLift" -p "L_ikGroup";
	rename -uid "021B204B-4891-D637-A77C-A1A9662DAB4D";
createNode ikHandle -n "ik_L_knee" -p "L_HeelLift";
	rename -uid "BF6C7830-45E7-71A0-FF7B-94A1720E6530";
	setAttr ".t" -type "double3" 22.205612822496317 16.116948273190772 -2.3084591372661856 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "ik_L_knee_poleVectorConstraint1" -p "ik_L_knee";
	rename -uid "5D93B5AC-4264-BC2F-9815-05B1E411BB1E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "L_KneePoleVW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -1.7763568394002505e-014 -39.352376827025125 36.255435513192282 ;
	setAttr -k on ".w0";
createNode transform -n "L_ToelLift" -p "L_ikGroup";
	rename -uid "4D5F1D76-4ED3-545F-1EC6-37BF05E39994";
createNode ikHandle -n "ik_L_Toe" -p "L_ToelLift";
	rename -uid "0EB27DFC-4428-B38A-D83F-2FAAB60E2190";
	setAttr ".t" -type "double3" 22.040967732361441 -0.58084723571701602 20.302455256305763 ;
	setAttr ".pv" -type "double3" -1.9993022951585098 0.052560684166653902 -0.005263749029804283 ;
	setAttr ".roc" yes;
createNode ikHandle -n "ik_L_Ball" -p "L_ToelLift";
	rename -uid "1DF12F3C-4696-6A67-B776-7D91BC3FF797";
	setAttr ".t" -type "double3" 22.088412822496505 -0.40094951734544182 4.0779867339946527 ;
	setAttr ".pv" -type "double3" 0.072458508194032115 0.72032298687355267 1.8643724303829048 ;
	setAttr ".roc" yes;
createNode parentConstraint -n "L_ikGroup_parentConstraint1" -p "L_ikGroup";
	rename -uid "AEFD76F6-445B-C5D6-383A-6BAD8ABB7885";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ct_L_FootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -0.23655353054188791 -0.17270839986442557 0.024560997684383093 ;
	setAttr ".lr" -type "double3" 70.06208581094603 0 0 ;
	setAttr ".rst" -type "double3" 0 1.8434019335561338 8.8817841970012523e-016 ;
	setAttr -k on ".w0";
createNode transform -n "R_ikGroup";
	rename -uid "1C6F4B67-4707-7BD9-390B-E382E0E59616";
createNode transform -n "R_HeelLift" -p "R_ikGroup";
	rename -uid "CC72ECE1-4D3D-1699-E71A-398AC74A1D1C";
createNode ikHandle -n "ik_R_knee" -p "R_HeelLift";
	rename -uid "A8BE8C26-4820-5D72-C22B-5493723F5571";
	setAttr ".t" -type "double3" -23.79440000000001 16.116900000000005 -2.308460000000002 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "ik_R_knee_poleVectorConstraint1" -p "ik_R_knee";
	rename -uid "7A4BF07F-40D3-2D68-F842-0DAD4AF86A3D";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "R_KneePoleVW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -3.5527136788005009e-015 -39.352416696389625 36.255435309450448 ;
	setAttr -k on ".w0";
createNode transform -n "R_ToelLift" -p "R_ikGroup";
	rename -uid "B97359AB-4B0D-57F1-C5A2-D1A1008F8F41";
createNode ikHandle -n "ik_R_Ball" -p "R_ToelLift";
	rename -uid "B612D6F1-4B21-7A93-DFFA-E8BA83E0A407";
	setAttr ".t" -type "double3" -23.67720000000002 -0.40094999999998393 4.07799 ;
	setAttr ".pv" -type "double3" -0.072458417053980248 0.72032520729410177 1.8643715760370141 ;
	setAttr ".roc" yes;
createNode ikHandle -n "ik_R_Toe" -p "R_ToelLift";
	rename -uid "C50A87B0-4134-EC3D-EC20-8EA718017F70";
	setAttr ".t" -type "double3" -23.629700000000035 -0.58084699999998601 20.302500000000002 ;
	setAttr ".pv" -type "double3" 1.9993022768242417 0.052560704645688565 -0.0052705040130933542 ;
	setAttr ".roc" yes;
createNode parentConstraint -n "R_ikGroup_parentConstraint1" -p "R_ikGroup";
	rename -uid "AD97D58C-476B-F6A0-A71C-A6832EEF0964";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ct_R_FootW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0.25347867550962455 -0.17419971449837238 0.024515979330443211 ;
	setAttr ".lr" -type "double3" 8.139805482998927 0 0 ;
	setAttr ".rst" -type "double3" 7.1054273576010019e-015 1.7913114041949676 4.4408920985006262e-016 ;
	setAttr -k on ".w0";
createNode joint -n "reference";
	rename -uid "2BC64C47-4241-B9BE-90E2-7E8F48B87149";
	setAttr ".ra" -type "double3" 0 0 -0.95724792866108643 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 1.3948967283889122 ;
	setAttr ".bps" -type "matrix" 0.99997082746890742 0.0076383382452317643 0 0 -0.0076383382452317643 0.99997082746890742 0 0
		 0 0 1 0 0 0 0 1;
createNode joint -n "Hips" -p "reference";
	rename -uid "F30F8843-47C9-56C0-8C1E-BE8630723A1B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".ra" -type "double3" 0 0 89.376324524770084 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 0.62367547523048639 ;
	setAttr ".bps" -type "matrix" -0.0076383382452417563 0.99997082746890731 0 0 -0.99997082746890731 -0.0076383382452417563 0 0
		 0 0 1 0 -0.79438717750410448 107.95687114580211 0 1;
	setAttr ".radi" 5;
createNode joint -n "Spine01" -p "|reference|Hips";
	rename -uid "BFD8BCAF-4783-CF86-7386-5AAED87A62D1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 42.000000000000014 9.1038288019264856e-015 0 ;
	setAttr ".ra" -type "double3" 0 0 90.252765297421092 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -90.252765297421192 ;
	setAttr ".bps" -type "matrix" -0.0044115723650070124 0.9999902689672876 0 0 -0.9999902689672876 -0.0044115723650070124 0 0
		 0 0 1 0 -1.1151973838042675 149.95564589949623 0 1;
	setAttr ".radi" 5;
createNode joint -n "Spine02" -p "Spine01";
	rename -uid "358009A5-4447-F23B-247E-189D03925AB5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 37.999999999999972 8.4376949871513917e-015 0 ;
	setAttr ".ra" -type "double3" 0 0 90.94894769646578 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -90.948947696466078 ;
	setAttr ".bps" -type "matrix" -0.016561504548029002 0.99986284887833754 0 0 -0.99986284887833754 -0.016561504548029002 0 0
		 0 0 1 0 -1.2828371336745423 187.95527612025313 0 1;
	setAttr ".radi" 5;
createNode joint -n "HeadBase" -p "Spine02";
	rename -uid "3410E591-4C8A-B5C5-A515-F0A8E698B9D3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 19.999999999999829 8.4376949871511897e-015 1.1368683772161603e-013 ;
	setAttr ".ra" -type "double3" 0 0 90.467983310073876 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -90.467983310074089 ;
	setAttr ".bps" -type "matrix" -0.0081677587874703361 0.99996664330186003 0 0 -0.99996664330186003 -0.0081677587874703361 0 0
		 0 0 1 0 -1.614067224635128 207.9525330978197 1.1368683772161603e-013 1;
	setAttr ".radi" 5;
createNode joint -n "HeadEnd" -p "HeadBase";
	rename -uid "DC994ECB-4405-1FB1-57D0-B7B01BF83F88";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 91.526490216998496 -2.191830121406098 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".radi" 5;
createNode joint -n "Plume01" -p "HeadBase";
	rename -uid "53E19813-4AAB-89DA-F111-71BC151B9012";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 91.77554860809866 -2.2592995944245637 -7.581488216923705 ;
	setAttr ".ra" -type "double3" 89.970870036799113 0.99561988153804248 88.324027025801186 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -9.9332075865146372 0.31779844601803869 -88.792331502726199 ;
	setAttr ".radi" 3;
createNode joint -n "Plume02" -p "Plume01";
	rename -uid "EB54C37F-4677-9506-D84E-1BBDB8AF8AE5";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 13.581292107936918 -7.1054273576010019e-015 -3.0156526404776801e-015 ;
	setAttr ".ra" -type "double3" -90 57.002800464538296 -90 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -147.00280046455376 -88.994954096434597 123.02098838901527 ;
	setAttr ".radi" 3;
createNode joint -n "Plume03" -p "Plume02";
	rename -uid "D5A15786-4E80-19CF-FD90-CCA333E6C450";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 11.283737696648402 1.1368683772161603e-013 -1.7763568394002505e-015 ;
	setAttr ".ra" -type "double3" -90.000000000000114 70.785526622822928 -90.000000000000242 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 19.214473377171096 -89.471367619315785 -27.831329927014085 ;
	setAttr ".radi" 3;
createNode joint -n "PlumeEnd" -p "Plume03";
	rename -uid "EE12C556-4204-D239-F189-2A8CF5E1DC80";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 25.738018898748201 2.2737367544323206e-013 -4.4408920985006262e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 268.24758233617877 5.7441583237622513 ;
	setAttr ".radi" 3;
createNode joint -n "HelmetBase" -p "HeadBase";
	rename -uid "7912512B-4167-8DDB-475E-CF8515112990";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 34.821552167563993 -1.8242530137842869 -6.7990287737498951 ;
	setAttr ".ra" -type "double3" -81.102634985140824 -84.83256737381042 81.758245790552124 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -41.345595162264473 -0.93963621355028981 -89.500209399267945 ;
	setAttr ".radi" 5;
createNode joint -n "HelmetEnd" -p "HelmetBase";
	rename -uid "69BAD76D-4409-5849-7D22-C0B36F60D8CA";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 59.967041663604277 -0.0063355210021711628 -0.55555381699809336 ;
	setAttr ".ra" -type "double3" 50.647820177208345 -0.58677188240573896 0.48114053968225295 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 39.349716082095554 90.758807859533974 43.692104398210333 ;
	setAttr ".radi" 5;
createNode orientConstraint -n "HeadBase_orientConstraint1" -p "HeadBase";
	rename -uid "0CAF665C-4BA0-F46E-D7C3-51AEAA1867AF";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "HeadCtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -2.1029205585461632 -2.3605679276487027e-016 -0.48096438639170974 ;
	setAttr ".rsrr" -type "double3" 0 0 -0.48096438639170108 ;
	setAttr -k on ".w0";
createNode joint -n "L_Clavicle" -p "Spine02";
	rename -uid "8CDBB8CD-40D8-1992-10AE-57B210F43276";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 11.200722895197259 -30.999999999999989 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 0.99986284887833776 0.016561504548029227 0 0 -0.016561504548029227 0.99986284887833776 0 0
		 0 0 1 0 29.527410358383889 199.66786946473079 0 1;
	setAttr ".radi" 5;
createNode joint -n "L_Humerus" -p "L_Clavicle";
	rename -uid "F7192F68-41BD-9809-8B69-F1B3FC1FD822";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 25.000000000000011 5.6843418860808015e-014 -3.1546056623016021 ;
	setAttr ".r" -type "double3" 10.547356360718242 44.73819531605848 -61.351360967891537 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 2.0868076394733173 0 ;
	setAttr ".bps" -type "matrix" 0.998434158179901 -0.052218358912657754 0.020061773901156668 0
		 0.053275246310150362 0.99698791205170501 -0.056363564058483126 0 -0.017058133256165127 0.057344103578753192 0.99820873261786491 0
		 54.523981580342344 200.08190707843158 -3.1546056623016021 1;
	setAttr ".radi" 5;
createNode joint -n "L_ForeArm" -p "L_Humerus";
	rename -uid "FCD83294-4396-DBF9-B96D-418E242B0592";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 46.999999999999993 -1.7053025658242404e-013 1.9984014443252818e-015 ;
	setAttr ".r" -type "double3" 6.169011104688324e-014 -51.00740379996018 4.1788655659238793e-012 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -3.3837863479491319 0 ;
	setAttr ".bps" -type "matrix" 0.99843375824155245 -0.052217015352050554 0.020085161472409171 0
		 0.053275246310150362 0.99698791205170501 -0.056363564058483126 0 -0.017081526109860471 0.057345327015421534 0.99820826230564352 0
		 101.45038701479768 197.6276442095365 -2.2117022889472273 1;
	setAttr ".radi" 5;
createNode joint -n "L_Hand" -p "L_ForeArm";
	rename -uid "B8756579-49D0-D575-6EA6-46A7FFF99B73";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 47.000000000000128 5.6843418860808015e-014 -3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 -4.3368086899420177e-018 6.9388939039072284e-018 0
		 2.2334564753201391e-017 1 4.163336342344337e-017 0 -1.0408340855860843e-017 -4.163336342344337e-017 0.99999999999999989 0
		 148.37677365215077 195.17344448799017 -1.2676996997440004 1;
	setAttr ".radi" 5;
createNode joint -n "L_EndHand" -p "L_Hand";
	rename -uid "4F188C22-4ED9-5AD6-74DC-D8818C482509";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 20 1.9895196601282805e-013 1.7763568394002505e-015 ;
	setAttr ".ra" -type "double3" 0 180 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5223195332566262 4.1534901098602948 0.11028529268091862 ;
	setAttr ".radi" 5;
createNode orientConstraint -n "L_Hand_orientConstraint1" -p "L_Hand";
	rename -uid "8924986D-49F8-6660-7752-269C49047A1A";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ct_R_Hand2W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 5.6417603611573002 -0.74696161716793885 6.4323470498308541 ;
	setAttr ".rsrr" -type "double3" 3.2879289185550968 0.97874695389818389 3.0543385991235912 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector1" -p "L_ForeArm";
	rename -uid "119536C3-41C6-1EA8-8D86-328259F06819";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "R_Clavicle" -p "Spine02";
	rename -uid "5D6EC256-493A-A34B-4C2C-85BB44D17EB6";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 11.673162207651302 30.825281453024701 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 89.124702400543939 ;
	setAttr ".bps" -type "matrix" 0.99999917382637515 0.0012854363332349839 -1.2246467991473532e-016 0
		 0.0012854363332349839 -0.99999917382637515 0 0 -1.2246457873764679e-016 -1.5742054910038912e-019 -1 0
		 -32.297215993764297 199.11632430163576 0 1;
	setAttr ".radi" 5;
createNode joint -n "R_Humerus" -p "R_Clavicle";
	rename -uid "9E32E7C1-429A-4298-26D4-85AF5129DD85";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -25.000029629748283 -4.1318752380448132e-005 3.154610000000003 ;
	setAttr ".r" -type "double3" 15.440451040948018 28.807262321983881 -67.935060006342567 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.35856390511946e-017 2.0868076394731498 7.4593720566049219e-016 ;
	setAttr ".bps" -type "matrix" 0.99942397863139254 0.033321858885667577 0.006431536130739767 0
		 0.021111148648948662 -0.758828724986776 0.65094798988680036 0 0.026571231422712564 -0.65043725281947629 -0.75909508614226529 0
		 -57.297225022260108 199.08422967393597 -3.1546099999999999 1;
	setAttr ".radi" 5;
createNode joint -n "R_ForeArm" -p "R_Humerus";
	rename -uid "F07AA439-441C-5A7A-7789-A386A6997544";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -47.000242032447105 -0.00023434786061216073 -8.5946018864024865e-006 ;
	setAttr ".r" -type "double3" -0.0012568632031172793 -65.244055952112021 -0.026768656487466704 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -2.0584552314110909e-017 -3.3837863479488339 6.9689101352179564e-016 ;
	setAttr ".bps" -type "matrix" 0.99942510665607209 0.033288378867448634 0.0064296203523820841 0
		 0.021086972463653483 -0.75882934026302207 0.6509480562597173 0 0.026547990089942533 -0.65043824932760819 -0.75909504545466522 0
		 -104.27039908668849 197.51827766147605 -3.4570397789339378 1;
	setAttr ".radi" 5;
createNode joint -n "R_Hand" -p "R_ForeArm";
	rename -uid "E125A6E4-4853-81D3-8529-778A3BBEC8DD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -46.999405245692927 0.00090540455664722685 -1.6531703053601632e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.99999999999999989 3.4694469519536142e-018 1.0408340855860843e-017 0
		 -1.0408340855860843e-017 1 -3.8857805861880479e-016 0 -6.9388939039072284e-018 5.5511151231257827e-017 1 0
		 -151.24276603377962 195.95306735842226 -3.7586261909813392 1;
	setAttr ".radi" 5;
createNode joint -n "R_EndHand" -p "R_Hand";
	rename -uid "6DA62759-47C4-D3CD-D550-59ADC65EC0E6";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -20.000709041543843 -0.00026696338920828566 1.8366170025707618e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.5223195332562445 4.1534901098602024 0.11028529268092088 ;
	setAttr ".radi" 5;
createNode orientConstraint -n "R_Hand_orientConstraint1" -p "R_Hand";
	rename -uid "8AD9472D-4580-2B2C-3C25-8F91DBA96E13";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "ct_R_Hand1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -138.22593457899774 4.1429763700943223 1.7090846157011941 ;
	setAttr ".rsrr" -type "double3" -139.40804625408569 -1.5212665194428212 1.2087101673414682 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "R_ForeArm";
	rename -uid "FD487F07-401D-E675-1BE4-BC89FAEBF3BC";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode orientConstraint -n "Spine02_orientConstraint1" -p "Spine02";
	rename -uid "72C60414-486A-017F-46F3-6AAE36367EFD";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "Chest_CtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -7.9688213833253752 -2.0499668845370319e-016 0.69618239904497714 ;
	setAttr ".rsrr" -type "double3" 0 0 0.69618239904498347 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "Spine01_orientConstraint1" -p "Spine01";
	rename -uid "B5CC0231-4530-2E9D-916B-2EA152C4EFAE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "nurbsCircle9W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" -3.0373756005162837 1.0871036508908507e-016 -0.18488350230720771 ;
	setAttr ".rsrr" -type "double3" 0 0 -0.18488350230720912 ;
	setAttr -k on ".w0";
createNode joint -n "HipsSway" -p "|reference|Hips";
	rename -uid "B67FCE1B-457F-EE41-3578-169F81BDB46C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -2.6015124977278674 0.01987181211792555 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -90.437648799728265 ;
	setAttr ".bps" -type "matrix" 1 2.1285057050235423e-015 0 0 -2.1285057050235423e-015 1 0 0
		 0 0 1 0 -0.79438717750410393 105.35528275315598 0 1;
	setAttr ".radi" 5;
createNode joint -n "L_Femur" -p "HipsSway";
	rename -uid "C5D8BB96-4271-64F6-0890-64BA476711C9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 22.999999999999996 -11 0 ;
	setAttr ".r" -type "double3" -25.981022428334761 8.9741140598071585 5.9794385661973752 ;
	setAttr ".ra" -type "double3" 0 0 -89.234254428610754 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.0129806667496939 -0.013538365766929973 -0.76562589014757576 ;
	setAttr ".bps" -type "matrix" -0.00045566368752379395 -0.99890412464381673 0.046801091228452441 0
		 0.99998832637509538 -0.00023002935928882233 0.0048264065337999236 0 -0.0048103517687913093 0.046802744108267 0.9988925686277762 0
		 22.205612822495919 94.355282753156018 0 1;
	setAttr ".radi" 5;
createNode joint -n "L_Knee" -p "L_Femur";
	rename -uid "A13E6066-4F78-30D4-3BB2-DC9C732DB1D0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 37.223624498579156 0.0030930882668313798 -2.8729275887621708 ;
	setAttr ".r" -type "double3" 83.968909023057179 -1.5936361004466748 -0.40580401713147723 ;
	setAttr ".ra" -type "double3" 0 0 -91.087279363339533 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.394830993362258 -0.10223653569245969 91.082462631372849 ;
	setAttr ".bps" -type "matrix" 1.2463983317277313e-006 -0.99954375931556316 -0.030203860875024996 0
		 0.99998832291154116 -0.00014471741371841859 0.004830434497355038 0 -0.0048326016812439854 -0.030203514202515248 0.99953208737429178 0
		 22.205564212961384 57.037989101053149 -1.1276248441071672 1;
	setAttr ".radi" 5;
createNode joint -n "L_Ankle" -p "L_Knee";
	rename -uid "536D9AF9-4B34-D88D-03F9-46A82082186F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 38.999999999999993 -1.4210854715202004e-014 1.7763568394002505e-015 ;
	setAttr ".r" -type "double3" 0.70208103479648487 -10.979188152987005 4.5389169596886099 ;
	setAttr ".ra" -type "double3" 93.772264485527671 -52.278007113931487 -94.764936702264052 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.1096454647408278 2.8183336766699516 91.088970300676365 ;
	setAttr ".bps" -type "matrix" 2.2481119358163954e-005 -0.65568230538895289 0.75503689571730703 0
		 0.013951622601164958 0.75496361486015484 0.65561825208278246 0 -0.9999026711242418 0.0105192507868245 0.0091648044981851619 0
		 22.205612822496306 18.055782487746193 -2.3055754182331398 1;
	setAttr ".radi" 5;
createNode joint -n "L_Ball" -p "L_Ankle";
	rename -uid "D4601E9C-4231-B50E-F486-7CB32C26FEE7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 15.635191375353306 -8.3175816148381845 0.00092829369432934072 ;
	setAttr ".r" -type "double3" -9.4789475460772357e-016 1.130886893313625e-016 -1.4000912256103731e-014 ;
	setAttr ".ra" -type "double3" 94.544985064908957 -85.422733295771678 2.4471944954502551 ;
	setAttr ".jo" -type "double3" -11.17432999567168 85.361735861056658 29.524569216667732 ;
	setAttr ".bps" -type "matrix" 0.0095593241108671428 -0.0049768146209813796 0.99994192363295387 0
		 -0.99538150205078235 -0.095571662878439184 0.0090400569565661712 0 0.095521121735830536 -0.99541011074374719 -0.0058674297141303291 0
		 22.088992356110644 1.5246024436786385 4.0464211295137602 1;
	setAttr ".radi" 5;
createNode joint -n "L_Toe" -p "L_Ball";
	rename -uid "ECC246B3-48B3-7AC8-CBAA-E98BE9138660";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 16.223257891270887 0.21936079425606181 0.16055308274027125 ;
	setAttr ".ra" -type "double3" 1.4555700776826828 -0.11189645737455367 89.999999999999972 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -91.453247771645266 -5.4370824865772178 -85.950349611321911 ;
	setAttr ".bps" -type "matrix" 0.00047470849924377556 0.99993156310529363 -0.01168947208583769 0
		 -0.99937345097432539 6.0683569672370652e-005 -0.035393527729961266 0 -0.03539039614793691 0.011698949666923216 0.99930508576569521 0
		 22.041064270111331 1.2630814588572796 20.269777833050416 1;
	setAttr ".radi" 5;
createNode ikEffector -n "effector6" -p "L_Ball";
	rename -uid "3C9F8705-4593-F80F-74A7-AA9BD911DE52";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector5" -p "L_Ankle";
	rename -uid "FB9951EA-489B-4071-3F9C-6990C90892CC";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector3" -p "L_Knee";
	rename -uid "C701E06F-4F33-9612-5D79-8494242D1A37";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode joint -n "R_Femur" -p "HipsSway";
	rename -uid "399198CE-49AC-06E6-7CC9-EFB19E7605B5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -23.000012822495929 -10.999966056766283 0 ;
	setAttr ".r" -type "double3" 21.032834812458802 193.74734707145871 3.6127682634807199 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 178.98701933324932 0.013538365766928212 0.76562589014725702 ;
	setAttr ".bps" -type "matrix" -0.99991019573185391 0.013182170989125865 0.0024147131296929402 0
		 -0.013398117603920484 -0.98734990237758447 -0.15798943230370097 0 0.00030152306176988472 -0.15800759678884949 0.98743785041938492 0
		 -23.794400000000007 94.35531669638965 0 1;
	setAttr ".radi" 5;
createNode joint -n "R_Knee" -p "R_Femur";
	rename -uid "5F4924FA-4ACA-0761-F700-3E825DBDBEFA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -0.49328325995498296 36.936009754266287 4.7725165370720752 ;
	setAttr ".r" -type "double3" -56.372991171242148 1.06989640524053 0.017111945354481576 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.3948309933632768 -0.10223653569248432 1.8482082027619784 ;
	setAttr ".bps" -type "matrix" -0.99981980869632447 -0.018980975639472385 -0.00026963348923465802 0
		 0.01898046454470784 -0.99936408916468988 -0.030185414585470693 0 0.00030348659253690306 -0.03018509320514709 0.99954428018176611 0
		 -23.79459501769578 57.125954658360726 -1.1241268797475472 1;
	setAttr ".radi" 5;
createNode joint -n "R_Ankle" -p "R_Knee";
	rename -uid "AE1BAA76-4D11-4881-D694-0A85DD1AF8B1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0.74004397034043734 38.993020301891434 -2.5790303990191887e-006 ;
	setAttr ".r" -type "double3" 149.86815373929983 11.547883959860529 172.69816158041007 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.1096454647415628 2.8183336766699112 0.0016909373369013976 ;
	setAttr ".bps" -type "matrix" 0.99860817692359849 -0.023013131573756257 0.047456345797964039 0
		 -0.020040068285179433 -0.99786251875094267 -0.062199592724950778 0 0.048786316159885448 0.061161993486097188 -0.99693485539846327 0
		 -23.794400000000003 18.143683761855833 -2.3013494819928098 1;
	setAttr ".radi" 5;
createNode joint -n "R_Ball" -p "R_Ankle";
	rename -uid "6F091F8D-472A-A3B2-1624-ABA8BAF0F332";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.79556222184338665 16.120578815686603 -7.2892981461462689 ;
	setAttr ".jo" -type "double3" -2.9839774796607137 -6.8982548622562225 -2.440110104439865 ;
	setAttr ".bps" -type "matrix" 0.997180630304077 0.027256695519254411 -0.069913254077709625 0
		 0.026234421635876696 -0.99953566102067282 -0.015498950590203051 0 -0.070303240805774853 0.01362111953317971 -0.9974326641106499 0
		 -23.678620564220228 1.5934259972925027 4.0006669503692276 1;
	setAttr ".radi" 5;
createNode joint -n "R_Toe" -p "R_Ball";
	rename -uid "89441F6B-4084-FF59-5B12-938DF2A74DF0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.0959105788523225 0.1307545171545561 -16.187996391774753 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.1411704923485018 0.54529673234109255 1.5437698750083988 ;
	setAttr ".bps" -type "matrix" 0.99814936603838467 0.00018905693111230613 -0.060809599033766495 0
		 0.00057138928758492945 -0.99998017985447063 0.0062700408693023065 0 -0.060807208383979336 -0.0062931832721989881 -0.99812969059779511 0
		 -23.629942488369309 1.2123626597378312 20.221695434975636 1;
	setAttr ".radi" 5;
createNode ikEffector -n "effector8" -p "R_Ball";
	rename -uid "1358C3A2-4BFF-C648-63B0-FAB286AF31FF";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector7" -p "R_Ankle";
	rename -uid "9C734773-42DF-0B67-8358-ED88335C9B99";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode ikEffector -n "effector4" -p "R_Knee";
	rename -uid "19B7B25A-436D-D78A-1AAF-7B954C8A92CF";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode orientConstraint -n "HipsSway_orientConstraint1" -p "HipsSway";
	rename -uid "F2904357-4A43-B466-AC21-A098E84A57BB";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "HipsW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 -1.2086107789561363e-013 ;
	setAttr ".rsrr" -type "double3" 0 0 -1.2086107789561363e-013 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "Hips_parentConstraint1" -p "|reference|Hips";
	rename -uid "91EE247D-4A6C-0DF0-BE8C-D490C9FE261B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "CenterW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.5612511283791264e-017 0.04218435838317447 
		0 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 1.0613242749583225 ;
	setAttr ".lr" -type "double3" 10.864469521955055 -0.39882584805522375 1.0153230187232953 ;
	setAttr ".rst" -type "double3" 0.019871812117899235 106.60151249772784 0 ;
	setAttr ".rsrr" -type "double3" 0 0 1.0038625713369224e-014 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "reference_parentConstraint1" -p "reference";
	rename -uid "53A12EF2-4521-E36C-ACD3-93BDDB688424";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "mainCtrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" 0 -0.0053264355375555695 0 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 1.3948967283889122 ;
	setAttr ".rsrr" -type "double3" 0 0 -1.987846675914698e-016 ;
	setAttr -k on ".w0";
createNode transform -n "SF_Character_Knight";
	rename -uid "DD6E9DBC-44EF-F078-F40C-0EA64373DB46";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" -1.1151973838034781 161.18006477963942 0 ;
	setAttr ".sp" -type "double3" -1.1151973838034781 161.18006477963942 0 ;
createNode mesh -n "SF_Character_KnightShape" -p "SF_Character_Knight";
	rename -uid "4A5F8F28-4885-CF41-FE08-C9ABEDC08A20";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50217896699905396 0.49392130970954895 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".ccls" -type "string" "colorSet0";
	setAttr ".clst[0].clsn" -type "string" "colorSet0";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".vcs" 2;
createNode mesh -n "SF_Character_KnightShapeOrig" -p "SF_Character_Knight";
	rename -uid "E349C1AB-4126-3D54-CB34-EC83E5697F83";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 549 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.52595329 0.009119451 0.52888548
		 0.0091194808 0.52888554 0.023000911 0.52595329 0.023000896 0.52788877 0.10354723
		 0.69263738 0.10354722 0.69263738 0.027187169 0.52788877 0.027187265 0.69263738 0.027187169
		 0.52788877 0.027187265 0.52788877 0.002524921 0.69263738 0.0025249124 0.51763391
		 0.027187288 0.51763397 0.10354722 0.3910687 0.10354724 0.39106876 0.027187273 0.51763397
		 0.10354722 0.3910687 0.10354724 0.3910687 0.027187273 0.51763397 0.027187288 0.61649823
		 0.03776364 0.61649823 0.038061202 0.6048941 0.038061142 0.60489416 0.03776364 0.68352848
		 0.023959236 0.53699768 0.023959314 0.53699768 0.0037033679 0.68352848 0.00370336
		 0.68352848 0.023959236 0.53699768 0.023959314 0.53699768 0.0037033679 0.68352848
		 0.00370336 0.8872174 0.097235411 0.88721144 0.066267401 0.8373186 0.066277578 0.83732462
		 0.097244605 0.79654467 0.097248614 0.79654711 0.11242311 0.83732516 0.1124191 0.74665195
		 0.097248599 0.75695574 0.10839213 0.78623968 0.10839216 0.94545949 0.068147957 0.89556658
		 0.068147928 0.89556658 0.10892193 0.94545949 0.10892196 0.7058773 0.066279262 0.70587218
		 0.097245306 0.74665153 0.066281572 0.79654461 0.066281602 0.74665016 0.11242309 0.70587307
		 0.11242028 0.95244753 0.068375349 0.95244849 0.1091482 0.98173153 0.10914826 0.98173153
		 0.068375379 0.84763175 0.1083861 0.87691486 0.10838042 0.76177341 0.22212903 0.76177341
		 0.12175304 0.70488745 0.12175304 0.70488745 0.22212903 0.91163653 0.22212903 0.86514753
		 0.22212903 0.86514753 0.23943304 0.91163653 0.23943304 0.86514753 0.12175405 0.80826145
		 0.12175405 0.80826241 0.22212903 0.98141414 0.12570532 0.92452914 0.12570532 0.92452818
		 0.17219333 0.98141414 0.17219333 0.91163653 0.12175405 0.80826241 0.23943304 0.76177341
		 0.23943304 0.93627733 0.18610452 0.93627733 0.23259251 0.9696663 0.23259251 0.96966535
		 0.18610452 0.82001048 0.234834 0.8533985 0.234834 0.71663648 0.234834 0.7500245 0.234834
		 0.86564159 0.61792421 0.86564147 0.5038842 0.80465484 0.5038842 0.80465472 0.61792421
		 0.92583352 0.56647146 0.92583358 0.61771244 0.9867183 0.61771238 0.98671818 0.56647146
		 0.75341368 0.61792421 0.75341368 0.5038842 0.69237161 0.5038842 0.69237161 0.61792421
		 0.98580587 0.55589259 0.98580587 0.50465178 0.92460883 0.50465167 0.92460877 0.55589247
		 0.9168824 0.5038842 0.9168824 0.61792421 0.31405377 0.80719042 0.1998038 0.80719042
		 0.1998038 0.60584378 0.31405377 0.60584378 0.75255662 0.62826431 0.86680669 0.62826431
		 0.86680675 0.80612814 0.75255758 0.80612814 0.60616684 0.80719042 0.49191785 0.80719042
		 0.49191785 0.60584378 0.60616684 0.60584378 0.62548518 0.62793285 0.73973405 0.62793285
		 0.73973411 0.8057968 0.62548524 0.8057968 0.021939829 0.80719042 0.021939829 0.60584378
		 0.26406899 0.36916786 0.328567 0.36916786 0.328567 0.45160288 0.26406899 0.45160288
		 0.39202014 0.42706543 0.39202014 0.44318974 0.33627918 0.44318974 0.33627918 0.42706546
		 0.20832804 0.45160288 0.14383003 0.45160288 0.14383003 0.36916786 0.20832804 0.36916786
		 0.023792118 0.37431324 0.07953307 0.37431324 0.07953307 0.43881124 0.023792118 0.43881124
		 0.088089019 0.45160288 0.088089019 0.36916786 0.39202014 0.39481696 0.33627918 0.39481696
		 0.39202014 0.37869275 0.33627918 0.37869275 0.31244248 0.45160288 0.28019351 0.45160288
		 0.19220354 0.45160288 0.15995453 0.45160288 0.18585658 0.092635326 0.18585658 0.013842285
		 0.12690163 0.013841331 0.12690163 0.092635326 0.076605678 0.11023831 0.1269176 0.1102333
		 0.076608658 0.092635326 0.017639637 0.092635326 0.029821634 0.10534327 0.064437628
		 0.10534929 0.24555188 0.014648795 0.2455669 0.064968824 0.30452186 0.064969778 0.30453092
		 0.014646769 0.23616362 0.092635326 0.23616362 0.013842285 0.076608658 0.013841331
		 0.1858356 0.11024027 0.23615265 0.11024027 0.29759133 0.074226558 0.24727345 0.074238256
		 0.24727178 0.10885536 0.29759097 0.10884757 0.017639637 0.013841331 0.13907063 0.10535245
		 0.1736877 0.10535412 0.34637493 0.34504279 0.32591394 0.31407481 0.39626792 0.3140758
		 0.39626798 0.34504282 0.21906126 0.31082222 0.21906123 0.35160118 0.23423618 0.35160017
		 0.23423624 0.3108232 0.31445688 0.31271869 0.24410288 0.31271774 0.26456386 0.34368578
		 0.31445685 0.34368569 0.18809523 0.31082422 0.11774124 0.31082517 0.11774124 0.3515982
		 0.18809524 0.35159922 0.086773261 0.31082121 0.086773261 0.35160324 0.051232263 0.35161519
		 0.066352248 0.35161322 0.066352248 0.31081024 0.051232249 0.31080824 0.021989226
		 0.31081617 0.021989226 0.35160723 0.2441029 0.34368569 0.32591394 0.34504277 0.27486789
		 0.35482872 0.30415288 0.35482869 0.38596395 0.35618576 0.35667992 0.35618582 0.37509015
		 0.59859747 0.32266086 0.59861594 0.29644424 0.58693701 0.29643971 0.56081003 0.40129131
		 0.56077391 0.40130061 0.58690041 0.028354168 0.56079191 0.13320708 0.56081003 0.13320482
		 0.58693701 0.10698978 0.59862053 0.054561727 0.59861147 0.028349698 0.58691889 0.87788939
		 0.80596268 0.87788939 0.64272672 0.98274338 0.6427266 0.98274338 0.80596358 0.5645377
		 0.58684087 0.56452817 0.56071389 0.98274338 0.80596358 0.95653003 0.80596352 0.95653003
		 0.64272678 0.98274338 0.6427266 0.90410316 0.64272672 0.90410316 0.80596298 0.87788939
		 0.80596268 0.87788939 0.64272672 0.26998273 0.46517903 0.26998255 0.54761404 0.20548463
		 0.54761404 0.20548463 0.465179 0.020982057 0.52073479 0.076723009 0.52073479 0.076723009
		 0.53685915 0.020982057 0.53685915 0.085245609 0.54761404 0.085245609 0.465179 0.14974362
		 0.465179 0.14974362 0.54761404 0.39248851 0.47594455 0.39248851 0.54044253 0.33674756
		 0.54044253 0.33674756 0.47594455 0.32572374 0.46517926 0.32572335 0.54761428 0.020982057
		 0.48848635 0.076723009 0.48848635 0.020982057 0.47236216 0.076723009 0.47236216;
	setAttr ".uvst[0].uvsp[250:499]" 0.25385812 0.54761404 0.22160915 0.54761404
		 0.10137013 0.54761404 0.13361913 0.54761404 0.14388275 0.22425914 0.13170278 0.21155417
		 0.19068077 0.21155417 0.1785008 0.22425914 0.13170075 0.22915113 0.081373811 0.22915113
		 0.081372738 0.2115531 0.022391677 0.21155417 0.069191813 0.22425914 0.034573674 0.22425914
		 0.25054735 0.18157533 0.2505483 0.1312553 0.30952936 0.1312553 0.30952936 0.18157533
		 0.1906828 0.13277116 0.24100769 0.13277224 0.24101269 0.2115531 0.081374764 0.13277116
		 0.13170075 0.13277116 0.24100959 0.22915113 0.1906828 0.22915113 0.30371839 0.19463784
		 0.30371842 0.22925681 0.25339839 0.22925681 0.25339836 0.19463876 0.022391677 0.13277021
		 0.34698552 0.28439239 0.39687848 0.28439239 0.39687854 0.25342345 0.3265245 0.25342238
		 0.21805179 0.25098187 0.23322678 0.25098288 0.23322678 0.29175988 0.21805179 0.29176086
		 0.26193443 0.28546637 0.31182739 0.28546637 0.3015224 0.29660839 0.2722384 0.29660839
		 0.18708587 0.2509838 0.18708581 0.29175884 0.11673179 0.29175788 0.11673179 0.25098485
		 0.085763842 0.29176283 0.085762829 0.25098085 0.050222844 0.29177588 0.050222874
		 0.2509678 0.065342814 0.25096977 0.065342814 0.29177386 0.020979822 0.29176688 0.020979822
		 0.25097686 0.24147339 0.25449741 0.24147339 0.28546637 0.3265245 0.28439242 0.31182739
		 0.25449836 0.38657454 0.29553545 0.35729051 0.29553542 0.80417323 0.051938251 0.79386842
		 0.040795475 0.84376168 0.040795222 0.83345711 0.051938221 0.79386389 0.055969719
		 0.75308597 0.055962224 0.75308776 0.040787727 0.70319474 0.040774167 0.74278069 0.051928237
		 0.71349561 0.051920027 0.89161509 0.052468069 0.89161509 0.011694044 0.94150811 0.011694074
		 0.94150811 0.052468039 0.84376252 0.0098281726 0.88453686 0.0098271295 0.88454139
		 0.040792093 0.75309646 0.0098207071 0.79386973 0.0098279566 0.88454115 0.055967141
		 0.84376419 0.055969168 0.979092 0.052039444 0.949808 0.052039444 0.94980812 0.011265457
		 0.979092 0.011265427 0.70320344 0.0098061934 0.71892065 0.36024863 0.70717162 0.34754461
		 0.76405853 0.34754354 0.75230962 0.36024755 0.91392362 0.3475396 0.91392457 0.36484355
		 0.86743557 0.36484462 0.86743462 0.34754163 0.81054753 0.34754258 0.85568464 0.3602466
		 0.82229656 0.3602466 0.98199046 0.25453752 0.98199046 0.30102554 0.92510438 0.30102554
		 0.92510545 0.25453752 0.76405853 0.24716769 0.81054753 0.24716668 0.86743367 0.24716668
		 0.91392267 0.24716471 0.81054753 0.36484557 0.7640596 0.36484665 0.96853173 0.36227971
		 0.93514359 0.36227947 0.93514335 0.31579143 0.96853185 0.31579149 0.70717257 0.2471687
		 0.86454749 0.49183792 0.80350649 0.49183792 0.80350649 0.37779593 0.86454749 0.37779593
		 0.92374724 0.43960747 0.98478818 0.4396075 0.984788 0.49084851 0.92374706 0.49084854
		 0.75226557 0.49183691 0.69122446 0.49183691 0.69122446 0.37779593 0.75226557 0.37779593
		 0.92226082 0.37751818 0.98330188 0.37751818 0.9833017 0.42875916 0.92226076 0.42875922
		 0.91578853 0.37779593 0.91578853 0.49183792 0.50111896 0.82511163 0.34220302 0.82511163
		 0.34220296 0.98523957 0.50111896 0.98523957 0.65991461 0.98523951 0.50366747 0.98523957
		 0.50366759 0.82511163 0.65991473 0.82511157 0.021948159 0.98523957 0.18207607 0.98523957
		 0.18207589 0.82511163 0.021947801 0.82511163 0.98317897 0.98523951 0.82305217 0.98523962
		 0.82305217 0.82511163 0.98317897 0.82511163 0.66269046 0.82487792 0.66269052 0.98500603
		 0.82281846 0.98500592 0.8228184 0.82487792 0.42867175 0.87296391 0.48500586 0.87296391
		 0.48500586 0.88704741 0.42867175 0.88704741 0.2851139 0.9859814 0.22877975 0.9859814
		 0.22877975 0.98001057 0.2851139 0.98001057 0.42867175 0.87296391 0.48500583 0.87296391
		 0.4850058 0.88704741 0.42867175 0.88704741 0.28579479 0.98532611 0.28579479 0.98136085
		 0.22818796 0.98136085 0.22818796 0.98532611 0.61663389 0.86576879 0.61663389 0.94098657
		 0.54141611 0.94098657 0.54141611 0.86576879 0.48333952 0.067262962 0.48333952 0.077537775
		 0.47610143 0.077537775 0.4803991 0.077457964 0.47316101 0.067183167 0.48327461 0.067183167
		 0.48327461 0.081263021 0.54825276 0.0018612146 0.69211119 0.0018613338 0.69211119
		 0.025153548 0.54825276 0.025153548 0.66912723 0.025153548 0.66912723 0.0018613338
		 0.61311609 0.0018613338 0.61311603 0.025153548 0.48897454 0.65691173 0.31561136 0.65691173
		 0.31561136 0.6480599 0.48897505 0.6480599 0.44786635 0.21590528 0.44412491 0.21590528
		 0.44412491 0.21180077 0.44786635 0.21180077 0.55696315 0.075282559 0.53307277 0.075282551
		 0.53307277 0.070178241 0.55696315 0.070178248 0.69184703 0.025153548 0.69184715 0.0018613338
		 0.4803991 0.081263021 0.47322598 0.081342831 0.47610143 0.081342831 0.47322598 0.067262962
		 0.47316101 0.077457964 0.59676081 0.096614614 0.57989448 0.096614614 0.58204466 0.09653838
		 0.59461057 0.09653838 0.57989448 0.096016638 0.5820446 0.096092865 0.59676081 0.096016638
		 0.59461063 0.096092865 0.48887366 0.65734553 0.31561136 0.65734553 0.31561136 0.65089691
		 0.4888733 0.65089691 0.48980582 0.093737267 0.47368824 0.093737267 0.47368824 0.090860121
		 0.48980582 0.090860121 0.53683364 0.082218461 0.5522396 0.082218491 0.55223966 0.085095637
		 0.53683364 0.085095577 0.47296739 0.091019236 0.48908496 0.091019206 0.48908496 0.093896382
		 0.47296751 0.093896352 0.59375411 0.097159684 0.58313447 0.097159684 0.58611184 0.096593499
		 0.59077674 0.096593499 0.58313447 0.095140278 0.58611184 0.095706463 0.59375411 0.095140278
		 0.59077674 0.095706463 0.4890672 0.65808368 0.31561136 0.65808368 0.31561136 0.65070832
		 0.48906675 0.65070832 0.48357451 0.093412869 0.47649455 0.093412869 0.47649455 0.092369072
		 0.48357451 0.092369072 0.53896981 0.082270704 0.54573745 0.082270704 0.54573739 0.083314501
		 0.53896981 0.083314501 0.47612011 0.088257901 0.48320007 0.088257901;
	setAttr ".uvst[0].uvsp[500:548]" 0.48320016 0.089301728 0.47612011 0.089301728
		 0.40037841 0.17645644 0.40037841 0.18372101 0.39701682 0.18372101 0.39701682 0.17645644
		 0.40172654 0.13244388 0.40172654 0.19714332 0.42293179 0.19714332 0.44458517 0.13244388
		 0.44793883 0.21264906 0.4516803 0.21264906 0.4516803 0.21695966 0.44793883 0.21695966
		 0.40172654 0.19714332 0.40172654 0.13244396 0.44431603 0.13244388 0.42266262 0.19714332
		 0.44821995 0.1971433 0.4484891 0.1971433 0.39861697 0.1900253 0.39919031 0.2034983
		 0.38880572 0.20395684 0.38823241 0.19048387 0.3896791 0.17646426 0.39694366 0.17646426
		 0.39694366 0.1872499 0.3896791 0.1872499 0.49105167 0.1971433 0.49105167 0.1324439
		 0.44786635 0.22087002 0.44412491 0.22087002 0.49105167 0.1324439 0.49105167 0.1971433
		 0.38552362 0.068695322 0.38335952 0.068695322 0.38335952 0.067651495 0.38552359 0.067651495
		 0.48320025 0.090345554 0.47612011 0.090345554 0.54573739 0.084358297 0.53896981 0.084358297
		 0.47649455 0.091325276 0.48357451 0.091325276 0.31405377 0.7065171 0.49191785 0.7065171
		 0.60616684 0.7065171 0.021939829 0.7065171 0.1998038 0.7065171;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".clst[0].clsn" -type "string" "colorSet0";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 238 ".vt";
	setAttr ".vt[0:165]"  -46.13156891 49.67648315 -2.33335781 -46.13156891 81.32736206 -34.7646904
		 46.13195038 81.32736206 -34.7646904 46.13195038 49.67648315 -2.33335781 -46.13160324 135.86730957 18.4627552
		 46.13198471 135.86730957 18.46274567 -46.13160324 104.21646118 50.89408875 46.13198471 104.21643066 50.89408112
		 -38.14547348 54.39732361 2.27388144 38.14585495 54.39732361 2.27388144 38.14587784 99.49560547 46.28685379
		 -38.14549637 99.49560547 46.2868576 -38.14547348 66.013458252 -9.62870693 38.14585495 66.013458252 -9.62870693
		 38.14587784 111.11175537 34.38426971 -38.14549637 111.11175537 34.38427353 -153.16995239 23.90899658 8.074157715
		 -153.16995239 23.90899658 -16.54467392 -168.45050049 23.90899658 -16.54467392 -168.45050049 23.90899658 8.074157715
		 -147.67254639 23.90908813 -11.45985794 -147.67254639 42.048019409 -11.45985413 -153.16995239 42.047958374 -16.54467773
		 -153.16995239 42.048339844 8.074157715 -147.67254639 42.047851563 2.98933029 -168.45053101 42.047958374 -16.54467773
		 -168.45053101 42.048339844 8.074157715 -147.67251587 23.90890503 2.98933411 -103.64163208 21.50933838 9.7993927
		 -103.64163208 21.50915527 -18.26991272 -153.16995239 21.50915527 -18.26991272 -153.16995239 21.50933838 9.7993927
		 -97.37417603 21.50933838 -12.47259903 -97.37417603 44.44741821 -12.47221375 -103.64163208 44.44761658 -18.26953125
		 -153.16995239 44.44761658 -18.26953125 -103.64163208 44.44741821 9.79901123 -153.16995239 44.44741821 9.79901123
		 -97.37417603 44.44799805 4.0016975403 -97.37418365 21.50915527 4.00207901 -47.37109375 20.33677673 10.82440758
		 -47.37109375 20.33657837 -19.29493332 -103.64161682 20.33657837 -19.29493332 -103.64161682 20.33677673 10.82440758
		 -47.37109375 45.62055969 10.82440758 -47.37109375 45.62055969 -19.2949276 -103.64163208 45.62052917 10.82440758
		 -103.64163208 45.62052917 -19.2949276 -43.88117218 -57.86965179 -32.42210388 -43.88117218 -57.86965179 23.95158386
		 -43.88117218 42.12882996 23.95158386 -43.88117218 42.12882996 -32.42210388 43.88152313 42.12882996 23.95158386
		 43.88152313 42.12882996 -32.42210388 43.88152313 -57.86965179 23.95158386 43.88152313 -57.86965179 -32.42210388
		 -35.71401215 -111.43650818 -20.37628174 -36.24720764 -111.43684387 11.44408226 -36.24710464 -70.76131439 11.44452095
		 -35.71390915 -70.7609787 -20.37584114 -8.7469902 -70.76139069 11.90532303 -8.21379662 -70.76105499 -19.91503906
		 -8.74709415 -111.43692017 11.90488434 -8.21390057 -111.43658447 -19.91547775 -8.61368084 -66.24171448 3.95028114
		 -36.11379242 -66.24163818 3.48947954 -8.34708309 -66.24154663 -11.9598999 -35.84719849 -66.24147034 -12.42070198
		 -9.79524803 -108.21308136 10.30463028 -9.82143879 -108.21337891 -18.79831314 -9.82134438 -147.088134766 -18.79790878
		 -9.79515362 -147.08782959 10.30503178 -34.64481735 -101.94515991 -12.76522636 -34.65031052 -108.21343994 -18.77606773
		 -9.81604481 -101.94509888 -12.78766441 -34.62430954 -108.21313477 10.32706738 -34.62982941 -101.94499207 4.31609535
		 -34.65021515 -147.088195801 -18.77566528 -34.62421417 -147.087890625 10.32747173
		 -9.80067348 -101.9449234 4.29384899 -12.16352463 -147.088012695 8.073966026 -12.16352367 -147.088012695 -16.544487
		 -12.16352367 -162.36856079 -16.544487 -12.16352272 -162.36856079 18.16989517 -12.16352367 -141.59057617 -11.45994854
		 -32.28223038 -141.59057617 -11.45985413 -32.28213501 -147.088012695 -16.54467773
		 -32.28213501 -162.36856079 -16.54467773 -32.28213501 -147.088012695 8.074157715 -32.28232574 -162.36856079 18.1697979
		 -32.28232574 -147.088012695 18.1697979 -12.16352272 -147.088012695 18.16989517 -32.28203964 -141.59057617 2.98933268
		 -12.16352367 -141.59057617 2.98942852 -40.27232742 -70.76194763 -30.10401917 -40.27232742 -70.76194763 21.63349915
		 -40.27232742 -57.87042236 21.63349915 -40.27232742 -57.87042236 -30.10401917 40.27267838 -57.87042236 -30.10401917
		 40.27267838 -57.87042236 21.63349915 40.27267838 -70.76194763 21.63349915 40.27267838 -70.76194763 -30.10401917
		 40.27267838 -52.10224152 -17.16963959 -40.27232742 -52.10224152 -17.16963959 40.27267838 -52.10224152 8.69911861
		 -40.27232742 -52.10224152 8.69911861 35.9782753 -111.43751526 -20.15147209 35.98643112 -111.43772888 11.6733551
		 35.98686218 -70.7621994 11.67362785 35.97870636 -70.76198578 -20.15119934 8.48279762 -70.76190948 11.68067932
		 8.47463799 -70.76169586 -20.14414787 8.48236561 -111.43743896 11.68040466 8.47420597 -111.43722534 -20.14442062
		 8.48080635 -66.24227142 3.72450304 35.98487473 -66.24256134 3.71745157 8.47672653 -66.24216461 -12.18791008
		 35.980793 -66.24245453 -12.19496155 9.7930975 -101.94523621 -12.75440311 9.78256893 -108.21424103 -18.76506805
		 9.83424854 -108.21405029 10.33783722 9.8232069 -101.94511414 4.32709122 34.62223053 -101.94497681 -12.79807186
		 34.6115036 -108.21398926 -18.80893135 34.66280365 -108.21380615 10.29417133 34.65195084 -101.94487 4.28323078
		 9.78296757 -147.088241577 -18.76481819 34.61190414 -147.087982178 -18.80867767 34.66320419 -147.087783813 10.2944231
		 9.83464718 -147.088043213 10.33808804 12.1634903 -147.088012695 8.073965073 12.16387558 -147.088012695 -16.544487
		 12.16387558 -162.36856079 -16.544487 12.16368103 -162.36856079 18.16970062 12.16387653 -141.59056091 -11.45994854
		 32.2826767 -141.59056091 -11.45985222 32.2826767 -147.088012695 -16.54467773 32.2826767 -147.088012695 8.074157715
		 32.2826767 -141.59056091 2.98933411 32.2826767 -162.36856079 -16.54467773 32.28286743 -162.36856079 18.1697979
		 32.28286743 -147.088012695 18.1697979 12.16368103 -147.088012695 18.16970062 12.16387653 -141.59056091 2.98942804
		 147.67175293 24.47471619 -11.45985413 153.17074585 24.4750824 -16.54429245 153.17074585 24.4750824 8.073773384
		 147.67178345 24.4750824 2.98933411 147.67175293 41.48165894 -11.45985413 153.17074585 41.48202515 -16.54467773
		 153.17074585 41.48202515 8.074157715 147.67178345 41.48127747 2.98933411 168.45053101 24.47509766 -16.54429245
		 168.45053101 41.48202515 -16.54467773 168.45053101 41.48202515 8.074157715 168.45053101 24.47509766 8.073773384
		 97.37419128 21.50856018 -12.47259903 103.64163971 21.50856018 -18.26991272 103.64161682 21.50856018 9.7993927
		 97.37420654 21.50856018 4.00207901 97.37420654 44.44778442 -12.47221756 103.64162445 44.44702148 -18.26953125
		 103.64162445 44.44702148 9.79901123 97.37419891 44.44702148 4.0016937256 153.17074585 21.5085907 -18.26991272
		 153.17074585 44.44702148 -18.26953125;
	setAttr ".vt[166:237]" 153.17074585 44.44702148 9.79901123 153.17074585 21.5085907 9.7993927
		 47.37102509 20.33628845 10.82441139 47.37102509 20.33628845 -19.29493141 103.64308929 20.33628845 -19.29493141
		 103.64308929 20.33628845 10.82441139 47.37102509 45.62008667 10.82441139 47.37102509 45.62008667 -19.29493141
		 103.64308929 45.62008667 10.82441139 103.64308929 45.62008667 -19.29493141 -35.55486679 41.75454712 -39.6269455
		 -35.55486679 41.75454712 31.48314285 -35.55486679 112.8638916 31.48314285 -35.55486679 112.8638916 -39.6269455
		 35.55521774 112.8638916 31.48313904 35.55521774 112.8638916 -39.6269455 35.55521774 41.75454712 31.48313904
		 35.55521774 41.75454712 -39.6269455 -27.72223282 112.8638916 -34.37233734 27.72258377 112.8638916 -34.37233734
		 -27.72223282 112.8638916 26.28855896 27.72258377 112.8638916 26.28853416 -27.72223282 120.32748413 -34.37233734
		 -27.72223282 120.32748413 26.28855896 27.72258377 120.32748413 26.28853416 27.72258377 120.32748413 -34.3723526
		 -41.63248444 50.77859497 -50.89408875 -41.63251495 50.77859497 12.67539597 -41.63248444 113.34909058 -50.89408875
		 41.63286591 113.34909058 -50.89408875 41.63289642 50.77859497 12.67538071 41.63286591 50.77859497 -50.89408875
		 -41.63251495 95.55929565 12.67539597 41.63289642 95.55929565 12.67538071 -41.63251495 113.34909058 36.21699524
		 41.63289642 113.34909058 36.21697998 -41.63251495 95.55929565 36.21699524 41.63289642 95.55929565 36.21697998
		 31.018013 113.34909058 -39.78898239 -31.017631531 113.34909058 -39.78898239 -31.017656326 113.34909058 25.11188889
		 31.018035889 113.34909058 25.11187744 31.018013 124.93432617 -39.78898239 -31.017631531 124.93432617 -39.78898239
		 -31.017656326 124.93432617 25.11188889 31.018035889 124.93432617 25.11187744 13.62550545 124.93432617 -21.59316635
		 -13.62512493 124.93432617 -21.59316635 -13.62512779 124.93432617 6.91607285 13.62550926 124.93432617 6.91607285
		 9.13198471 133.34042358 -18.71221161 -9.13160324 133.34042358 -18.71221161 -9.13160515 133.34042358 4.03512001
		 9.13198662 133.34042358 4.03512001 9.13198471 162.36856079 -21.59316635 -9.13160324 162.36856079 -21.59316635
		 -9.13160515 154.38320923 -1.55735588 9.13198662 154.38320923 -1.55735588 9.13198471 141.79052734 -18.71221161
		 -9.13160324 141.79052734 -18.71221161 -9.13160324 141.79052734 -45.82794189 9.13198471 141.79052734 -45.82794189
		 -9.13160324 155.85321045 -45.82794189 9.13198471 155.85321045 -45.82794189 -13.62512493 131.93759155 -21.59316635
		 13.62550545 131.93759155 -21.59316635 13.62550926 131.93759155 6.91607285 -13.62512779 131.93759155 6.91607285
		 -43.88117218 -7.8704071 23.95158386 43.88152313 -7.8704071 23.95158386 43.88152313 -7.8704071 -32.42210388
		 -43.88117218 -7.8704071 -32.42210388;
	setAttr -s 407 ".ed";
	setAttr ".ed[0:165]"  12 13 0 13 14 0 14 15 0 15 12 0 3 0 0 0 1 0 1 2 0 2 3 0
		 1 4 0 4 5 0 5 2 0 4 6 0 6 7 0 7 5 0 7 3 0 0 6 0 3 9 1 9 8 0 8 0 0 7 10 0 10 9 0 6 11 0
		 11 10 0 8 11 0 9 13 0 12 8 0 10 14 0 11 15 0 16 19 0 19 18 0 18 17 0 17 16 0 17 22 0
		 22 21 0 21 20 0 20 17 0 23 24 0 24 21 0 22 23 0 19 26 0 26 25 0 25 18 0 16 23 0 23 26 0
		 25 22 0 16 27 0 27 24 0 27 20 0 28 31 0 31 30 0 30 29 0 29 28 0 29 34 0 34 33 0 33 32 0
		 32 29 0 35 37 0 37 36 0 36 34 0 34 35 0 31 37 0 35 30 0 28 36 0 38 36 0 28 39 0 39 38 0
		 39 32 0 33 38 0 40 43 0 43 42 0 42 41 0 41 40 0 41 45 0 45 44 0 44 40 0 45 47 0 47 46 0
		 46 44 0 47 42 0 43 46 0 50 51 0 51 237 0 237 234 1 234 50 0 50 52 0 52 53 0 53 51 0
		 52 235 0 235 236 1 236 53 0 49 48 0 48 55 0 55 54 0 54 49 0 234 235 1 236 237 1 56 57 0
		 57 58 0 58 59 1 59 56 0 65 58 0 58 60 0 60 64 0 64 65 0 61 60 1 60 62 0 62 63 0 63 61 0
		 62 57 0 56 63 0 59 61 0 67 65 0 64 66 0 66 67 0 59 67 0 66 61 0 68 71 0 71 70 0 70 69 0
		 69 68 1 72 74 0 74 69 0 69 73 0 73 72 0 75 76 0 76 72 0 73 75 0 77 70 0 71 78 0 78 77 0
		 68 75 0 75 78 0 77 73 0 68 79 0 79 76 0 79 74 0 80 83 1 83 82 0 82 81 0 81 80 1 81 86 0
		 86 85 0 85 84 0 84 81 0 87 89 0 89 88 0 88 86 0 86 87 0 83 89 0 87 82 0 83 91 0 91 90 0
		 90 89 0 92 88 0 88 80 0 80 93 0 93 92 0 93 84 0 85 92 0 90 88 0 91 80 0 105 103 0
		 103 97 0 97 94 0 94 95 0 95 96 0;
	setAttr ".ed[166:331]" 96 105 0 100 101 0 101 98 0 98 102 0 102 104 0 104 99 0
		 99 100 0 94 101 0 100 95 0 99 96 0 97 98 0 103 102 0 105 104 0 106 109 0 109 108 0
		 108 107 0 107 106 0 115 114 0 114 110 0 110 108 0 108 115 0 111 113 0 113 112 0 112 110 0
		 110 111 1 113 106 0 107 112 0 111 109 0 117 116 0 116 114 0 115 117 0 111 116 0 117 109 0
		 118 119 0 119 120 0 120 121 0 121 118 0 118 122 0 122 123 0 123 119 0 124 123 0 122 125 0
		 125 124 0 126 127 0 127 128 0 128 129 0 129 126 0 128 124 0 124 120 0 120 129 0 126 119 0
		 123 127 0 125 121 0 130 131 0 131 132 0 132 133 0 133 130 0 131 134 0 134 135 0 135 136 0
		 136 131 0 137 136 0 135 138 0 138 137 0 132 139 0 139 140 0 140 133 0 140 141 0 141 142 0
		 142 133 0 136 139 0 138 143 0 143 130 0 130 137 0 134 143 0 140 137 0 137 141 0 130 142 0
		 144 145 0 145 146 0 146 147 0 147 144 0 144 148 0 148 149 0 149 145 0 150 149 0 148 151 0
		 151 150 0 152 153 0 153 154 0 154 155 0 155 152 0 154 150 0 150 146 0 146 155 0 152 145 0
		 149 153 0 151 147 0 156 157 0 157 158 1 158 159 0 159 156 0 156 160 0 160 161 0 161 157 0
		 162 161 0 160 163 0 163 162 0 164 165 0 165 166 0 166 167 0 167 164 0 166 162 0 162 158 0
		 158 167 0 164 157 0 161 165 0 163 159 0 168 169 0 169 170 0 170 171 0 171 168 0 172 173 0
		 173 169 0 168 172 0 172 174 0 174 175 0 175 173 0 174 171 0 170 175 0 176 177 0 177 178 0
		 178 179 0 179 176 0 178 180 0 180 181 0 181 179 0 180 182 0 182 183 0 183 181 0 176 183 0
		 182 177 0 189 188 0 188 184 0 184 186 0 186 189 0 186 187 0 187 190 0 190 189 0 191 190 0
		 187 185 0 185 191 0 185 184 0 188 191 0 192 193 0 193 198 0 198 192 1 199 197 1 197 195 0
		 195 201 0 201 199 1 192 197 0 197 196 0 196 193 0 199 198 0 196 199 0;
	setAttr ".ed[332:406]" 192 194 0 194 195 0 220 221 0 221 222 0 222 223 0 223 220 0
		 201 200 0 200 202 0 202 203 0 203 201 0 199 203 0 202 198 0 200 198 1 200 194 0 194 205 0
		 205 204 0 204 195 0 200 206 0 206 205 0 201 207 0 207 206 0 204 207 0 205 209 0 209 208 0
		 208 204 0 206 210 0 210 209 0 207 211 0 211 210 0 208 211 0 209 213 0 213 212 0 212 208 0
		 210 214 0 214 213 0 211 215 0 215 214 0 212 215 0 213 230 0 230 231 0 231 212 0 214 233 0
		 233 230 0 215 232 0 232 233 0 231 232 0 216 217 0 217 225 0 225 224 0 224 216 0 217 218 0
		 218 222 0 222 225 1 218 219 0 219 223 0 219 216 0 224 223 1 224 220 1 221 225 1 227 226 0
		 226 228 0 228 229 0 229 227 0 225 226 0 227 224 0 221 228 0 220 229 0 230 217 0 216 231 0
		 219 232 0 218 233 0 54 235 0 234 49 0 55 236 0 48 237 0;
	setAttr -s 810 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr ".n[166:331]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr ".n[332:497]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr ".n[498:663]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr ".n[664:809]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr -s 204 -ch 810 ".fc[0:203]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 28 29 30 31
		f 4 4 5 6 7
		mu 0 4 3 0 1 2
		f 4 -7 8 9 10
		mu 0 4 20 21 22 23
		f 4 -10 11 12 13
		mu 0 4 4 5 6 7
		f 4 -14 14 -8 -11
		mu 0 4 12 13 14 15
		f 4 -9 -6 15 -12
		mu 0 4 16 17 18 19
		f 4 -5 16 17 18
		mu 0 4 8 9 25 24
		f 4 -15 19 20 -17
		mu 0 4 9 10 26 25
		f 4 -13 21 22 -20
		mu 0 4 10 11 27 26
		f 4 -16 -19 23 -22
		mu 0 4 11 8 24 27
		f 4 -18 24 -1 25
		mu 0 4 24 25 29 28
		f 4 -21 26 -2 -25
		mu 0 4 25 26 30 29
		f 4 -23 27 -3 -27
		mu 0 4 26 27 31 30
		f 4 -24 -26 -4 -28
		mu 0 4 27 24 28 31
		f 4 28 29 30 31
		mu 0 4 32 33 34 35
		f 4 32 33 34 35
		mu 0 4 35 36 37 38
		f 4 36 37 -34 38
		mu 0 4 39 40 41 36
		f 4 -30 39 40 41
		mu 0 4 42 43 44 45
		f 4 -29 42 43 -40
		mu 0 4 46 47 39 48
		f 4 44 -33 -31 -42
		mu 0 4 49 36 35 34
		f 4 -37 -43 45 46
		mu 0 4 50 39 47 51
		f 4 -47 47 -35 -38
		mu 0 4 52 53 54 55
		f 4 -41 -44 -39 -45
		mu 0 4 49 48 39 36
		f 4 -48 -46 -32 -36
		mu 0 4 56 57 32 35
		f 4 48 49 50 51
		mu 0 4 58 59 60 61
		f 4 52 53 54 55
		mu 0 4 62 63 64 65
		f 4 56 57 58 59
		mu 0 4 66 67 68 63
		f 4 -50 60 -57 61
		mu 0 4 69 70 71 72
		f 4 -49 62 -58 -61
		mu 0 4 59 58 68 67
		f 4 -60 -53 -51 -62
		mu 0 4 66 63 62 73
		f 4 63 -63 64 65
		mu 0 4 74 68 58 75
		f 4 -66 66 -55 67
		mu 0 4 76 77 78 79
		f 4 -64 -68 -54 -59
		mu 0 4 68 80 81 63
		f 4 -67 -65 -52 -56
		mu 0 4 82 83 58 61
		f 4 68 69 70 71
		mu 0 4 84 85 86 87
		f 4 72 73 74 -72
		mu 0 4 88 89 90 91
		f 4 75 76 77 -74
		mu 0 4 92 93 94 95
		f 4 78 -70 79 -77
		mu 0 4 96 97 98 99
		f 4 -80 -69 -75 -78
		mu 0 4 100 85 84 101
		f 4 -71 -79 -76 -73
		mu 0 4 87 86 93 92
		f 4 80 81 82 83
		mu 0 4 102 103 548 544
		f 4 -81 84 85 86
		mu 0 4 106 107 108 109
		f 4 -86 87 88 89
		mu 0 4 110 111 545 546
		f 4 90 91 92 93
		mu 0 4 114 115 116 117
		f 4 94 -88 -85 -84
		mu 0 4 544 545 111 102
		f 4 -87 -90 95 -82
		mu 0 4 103 118 547 548
		f 4 96 97 98 99
		mu 0 4 120 121 122 123
		f 4 100 101 102 103
		mu 0 4 124 125 126 127
		f 4 104 105 106 107
		mu 0 4 128 129 130 131
		f 4 108 -97 109 -107
		mu 0 4 132 133 134 135
		f 4 -102 -98 -109 -106
		mu 0 4 129 136 137 130
		f 4 -100 110 -108 -110
		mu 0 4 120 123 128 131
		f 4 111 -104 112 113
		mu 0 4 138 124 127 139
		f 4 114 -114 115 -111
		mu 0 4 140 138 139 141
		f 4 -99 -101 -112 -115
		mu 0 4 123 122 142 143
		f 4 -113 -103 -105 -116
		mu 0 4 144 145 129 128
		f 4 116 117 118 119
		mu 0 4 146 147 148 149
		f 4 120 121 122 123
		mu 0 4 150 151 149 152
		f 4 124 125 -124 126
		mu 0 4 153 154 155 152
		f 4 127 -118 128 129
		mu 0 4 156 157 158 159
		f 4 -117 130 131 -129
		mu 0 4 147 146 160 161
		f 4 -123 -119 -128 132
		mu 0 4 152 149 148 162
		f 4 -131 133 134 -125
		mu 0 4 160 146 163 164
		f 4 -135 135 -121 -126
		mu 0 4 165 166 167 168
		f 4 -130 -132 -127 -133
		mu 0 4 162 169 153 152
		f 4 -136 -134 -120 -122
		mu 0 4 170 171 146 149
		f 4 136 137 138 139
		mu 0 4 172 173 174 175
		f 4 140 141 142 143
		mu 0 4 176 177 178 179
		f 4 144 145 146 147
		mu 0 4 180 181 182 183
		f 4 -138 148 -145 149
		mu 0 4 184 185 186 187
		f 4 150 151 152 -149
		mu 0 4 185 188 189 186
		f 4 -148 -141 -139 -150
		mu 0 4 187 177 176 184
		f 4 153 154 155 156
		mu 0 4 190 191 192 193
		f 4 -157 157 -143 158
		mu 0 4 190 193 194 195
		f 3 -153 159 -146
		mu 0 3 181 196 182
		f 4 -160 -152 160 -155
		mu 0 4 191 189 188 192
		f 3 -161 -151 -137
		mu 0 3 172 197 173
		f 4 -154 -159 -142 -147
		mu 0 4 182 198 199 183
		f 4 -158 -156 -140 -144
		mu 0 4 200 201 172 175
		f 6 161 162 163 164 165 166
		mu 0 6 202 203 204 205 206 207
		f 6 167 168 169 170 171 172
		mu 0 6 208 209 210 211 212 213
		f 4 173 -168 174 -165
		mu 0 4 214 215 216 217
		f 4 175 -166 -175 -173
		mu 0 4 218 207 206 219
		f 4 -164 176 -169 -174
		mu 0 4 205 204 210 209
		f 4 -163 177 -170 -177
		mu 0 4 220 221 222 223
		f 4 -171 -178 -162 178
		mu 0 4 224 222 221 225
		f 4 -179 -167 -176 -172
		mu 0 4 224 225 226 227
		f 4 179 180 181 182
		mu 0 4 228 229 230 231
		f 4 183 184 185 186
		mu 0 4 232 233 234 235
		f 4 187 188 189 190
		mu 0 4 236 237 238 239
		f 4 -189 191 -183 192
		mu 0 4 240 241 242 243
		f 4 -190 -193 -182 -186
		mu 0 4 239 238 231 230
		f 4 -192 -188 193 -180
		mu 0 4 228 244 245 229
		f 4 194 195 -184 196
		mu 0 4 246 247 233 232
		f 4 -194 197 -195 198
		mu 0 4 248 249 247 246
		f 4 -199 -197 -187 -181
		mu 0 4 229 250 251 230
		f 4 -198 -191 -185 -196
		mu 0 4 252 236 239 253
		f 4 199 200 201 202
		mu 0 4 254 255 256 257
		f 4 -200 203 204 205
		mu 0 4 255 258 259 260
		f 4 206 -205 207 208
		mu 0 4 261 260 262 263
		f 4 209 210 211 212
		mu 0 4 264 265 266 267
		f 4 -212 213 214 215
		mu 0 4 268 269 270 256
		f 4 -210 216 -206 217
		mu 0 4 271 272 255 260
		f 4 218 -202 -215 -209
		mu 0 4 273 274 256 270
		f 4 -208 -204 -203 -219
		mu 0 4 275 276 277 278
		f 4 -218 -207 -214 -211
		mu 0 4 271 260 261 279
		f 4 -201 -217 -213 -216
		mu 0 4 256 255 272 268
		f 4 219 220 221 222
		mu 0 4 280 281 282 283
		f 4 223 224 225 226
		mu 0 4 284 285 286 287
		f 4 227 -226 228 229
		mu 0 4 288 289 290 291
		f 4 230 231 232 -222
		mu 0 4 292 293 294 295
		f 4 -233 233 234 235
		mu 0 4 295 294 296 297
		f 4 -231 -221 -227 236
		mu 0 4 293 292 284 287
		f 4 237 238 239 -230
		mu 0 4 298 299 300 301
		f 4 -229 -225 240 -238
		mu 0 4 298 302 303 299
		f 3 241 242 -234
		mu 0 3 304 288 305
		f 4 -240 243 -235 -243
		mu 0 4 301 300 297 296
		f 3 -223 -236 -244
		mu 0 3 280 283 306
		f 4 -237 -228 -242 -232
		mu 0 4 307 289 288 304
		f 4 -224 -220 -239 -241
		mu 0 4 308 281 280 309
		f 4 244 245 246 247
		mu 0 4 310 311 312 313
		f 4 -245 248 249 250
		mu 0 4 311 314 315 316
		f 4 251 -250 252 253
		mu 0 4 317 316 318 319
		f 4 254 255 256 257
		mu 0 4 320 321 322 323
		f 4 -257 258 259 260
		mu 0 4 324 325 326 312
		f 4 -255 261 -251 262
		mu 0 4 327 328 311 316
		f 4 263 -247 -260 -254
		mu 0 4 329 330 312 326
		f 4 -253 -249 -248 -264
		mu 0 4 331 332 333 334
		f 4 -263 -252 -259 -256
		mu 0 4 327 316 317 335
		f 4 -246 -262 -258 -261
		mu 0 4 312 311 328 324
		f 4 264 265 266 267
		mu 0 4 336 337 338 339
		f 4 -265 268 269 270
		mu 0 4 340 341 342 343
		f 4 271 -270 272 273
		mu 0 4 344 343 345 346
		f 4 274 275 276 277
		mu 0 4 347 348 349 350
		f 4 -277 278 279 280
		mu 0 4 351 352 344 338
		f 4 -275 281 -271 282
		mu 0 4 353 354 340 343
		f 4 283 -267 -280 -274
		mu 0 4 355 356 338 344
		f 4 -273 -269 -268 -284
		mu 0 4 357 358 359 360
		f 4 -283 -272 -279 -276
		mu 0 4 353 343 344 352
		f 4 -266 -282 -278 -281
		mu 0 4 338 337 361 351
		f 4 284 285 286 287
		mu 0 4 362 363 364 365
		f 4 288 289 -285 290
		mu 0 4 366 367 368 369
		f 4 -289 291 292 293
		mu 0 4 370 371 372 373
		f 4 -293 294 -287 295
		mu 0 4 374 375 376 377
		f 4 -292 -291 -288 -295
		mu 0 4 378 379 362 365
		f 4 -290 -294 -296 -286
		mu 0 4 363 370 373 364
		f 4 296 297 298 299
		mu 0 4 380 381 382 383
		f 4 -299 300 301 302
		mu 0 4 384 385 386 387
		f 4 -302 303 304 305
		mu 0 4 388 389 390 391
		f 4 -297 306 -305 307
		mu 0 4 392 393 394 395
		f 4 -301 -298 -308 -304
		mu 0 4 389 382 381 390
		f 4 -300 -303 -306 -307
		mu 0 4 396 397 398 399
		f 4 308 309 310 311
		mu 0 4 400 401 402 403
		f 4 312 313 314 -312
		mu 0 4 404 405 406 407
		f 4 315 -314 316 317
		mu 0 4 408 409 410 411
		f 4 -318 318 -310 319
		mu 0 4 412 413 414 415
		f 4 -316 -320 -309 -315
		mu 0 4 416 417 418 419
		f 3 320 321 322
		mu 0 3 420 421 422
		f 4 323 324 325 326
		mu 0 4 423 424 425 426
		f 4 -321 327 328 329
		mu 0 4 427 428 429 430
		f 4 330 -322 -330 331
		mu 0 4 431 432 433 434
		f 4 -328 332 333 -325
		mu 0 4 435 436 437 438
		f 4 334 335 336 337
		mu 0 4 439 440 441 442
		f 4 338 339 340 341
		mu 0 4 443 444 445 446
		f 4 -331 342 -341 343
		mu 0 4 432 431 447 448
		f 3 -327 -342 -343
		mu 0 3 423 426 449
		f 3 344 -344 -340
		mu 0 3 450 422 451
		f 4 -323 -345 345 -333
		mu 0 4 420 422 450 452
		f 3 -324 -332 -329
		mu 0 3 424 423 453
		f 4 -334 346 347 348
		mu 0 4 454 455 456 457
		f 4 -346 349 350 -347
		mu 0 4 455 458 459 456
		f 4 -339 351 352 -350
		mu 0 4 458 460 461 459
		f 4 -326 -349 353 -352
		mu 0 4 460 454 457 461
		f 4 -348 354 355 356
		mu 0 4 462 463 464 465
		f 4 -351 357 358 -355
		mu 0 4 466 467 468 469
		f 4 -353 359 360 -358
		mu 0 4 470 471 472 473
		f 4 -354 -357 361 -360
		mu 0 4 474 475 476 477
		f 4 -356 362 363 364
		mu 0 4 478 479 480 481
		f 4 -359 365 366 -363
		mu 0 4 479 482 483 480
		f 4 -361 367 368 -366
		mu 0 4 482 484 485 483
		f 4 -362 -365 369 -368
		mu 0 4 484 478 481 485
		f 4 -364 370 371 372
		mu 0 4 486 487 488 489
		f 4 -367 373 374 -371
		mu 0 4 490 491 492 493
		f 4 -369 375 376 -374
		mu 0 4 494 495 496 497
		f 4 -370 -373 377 -376
		mu 0 4 498 499 500 501
		f 4 378 379 380 381
		mu 0 4 502 503 504 505
		f 4 382 383 384 -380
		mu 0 4 506 507 508 509
		f 4 385 386 -337 -384
		mu 0 4 510 511 512 513
		f 4 387 -382 388 -387
		mu 0 4 514 515 516 517
		f 3 -389 389 -338
		mu 0 3 517 516 518
		f 3 -385 -336 390
		mu 0 3 509 508 519
		f 4 391 392 393 394
		mu 0 4 520 521 522 523
		f 4 -381 395 -392 396
		mu 0 4 524 525 526 527
		f 4 -391 397 -393 -396
		mu 0 4 509 519 528 529
		f 4 -335 398 -394 -398
		mu 0 4 440 439 530 531
		f 4 -390 -397 -395 -399
		mu 0 4 518 516 532 533
		f 4 -372 399 -379 400
		mu 0 4 534 535 536 537
		f 4 -378 -401 -388 401
		mu 0 4 501 500 538 539
		f 4 -377 -402 -386 402
		mu 0 4 497 496 540 541
		f 4 -375 -403 -383 -400
		mu 0 4 493 492 542 543
		f 4 403 -95 404 -94
		mu 0 4 112 545 544 105
		f 4 -89 -404 -93 405
		mu 0 4 546 545 112 113
		f 4 -96 -406 -92 406
		mu 0 4 548 547 119 104
		f 4 -83 -407 -91 -405
		mu 0 4 544 548 104 105;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "SF_Character_KnightShapeOrig1" -p "SF_Character_Knight";
	rename -uid "49159998-4020-0DB7-4B42-2F9FA9643EBE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 569 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.52595329 0.009119451 0.52888548
		 0.0091194808 0.52888554 0.023000911 0.52595329 0.023000896 0.52788877 0.10354723
		 0.69263738 0.10354722 0.69263738 0.027187169 0.52788877 0.027187265 0.69263738 0.027187169
		 0.52788877 0.027187265 0.52788877 0.002524921 0.69263738 0.0025249124 0.51763391
		 0.027187288 0.51763397 0.10354722 0.3910687 0.10354724 0.39106876 0.027187273 0.51763397
		 0.10354722 0.3910687 0.10354724 0.3910687 0.027187273 0.51763397 0.027187288 0.61649823
		 0.03776364 0.61649823 0.038061202 0.6048941 0.038061142 0.60489416 0.03776364 0.68352848
		 0.023959236 0.53699768 0.023959314 0.53699768 0.0037033679 0.68352848 0.00370336
		 0.68352848 0.023959236 0.53699768 0.023959314 0.53699768 0.0037033679 0.68352848
		 0.00370336 0.8872174 0.097235411 0.88721144 0.066267401 0.8373186 0.066277578 0.83732462
		 0.097244605 0.79654467 0.097248614 0.79654711 0.11242311 0.83732516 0.1124191 0.74665195
		 0.097248599 0.75695574 0.10839213 0.78623968 0.10839216 0.94545949 0.068147957 0.89556658
		 0.068147928 0.89556658 0.10892193 0.94545949 0.10892196 0.7058773 0.066279262 0.70587218
		 0.097245306 0.74665153 0.066281572 0.79654461 0.066281602 0.74665016 0.11242309 0.70587307
		 0.11242028 0.95244753 0.068375349 0.95244849 0.1091482 0.98173153 0.10914826 0.98173153
		 0.068375379 0.84763175 0.1083861 0.87691486 0.10838042 0.76177341 0.22212903 0.76177341
		 0.12175304 0.70488745 0.12175304 0.70488745 0.22212903 0.91163653 0.22212903 0.86514753
		 0.22212903 0.86514753 0.23943304 0.91163653 0.23943304 0.86514753 0.12175405 0.80826145
		 0.12175405 0.80826241 0.22212903 0.98141414 0.12570532 0.92452914 0.12570532 0.92452818
		 0.17219333 0.98141414 0.17219333 0.91163653 0.12175405 0.80826241 0.23943304 0.76177341
		 0.23943304 0.93627733 0.18610452 0.93627733 0.23259251 0.9696663 0.23259251 0.96966535
		 0.18610452 0.82001048 0.234834 0.8533985 0.234834 0.71663648 0.234834 0.7500245 0.234834
		 0.86564159 0.61792421 0.86564147 0.5038842 0.80465484 0.5038842 0.80465472 0.61792421
		 0.92583352 0.56647146 0.92583358 0.61771244 0.9867183 0.61771238 0.98671818 0.56647146
		 0.75341368 0.61792421 0.75341368 0.5038842 0.69237161 0.5038842 0.69237161 0.61792421
		 0.98580587 0.55589259 0.98580587 0.50465178 0.92460883 0.50465167 0.92460877 0.55589247
		 0.9168824 0.5038842 0.9168824 0.61792421 0.31405377 0.80719042 0.1998038 0.80719042
		 0.1998038 0.60584378 0.31405377 0.60584378 0.75255662 0.62826431 0.86680669 0.62826431
		 0.86680675 0.80612814 0.75255758 0.80612814 0.60616684 0.80719042 0.49191785 0.80719042
		 0.49191785 0.60584378 0.60616684 0.60584378 0.62548518 0.62793285 0.73973405 0.62793285
		 0.73973411 0.8057968 0.62548524 0.8057968 0.021939829 0.80719042 0.021939829 0.60584378
		 0.26406899 0.36916786 0.328567 0.36916786 0.328567 0.45160288 0.26406899 0.45160288
		 0.39202014 0.42706543 0.39202014 0.44318974 0.33627918 0.44318974 0.33627918 0.42706546
		 0.20832804 0.45160288 0.14383003 0.45160288 0.14383003 0.36916786 0.20832804 0.36916786
		 0.023792118 0.37431324 0.07953307 0.37431324 0.07953307 0.43881124 0.023792118 0.43881124
		 0.088089019 0.45160288 0.088089019 0.36916786 0.39202014 0.39481696 0.33627918 0.39481696
		 0.39202014 0.37869275 0.33627918 0.37869275 0.31244248 0.45160288 0.28019351 0.45160288
		 0.19220354 0.45160288 0.15995453 0.45160288 0.18585658 0.092635326 0.18585658 0.013842285
		 0.12690163 0.013841331 0.12690163 0.092635326 0.076605678 0.11023831 0.1269176 0.1102333
		 0.076608658 0.092635326 0.017639637 0.092635326 0.029821634 0.10534327 0.064437628
		 0.10534929 0.24555188 0.014648795 0.2455669 0.064968824 0.30452186 0.064969778 0.30453092
		 0.014646769 0.23616362 0.092635326 0.23616362 0.013842285 0.076608658 0.013841331
		 0.1858356 0.11024027 0.23615265 0.11024027 0.29759133 0.074226558 0.24727345 0.074238256
		 0.24727178 0.10885536 0.29759097 0.10884757 0.017639637 0.013841331 0.13907063 0.10535245
		 0.1736877 0.10535412 0.34637493 0.34504279 0.32591394 0.31407481 0.39626792 0.3140758
		 0.39626798 0.34504282 0.21906126 0.31082222 0.21906123 0.35160118 0.23423618 0.35160017
		 0.23423624 0.3108232 0.31445688 0.31271869 0.24410288 0.31271774 0.26456386 0.34368578
		 0.31445685 0.34368569 0.18809523 0.31082422 0.11774124 0.31082517 0.11774124 0.3515982
		 0.18809524 0.35159922 0.086773261 0.31082121 0.086773261 0.35160324 0.051232263 0.35161519
		 0.066352248 0.35161322 0.066352248 0.31081024 0.051232249 0.31080824 0.021989226
		 0.31081617 0.021989226 0.35160723 0.2441029 0.34368569 0.32591394 0.34504277 0.27486789
		 0.35482872 0.30415288 0.35482869 0.38596395 0.35618576 0.35667992 0.35618582 0.37509015
		 0.59859747 0.32266086 0.59861594 0.29644424 0.58693701 0.29643971 0.56081003 0.40129131
		 0.56077391 0.40130061 0.58690041 0.028354168 0.56079191 0.13320708 0.56081003 0.13320482
		 0.58693701 0.10698978 0.59862053 0.054561727 0.59861147 0.028349698 0.58691889 0.87788939
		 0.80596268 0.87788939 0.64272672 0.98274338 0.6427266 0.98274338 0.80596358 0.5645377
		 0.58684087 0.56452817 0.56071389 0.98274338 0.80596358 0.95653003 0.80596352 0.95653003
		 0.64272678 0.98274338 0.6427266 0.90410316 0.64272672 0.90410316 0.80596298 0.87788939
		 0.80596268 0.87788939 0.64272672 0.26998273 0.46517903 0.26998255 0.54761404 0.20548463
		 0.54761404 0.20548463 0.465179 0.020982057 0.52073479 0.076723009 0.52073479 0.076723009
		 0.53685915 0.020982057 0.53685915 0.085245609 0.54761404 0.085245609 0.465179 0.14974362
		 0.465179 0.14974362 0.54761404 0.39248851 0.47594455 0.39248851 0.54044253 0.33674756
		 0.54044253 0.33674756 0.47594455 0.32572374 0.46517926 0.32572335 0.54761428 0.020982057
		 0.48848635 0.076723009 0.48848635 0.020982057 0.47236216 0.076723009 0.47236216;
	setAttr ".uvst[0].uvsp[250:499]" 0.25385812 0.54761404 0.22160915 0.54761404
		 0.10137013 0.54761404 0.13361913 0.54761404 0.14388275 0.22425914 0.13170278 0.21155417
		 0.19068077 0.21155417 0.1785008 0.22425914 0.13170075 0.22915113 0.081373811 0.22915113
		 0.081372738 0.2115531 0.022391677 0.21155417 0.069191813 0.22425914 0.034573674 0.22425914
		 0.25054735 0.18157533 0.2505483 0.1312553 0.30952936 0.1312553 0.30952936 0.18157533
		 0.1906828 0.13277116 0.24100769 0.13277224 0.24101269 0.2115531 0.081374764 0.13277116
		 0.13170075 0.13277116 0.24100959 0.22915113 0.1906828 0.22915113 0.30371839 0.19463784
		 0.30371842 0.22925681 0.25339839 0.22925681 0.25339836 0.19463876 0.022391677 0.13277021
		 0.34698552 0.28439239 0.39687848 0.28439239 0.39687854 0.25342345 0.3265245 0.25342238
		 0.21805179 0.25098187 0.23322678 0.25098288 0.23322678 0.29175988 0.21805179 0.29176086
		 0.26193443 0.28546637 0.31182739 0.28546637 0.3015224 0.29660839 0.2722384 0.29660839
		 0.18708587 0.2509838 0.18708581 0.29175884 0.11673179 0.29175788 0.11673179 0.25098485
		 0.085763842 0.29176283 0.085762829 0.25098085 0.050222844 0.29177588 0.050222874
		 0.2509678 0.065342814 0.25096977 0.065342814 0.29177386 0.020979822 0.29176688 0.020979822
		 0.25097686 0.24147339 0.25449741 0.24147339 0.28546637 0.3265245 0.28439242 0.31182739
		 0.25449836 0.38657454 0.29553545 0.35729051 0.29553542 0.80417323 0.051938251 0.79386842
		 0.040795475 0.84376168 0.040795222 0.83345711 0.051938221 0.79386389 0.055969719
		 0.75308597 0.055962224 0.75308776 0.040787727 0.70319474 0.040774167 0.74278069 0.051928237
		 0.71349561 0.051920027 0.89161509 0.052468069 0.89161509 0.011694044 0.94150811 0.011694074
		 0.94150811 0.052468039 0.84376252 0.0098281726 0.88453686 0.0098271295 0.88454139
		 0.040792093 0.75309646 0.0098207071 0.79386973 0.0098279566 0.88454115 0.055967141
		 0.84376419 0.055969168 0.979092 0.052039444 0.949808 0.052039444 0.94980812 0.011265457
		 0.979092 0.011265427 0.70320344 0.0098061934 0.71892065 0.36024863 0.70717162 0.34754461
		 0.76405853 0.34754354 0.75230962 0.36024755 0.91392362 0.3475396 0.91392457 0.36484355
		 0.86743557 0.36484462 0.86743462 0.34754163 0.81054753 0.34754258 0.85568464 0.3602466
		 0.82229656 0.3602466 0.98199046 0.25453752 0.98199046 0.30102554 0.92510438 0.30102554
		 0.92510545 0.25453752 0.76405853 0.24716769 0.81054753 0.24716668 0.86743367 0.24716668
		 0.91392267 0.24716471 0.81054753 0.36484557 0.7640596 0.36484665 0.96853173 0.36227971
		 0.93514359 0.36227947 0.93514335 0.31579143 0.96853185 0.31579149 0.70717257 0.2471687
		 0.86454749 0.49183792 0.80350649 0.49183792 0.80350649 0.37779593 0.86454749 0.37779593
		 0.92374724 0.43960747 0.98478818 0.4396075 0.984788 0.49084851 0.92374706 0.49084854
		 0.75226557 0.49183691 0.69122446 0.49183691 0.69122446 0.37779593 0.75226557 0.37779593
		 0.92226082 0.37751818 0.98330188 0.37751818 0.9833017 0.42875916 0.92226076 0.42875922
		 0.91578853 0.37779593 0.91578853 0.49183792 0.50111896 0.82511163 0.34220302 0.82511163
		 0.34220296 0.98523957 0.50111896 0.98523957 0.65991461 0.98523951 0.50366747 0.98523957
		 0.50366759 0.82511163 0.65991473 0.82511157 0.021948159 0.98523957 0.18207607 0.98523957
		 0.18207589 0.82511163 0.021947801 0.82511163 0.98317897 0.98523951 0.82305217 0.98523962
		 0.82305217 0.82511163 0.98317897 0.82511163 0.66269046 0.82487792 0.66269052 0.98500603
		 0.82281846 0.98500592 0.8228184 0.82487792 0.42867175 0.87296391 0.48500586 0.87296391
		 0.48500586 0.88704741 0.42867175 0.88704741 0.2851139 0.9859814 0.22877975 0.9859814
		 0.22877975 0.98001057 0.2851139 0.98001057 0.42867175 0.87296391 0.48500583 0.87296391
		 0.4850058 0.88704741 0.42867175 0.88704741 0.28579479 0.98532611 0.28579479 0.98136085
		 0.22818796 0.98136085 0.22818796 0.98532611 0.61663389 0.86576879 0.61663389 0.94098657
		 0.54141611 0.94098657 0.54141611 0.86576879 0.48333952 0.067262962 0.48333952 0.077537775
		 0.47610143 0.077537775 0.4803991 0.077457964 0.47316101 0.067183167 0.48327461 0.067183167
		 0.48327461 0.081263021 0.54825276 0.0018612146 0.69211119 0.0018613338 0.69211119
		 0.025153548 0.54825276 0.025153548 0.66912723 0.025153548 0.66912723 0.0018613338
		 0.61311609 0.0018613338 0.61311603 0.025153548 0.48897454 0.65691173 0.31561136 0.65691173
		 0.31561136 0.6480599 0.48897505 0.6480599 0.44786635 0.21590528 0.44412491 0.21590528
		 0.44412491 0.21180077 0.44786635 0.21180077 0.55696315 0.075282559 0.53307277 0.075282551
		 0.53307277 0.070178241 0.55696315 0.070178248 0.69184703 0.025153548 0.69184715 0.0018613338
		 0.4803991 0.081263021 0.47322598 0.081342831 0.47610143 0.081342831 0.47322598 0.067262962
		 0.47316101 0.077457964 0.59676081 0.096614614 0.57989448 0.096614614 0.58204466 0.09653838
		 0.59461057 0.09653838 0.57989448 0.096016638 0.5820446 0.096092865 0.59676081 0.096016638
		 0.59461063 0.096092865 0.48887366 0.65734553 0.31561136 0.65734553 0.31561136 0.65089691
		 0.4888733 0.65089691 0.48980582 0.093737267 0.47368824 0.093737267 0.47368824 0.090860121
		 0.48980582 0.090860121 0.53683364 0.082218461 0.5522396 0.082218491 0.55223966 0.085095637
		 0.53683364 0.085095577 0.47296739 0.091019236 0.48908496 0.091019206 0.48908496 0.093896382
		 0.47296751 0.093896352 0.59375411 0.097159684 0.58313447 0.097159684 0.58611184 0.096593499
		 0.59077674 0.096593499 0.58313447 0.095140278 0.58611184 0.095706463 0.59375411 0.095140278
		 0.59077674 0.095706463 0.4890672 0.65808368 0.31561136 0.65808368 0.31561136 0.65070832
		 0.48906675 0.65070832 0.48357451 0.093412869 0.47649455 0.093412869 0.47649455 0.092369072
		 0.48357451 0.092369072 0.53896981 0.082270704 0.54573745 0.082270704 0.54573739 0.083314501
		 0.53896981 0.083314501 0.47612011 0.088257901 0.48320007 0.088257901;
	setAttr ".uvst[0].uvsp[500:568]" 0.48320016 0.089301728 0.47612011 0.089301728
		 0.40037841 0.17645644 0.40037841 0.18372101 0.39701682 0.18372101 0.39701682 0.17645644
		 0.40172654 0.13244388 0.40172654 0.19714332 0.42293179 0.19714332 0.44458517 0.13244388
		 0.44793883 0.21264906 0.4516803 0.21264906 0.4516803 0.21695966 0.44793883 0.21695966
		 0.40172654 0.19714332 0.40172654 0.13244396 0.44431603 0.13244388 0.42266262 0.19714332
		 0.44821995 0.1971433 0.4484891 0.1971433 0.39861697 0.1900253 0.39919031 0.2034983
		 0.38880572 0.20395684 0.38823241 0.19048387 0.3896791 0.17646426 0.39694366 0.17646426
		 0.39694366 0.1872499 0.3896791 0.1872499 0.49105167 0.1971433 0.49105167 0.1324439
		 0.44786635 0.22087002 0.44412491 0.22087002 0.49105167 0.1324439 0.49105167 0.1971433
		 0.38552362 0.068695322 0.38335952 0.068695322 0.38335952 0.067651495 0.38552359 0.067651495
		 0.48320025 0.090345554 0.47612011 0.090345554 0.54573739 0.084358297 0.53896981 0.084358297
		 0.47649455 0.091325276 0.48357451 0.091325276 0.31405377 0.7065171 0.49191785 0.7065171
		 0.60616684 0.7065171 0.021939829 0.7065171 0.1998038 0.7065171 0.14950433 0.29175833
		 0.27424592 0.25449786 0.35929704 0.25342286 0.14950436 0.25098437 0.37022683 0.28439239
		 0.036600776 0.25097203 0.37093168 0.29553545 0.28587955 0.29660839 0.036600761 0.29177168
		 0.28517574 0.28546637 0.27682227 0.31271815 0.1504606 0.35159868 0.28776747 0.34368575
		 0.037632272 0.3516115 0.28848737 0.35482872 0.037632264 0.31081194 0.37029898 0.35618579
		 0.36957857 0.34504282 0.35863328 0.31407529 0.1504606 0.31082472;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".clst[0].clsn" -type "string" "colorSet0";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 250 ".pt";
	setAttr ".pt[0:165]" -type "float3"  -1.1151974 161.18007 9.5367432e-007 
		-1.1151974 161.18005 2.3841858e-007 -1.1151974 161.18005 2.3841858e-007 -1.1151974 
		161.18007 9.5367432e-007 -1.1151974 161.18005 -1.0490417e-005 -1.1151974 161.18007 
		-1.5258789e-005 -1.1151974 161.18007 5.1259995e-006 -1.1151974 161.18007 -2.3841858e-007 
		-1.1151974 161.18005 -3.8146973e-006 -1.1151974 161.18005 -3.8146973e-006 -1.1151974 
		161.18008 4.6491623e-006 -1.1151974 161.18008 6.0796738e-006 -1.1151974 161.18007 
		2.8610229e-006 -1.1151974 161.18007 2.8610229e-006 -1.1151974 161.18007 3.8146973e-006 
		-1.1151974 161.18007 4.7683716e-007 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0;
	setAttr ".pt[166:249]" -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 
		0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 
		161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 -1.1151974 161.18007 0 
		-1.1151974 161.18007 -2.1412852 -1.1151974 161.18007 -2.1412852 -1.1151974 161.18007 
		1.6082315 -1.1151974 161.18007 3.6253352 -1.1151974 161.18007 3.6253352 -1.1151974 
		161.18007 1.6082315 -1.1151974 161.18007 -2.1760216 -1.1151974 161.18007 1.6082315 
		-1.1151974 161.18007 3.6253352 -1.1151974 161.18007 3.6253352 -1.1151974 161.18007 
		1.6082315 -1.1151974 161.18007 -2.1760216;
	setAttr -s 250 ".vt";
	setAttr ".vt[0:165]"  -46.13156891 49.67648315 -2.33335781 -46.13156891 81.32736206 -34.7646904
		 46.13195038 81.32736206 -34.7646904 46.13195038 49.67648315 -2.33335781 -46.13160324 135.86730957 18.4627552
		 46.13198471 135.86730957 18.46274567 -46.13160324 104.21646118 50.89408875 46.13198471 104.21643066 50.89408112
		 -38.14547348 54.39732361 2.27388144 38.14585495 54.39732361 2.27388144 38.14587784 99.49560547 46.28685379
		 -38.14549637 99.49560547 46.2868576 -38.14547348 66.013458252 -9.62870693 38.14585495 66.013458252 -9.62870693
		 38.14587784 111.11175537 34.38426971 -38.14549637 111.11175537 34.38427353 -153.16995239 23.90899658 8.074157715
		 -153.16995239 23.90899658 -16.54467392 -168.45050049 23.90899658 -16.54467392 -168.45050049 23.90899658 8.074157715
		 -147.67254639 23.90908813 -11.45985794 -147.67254639 42.048019409 -11.45985413 -153.16995239 42.047958374 -16.54467773
		 -153.16995239 42.048339844 8.074157715 -147.67254639 42.047851563 2.98933029 -168.45053101 42.047958374 -16.54467773
		 -168.45053101 42.048339844 8.074157715 -147.67251587 23.90890503 2.98933411 -103.64163208 21.50933838 9.7993927
		 -103.64163208 21.50915527 -18.26991272 -153.16995239 21.50915527 -18.26991272 -153.16995239 21.50933838 9.7993927
		 -97.37417603 21.50933838 -12.47259903 -97.37417603 44.44741821 -12.47221375 -103.64163208 44.44761658 -18.26953125
		 -153.16995239 44.44761658 -18.26953125 -103.64163208 44.44741821 9.79901123 -153.16995239 44.44741821 9.79901123
		 -97.37417603 44.44799805 4.0016975403 -97.37418365 21.50915527 4.00207901 -47.37109375 20.33677673 10.82440758
		 -47.37109375 20.33657837 -19.29493332 -103.64161682 20.33657837 -19.29493332 -103.64161682 20.33677673 10.82440758
		 -47.37109375 45.62055969 10.82440758 -47.37109375 45.62055969 -19.2949276 -103.64163208 45.62052917 10.82440758
		 -103.64163208 45.62052917 -19.2949276 -43.88117218 -57.86965179 -32.42210388 -43.88117218 -57.86965179 23.95158386
		 -43.88117218 42.12882996 23.95158386 -43.88117218 42.12882996 -32.42210388 43.88152313 42.12882996 23.95158386
		 43.88152313 42.12882996 -32.42210388 43.88152313 -57.86965179 23.95158386 43.88152313 -57.86965179 -32.42210388
		 -35.71401215 -111.43650818 -20.37628174 -36.24720764 -111.43684387 11.44408226 -36.24710464 -70.76131439 11.44452095
		 -35.71390915 -70.7609787 -20.37584114 -8.7469902 -70.76139069 11.90532303 -8.21379662 -70.76105499 -19.91503906
		 -8.74709415 -111.43692017 11.90488434 -8.21390057 -111.43658447 -19.91547775 -8.61368084 -66.24171448 3.95028114
		 -36.11379242 -66.24163818 3.48947954 -8.34708309 -66.24154663 -11.9598999 -35.84719849 -66.24147034 -12.42070198
		 -9.79524803 -108.21308136 10.30463028 -9.82143879 -108.21337891 -18.79831314 -9.82134438 -147.088134766 -18.79790878
		 -9.79515362 -147.08782959 10.30503178 -34.64481735 -101.94515991 -12.76522636 -34.65031052 -108.21343994 -18.77606773
		 -9.81604481 -101.94509888 -12.78766441 -34.62430954 -108.21313477 10.32706738 -34.62982941 -101.94499207 4.31609535
		 -34.65021515 -147.088195801 -18.77566528 -34.62421417 -147.087890625 10.32747173
		 -9.80067348 -101.9449234 4.29384899 -12.16352463 -147.088012695 8.073966026 -12.16352367 -147.088012695 -16.544487
		 -12.16352367 -162.36856079 -16.544487 -12.16352272 -162.36856079 18.16989517 -12.16352367 -141.59057617 -11.45994854
		 -32.28223038 -141.59057617 -11.45985413 -32.28213501 -147.088012695 -16.54467773
		 -32.28213501 -162.36856079 -16.54467773 -32.28213501 -147.088012695 8.074157715 -32.28232574 -162.36856079 18.1697979
		 -32.28232574 -147.088012695 18.1697979 -12.16352272 -147.088012695 18.16989517 -32.28203964 -141.59057617 2.98933268
		 -12.16352367 -141.59057617 2.98942852 -40.27232742 -70.76194763 -30.10401917 -40.27232742 -70.76194763 21.63349915
		 -40.27232742 -57.87042236 21.63349915 -40.27232742 -57.87042236 -30.10401917 40.27267838 -57.87042236 -30.10401917
		 40.27267838 -57.87042236 21.63349915 40.27267838 -70.76194763 21.63349915 40.27267838 -70.76194763 -30.10401917
		 40.27267838 -52.10224152 -17.16963959 -40.27232742 -52.10224152 -17.16963959 40.27267838 -52.10224152 8.69911861
		 -40.27232742 -52.10224152 8.69911861 35.9782753 -111.43751526 -20.15147209 35.98643112 -111.43772888 11.6733551
		 35.98686218 -70.7621994 11.67362785 35.97870636 -70.76198578 -20.15119934 8.48279762 -70.76190948 11.68067932
		 8.47463799 -70.76169586 -20.14414787 8.48236561 -111.43743896 11.68040466 8.47420597 -111.43722534 -20.14442062
		 8.48080635 -66.24227142 3.72450304 35.98487473 -66.24256134 3.71745157 8.47672653 -66.24216461 -12.18791008
		 35.980793 -66.24245453 -12.19496155 9.7930975 -101.94523621 -12.75440311 9.78256893 -108.21424103 -18.76506805
		 9.83424854 -108.21405029 10.33783722 9.8232069 -101.94511414 4.32709122 34.62223053 -101.94497681 -12.79807186
		 34.6115036 -108.21398926 -18.80893135 34.66280365 -108.21380615 10.29417133 34.65195084 -101.94487 4.28323078
		 9.78296757 -147.088241577 -18.76481819 34.61190414 -147.087982178 -18.80867767 34.66320419 -147.087783813 10.2944231
		 9.83464718 -147.088043213 10.33808804 12.1634903 -147.088012695 8.073965073 12.16387558 -147.088012695 -16.544487
		 12.16387558 -162.36856079 -16.544487 12.16368103 -162.36856079 18.16970062 12.16387653 -141.59056091 -11.45994854
		 32.2826767 -141.59056091 -11.45985222 32.2826767 -147.088012695 -16.54467773 32.2826767 -147.088012695 8.074157715
		 32.2826767 -141.59056091 2.98933411 32.2826767 -162.36856079 -16.54467773 32.28286743 -162.36856079 18.1697979
		 32.28286743 -147.088012695 18.1697979 12.16368103 -147.088012695 18.16970062 12.16387653 -141.59056091 2.98942804
		 147.67175293 24.47471619 -11.45985413 153.17074585 24.4750824 -16.54429245 153.17074585 24.4750824 8.073773384
		 147.67178345 24.4750824 2.98933411 147.67175293 41.48165894 -11.45985413 153.17074585 41.48202515 -16.54467773
		 153.17074585 41.48202515 8.074157715 147.67178345 41.48127747 2.98933411 168.45053101 24.47509766 -16.54429245
		 168.45053101 41.48202515 -16.54467773 168.45053101 41.48202515 8.074157715 168.45053101 24.47509766 8.073773384
		 97.37419128 21.50856018 -12.47259903 103.64163971 21.50856018 -18.26991272 103.64161682 21.50856018 9.7993927
		 97.37420654 21.50856018 4.00207901 97.37420654 44.44778442 -12.47221756 103.64162445 44.44702148 -18.26953125
		 103.64162445 44.44702148 9.79901123 97.37419891 44.44702148 4.0016937256 153.17074585 21.5085907 -18.26991272
		 153.17074585 44.44702148 -18.26953125;
	setAttr ".vt[166:249]" 153.17074585 44.44702148 9.79901123 153.17074585 21.5085907 9.7993927
		 47.37102509 20.33628845 10.82441139 47.37102509 20.33628845 -19.29493141 103.64308929 20.33628845 -19.29493141
		 103.64308929 20.33628845 10.82441139 47.37102509 45.62008667 10.82441139 47.37102509 45.62008667 -19.29493141
		 103.64308929 45.62008667 10.82441139 103.64308929 45.62008667 -19.29493141 -35.55486679 41.75454712 -39.6269455
		 -35.55486679 41.75454712 31.48314285 -35.55486679 112.8638916 31.48314285 -35.55486679 112.8638916 -39.6269455
		 35.55521774 112.8638916 31.48313904 35.55521774 112.8638916 -39.6269455 35.55521774 41.75454712 31.48313904
		 35.55521774 41.75454712 -39.6269455 -27.72223282 112.8638916 -34.37233734 27.72258377 112.8638916 -34.37233734
		 -27.72223282 112.8638916 26.28855896 27.72258377 112.8638916 26.28853416 -27.72223282 120.32748413 -34.37233734
		 -27.72223282 120.32748413 26.28855896 27.72258377 120.32748413 26.28853416 27.72258377 120.32748413 -34.3723526
		 -41.63248444 50.77859497 -50.89408875 -41.63251495 50.77859497 12.67539597 -41.63248444 113.34909058 -50.89408875
		 41.63286591 113.34909058 -50.89408875 41.63289642 50.77859497 12.67538071 41.63286591 50.77859497 -50.89408875
		 -41.63251495 95.55929565 12.67539597 41.63289642 95.55929565 12.67538071 -41.63251495 113.34909058 36.21699524
		 41.63289642 113.34909058 36.21697998 -41.63251495 95.55929565 36.21699524 41.63289642 95.55929565 36.21697998
		 31.018013 113.34909058 -39.78898239 -31.017631531 113.34909058 -39.78898239 -31.017656326 113.34909058 25.11188889
		 31.018035889 113.34909058 25.11187744 31.018013 124.93432617 -39.78898239 -31.017631531 124.93432617 -39.78898239
		 -31.017656326 124.93432617 25.11188889 31.018035889 124.93432617 25.11187744 13.62550545 124.93432617 -21.59316635
		 -13.62512493 124.93432617 -21.59316635 -13.62512779 124.93432617 6.91607285 13.62550926 124.93432617 6.91607285
		 9.13198471 133.34042358 -18.71221161 -9.13160324 133.34042358 -18.71221161 -9.13160515 133.34042358 4.03512001
		 9.13198662 133.34042358 4.03512001 9.13198471 162.36856079 -21.59316635 -9.13160324 162.36856079 -21.59316635
		 -9.13160515 154.38320923 -1.55735588 9.13198662 154.38320923 -1.55735588 9.13198471 141.79052734 -18.71221161
		 -9.13160324 141.79052734 -18.71221161 -9.13160324 141.79052734 -45.82794189 9.13198471 141.79052734 -45.82794189
		 -9.13160324 155.85321045 -45.82794189 9.13198471 155.85321045 -45.82794189 -13.62512493 131.93759155 -21.59316635
		 13.62550545 131.93759155 -21.59316635 13.62550926 131.93759155 6.91607285 -13.62512779 131.93759155 6.91607285
		 -43.88117218 -7.8704071 23.95158386 43.88152313 -7.8704071 23.95158386 43.88152313 -7.8704071 -32.42210388
		 -43.88117218 -7.8704071 -32.42210388 32.28277969 -162.36856079 1.99898481 12.16377163 -162.36856079 1.99902105
		 12.16366959 -147.088012695 -3.39388514 12.16387653 -141.59056091 -3.74142885 32.2826767 -141.59056091 -3.74143434
		 32.2826767 -147.088012695 -3.39387083 -32.28223801 -162.36856079 2.025218964 -32.28213501 -147.088012695 -3.3752656
		 -32.28212738 -141.59057617 -3.7305162 -12.16352367 -141.59057617 -3.7305088 -12.16352463 -147.088012695 -3.37527943
		 -12.16352272 -162.36856079 2.025360107;
	setAttr -s 431 ".ed";
	setAttr ".ed[0:165]"  12 13 0 13 14 0 14 15 0 15 12 0 3 0 0 0 1 0 1 2 0 2 3 0
		 1 4 0 4 5 0 5 2 0 4 6 0 6 7 0 7 5 0 7 3 0 0 6 0 3 9 1 9 8 0 8 0 0 7 10 0 10 9 0 6 11 0
		 11 10 0 8 11 0 9 13 0 12 8 0 10 14 0 11 15 0 16 19 0 19 18 0 18 17 0 17 16 0 17 22 0
		 22 21 0 21 20 0 20 17 0 23 24 0 24 21 0 22 23 0 19 26 0 26 25 0 25 18 0 16 23 0 23 26 0
		 25 22 0 16 27 0 27 24 0 27 20 0 28 31 0 31 30 0 30 29 0 29 28 0 29 34 0 34 33 0 33 32 0
		 32 29 0 35 37 0 37 36 0 36 34 0 34 35 0 31 37 0 35 30 0 28 36 0 38 36 0 28 39 0 39 38 0
		 39 32 0 33 38 0 40 43 0 43 42 0 42 41 0 41 40 0 41 45 0 45 44 0 44 40 0 45 47 0 47 46 0
		 46 44 0 47 42 0 43 46 0 50 51 0 51 237 0 237 234 1 234 50 0 50 52 0 52 53 0 53 51 0
		 52 235 0 235 236 1 236 53 0 49 48 0 48 55 0 55 54 0 54 49 0 234 235 1 236 237 1 56 57 0
		 57 58 0 58 59 1 59 56 0 65 58 0 58 60 0 60 64 0 64 65 0 61 60 1 60 62 0 62 63 0 63 61 0
		 62 57 0 56 63 0 59 61 0 67 65 0 64 66 0 66 67 0 59 67 0 66 61 0 68 71 0 71 70 0 70 69 0
		 69 68 1 72 74 0 74 69 0 69 73 0 73 72 0 75 76 0 76 72 0 73 75 0 77 70 0 71 78 0 78 77 0
		 68 75 0 75 78 0 77 73 0 68 79 0 79 76 0 79 74 0 80 83 1 83 249 0 82 81 0 81 248 1
		 81 86 0 86 85 0 85 84 0 84 81 0 87 244 0 89 88 0 88 245 0 86 87 0 83 89 0 87 82 0
		 83 91 0 91 90 0 90 89 0 92 88 0 88 80 0 80 93 0 93 92 0 93 247 0 85 246 0 90 88 0
		 91 80 0 105 103 0 103 97 0 97 94 0 94 95 0 95 96 0;
	setAttr ".ed[166:331]" 96 105 0 100 101 0 101 98 0 98 102 0 102 104 0 104 99 0
		 99 100 0 94 101 0 100 95 0 99 96 0 97 98 0 103 102 0 105 104 0 106 109 0 109 108 0
		 108 107 0 107 106 0 115 114 0 114 110 0 110 108 0 108 115 0 111 113 0 113 112 0 112 110 0
		 110 111 1 113 106 0 107 112 0 111 109 0 117 116 0 116 114 0 115 117 0 111 116 0 117 109 0
		 118 119 0 119 120 0 120 121 0 121 118 0 118 122 0 122 123 0 123 119 0 124 123 0 122 125 0
		 125 124 0 126 127 0 127 128 0 128 129 0 129 126 0 128 124 0 124 120 0 120 129 0 126 119 0
		 123 127 0 125 121 0 130 240 0 131 132 0 132 239 0 133 130 0 131 134 0 134 135 0 135 136 0
		 136 131 0 137 243 0 135 242 0 138 137 0 132 139 0 139 238 0 140 133 0 140 141 0 141 142 0
		 142 133 0 136 139 0 138 143 0 143 130 0 130 137 0 134 241 0 140 137 0 137 141 0 130 142 0
		 144 145 0 145 146 0 146 147 0 147 144 0 144 148 0 148 149 0 149 145 0 150 149 0 148 151 0
		 151 150 0 152 153 0 153 154 0 154 155 0 155 152 0 154 150 0 150 146 0 146 155 0 152 145 0
		 149 153 0 151 147 0 156 157 0 157 158 1 158 159 0 159 156 0 156 160 0 160 161 0 161 157 0
		 162 161 0 160 163 0 163 162 0 164 165 0 165 166 0 166 167 0 167 164 0 166 162 0 162 158 0
		 158 167 0 164 157 0 161 165 0 163 159 0 168 169 0 169 170 0 170 171 0 171 168 0 172 173 0
		 173 169 0 168 172 0 172 174 0 174 175 0 175 173 0 174 171 0 170 175 0 176 177 0 177 178 0
		 178 179 0 179 176 0 178 180 0 180 181 0 181 179 0 180 182 0 182 183 0 183 181 0 176 183 0
		 182 177 0 189 188 0 188 184 0 184 186 0 186 189 0 186 187 0 187 190 0 190 189 0 191 190 0
		 187 185 0 185 191 0 185 184 0 188 191 0 192 193 0 193 198 0 198 192 1 199 197 1 197 195 0
		 195 201 0 201 199 1 192 197 0 197 196 0 196 193 0 199 198 0 196 199 0;
	setAttr ".ed[332:430]" 192 194 0 194 195 0 220 221 0 221 222 0 222 223 0 223 220 0
		 201 200 0 200 202 0 202 203 0 203 201 0 199 203 0 202 198 0 200 198 1 200 194 0 194 205 0
		 205 204 0 204 195 0 200 206 0 206 205 0 201 207 0 207 206 0 204 207 0 205 209 0 209 208 0
		 208 204 0 206 210 0 210 209 0 207 211 0 211 210 0 208 211 0 209 213 0 213 212 0 212 208 0
		 210 214 0 214 213 0 211 215 0 215 214 0 212 215 0 213 230 0 230 231 0 231 212 0 214 233 0
		 233 230 0 215 232 0 232 233 0 231 232 0 216 217 0 217 225 0 225 224 0 224 216 0 217 218 0
		 218 222 0 222 225 1 218 219 0 219 223 0 219 216 0 224 223 1 224 220 1 221 225 1 227 226 0
		 226 228 0 228 229 0 229 227 0 225 226 0 227 224 0 221 228 0 220 229 0 230 217 0 216 231 0
		 219 232 0 218 233 0 54 235 0 234 49 0 55 236 0 48 237 0 238 140 0 239 133 0 238 239 1
		 240 131 0 239 240 1 241 143 0 240 241 1 242 138 0 241 242 1 243 136 0 242 243 1 243 238 1
		 244 89 0 245 86 0 244 245 1 246 92 0 245 246 1 247 84 0 246 247 1 248 80 1 247 248 1
		 249 82 0 248 249 1 249 244 1;
	setAttr -s 216 -ch 858 ".fc[0:215]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 28 29 30 31
		f 4 4 5 6 7
		mu 0 4 3 0 1 2
		f 4 -7 8 9 10
		mu 0 4 20 21 22 23
		f 4 -10 11 12 13
		mu 0 4 4 5 6 7
		f 4 -14 14 -8 -11
		mu 0 4 12 13 14 15
		f 4 -9 -6 15 -12
		mu 0 4 16 17 18 19
		f 4 -5 16 17 18
		mu 0 4 8 9 25 24
		f 4 -15 19 20 -17
		mu 0 4 9 10 26 25
		f 4 -13 21 22 -20
		mu 0 4 10 11 27 26
		f 4 -16 -19 23 -22
		mu 0 4 11 8 24 27
		f 4 -18 24 -1 25
		mu 0 4 24 25 29 28
		f 4 -21 26 -2 -25
		mu 0 4 25 26 30 29
		f 4 -23 27 -3 -27
		mu 0 4 26 27 31 30
		f 4 -24 -26 -4 -28
		mu 0 4 27 24 28 31
		f 4 28 29 30 31
		mu 0 4 32 33 34 35
		f 4 32 33 34 35
		mu 0 4 35 36 37 38
		f 4 36 37 -34 38
		mu 0 4 39 40 41 36
		f 4 -30 39 40 41
		mu 0 4 42 43 44 45
		f 4 -29 42 43 -40
		mu 0 4 46 47 39 48
		f 4 44 -33 -31 -42
		mu 0 4 49 36 35 34
		f 4 -37 -43 45 46
		mu 0 4 50 39 47 51
		f 4 -47 47 -35 -38
		mu 0 4 52 53 54 55
		f 4 -41 -44 -39 -45
		mu 0 4 49 48 39 36
		f 4 -48 -46 -32 -36
		mu 0 4 56 57 32 35
		f 4 48 49 50 51
		mu 0 4 58 59 60 61
		f 4 52 53 54 55
		mu 0 4 62 63 64 65
		f 4 56 57 58 59
		mu 0 4 66 67 68 63
		f 4 -50 60 -57 61
		mu 0 4 69 70 71 72
		f 4 -49 62 -58 -61
		mu 0 4 59 58 68 67
		f 4 -60 -53 -51 -62
		mu 0 4 66 63 62 73
		f 4 63 -63 64 65
		mu 0 4 74 68 58 75
		f 4 -66 66 -55 67
		mu 0 4 76 77 78 79
		f 4 -64 -68 -54 -59
		mu 0 4 68 80 81 63
		f 4 -67 -65 -52 -56
		mu 0 4 82 83 58 61
		f 4 68 69 70 71
		mu 0 4 84 85 86 87
		f 4 72 73 74 -72
		mu 0 4 88 89 90 91
		f 4 75 76 77 -74
		mu 0 4 92 93 94 95
		f 4 78 -70 79 -77
		mu 0 4 96 97 98 99
		f 4 -80 -69 -75 -78
		mu 0 4 100 85 84 101
		f 4 -71 -79 -76 -73
		mu 0 4 87 86 93 92
		f 4 80 81 82 83
		mu 0 4 102 103 548 544
		f 4 -81 84 85 86
		mu 0 4 106 107 108 109
		f 4 -86 87 88 89
		mu 0 4 110 111 545 546
		f 4 90 91 92 93
		mu 0 4 114 115 116 117
		f 4 94 -88 -85 -84
		mu 0 4 544 545 111 102
		f 4 -87 -90 95 -82
		mu 0 4 103 118 547 548
		f 4 96 97 98 99
		mu 0 4 120 121 122 123
		f 4 100 101 102 103
		mu 0 4 124 125 126 127
		f 4 104 105 106 107
		mu 0 4 128 129 130 131
		f 4 108 -97 109 -107
		mu 0 4 132 133 134 135
		f 4 -102 -98 -109 -106
		mu 0 4 129 136 137 130
		f 4 -100 110 -108 -110
		mu 0 4 120 123 128 131
		f 4 111 -104 112 113
		mu 0 4 138 124 127 139
		f 4 114 -114 115 -111
		mu 0 4 140 138 139 141
		f 4 -99 -101 -112 -115
		mu 0 4 123 122 142 143
		f 4 -113 -103 -105 -116
		mu 0 4 144 145 129 128
		f 4 116 117 118 119
		mu 0 4 146 147 148 149
		f 4 120 121 122 123
		mu 0 4 150 151 149 152
		f 4 124 125 -124 126
		mu 0 4 153 154 155 152
		f 4 127 -118 128 129
		mu 0 4 156 157 158 159
		f 4 -117 130 131 -129
		mu 0 4 147 146 160 161
		f 4 -123 -119 -128 132
		mu 0 4 152 149 148 162
		f 4 -131 133 134 -125
		mu 0 4 160 146 163 164
		f 4 -135 135 -121 -126
		mu 0 4 165 166 167 168
		f 4 -130 -132 -127 -133
		mu 0 4 162 169 153 152
		f 4 -136 -134 -120 -122
		mu 0 4 170 171 146 149
		f 4 429 428 138 139
		mu 0 4 566 567 174 175
		f 4 140 141 142 143
		mu 0 4 176 177 178 179
		f 4 144 421 420 147
		mu 0 4 180 559 561 183
		f 4 -429 430 -145 149
		mu 0 4 184 568 560 187
		f 4 150 151 152 -149
		mu 0 4 185 188 189 186
		f 4 -148 -141 -139 -150
		mu 0 4 187 177 176 184
		f 4 153 154 155 156
		mu 0 4 190 191 192 193
		f 4 425 424 -143 158
		mu 0 4 562 564 194 195
		f 3 -153 159 -146
		mu 0 3 181 196 182
		f 4 -160 -152 160 -155
		mu 0 4 191 189 188 192
		f 3 -161 -151 -137
		mu 0 3 172 197 173
		f 4 423 -159 -142 -421
		mu 0 4 561 563 199 183
		f 4 -425 427 -140 -144
		mu 0 4 200 565 566 175
		f 6 161 162 163 164 165 166
		mu 0 6 202 203 204 205 206 207
		f 6 167 168 169 170 171 172
		mu 0 6 208 209 210 211 212 213
		f 4 173 -168 174 -165
		mu 0 4 214 215 216 217
		f 4 175 -166 -175 -173
		mu 0 4 218 207 206 219
		f 4 -164 176 -169 -174
		mu 0 4 205 204 210 209
		f 4 -163 177 -170 -177
		mu 0 4 220 221 222 223
		f 4 -171 -178 -162 178
		mu 0 4 224 222 221 225
		f 4 -179 -167 -176 -172
		mu 0 4 224 225 226 227
		f 4 179 180 181 182
		mu 0 4 228 229 230 231
		f 4 183 184 185 186
		mu 0 4 232 233 234 235
		f 4 187 188 189 190
		mu 0 4 236 237 238 239
		f 4 -189 191 -183 192
		mu 0 4 240 241 242 243
		f 4 -190 -193 -182 -186
		mu 0 4 239 238 231 230
		f 4 -192 -188 193 -180
		mu 0 4 228 244 245 229
		f 4 194 195 -184 196
		mu 0 4 246 247 233 232
		f 4 -194 197 -195 198
		mu 0 4 248 249 247 246
		f 4 -199 -197 -187 -181
		mu 0 4 229 250 251 230
		f 4 -198 -191 -185 -196
		mu 0 4 252 236 239 253
		f 4 199 200 201 202
		mu 0 4 254 255 256 257
		f 4 -200 203 204 205
		mu 0 4 255 258 259 260
		f 4 206 -205 207 208
		mu 0 4 261 260 262 263
		f 4 209 210 211 212
		mu 0 4 264 265 266 267
		f 4 -212 213 214 215
		mu 0 4 268 269 270 256
		f 4 -210 216 -206 217
		mu 0 4 271 272 255 260
		f 4 218 -202 -215 -209
		mu 0 4 273 274 256 270
		f 4 -208 -204 -203 -219
		mu 0 4 275 276 277 278
		f 4 -218 -207 -214 -211
		mu 0 4 271 260 261 279
		f 4 -201 -217 -213 -216
		mu 0 4 256 255 272 268
		f 4 410 220 221 411
		mu 0 4 553 281 282 551
		f 4 223 224 225 226
		mu 0 4 284 285 286 287
		f 4 416 -226 228 417
		mu 0 4 558 289 290 556
		f 4 230 231 409 -222
		mu 0 4 292 293 549 552
		f 4 -233 233 234 235
		mu 0 4 295 294 296 297
		f 4 -231 -221 -227 236
		mu 0 4 293 292 284 287
		f 4 237 238 239 -230
		mu 0 4 298 299 300 301
		f 4 -229 -225 240 415
		mu 0 4 557 302 303 554
		f 3 241 242 -234
		mu 0 3 304 288 305
		f 4 -240 243 -235 -243
		mu 0 4 301 300 297 296
		f 3 -223 -236 -244
		mu 0 3 280 283 306
		f 4 -237 -417 418 -232
		mu 0 4 307 289 558 550
		f 4 -224 -411 413 -241
		mu 0 4 308 281 553 555
		f 4 244 245 246 247
		mu 0 4 310 311 312 313
		f 4 -245 248 249 250
		mu 0 4 311 314 315 316
		f 4 251 -250 252 253
		mu 0 4 317 316 318 319
		f 4 254 255 256 257
		mu 0 4 320 321 322 323
		f 4 -257 258 259 260
		mu 0 4 324 325 326 312
		f 4 -255 261 -251 262
		mu 0 4 327 328 311 316
		f 4 263 -247 -260 -254
		mu 0 4 329 330 312 326
		f 4 -253 -249 -248 -264
		mu 0 4 331 332 333 334
		f 4 -263 -252 -259 -256
		mu 0 4 327 316 317 335
		f 4 -246 -262 -258 -261
		mu 0 4 312 311 328 324
		f 4 264 265 266 267
		mu 0 4 336 337 338 339
		f 4 -265 268 269 270
		mu 0 4 340 341 342 343
		f 4 271 -270 272 273
		mu 0 4 344 343 345 346
		f 4 274 275 276 277
		mu 0 4 347 348 349 350
		f 4 -277 278 279 280
		mu 0 4 351 352 344 338
		f 4 -275 281 -271 282
		mu 0 4 353 354 340 343
		f 4 283 -267 -280 -274
		mu 0 4 355 356 338 344
		f 4 -273 -269 -268 -284
		mu 0 4 357 358 359 360
		f 4 -283 -272 -279 -276
		mu 0 4 353 343 344 352
		f 4 -266 -282 -278 -281
		mu 0 4 338 337 361 351
		f 4 284 285 286 287
		mu 0 4 362 363 364 365
		f 4 288 289 -285 290
		mu 0 4 366 367 368 369
		f 4 -289 291 292 293
		mu 0 4 370 371 372 373
		f 4 -293 294 -287 295
		mu 0 4 374 375 376 377
		f 4 -292 -291 -288 -295
		mu 0 4 378 379 362 365
		f 4 -290 -294 -296 -286
		mu 0 4 363 370 373 364
		f 4 296 297 298 299
		mu 0 4 380 381 382 383
		f 4 -299 300 301 302
		mu 0 4 384 385 386 387
		f 4 -302 303 304 305
		mu 0 4 388 389 390 391
		f 4 -297 306 -305 307
		mu 0 4 392 393 394 395
		f 4 -301 -298 -308 -304
		mu 0 4 389 382 381 390
		f 4 -300 -303 -306 -307
		mu 0 4 396 397 398 399
		f 4 308 309 310 311
		mu 0 4 400 401 402 403
		f 4 312 313 314 -312
		mu 0 4 404 405 406 407
		f 4 315 -314 316 317
		mu 0 4 408 409 410 411
		f 4 -318 318 -310 319
		mu 0 4 412 413 414 415
		f 4 -316 -320 -309 -315
		mu 0 4 416 417 418 419
		f 3 320 321 322
		mu 0 3 420 421 422
		f 4 323 324 325 326
		mu 0 4 423 424 425 426
		f 4 -321 327 328 329
		mu 0 4 427 428 429 430
		f 4 330 -322 -330 331
		mu 0 4 431 432 433 434
		f 4 -328 332 333 -325
		mu 0 4 435 436 437 438
		f 4 334 335 336 337
		mu 0 4 439 440 441 442
		f 4 338 339 340 341
		mu 0 4 443 444 445 446
		f 4 -331 342 -341 343
		mu 0 4 432 431 447 448
		f 3 -327 -342 -343
		mu 0 3 423 426 449
		f 3 344 -344 -340
		mu 0 3 450 422 451
		f 4 -323 -345 345 -333
		mu 0 4 420 422 450 452
		f 3 -324 -332 -329
		mu 0 3 424 423 453
		f 4 -334 346 347 348
		mu 0 4 454 455 456 457
		f 4 -346 349 350 -347
		mu 0 4 455 458 459 456
		f 4 -339 351 352 -350
		mu 0 4 458 460 461 459
		f 4 -326 -349 353 -352
		mu 0 4 460 454 457 461
		f 4 -348 354 355 356
		mu 0 4 462 463 464 465
		f 4 -351 357 358 -355
		mu 0 4 466 467 468 469
		f 4 -353 359 360 -358
		mu 0 4 470 471 472 473
		f 4 -354 -357 361 -360
		mu 0 4 474 475 476 477
		f 4 -356 362 363 364
		mu 0 4 478 479 480 481
		f 4 -359 365 366 -363
		mu 0 4 479 482 483 480
		f 4 -361 367 368 -366
		mu 0 4 482 484 485 483
		f 4 -362 -365 369 -368
		mu 0 4 484 478 481 485
		f 4 -364 370 371 372
		mu 0 4 486 487 488 489
		f 4 -367 373 374 -371
		mu 0 4 490 491 492 493
		f 4 -369 375 376 -374
		mu 0 4 494 495 496 497
		f 4 -370 -373 377 -376
		mu 0 4 498 499 500 501
		f 4 378 379 380 381
		mu 0 4 502 503 504 505
		f 4 382 383 384 -380
		mu 0 4 506 507 508 509
		f 4 385 386 -337 -384
		mu 0 4 510 511 512 513
		f 4 387 -382 388 -387
		mu 0 4 514 515 516 517
		f 3 -389 389 -338
		mu 0 3 517 516 518
		f 3 -385 -336 390
		mu 0 3 509 508 519
		f 4 391 392 393 394
		mu 0 4 520 521 522 523
		f 4 -381 395 -392 396
		mu 0 4 524 525 526 527
		f 4 -391 397 -393 -396
		mu 0 4 509 519 528 529
		f 4 -335 398 -394 -398
		mu 0 4 440 439 530 531
		f 4 -390 -397 -395 -399
		mu 0 4 518 516 532 533
		f 4 -372 399 -379 400
		mu 0 4 534 535 536 537
		f 4 -378 -401 -388 401
		mu 0 4 501 500 538 539
		f 4 -377 -402 -386 402
		mu 0 4 497 496 540 541
		f 4 -375 -403 -383 -400
		mu 0 4 493 492 542 543
		f 4 403 -95 404 -94
		mu 0 4 112 545 544 105
		f 4 -89 -404 -93 405
		mu 0 4 546 545 112 113
		f 4 -96 -406 -92 406
		mu 0 4 548 547 119 104
		f 4 -83 -407 -91 -405
		mu 0 4 544 548 104 105
		f 4 -410 407 232 -409
		mu 0 4 552 549 294 295
		f 4 219 -412 408 222
		mu 0 4 280 553 551 283
		f 4 -414 -220 -239 -413
		mu 0 4 555 553 280 309
		f 4 -415 -416 412 -238
		mu 0 4 298 557 554 299
		f 4 227 -418 414 229
		mu 0 4 288 558 556 291
		f 4 -419 -228 -242 -408
		mu 0 4 550 558 288 304
		f 4 419 145 146 -422
		mu 0 4 559 181 182 561
		f 4 -154 -423 -424 -147
		mu 0 4 182 198 563 561
		f 4 -157 157 -426 422
		mu 0 4 190 193 564 562
		f 4 -428 -158 -156 -427
		mu 0 4 566 565 201 172
		f 4 136 137 -430 426
		mu 0 4 172 173 567 566
		f 4 -431 -138 148 -420
		mu 0 4 560 568 185 186;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "mainCtrl";
	rename -uid "C9B410F2-480E-9A2A-5F22-1581CACAD570";
createNode nurbsCurve -n "mainCtrlShape" -p "mainCtrl";
	rename -uid "FF6C8570-4A7B-C96D-B5EF-AD800B1069E5";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.73096171120628217 4.8572257327353668e-017 -1.1556699052575017
		-1.652944894054669e-013 6.765421556309385e-017 -0.92216504737125038
		-0.73096171120573461 4.8572257327349847e-017 -1.1556699052574391
		-1.3187938422952192 -1.6069959455967096e-030 1.3022039812819024
		0.0061370803860868383 -4.7704895589355299e-017 0.41155334452484077
		-7.0001668152551229e-014 -6.7654215563098324e-017 1.5261698431338915
		-0.0061370803859227925 -4.7704895589358306e-017 0.4115533445249252
		1.3187938422948291 -7.6135862561516208e-030 1.3022039812819479
		0.73096171120628217 4.8572257327353668e-017 -1.1556699052575017
		-1.652944894054669e-013 6.765421556309385e-017 -0.92216504737125038
		-0.73096171120573461 4.8572257327349847e-017 -1.1556699052574391
		;
createNode transform -n "R_ElbowPoleV" -p "mainCtrl";
	rename -uid "BF5EC217-4419-995D-DCDE-96A94B613112";
	setAttr ".rp" -type "double3" -1.1092213183224142 2.0817085698035438 -0.43415614858011992 ;
	setAttr ".sp" -type "double3" -1.1092213183224142 2.0817085698035438 -0.43415614858011992 ;
createNode mesh -n "R_ElbowPoleVShape" -p "R_ElbowPoleV";
	rename -uid "7691984B-47A2-B697-C52D-E0923EFD7C6C";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 23 ".uvst[0].uvsp[0:22]" -type "float2" 0 0.25 0.25 0.25
		 0.5 0.25 0.75 0.25 1 0.25 0 0.5 0.25 0.5 0.5 0.5 0.75 0.5 1 0.5 0 0.75 0.25 0.75
		 0.5 0.75 0.75 0.75 1 0.75 0.125 0 0.375 0 0.625 0 0.875 0 0.125 1 0.375 1 0.625 1
		 0.875 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  -1.1092215 2.7289526 0.21308804 
		-0.46197706 2.7289526 -0.43415609 -1.1092213 2.7289526 -1.0814003 -1.7564656 2.7289526 
		-0.43415615 -1.1092215 2.0817087 0.48118544 -0.19387984 2.0817087 -0.43415606 -1.1092212 
		2.0817087 -1.3494977 -2.0245628 2.0817087 -0.43415615 -1.1092215 1.4344643 0.21308804 
		-0.46197706 1.4344643 -0.43415609 -1.1092213 1.4344643 -1.0814003 -1.7564656 1.4344643 
		-0.43415615 -1.1092213 2.99705 -0.43415615 -1.1092213 1.1663671 -0.43415615;
	setAttr -s 14 ".vt[0:13]"  9.2725848e-008 -0.70710677 -0.70710677 -0.70710677 -0.70710677 -6.1817239e-008
		 -3.090862e-008 -0.70710677 0.70710677 0.70710677 -0.70710677 0 1.3113416e-007 0 -1
		 -1 0 -8.7422777e-008 -4.3711388e-008 0 1 1 0 0 9.2725848e-008 0.70710677 -0.70710677
		 -0.70710677 0.70710677 -6.1817239e-008 -3.090862e-008 0.70710677 0.70710677 0.70710677 0.70710677 0
		 0 -1 0 0 1 0;
	setAttr -s 28 ".ed[0:27]"  0 1 0 1 2 0 2 3 0 3 0 0 4 5 0 5 6 0 6 7 0
		 7 4 0 8 9 0 9 10 0 10 11 0 11 8 0 0 4 0 1 5 0 2 6 0 3 7 0 4 8 0 5 9 0 6 10 0 7 11 0
		 12 0 0 12 1 0 12 2 0 12 3 0 8 13 0 9 13 0 10 13 0 11 13 0;
	setAttr -s 16 -ch 56 ".fc[0:15]" -type "polyFaces" 
		f 4 0 13 -5 -13
		mu 0 4 0 1 6 5
		f 4 1 14 -6 -14
		mu 0 4 1 2 7 6
		f 4 2 15 -7 -15
		mu 0 4 2 3 8 7
		f 4 3 12 -8 -16
		mu 0 4 3 4 9 8
		f 4 4 17 -9 -17
		mu 0 4 5 6 11 10
		f 4 5 18 -10 -18
		mu 0 4 6 7 12 11
		f 4 6 19 -11 -19
		mu 0 4 7 8 13 12
		f 4 7 16 -12 -20
		mu 0 4 8 9 14 13
		f 3 -1 -21 21
		mu 0 3 1 0 15
		f 3 -2 -22 22
		mu 0 3 2 1 16
		f 3 -3 -23 23
		mu 0 3 3 2 17
		f 3 -4 -24 20
		mu 0 3 4 3 18
		f 3 8 25 -25
		mu 0 3 10 11 19
		f 3 9 26 -26
		mu 0 3 11 12 20
		f 3 10 27 -27
		mu 0 3 12 13 21
		f 3 11 24 -28
		mu 0 3 13 14 22;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "R_KneePoleV" -p "mainCtrl";
	rename -uid "610BB8C1-4A5A-AF70-00F2-5C89793BE5A7";
	setAttr ".rp" -type "double3" -0.25347867550962461 0.5806123669196751 0.38622447812360772 ;
	setAttr ".sp" -type "double3" -0.25347867550962461 0.5806123669196751 0.38622447812360772 ;
createNode mesh -n "R_KneePoleVShape" -p "R_KneePoleV";
	rename -uid "4C944582-49F9-6DE7-47D5-11BE171ACEA4";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 23 ".uvst[0].uvsp[0:22]" -type "float2" 0 0.25 0.25 0.25
		 0.5 0.25 0.75 0.25 1 0.25 0 0.5 0.25 0.5 0.5 0.5 0.75 0.5 1 0.5 0 0.75 0.25 0.75
		 0.5 0.75 0.75 0.75 1 0.75 0.125 0 0.375 0 0.625 0 0.875 0 0.125 1 0.375 1 0.625 1
		 0.875 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  -0.25347877 1.2563267 1.0619388 
		0.42223561 1.2563267 0.38622454 -0.25347865 1.2563267 -0.28948981 -0.92919296 1.2563267 
		0.38622448 -0.2534788 0.58061236 1.3418288 0.70212567 0.58061236 0.38622457 -0.25347865 
		0.58061236 -0.56937987 -1.209083 0.58061236 0.38622448 -0.25347877 -0.095101915 1.0619388 
		0.42223561 -0.095101915 0.38622454 -0.25347865 -0.095101915 -0.28948981 -0.92919296 
		-0.095101915 0.38622448 -0.25347868 1.5362167 0.38622448 -0.25347868 -0.37499195 
		0.38622448;
	setAttr -s 14 ".vt[0:13]"  9.2725848e-008 -0.70710677 -0.70710677 -0.70710677 -0.70710677 -6.1817239e-008
		 -3.090862e-008 -0.70710677 0.70710677 0.70710677 -0.70710677 0 1.3113416e-007 0 -1
		 -1 0 -8.7422777e-008 -4.3711388e-008 0 1 1 0 0 9.2725848e-008 0.70710677 -0.70710677
		 -0.70710677 0.70710677 -6.1817239e-008 -3.090862e-008 0.70710677 0.70710677 0.70710677 0.70710677 0
		 0 -1 0 0 1 0;
	setAttr -s 28 ".ed[0:27]"  0 1 0 1 2 0 2 3 0 3 0 0 4 5 0 5 6 0 6 7 0
		 7 4 0 8 9 0 9 10 0 10 11 0 11 8 0 0 4 0 1 5 0 2 6 0 3 7 0 4 8 0 5 9 0 6 10 0 7 11 0
		 12 0 0 12 1 0 12 2 0 12 3 0 8 13 0 9 13 0 10 13 0 11 13 0;
	setAttr -s 16 -ch 56 ".fc[0:15]" -type "polyFaces" 
		f 4 0 13 -5 -13
		mu 0 4 0 1 6 5
		f 4 1 14 -6 -14
		mu 0 4 1 2 7 6
		f 4 2 15 -7 -15
		mu 0 4 2 3 8 7
		f 4 3 12 -8 -16
		mu 0 4 3 4 9 8
		f 4 4 17 -9 -17
		mu 0 4 5 6 11 10
		f 4 5 18 -10 -18
		mu 0 4 6 7 12 11
		f 4 6 19 -11 -19
		mu 0 4 7 8 13 12
		f 4 7 16 -12 -20
		mu 0 4 8 9 14 13
		f 3 -1 -21 21
		mu 0 3 1 0 15
		f 3 -2 -22 22
		mu 0 3 2 1 16
		f 3 -3 -23 23
		mu 0 3 3 2 17
		f 3 -4 -24 20
		mu 0 3 4 3 18
		f 3 8 25 -25
		mu 0 3 10 11 19
		f 3 9 26 -26
		mu 0 3 11 12 20
		f 3 10 27 -27
		mu 0 3 12 13 21
		f 3 11 24 -28
		mu 0 3 13 14 22;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "L_KneePoleV" -p "mainCtrl";
	rename -uid "52378935-4E0A-8824-65B2-90AE605B12B6";
	setAttr ".rp" -type "double3" 0.23655353054188361 0.5806124300499832 0.38622448029404322 ;
	setAttr ".sp" -type "double3" 0.23655353054188361 0.5806124300499832 0.38622448029404322 ;
createNode mesh -n "L_KneePoleVShape" -p "L_KneePoleV";
	rename -uid "5D9613E9-46EC-BE2D-BA88-C897396B4857";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 22;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 23 ".uvst[0].uvsp[0:22]" -type "float2" 0 0.25 0.25 0.25
		 0.5 0.25 0.75 0.25 1 0.25 0 0.5 0.25 0.5 0.5 0.5 0.75 0.5 1 0.5 0 0.75 0.25 0.75
		 0.5 0.75 0.75 0.75 1 0.75 0.125 0 0.375 0 0.625 0 0.875 0 0.125 1 0.375 1 0.625 1
		 0.875 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  0.23655345 1.2563267 1.0619388 
		0.9122678 1.2563267 0.38622454 0.23655356 1.2563267 -0.28948981 -0.43916076 1.2563267 
		0.38622448 0.2365534 0.58061242 1.3418288 1.1921579 0.58061242 0.38622457 0.23655358 
		0.58061242 -0.56937981 -0.71905077 0.58061242 0.38622448 0.23655345 -0.095101848 
		1.0619388 0.9122678 -0.095101848 0.38622454 0.23655356 -0.095101848 -0.28948981 -0.43916076 
		-0.095101848 0.38622448 0.23655353 1.5362167 0.38622448 0.23655353 -0.37499189 0.38622448;
	setAttr -s 14 ".vt[0:13]"  9.2725848e-008 -0.70710677 -0.70710677 -0.70710677 -0.70710677 -6.1817239e-008
		 -3.090862e-008 -0.70710677 0.70710677 0.70710677 -0.70710677 0 1.3113416e-007 0 -1
		 -1 0 -8.7422777e-008 -4.3711388e-008 0 1 1 0 0 9.2725848e-008 0.70710677 -0.70710677
		 -0.70710677 0.70710677 -6.1817239e-008 -3.090862e-008 0.70710677 0.70710677 0.70710677 0.70710677 0
		 0 -1 0 0 1 0;
	setAttr -s 28 ".ed[0:27]"  0 1 0 1 2 0 2 3 0 3 0 0 4 5 0 5 6 0 6 7 0
		 7 4 0 8 9 0 9 10 0 10 11 0 11 8 0 0 4 0 1 5 0 2 6 0 3 7 0 4 8 0 5 9 0 6 10 0 7 11 0
		 12 0 0 12 1 0 12 2 0 12 3 0 8 13 0 9 13 0 10 13 0 11 13 0;
	setAttr -s 16 -ch 56 ".fc[0:15]" -type "polyFaces" 
		f 4 0 13 -5 -13
		mu 0 4 0 1 6 5
		f 4 1 14 -6 -14
		mu 0 4 1 2 7 6
		f 4 2 15 -7 -15
		mu 0 4 2 3 8 7
		f 4 3 12 -8 -16
		mu 0 4 3 4 9 8
		f 4 4 17 -9 -17
		mu 0 4 5 6 11 10
		f 4 5 18 -10 -18
		mu 0 4 6 7 12 11
		f 4 6 19 -11 -19
		mu 0 4 7 8 13 12
		f 4 7 16 -12 -20
		mu 0 4 8 9 14 13
		f 3 -1 -21 21
		mu 0 3 1 0 15
		f 3 -2 -22 22
		mu 0 3 2 1 16
		f 3 -3 -23 23
		mu 0 3 3 2 17
		f 3 -4 -24 20
		mu 0 3 4 3 18
		f 3 8 25 -25
		mu 0 3 10 11 19
		f 3 9 26 -26
		mu 0 3 11 12 20
		f 3 10 27 -27
		mu 0 3 12 13 21
		f 3 11 24 -28
		mu 0 3 13 14 22;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "L_ElbowPoleV" -p "mainCtrl";
	rename -uid "C756D8FB-41D5-3732-1B96-8AAD7AF5FC27";
	setAttr ".rp" -type "double3" 1.0827107078589648 2.0789103403481719 -0.43254836939121427 ;
	setAttr ".sp" -type "double3" 1.0827107078589719 2.0789103403481719 -0.43254836939121377 ;
createNode mesh -n "L_ElbowPoleVShape" -p "L_ElbowPoleV";
	rename -uid "A75B11C4-4120-23B9-69E0-4EA86DF4822C";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 14;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 23 ".uvst[0].uvsp[0:22]" -type "float2" 0 0.25 0.25 0.25
		 0.5 0.25 0.75 0.25 1 0.25 0 0.5 0.25 0.5 0.5 0.5 0.75 0.5 1 0.5 0 0.75 0.25 0.75
		 0.5 0.75 0.75 0.75 1 0.75 0.125 0 0.375 0 0.625 0 0.875 0 0.125 1 0.375 1 0.625 1
		 0.875 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  1.0827103 2.7261505 0.21469498 
		1.7299576 2.7261505 -0.43254852 1.0827103 2.7261505 -1.079792 0.43546295 2.7261505 
		-0.43254852 1.0827103 2.0789032 0.4827919 1.9980545 2.0789032 -0.43254852 1.0827103 
		2.0789032 -1.3478889 0.16736603 2.0789032 -0.43254852 1.0827103 1.4316711 0.21469498 
		1.7299576 1.4316711 -0.43254852 1.0827103 1.4316711 -1.079792 0.43546295 1.4316711 
		-0.43254852 1.0827103 2.9942474 -0.43254852 1.0827103 1.1635742 -0.43254852;
	setAttr -s 14 ".vt[0:13]"  9.2725848e-008 -0.70710677 -0.70710677 -0.70710677 -0.70710677 -6.1817239e-008
		 -3.090862e-008 -0.70710677 0.70710677 0.70710677 -0.70710677 0 1.3113416e-007 0 -1
		 -1 0 -8.7422777e-008 -4.3711388e-008 0 1 1 0 0 9.2725848e-008 0.70710677 -0.70710677
		 -0.70710677 0.70710677 -6.1817239e-008 -3.090862e-008 0.70710677 0.70710677 0.70710677 0.70710677 0
		 0 -1 0 0 1 0;
	setAttr -s 28 ".ed[0:27]"  0 1 0 1 2 0 2 3 0 3 0 0 4 5 0 5 6 0 6 7 0
		 7 4 0 8 9 0 9 10 0 10 11 0 11 8 0 0 4 0 1 5 0 2 6 0 3 7 0 4 8 0 5 9 0 6 10 0 7 11 0
		 12 0 0 12 1 0 12 2 0 12 3 0 8 13 0 9 13 0 10 13 0 11 13 0;
	setAttr -s 16 -ch 56 ".fc[0:15]" -type "polyFaces" 
		f 4 0 13 -5 -13
		mu 0 4 0 1 6 5
		f 4 1 14 -6 -14
		mu 0 4 1 2 7 6
		f 4 2 15 -7 -15
		mu 0 4 2 3 8 7
		f 4 3 12 -8 -16
		mu 0 4 3 4 9 8
		f 4 4 17 -9 -17
		mu 0 4 5 6 11 10
		f 4 5 18 -10 -18
		mu 0 4 6 7 12 11
		f 4 6 19 -11 -19
		mu 0 4 7 8 13 12
		f 4 7 16 -12 -20
		mu 0 4 8 9 14 13
		f 3 -1 -21 21
		mu 0 3 1 0 15
		f 3 -2 -22 22
		mu 0 3 2 1 16
		f 3 -3 -23 23
		mu 0 3 3 2 17
		f 3 -4 -24 20
		mu 0 3 4 3 18
		f 3 8 25 -25
		mu 0 3 10 11 19
		f 3 9 26 -26
		mu 0 3 11 12 20
		f 3 10 27 -27
		mu 0 3 12 13 21
		f 3 11 24 -28
		mu 0 3 13 14 22;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ct_R_Wrist" -p "mainCtrl";
	rename -uid "831E452B-41FA-85FF-8149-B687EDC4AD87";
	setAttr ".rp" -type "double3" -1.7027706076013924 2.0862582713497662 -0.04050455945920909 ;
	setAttr ".sp" -type "double3" -1.7027706076013924 2.0862582713497662 -0.04050455945920909 ;
createNode nurbsCurve -n "ct_R_WristShape" -p "ct_R_Wrist";
	rename -uid "8C24D017-4E01-6CB8-5BF9-16A9F1D6AFD4";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.7027706076013924 2.2949511116728805 -0.24919739978232303
		-1.702770607601392 2.0862582713497666 -0.33564080461431978
		-1.702770607601392 1.8775654310266527 -0.24919739978232316
		-1.702770607601392 1.7911220261946559 -0.040504559459209187
		-1.702770607601392 1.8775654310266527 0.16818828086390492
		-1.702770607601392 2.0862582713497666 0.25463168569590161
		-1.7027706076013924 2.2949511116728805 0.16818828086390494
		-1.7027706076013924 2.3813945165048769 -0.040504559459208937
		-1.7027706076013924 2.2949511116728805 -0.24919739978232303
		-1.702770607601392 2.0862582713497666 -0.33564080461431978
		-1.702770607601392 1.8775654310266527 -0.24919739978232316
		;
createNode transform -n "ct_R_Hand" -p "ct_R_Wrist";
	rename -uid "ADACAE45-4EB2-FACE-6BD0-FD84B3E55FF1";
	setAttr ".rp" -type "double3" -1.6111696876010524 2.0821363278042861 -0.040040160232060258 ;
	setAttr ".sp" -type "double3" -1.6111696876010524 2.0821363278042861 -0.040040160232060258 ;
createNode nurbsCurve -n "ct_R_HandShape" -p "|mainCtrl|ct_R_Wrist|ct_R_Hand";
	rename -uid "E7856E9C-4421-916C-717A-DE81F8230AB3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-1.6111696876010522 2.3481111445504155 -0.30601497697819496
		-1.6111696876010519 2.0821363278042861 -0.23561590708657179
		-1.6111696876010515 1.8161615110581464 -0.30601497697819507
		-1.6111696876010519 1.8865605809497743 -0.040040160232060321
		-1.6111696876010515 1.8161615110581464 0.22593465651407368
		-1.6111696876010519 2.0821363278042861 0.15553558662245129
		-1.6111696876010522 2.3481111445504155 0.22593465651407368
		-1.6111696876010522 2.2777120746587971 -0.040040160232060154
		-1.6111696876010522 2.3481111445504155 -0.30601497697819496
		-1.6111696876010519 2.0821363278042861 -0.23561590708657179
		-1.6111696876010515 1.8161615110581464 -0.30601497697819507
		;
createNode transform -n "SF_Wep_Undead_Longsword_01" -p "|mainCtrl|ct_R_Wrist|ct_R_Hand";
	rename -uid "A62A1410-45D3-6298-B6D5-67B817330F2F";
	setAttr ".v" no;
	setAttr ".rp" -type "double3" -1.7600739258323699 1.8474900668750462 0 ;
	setAttr ".sp" -type "double3" -1.7600739258323699 1.8474900668750462 0 ;
createNode mesh -n "SF_Wep_Undead_Longsword_01Shape" -p "SF_Wep_Undead_Longsword_01";
	rename -uid "C818803E-4F5A-A43C-DE40-0397B9CC8CED";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.71873405575752258 0.0086164958775043488 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 304 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.70268178 0.0092456341 0.70268178
		 0.0092456341 0.70268178 0.0092456341 0.70268178 0.0092456341 0.70268178 0.0092456341
		 0.70268178 0.0092456341 0.67049199 0.0092456341 0.67049205 0.0092456341 0.67049199
		 0.0092456341 0.67049199 0.0092456341 0.67049205 0.0092456341 0.67049199 0.0092456341
		 0.67049199 0.0092456341 0.67049199 0.0092456341 0.64133775 0.0092456341 0.64133775
		 0.0092456341 0.64133775 0.0092456341 0.64133781 0.0092456341 0.67352754 0.0092456341
		 0.67352754 0.0092456341 0.67352754 0.0092456341 0.67352754 0.0092456341 0.64133775
		 0.0092456341 0.64133775 0.0092456341 0.64133781 0.0092456341 0.64133775 0.0092456341
		 0.67352754 0.0092456341 0.67352754 0.0092456341 0.67352754 0.0092456341 0.67352754
		 0.0092456341 0.63796824 0.0092456341 0.6379683 0.0092456341 0.6379683 0.0092456341
		 0.63796824 0.0092456341 0.69979411 0.0092456341 0.69979405 0.0092456341 0.69979405
		 0.0092456341 0.69979411 0.0092456341 0.73399609 0.0092456341 0.73399609 0.0092456341
		 0.73399609 0.0092456341 0.73399609 0.0092456341 0.69979405 0.0092456341 0.69979405
		 0.0092456341 0.69979405 0.0092456341 0.69979405 0.0092456341 0.6379683 0.0092456341
		 0.63796824 0.0092456341 0.63796824 0.0092456341 0.6379683 0.0092456341 0.69979405
		 0.0092456341 0.69979405 0.0092456341 0.69979405 0.0092456341 0.69979405 0.0092456341
		 0.73399609 0.0092456341 0.73399609 0.0092456341 0.73399609 0.0092456341 0.73399609
		 0.0092456341 0.69979405 0.0092456341 0.69979411 0.0092456341 0.69979411 0.0092456341
		 0.69979405 0.0092456341 0.70396537 0.0092456341 0.70396537 0.0092456341 0.70396537
		 0.0092456331 0.70396537 0.0092456341 0.70268172 0.0092456331 0.70268172 0.0092456331
		 0.70268178 0.0092456341 0.70268178 0.0092456341 0.70268178 0.0092456341 0.67049199
		 0.0092456331 0.67049199 0.0092456341 0.67049199 0.0092456341 0.67049199 0.0092456341
		 0.73615515 0.0092456341 0.73615515 0.0092456341 0.73615515 0.0092456341 0.73615515
		 0.0092456341 0.67352754 0.0092456341 0.67352754 0.0092456341 0.67352754 0.0092456341
		 0.67352754 0.0092456341 0.70268178 0.0092456341 0.70268178 0.0092456341 0.67049199
		 0.0092456341 0.67049199 0.0092456331 0.67049199 0.0092456341 0.67049199 0.0092456341
		 0.70396537 0.0092456341 0.70396537 0.0092456341 0.70396537 0.0092456341 0.70396537
		 0.0092456331 0.73615515 0.0092456341 0.73615515 0.0092456341 0.73615509 0.0092456331
		 0.73615509 0.0092456331 0.70268178 0.0092456341 0.70268178 0.0092456341 0.70268178
		 0.0092456341 0.70268178 0.0092456341 0.70268178 0.0092456341 0.63647878 0.0092456341
		 0.63647878 0.0092456341 0.63647878 0.0092456341 0.63647878 0.0092456341 0.66866857
		 0.0092456341 0.66866857 0.0092456341 0.66866857 0.0092456341 0.67049199 0.0092456341
		 0.67049199 0.0092456341 0.67049199 0.0092456341 0.67049199 0.0092456341 0.66866857
		 0.0092456341 0.66866857 0.0092456341 0.66866857 0.0092456341 0.79765385 0.0079873577
		 0.79765385 0.0079873577 0.79765385 0.0079873577 0.79765385 0.0079873577 0.76777834
		 0.0079873577 0.76777834 0.0079873577 0.76777834 0.0079873577 0.76777834 0.0079873577
		 0.73404467 0.008012481 0.73404467 0.008012481 0.73404467 0.008012481 0.73624295 0.0079873577
		 0.73624295 0.0079873577 0.73624307 0.0079873577 0.73624295 0.0079873577 0.70416903
		 0.008012481 0.70416915 0.008012481 0.70416903 0.008012481 0.70416903 0.008012481
		 0.76611859 0.0079873577 0.76611859 0.0079873577 0.76611859 0.0079873577 0.76611859
		 0.0079873577 0.70120275 0.008012481 0.70120275 0.008012481 0.70120275 0.008012481
		 0.67132735 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481
		 0.67132735 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481
		 0.70416915 0.008012481 0.70416903 0.008012481 0.70416903 0.008012481 0.70416903 0.008012481
		 0.70120275 0.008012481 0.70120275 0.008012481 0.70120275 0.008012481 0.73404467 0.008012481
		 0.73404467 0.008012481 0.73404467 0.008012481 0.76777834 0.0079873577 0.76777834
		 0.0079873577 0.76777834 0.0079873577 0.76777834 0.0079873577 0.67339528 0.008012481
		 0.67339516 0.008012481 0.67339516 0.008012481 0.67339528 0.008012481 0.64055347 0.008012481
		 0.64055347 0.008012481 0.64055347 0.008012481 0.64055336 0.008012481 0.73624295 0.0079873577
		 0.73624307 0.0079873577 0.73624307 0.0079873577 0.73624295 0.0079873577 0.64055347
		 0.008012481 0.64055336 0.008012481 0.64055347 0.008012481 0.64055347 0.008012481
		 0.67339516 0.008012481 0.67339528 0.008012481 0.67339528 0.008012481 0.67339516 0.008012481
		 0.80098933 0.0079873577 0.80098933 0.0079873577 0.80098933 0.0079873577 0.80098933
		 0.0079873577 0.73278344 0.008012481 0.73278332 0.008012481 0.73278332 0.008012481
		 0.73278344 0.008012481 0.69994152 0.008012481 0.69994164 0.008012481 0.69994164 0.008012481
		 0.69994152 0.008012481 0.76945394 0.0079873577 0.76945406 0.0079873577 0.76945406
		 0.0079873577 0.76945394 0.0079873577 0.69994164 0.008012481 0.69994152 0.008012481
		 0.69994152 0.008012481 0.69994164 0.008012481 0.73278332 0.008012481 0.73278344 0.008012481
		 0.73278344 0.008012481 0.73278332 0.008012481 0.79991788 0.0079873577 0.79991788
		 0.0079873577 0.79991788 0.0079873577 0.79991788 0.0079873577 0.73278332 0.008012481
		 0.73278344 0.008012481 0.73278344 0.008012481 0.73278332 0.008012481 0.69994164 0.008012481
		 0.69994164 0.008012481 0.69994164 0.008012481 0.69994164 0.008012481 0.76838273 0.0079873577
		 0.76838273 0.0079873577 0.76838273 0.0079873577 0.76838273 0.0079873577 0.69994164
		 0.008012481 0.69994164 0.008012481 0.69994164 0.008012481 0.69994164 0.008012481
		 0.73278344 0.008012481 0.73278332 0.008012481 0.73278332 0.008012481 0.73278344 0.008012481
		 0.76777834 0.0079873577 0.76777834 0.0079873577 0.76777834 0.0079873577 0.76777834
		 0.0079873577 0.70416903 0.008012481 0.70416903 0.008012481 0.70416903 0.008012481
		 0.70416903 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481
		 0.67132735 0.008012481 0.73624307 0.0079873577 0.73624307 0.0079873577 0.73624295
		 0.0079873577 0.73624295 0.0079873577 0.67132735 0.008012481 0.67132735 0.008012481;
	setAttr ".uvst[0].uvsp[250:303]" 0.67132735 0.008012481 0.67132735 0.008012481
		 0.70416903 0.008012481 0.70416903 0.008012481 0.70416903 0.008012481 0.70416903 0.008012481
		 0.73624295 0.0079873577 0.73624307 0.0079873577 0.73624307 0.0079873577 0.73624307
		 0.0079873577 0.67132723 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481
		 0.67132735 0.008012481 0.70416903 0.008012481 0.70416915 0.008012481 0.70416903 0.008012481
		 0.70416915 0.008012481 0.76777834 0.0079873577 0.76777834 0.0079873577 0.76777834
		 0.0079873577 0.76777834 0.0079873577 0.70416903 0.008012481 0.70416915 0.008012481
		 0.70416903 0.008012481 0.70416915 0.008012481 0.67132735 0.008012481 0.67132735 0.008012481
		 0.67132723 0.008012481 0.67132735 0.008012481 0.76777834 0.0079873577 0.76777834
		 0.0079873577 0.76777834 0.0079873577 0.76777834 0.0079873577 0.67339516 0.008012481
		 0.67339516 0.008012481 0.67339528 0.008012481 0.67339516 0.008012481 0.64055347 0.008012481
		 0.64055347 0.008012481 0.64055347 0.008012481 0.64055347 0.008012481 0.73624307 0.0079873577
		 0.73624307 0.0079873577 0.73624307 0.0079873577 0.73624307 0.0079873577 0.64055347
		 0.008012481 0.64055347 0.008012481 0.64055347 0.008012481 0.64055347 0.008012481
		 0.67339528 0.008012481 0.67339516 0.008012481 0.67339516 0.008012481 0.67339516 0.008012481;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 82 ".pt[0:81]" -type "float3"  -1.7600739 1.8474901 -0.0032156075 
		-1.7600739 1.8001249 -0.0052787066 -1.7600739 1.8001249 0.00527925 -1.7600739 1.8474901 
		0.0032161511 -1.7557884 1.8117822 0.00527925 -1.7557884 1.8117822 -0.0052787066 -1.7643594 
		1.8117822 -0.0052787066 -1.7643594 1.8117822 0.00527925 -1.797583 1.8614916 -0.0023566624 
		-1.797583 1.8614916 0.0023572061 -1.8002856 1.8598052 0.0023572061 -1.8002856 1.8598052 
		-0.0023566624 -1.7225648 1.8614916 0.0023572061 -1.7225648 1.8614916 -0.0023566624 
		-1.7198622 1.8598052 -0.0023566624 -1.7198622 1.8598052 0.0023572061 -1.7574633 1.8178579 
		0.0032161511 -1.7574633 1.817858 -0.0032156075 -1.7626847 1.817858 -0.0032156075 
		-1.7626847 1.8178579 0.0032161511 -1.7600739 1.8091177 -0.0052787066 -1.7600739 1.817858 
		-0.0032156075 -1.7600739 1.8178579 0.0032161511 -1.7600739 1.8091177 0.00527925 -1.7574632 
		1.837912 -0.0032156075 -1.7574633 1.837912 0.0032161511 -1.7626847 1.837912 -0.0032156075 
		-1.7626847 1.837912 0.0032161511 -1.7554102 1.8408515 0.0057453276 -1.7554102 1.8408515 
		-0.0057453276 -1.7647377 1.8408515 -0.0057453276 -1.7647377 1.8408515 0.0057453276 
		-1.7512316 1.6595612 -0.0018014289 -1.7689158 1.6595612 -0.0018014289 -1.7512316 
		1.6595612 0.0018025161 -1.7689158 1.6595612 0.0018025161 -1.7643254 1.8045818 0.0018025161 
		-1.7643254 1.8045818 -0.0018014289 -1.7558219 1.8045818 -0.0018014289 -1.7558219 
		1.8045818 0.0018025161 -1.7600735 1.6383322 0.0018025161 -1.7600735 1.6383322 -0.0018014289 
		-1.7600735 1.8045818 -0.0047591715 -1.7600735 1.8045818 0.0047600777 -1.7600735 1.6595612 
		-0.0047591715 -1.7600735 1.6595612 0.0047600777 -1.7643254 1.7965868 0.0018025161 
		-1.7643254 1.7965869 -0.0018014289 -1.7600735 1.7965868 -0.0047591715 -1.7558219 
		1.7965869 -0.0018014289 -1.7558219 1.7965868 0.0018025161 -1.7600735 1.7965869 0.0047600777 
		-1.7733719 1.7922273 0.0018025161 -1.7733719 1.7922274 -0.0018014289 -1.7600735 1.7922273 
		-0.0047591715 -1.7467749 1.7922274 -0.0018014289 -1.7467752 1.7922273 0.0018025161 
		-1.7600735 1.7922274 0.0047600777 -1.7689158 1.7811826 0.0018025161 -1.7689157 1.7811828 
		-0.0018014289 -1.7600735 1.7811826 -0.0047591715 -1.751231 1.7811828 -0.0018014289 
		-1.7512316 1.7811826 0.0018025161 -1.7600735 1.7811828 0.0047600777 -1.768916 1.7296395 
		0.0018025161 -1.7689158 1.7296395 -0.0018014289 -1.7600735 1.7296396 -0.0047591715 
		-1.7512312 1.7296395 -0.0018014289 -1.7512316 1.7296395 0.0018025161 -1.7600735 1.7296396 
		0.0047600777 -1.7512317 1.7440804 0.0018019724 -1.7512312 1.7440804 -0.0018014289 
		-1.7600735 1.7440804 -0.0047591715 -1.7689158 1.7440804 -0.0018014289 -1.768916 1.7440804 
		0.0018019724 -1.7600735 1.7440805 0.0047600777 -1.7640864 1.73686 0.0018025161 -1.7640864 
		1.73686 -0.0018014289 -1.7600735 1.73686 -0.0047591715 -1.7560608 1.73686 -0.0018014289 
		-1.7560608 1.73686 0.0018025161 -1.7600735 1.73686 0.0047600777;
	setAttr -s 82 ".vt[0:81]"  0 0 0.033845901 0 0.49854285 0.055561066
		 0 0.49854285 -0.055566788 0 0 -0.033851624 -0.045106888 0.37584338 -0.055566788 -0.045106888 0.37584338 0.055561066
		 0.045106888 0.37584338 0.055561066 0.045106888 0.37584338 -0.055566788 0.19465256 0.28052762 0.024805069
		 0.19465256 0.28052762 -0.024810791 0.20867729 0.29827693 -0.024810791 0.20867729 0.29827693 0.024805069
		 -0.19465256 0.28052762 -0.024810791 -0.19465256 0.28052762 0.024805069 -0.20867729 0.29827693 0.024805069
		 -0.20867729 0.29827693 -0.024810791 -0.027477264 0.31189343 -0.033851624 -0.027477264 0.31189328 0.033845901
		 0.027479172 0.31189328 0.033845901 0.027479172 0.31189343 -0.033851624 0 0.40388885 0.055561066
		 0 0.31189328 0.033845901 0 0.31189343 -0.033851624 0 0.40388885 -0.055566788 -0.027479172 0.10081492 0.033845901
		 -0.027477264 0.10081494 -0.033851624 0.027479172 0.10081492 0.033845901 0.027479172 0.10081494 -0.033851624
		 -0.049087524 0.069873869 -0.060472488 -0.049087524 0.069873862 0.060472488 0.049089432 0.069873862 0.060472488
		 0.049089432 0.069873869 -0.060472488 -0.093070984 1.97804773 0.018960953 0.093065262 1.97804773 0.018960953
		 -0.093070984 1.97804773 -0.018972397 0.093065262 1.97804773 -0.018972397 0.044748306 0.45163226 -0.018972397
		 0.044748306 0.45163226 0.018960953 -0.044754028 0.45163226 0.018960953 -0.044754028 0.45163226 -0.018972397
		 -3.8146973e-006 2.20149255 -0.018972397 -3.8146973e-006 2.20149255 0.018960953 -3.8146973e-006 0.45163226 0.050092697
		 -3.8146973e-006 0.45163226 -0.050102234 -3.8146973e-006 1.97804749 0.050092697 -3.8146973e-006 1.97804749 -0.050102234
		 0.044748306 0.53578329 -0.018972397 0.044748306 0.53578258 0.018960953 -3.8146973e-006 0.53578329 0.050092697
		 -0.044754028 0.53578258 0.018960953 -0.044754028 0.53578329 -0.018972397 -3.8146973e-006 0.53578258 -0.050102234
		 0.13996887 0.58166879 -0.018972397 0.13996887 0.5816679 0.018960953 -3.8146973e-006 0.58166879 0.050092697
		 -0.13997841 0.5816679 0.018960953 -0.1399765 0.58166879 -0.018972397 -3.8146973e-006 0.5816679 -0.050102234
		 0.093065262 0.69791883 -0.018972397 0.093063354 0.69791812 0.018960953 -3.8146973e-006 0.69791883 0.050092697
		 -0.093076706 0.69791812 0.018960953 -0.093070984 0.69791883 -0.018972397 -3.8146973e-006 0.69791812 -0.050102234
		 0.093067169 1.24043632 -0.018972397 0.093065262 1.24043632 0.018960953 -3.8146973e-006 1.24043608 0.050092697
		 -0.093074799 1.24043632 0.018960953 -0.093070984 1.24043632 -0.018972397 -3.8146973e-006 1.24043572 -0.050102234
		 -0.093069077 1.088439107 -0.018966675 -0.093074799 1.088438869 0.018960953 -3.8146973e-006 1.088438511 0.050092697
		 0.093065262 1.088438869 0.018960953 0.093067169 1.088439107 -0.018966675 -3.8146973e-006 1.088438272 -0.050102234
		 0.042232513 1.16443753 -0.018972397 0.042232513 1.16443753 0.018960953 -3.8146973e-006 1.16443729 0.050092697
		 -0.042240143 1.16443753 0.018960953 -0.042240143 1.16443753 -0.018972397 -3.8146973e-006 1.16443729 -0.050102234;
	setAttr -s 159 ".ed[0:158]"  5 20 0 20 1 1 1 5 0 2 7 0 7 23 0 23 2 1 8 9 0
		 9 10 0 10 11 0 11 8 0 12 13 0 13 14 0 14 15 0 15 12 0 16 17 0 17 5 0 5 4 0 4 16 0
		 20 21 0 21 18 0 18 6 0 6 20 0 18 19 0 19 7 0 7 6 0 22 23 0 19 22 0 7 9 0 8 6 0 2 10 0
		 2 1 0 1 11 0 1 6 0 5 13 0 12 4 0 1 14 0 2 15 0 2 4 0 28 29 0 29 24 0 24 25 0 25 28 0
		 24 26 0 26 18 0 21 17 0 17 24 0 26 27 0 27 19 0 25 27 0 27 31 0 31 28 0 4 23 0 22 16 0
		 16 25 0 30 31 0 26 30 0 29 30 0 3 0 0 0 29 0 28 3 0 0 30 0 3 31 0 40 41 0 41 33 0
		 33 35 0 35 40 0 37 36 0 36 46 0 46 47 1 47 37 0 41 44 0 44 33 1 39 38 0 38 49 0 49 50 1
		 50 39 0 43 51 0 51 46 1 36 43 0 32 41 0 40 34 0 34 32 0 32 44 1 50 51 1 43 39 0 38 42 0
		 42 48 0 48 49 1 47 48 1 42 37 0 40 45 0 45 34 1 35 45 1 46 52 0 52 53 1 53 47 0 53 54 1
		 54 48 0 54 55 1 55 49 0 55 56 1 56 50 0 56 57 1 57 51 0 57 52 1 52 58 0 58 59 1 59 53 0
		 59 60 1 60 54 0 60 61 1 61 55 0 61 62 1 62 56 0 62 63 1 63 57 0 63 58 1 73 74 1 74 76 0
		 76 77 1 77 73 0 77 78 1 78 72 0 72 73 1 71 72 1 78 79 1 79 71 0 70 71 1 79 80 1 80 70 0
		 80 81 1 81 75 0 75 70 1 74 75 1 81 76 1 65 64 1 64 35 0 33 65 0 44 66 0 66 65 1 67 66 1
		 32 67 0 68 67 1 34 68 0 45 69 0 69 68 1 64 69 1 61 71 0 70 62 0 60 72 0 59 73 0 58 74 0
		 63 75 0 76 64 0 65 77 0 66 78 0 67 79 0 68 80 0 69 81 0;
	setAttr -s 312 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr ".n[166:311]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr -s 80 -ch 312 ".fc[0:79]" -type "polyFaces" 
		f 3 0 1 2
		mu 0 3 0 1 2
		f 3 3 4 5
		mu 0 3 3 4 5
		f 4 6 7 8 9
		mu 0 4 6 7 8 9
		f 4 10 11 12 13
		mu 0 4 10 11 12 13
		f 4 14 15 16 17
		mu 0 4 14 15 16 17
		f 4 18 19 20 21
		mu 0 4 18 19 20 21
		f 4 22 23 24 -21
		mu 0 4 22 23 24 25
		f 4 25 -5 -24 26
		mu 0 4 26 27 28 29
		f 4 -25 27 -7 28
		mu 0 4 30 31 32 33
		f 4 -4 29 -8 -28
		mu 0 4 34 35 36 37
		f 4 30 31 -9 -30
		mu 0 4 38 39 40 41
		f 4 32 -29 -10 -32
		mu 0 4 42 43 44 45
		f 4 -17 33 -11 34
		mu 0 4 46 47 48 49
		f 4 -3 35 -12 -34
		mu 0 4 50 51 52 53
		f 4 -31 36 -13 -36
		mu 0 4 54 55 56 57
		f 4 37 -35 -14 -37
		mu 0 4 58 59 60 61
		f 4 38 39 40 41
		mu 0 4 62 63 64 65
		f 5 42 43 -20 44 45
		mu 0 5 66 67 68 69 70
		f 4 46 47 -23 -44
		mu 0 4 71 72 73 74
		f 4 48 49 50 -42
		mu 0 4 75 76 77 78
		f 4 -45 -19 -1 -16
		mu 0 4 79 19 18 80
		f 4 51 -26 52 -18
		mu 0 4 81 27 26 82
		f 3 -2 -22 -33
		mu 0 3 2 1 83
		f 3 -6 -52 -38
		mu 0 3 3 5 84
		f 4 -41 -46 -15 53
		mu 0 4 85 86 87 88
		f 4 54 -50 -47 55
		mu 0 4 89 90 91 92
		f 4 56 -56 -43 -40
		mu 0 4 93 94 95 96
		f 5 -27 -48 -49 -54 -53
		mu 0 5 97 98 99 100 101
		f 4 57 58 -39 59
		mu 0 4 102 103 104 105
		f 3 60 -57 -59
		mu 0 3 106 107 108
		f 4 -58 61 -55 -61
		mu 0 4 109 110 111 112
		f 3 -51 -62 -60
		mu 0 3 113 114 115
		f 4 62 63 64 65
		mu 0 4 116 117 118 119
		f 4 66 67 68 69
		mu 0 4 120 121 122 123
		f 3 70 71 -64
		mu 0 3 124 125 126
		f 4 72 73 74 75
		mu 0 4 127 128 129 130
		f 4 76 77 -68 78
		mu 0 4 131 132 133 134
		f 4 79 -63 80 81
		mu 0 4 135 136 137 138
		f 3 82 -71 -80
		mu 0 3 139 140 141
		f 4 83 -77 84 -76
		mu 0 4 142 143 144 145
		f 4 85 86 87 -74
		mu 0 4 146 147 148 149
		f 4 88 -87 89 -70
		mu 0 4 150 151 152 153
		f 3 -81 90 91
		mu 0 3 154 155 156
		f 3 92 -91 -66
		mu 0 3 157 158 159
		f 4 -69 93 94 95
		mu 0 4 160 161 162 163
		f 4 96 97 -89 -96
		mu 0 4 164 165 166 167
		f 4 -88 -98 98 99
		mu 0 4 168 169 170 171
		f 4 -75 -100 100 101
		mu 0 4 172 173 174 175
		f 4 102 103 -84 -102
		mu 0 4 176 177 178 179
		f 4 -78 -104 104 -94
		mu 0 4 180 181 182 183
		f 4 -95 105 106 107
		mu 0 4 184 185 186 187
		f 4 108 109 -97 -108
		mu 0 4 188 189 190 191
		f 4 -99 -110 110 111
		mu 0 4 192 193 194 195
		f 4 -101 -112 112 113
		mu 0 4 196 197 198 199
		f 4 114 115 -103 -114
		mu 0 4 200 201 202 203
		f 4 -105 -116 116 -106
		mu 0 4 204 205 206 207
		f 4 117 118 119 120
		mu 0 4 208 209 210 211
		f 4 121 122 123 -121
		mu 0 4 212 213 214 215
		f 4 124 -123 125 126
		mu 0 4 216 217 218 219
		f 4 127 -127 128 129
		mu 0 4 220 221 222 223
		f 4 130 131 132 -130
		mu 0 4 224 225 226 227
		f 4 133 -132 134 -119
		mu 0 4 228 229 230 231
		f 4 135 136 -65 137
		mu 0 4 232 233 234 235
		f 4 -72 138 139 -138
		mu 0 4 236 237 238 239
		f 4 140 -139 -83 141
		mu 0 4 240 241 242 243
		f 4 142 -142 -82 143
		mu 0 4 244 245 246 247
		f 4 -92 144 145 -144
		mu 0 4 248 249 250 251
		f 4 146 -145 -93 -137
		mu 0 4 252 253 254 255
		f 4 -113 147 -128 148
		mu 0 4 256 257 258 259
		f 4 -111 149 -125 -148
		mu 0 4 260 261 262 263
		f 4 -124 -150 -109 150
		mu 0 4 264 265 266 267
		f 4 -107 151 -118 -151
		mu 0 4 268 269 270 271
		f 4 -117 152 -134 -152
		mu 0 4 272 273 274 275
		f 4 -133 -153 -115 -149
		mu 0 4 276 277 278 279
		f 4 -120 153 -136 154
		mu 0 4 280 281 282 283
		f 4 -140 155 -122 -155
		mu 0 4 284 285 286 287
		f 4 -126 -156 -141 156
		mu 0 4 288 289 290 291
		f 4 -129 -157 -143 157
		mu 0 4 292 293 294 295
		f 4 -146 158 -131 -158
		mu 0 4 296 297 298 299
		f 4 -135 -159 -147 -154
		mu 0 4 300 301 302 303;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "ct_L_Wrist" -p "mainCtrl";
	rename -uid "7F2EB0CD-48D5-CB7B-6F2F-8EA08EE2CBE6";
	setAttr ".rp" -type "double3" 1.6306213780541816 2.07869427155986 -0.012888103354826978 ;
	setAttr ".sp" -type "double3" 1.6306213780541861 2.078694271559872 -0.012888103354826954 ;
createNode nurbsCurve -n "ct_L_WristShape" -p "ct_L_Wrist";
	rename -uid "0FFF44C6-4C0D-BC3F-72E8-539725F36C31";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.6306213780541861 2.2873871118829863 -0.2215809436779409
		1.6306213780541861 2.078694271559872 -0.30802434850993765
		1.6306213780541861 1.8700014312367577 -0.22158094367794101
		1.6306213780541861 1.7835580264047612 -0.012888103354827039
		1.6306213780541861 1.8700014312367577 0.19580473696828707
		1.6306213780541861 2.078694271559872 0.28224814180028379
		1.6306213780541861 2.2873871118829858 0.19580473696828715
		1.6306213780541861 2.3738305167149831 -0.012888103354826796
		1.6306213780541861 2.2873871118829863 -0.2215809436779409
		1.6306213780541861 2.078694271559872 -0.30802434850993765
		1.6306213780541861 1.8700014312367577 -0.22158094367794101
		;
createNode transform -n "ct_R_Hand" -p "ct_L_Wrist";
	rename -uid "344C33C9-487C-9FEB-0260-FB9054361AA2";
	setAttr ".rp" -type "double3" 1.5771632250939462 2.0833855917282036 -0.020367338072050552 ;
	setAttr ".sp" -type "double3" 1.5771632250939462 2.0833855917282036 -0.020367338072050552 ;
createNode nurbsCurve -n "ct_R_HandShape" -p "|mainCtrl|ct_L_Wrist|ct_R_Hand";
	rename -uid "C4FA6F40-4FEB-F7C6-A58E-40B3116E179E";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		1.5771632250939465 2.3493604084743329 -0.28634215481818526
		1.5771632250939467 2.0833855917282036 -0.21594308492656208
		1.5771632250939471 1.8174107749820638 -0.28634215481818537
		1.5771632250939467 1.8878098448736917 -0.020367338072050611
		1.5771632250939471 1.8174107749820638 0.24560747867408339
		1.5771632250939467 2.0833855917282036 0.17520840878246099
		1.5771632250939465 2.3493604084743329 0.24560747867408339
		1.5771632250939465 2.2789613385827145 -0.020367338072050444
		1.5771632250939465 2.3493604084743329 -0.28634215481818526
		1.5771632250939467 2.0833855917282036 -0.21594308492656208
		1.5771632250939471 1.8174107749820638 -0.28634215481818537
		;
createNode transform -n "SF_Wep_Undead_Shield_06" -p "|mainCtrl|ct_L_Wrist|ct_R_Hand";
	rename -uid "AA1001DB-4F64-17DD-77E6-2DABC0D53BD0";
	setAttr ".v" no;
	setAttr ".rp" -type "double3" 1.8038102021688991 1.9694159477723909 0 ;
	setAttr ".sp" -type "double3" 1.8038102021689122 1.9694159477724043 0 ;
createNode mesh -n "SF_Wep_Undead_Shield_06Shape" -p "SF_Wep_Undead_Shield_06";
	rename -uid "86C420A3-42C9-488E-7BD4-2E96B96C29BD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.58631393313407898 0.34691210091114044 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 227 ".uvst[0].uvsp[0:226]" -type "float2" 0.70592189 0.0092456341
		 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.74712408
		 0.0092456341 0.74712408 0.0092456341 0.74712408 0.0092456341 0.74712408 0.0092456341
		 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189
		 0.0092456341 0.67144454 0.0092456341 0.67144442 0.0092456341 0.67144442 0.0092456341
		 0.67144454 0.0092456341 0.63986027 0.0092456341 0.63986027 0.0092456341 0.66963172
		 0.0092456341 0.66963172 0.0092456341 0.63986027 0.0092456341 0.63986027 0.0092456341
		 0.66963172 0.0092456341 0.66963172 0.0092456341 0.66963172 0.0092456341 0.70592189
		 0.0092456341 0.63986027 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341
		 0.66963172 0.0092456341 0.70592189 0.0092456341 0.63986027 0.0092456341 0.70592189
		 0.0092456341 0.70438612 0.0092456341 0.63544965 0.0092456341 0.70592189 0.0092456341
		 0.70438612 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.63544965
		 0.0092456341 0.63544965 0.0092456341 0.70592189 0.0092456341 0.66963172 0.0092456341
		 0.70592189 0.0092456341 0.70592189 0.0092456341 0.63544965 0.0092456341 0.70592189
		 0.0092456341 0.66963172 0.0092456341 0.70592189 0.0092456341 0.70438612 0.0092456341
		 0.70438612 0.0092456341 0.70592189 0.0092456341 0.66963172 0.0092456341 0.70592189
		 0.0092456341 0.70592189 0.0092456341 0.70438612 0.0092456341 0.70592189 0.0092456341
		 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189
		 0.0092456341 0.74712408 0.0092456341 0.74712408 0.0092456341 0.70592189 0.0092456341
		 0.70592189 0.0092456341 0.67144448 0.0092456341 0.67144448 0.0092456341 0.70592189
		 0.0092456341 0.66963172 0.0092456341 0.66963172 0.0092456341 0.66963172 0.0092456341
		 0.70438612 0.0092456341 0.70438612 0.0092456341 0.63544965 0.0092456341 0.63544965
		 0.0092456341 0.70438612 0.0092456341 0.66963172 0.0092456341 0.63544965 0.0092456341
		 0.66963172 0.0092456341 0.63544965 0.0092456341 0.63986027 0.0092456341 0.66963172
		 0.0092456341 0.63986027 0.0092456341 0.66963172 0.0092456341 0.67144448 0.0092456341
		 0.67144448 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189
		 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341
		 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189 0.0092456341 0.70592189
		 0.0092456341 0.70592189 0.0092456341 0.43024495 0.6183911 0.70592189 0.0092456341
		 0.43519157 0.62825698 0.43519157 0.62825698 0.70592189 0.0092456341 0.43519157 0.62825698
		 0.43024495 0.6183911 0.43024495 0.6183911 0.42835143 0.61625755 0.43519157 0.62825698
		 0.43024495 0.6183911 0.43024495 0.6183911 0.43024495 0.6183911 0.43024495 0.6183911
		 0.70592189 0.0092456341 0.43024495 0.6183911 0.43024495 0.6183911 0.43024495 0.6183911
		 0.43519157 0.62825698 0.43519157 0.62825698 0.43519157 0.62825698 0.43519157 0.62825698
		 0.43519157 0.62825698 0.43519157 0.62825698 0.43519157 0.62825698 0.43519157 0.62825698
		 0.67102861 0.0077046454 0.67102861 0.0077046454 0.67102861 0.0077046454 0.67102861
		 0.0077046454 0.64310914 0.0077046454 0.64310914 0.0077046454 0.64310914 0.0077046454
		 0.64310914 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006
		 0.0077046454 0.70703006 0.0077046454 0.73862308 0.0077046454 0.73862308 0.0077046454
		 0.73862308 0.0077046454 0.73862308 0.0077046454 0.70703006 0.0077046454 0.70703006
		 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.67102861 0.0077046454
		 0.67102861 0.0077046454 0.67102861 0.0077046454 0.67102861 0.0077046454 0.64310914
		 0.0077046454 0.64310914 0.0077046454 0.64310914 0.0077046454 0.64310914 0.0077046454
		 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006
		 0.0077046454 0.73862308 0.0077046454 0.73862308 0.0077046454 0.73862308 0.0077046454
		 0.73862308 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006
		 0.0077046454 0.70703006 0.0077046454 0.67102861 0.0077046454 0.67102861 0.0077046454
		 0.67102861 0.0077046454 0.67102861 0.0077046454 0.64310914 0.0077046454 0.64310914
		 0.0077046454 0.64310914 0.0077046454 0.64310914 0.0077046454 0.70703006 0.0077046454
		 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.73862308
		 0.0077046454 0.73862308 0.0077046454 0.73862308 0.0077046454 0.73862308 0.0077046454
		 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006
		 0.0077046454 0.67102861 0.0077046454 0.67102861 0.0077046454 0.67102861 0.0077046454
		 0.67102861 0.0077046454 0.64310914 0.0077046454 0.64310914 0.0077046454 0.64310914
		 0.0077046454 0.64310914 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454
		 0.70703006 0.0077046454 0.70703006 0.0077046454 0.73862308 0.0077046454 0.73862308
		 0.0077046454 0.73862308 0.0077046454 0.73862308 0.0077046454 0.70703006 0.0077046454
		 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70703006 0.0077046454 0.70592189
		 0.0092456341 0.43519157 0.62825698 0.43519157 0.62825698 0.43519157 0.62825698 0.70592189
		 0.0092456341 0.43519157 0.62825698 0.43519157 0.62825698 0.70592189 0.0092456341
		 0.43519157 0.62825698 0.43519157 0.62825698 0.43519157 0.62825698 0.70592189 0.0092456341
		 0.45716208 0.61625755 0.45716208 0.65154511 0.42849973 0.65154511 0.42904368 0.63418573
		 0.42803118 0.66651899 0.45716208 0.68611956 0.42550379 0.68611956 0.48582342 0.65154511
		 0.48629197 0.66651899 0.48881936 0.68611956 0.4859722 0.61625755 0.48527995 0.63418573;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 89 ".pt[0:88]" -type "float3"  1.9306641 1.9456558 0.071959496 
		1.7362976 1.9456558 -0.12926769 2.3067932 1.9910126 0.46134567 1.3601532 1.9910126 
		-0.51866913 2.2474518 1.9910126 0.51865768 1.3008118 1.9910126 -0.46135712 1.8713226 
		1.9456558 0.12927151 1.6769562 1.9456558 -0.071955681 2.1826172 1.9518127 0.4515419 
		2.2419586 1.9518127 0.39422989 1.4250031 1.9518127 -0.45153809 1.3656616 1.9518127 
		-0.39422607 1.3005371 1.9716339 -0.58038712 2.3664246 1.9716339 0.52307892 2.3070831 
		1.9716339 0.58039093 1.2411957 1.9716339 -0.5230751 2.1634369 1.9651642 0.4316864 
		2.2227783 1.9651642 0.37437439 1.4441528 1.9651642 -0.43170929 1.3848114 1.9651642 
		-0.37439728 1.4301453 1.9772034 -0.44621658 2.2367859 1.9772034 0.38887405 2.1774445 
		1.9772034 0.44618607 1.3708038 1.9772034 -0.38890457 1.7741394 1.9518127 0.028653964 
		1.7741394 1.9651642 0.028653964 1.7741394 1.9716339 0.028653964 1.7741394 1.9772034 
		0.028653964 1.7741394 1.9969177 0.028653964 1.8334808 1.9969177 -0.028658047 1.8334808 
		1.9373703 -0.028658047 1.7741394 1.9373703 0.028653964 1.5374908 1.9654083 -0.33508492 
		1.428299 1.9710693 -0.44812775 1.8334808 1.9436951 -0.028658047 1.7595978 1.9487534 
		-0.10514641 1.5229187 1.9537201 -0.35016251 1.5268402 1.9759521 -0.3461113 1.8334808 
		1.9932404 -0.028658047 1.4736328 1.988739 -0.40119553 2.1401367 1.9759521 0.28880692 
		2.1933289 1.988739 0.34388733 2.2386627 1.9710693 0.39081955 2.1294708 1.9654083 
		0.27777672 2.144043 1.9537201 0.29285431 1.9073792 1.9487534 0.047842026 1.8334808 
		1.9710693 -0.028658047 1.8334808 1.9537201 -0.028658047 1.8334808 1.988739 -0.028658047 
		1.9896851 1.9942169 0.23901558 2.0362701 1.9942169 0.1940136 1.8990936 1.9959412 
		0.051999569 1.8525085 1.9959412 0.097001553 2.0288696 2.0010071 0.25282097 2.0487213 
		2.0010071 0.23365021 2.0134583 2.0014648 0.197155 1.9936066 2.0014648 0.21632576 
		2.2080383 1.9914856 0.46506119 2.2546234 1.9914856 0.4200592 2.1174316 1.9931946 
		0.27803993 2.0708466 1.9931946 0.32304192 2.2472076 1.998291 0.47885895 2.2670593 
		1.998291 0.45968628 2.2318115 1.9987183 0.4231987 2.2119598 1.9987183 0.44236755 
		1.5713501 1.9942169 -0.19407845 1.6179352 1.9942169 -0.23908043 1.7551117 1.9959412 
		-0.097067356 1.7085266 1.9959412 -0.052065372 1.5588989 2.0010071 -0.23371315 1.5787506 
		2.0010071 -0.25288391 1.6139984 2.0014648 -0.21639442 1.5941467 2.0014648 -0.19722366 
		1.3529968 1.9914856 -0.42012405 1.3995819 1.9914856 -0.46512604 1.5367584 1.9931946 
		-0.32311249 1.4901733 1.9931946 -0.2781105 1.3405609 1.998291 -0.45975113 1.3604126 
		1.998291 -0.47891998 1.3956604 1.9987183 -0.44244003 1.3758087 1.9987183 -0.42327118 
		2.1667328 1.9823456 0.31634903 1.8334808 1.9823456 -0.028658047 1.5002289 1.9823456 
		-0.3736496 2.1351929 1.9710693 0.28369904 1.5317688 1.9710693 -0.34100151 2.1367645 
		1.9595642 0.28532028 1.8334656 1.9595642 -0.02865614 1.5301971 1.9595642 -0.34262848;
	setAttr -s 89 ".vt[0:88]"  -0.097187042 -0.67391074 0.028654099 0.097187042 -0.67391074 0.028654099
		 -0.47330856 0.61250824 0.028654099 0.47332382 0.61250824 0.028654099 -0.47330856 0.61250824 -0.028657913
		 0.47332382 0.61250824 -0.028657913 -0.097187042 -0.67391074 -0.028657913 0.097187042 -0.67391074 -0.028657913
		 -0.4084816 -0.49909061 -0.028657913 -0.4084816 -0.49909052 0.028654099 0.4084816 -0.49909052 0.028654099
		 0.4084816 -0.49909061 -0.028657913 0.53293991 0.062748611 0.028654099 -0.53293991 0.062748611 0.028654099
		 -0.53293991 0.062748611 -0.028657913 0.53293991 0.062748611 -0.028657913 -0.3893013 -0.12052768 -0.028657913
		 -0.3893013 -0.12052768 0.028654099 0.38932419 -0.12052768 0.028654099 0.38932419 -0.12052768 -0.028657913
		 0.40333939 0.22084039 0.028654099 -0.40330887 0.22084039 0.028654099 -0.40330887 0.22084039 -0.028657913
		 0.40333939 0.22084039 -0.028657913 3.8146973e-006 -0.49909061 -0.028657913 3.8146973e-006 -0.12052768 -0.028657913
		 3.8146973e-006 0.062748611 -0.028657913 3.8146973e-006 0.22084039 -0.028657913 3.8146973e-006 0.78039044 -0.028657913
		 3.8146973e-006 0.78039044 0.028654099 3.8146973e-006 -0.90890282 0.028654099 3.8146973e-006 -0.90890282 -0.028657913
		 0.2959938 -0.11354357 0.028654099 0.4051857 0.046985447 0.028654099 3.8146973e-006 -0.72949684 0.028654099
		 0.073886871 -0.58588243 0.028654099 0.31055832 -0.44512132 0.028654099 0.30664444 0.18545574 0.028654099
		 3.8146973e-006 0.67555755 0.028654099 0.35985184 0.54796499 0.028654099 -0.30664825 0.18545574 0.028654099
		 -0.35985184 0.54796499 0.028654099 -0.4051857 0.046985447 0.028654099 -0.2959938 -0.11354357 0.028654099
		 -0.31055832 -0.44512132 0.028654099 -0.073890686 -0.58588243 0.028654099 3.8146973e-006 0.046985447 0.028654099
		 3.8146973e-006 -0.44512111 0.028654099 3.8146973e-006 0.54796475 0.028654099 -0.20915604 0.70338017 -0.022483826
		 -0.20915604 0.70338017 0.022518158 -0.071979523 0.75210756 0.022518158 -0.071979523 0.75210756 -0.022483826
		 -0.23496628 0.89638227 -0.0095691681 -0.23496628 0.89638227 0.009601593 -0.19971466 0.90890282 0.009601593
		 -0.19971466 0.90890282 -0.0095691681 -0.42750168 0.62582725 -0.022483826 -0.42750168 0.62582725 0.022518158
		 -0.29032135 0.67455465 0.022518158 -0.29032135 0.67455465 -0.022483826 -0.45330429 0.81882924 -0.0095691681
		 -0.45330429 0.81882924 0.009601593 -0.4180603 0.83134979 0.009601593 -0.4180603 0.83134979 -0.0095691681
		 0.20918655 0.70338017 -0.022483826 0.20918655 0.70338017 0.022518158 0.07201004 0.75210756 0.022518158
		 0.07201004 0.75210756 -0.022483826 0.2349968 0.89638227 -0.0095691681 0.2349968 0.89638227 0.009601593
		 0.19974899 0.90890282 0.009601593 0.19974899 0.90890282 -0.0095691681 0.4275322 0.62582725 -0.022483826
		 0.4275322 0.62582725 0.022518158 0.29035568 0.67455465 0.022518158 0.29035568 0.67455465 -0.022483826
		 0.45333481 0.81882924 -0.0095691681 0.45333481 0.81882924 0.009601593 0.41809464 0.83134979 0.009601593
		 0.41809464 0.83134979 -0.0095691681 -0.33325195 0.36671036 0.028654099 3.8146973e-006 0.36671036 0.028654099
		 0.33324814 0.36671036 0.028654099 -0.30171585 0.046985447 0.028656006 0.30170822 0.046985447 0.028654099
		 -0.30327988 -0.2793324 0.028654099 3.8146973e-006 -0.2793324 0.028652191 0.30327988 -0.2793324 0.028654099;
	setAttr -s 161 ".ed[0:160]"  85 32 1 32 33 0 33 85 1 28 29 1 29 3 0 3 5 0
		 5 28 0 25 26 1 26 15 1 15 19 0 19 25 1 30 31 1 31 7 0 7 1 0 1 30 0 18 19 1 15 12 1
		 12 18 0 16 17 1 17 13 0 13 14 1 14 16 0 6 0 0 0 9 0 9 8 1 8 6 0 7 11 0 11 10 1 10 1 0
		 7 24 1 24 11 1 13 21 0 21 22 1 22 14 0 26 27 1 27 23 1 23 15 0 23 20 1 20 12 0 9 17 0
		 16 8 0 11 19 0 18 10 0 24 25 1 21 2 0 2 4 0 4 22 0 27 28 1 5 23 0 3 20 0 16 25 1
		 24 8 1 14 26 1 22 27 1 4 28 0 2 29 0 6 31 0 30 0 0 24 6 1 12 33 1 32 18 1 1 35 1
		 35 34 1 34 30 1 10 36 1 36 35 0 20 37 1 37 33 1 32 88 0 88 36 0 3 39 1 39 83 1 83 37 1
		 29 38 1 38 39 1 2 41 1 41 38 1 21 40 1 40 81 0 81 41 0 13 42 1 42 40 0 17 43 1 43 42 0
		 9 44 1 44 86 0 86 43 0 34 45 1 45 0 1 45 44 0 86 87 1 87 46 1 46 84 1 84 43 1 48 39 1
		 38 48 1 41 48 1 34 47 1 47 45 1 35 47 1 81 82 1 82 48 1 82 83 1 53 54 0 54 55 0 55 56 0
		 56 53 0 49 50 0 50 54 0 53 49 0 50 51 0 51 55 0 51 52 0 52 56 0 52 49 0 61 62 0 62 63 0
		 63 64 0 64 61 0 57 58 0 58 62 0 61 57 0 58 59 0 59 63 0 59 60 0 60 64 0 60 57 0 69 72 0
		 72 71 0 71 70 0 70 69 0 65 69 0 70 66 0 66 65 0 71 67 0 67 66 0 72 68 0 68 67 0 65 68 0
		 77 80 0 80 79 0 79 78 0 78 77 0 73 77 0 78 74 0 74 73 0 79 75 0 75 74 0 80 76 0 76 75 0
		 73 76 0 40 84 1 46 82 1 37 85 1 42 84 1 46 85 1 87 88 1 44 47 1 47 87 1 47 36 1 31 24 1;
	setAttr -s 306 ".n";
	setAttr ".n[0:165]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr ".n[166:305]" -type "float3"  1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020;
	setAttr -s 78 -ch 306 ".fc[0:77]" -type "polyFaces" 
		f 3 0 1 2
		mu 0 3 209 100 102
		f 4 3 4 5 6
		mu 0 4 61 62 5 6
		f 4 7 8 9 10
		mu 0 4 57 58 38 46
		f 4 11 12 13 14
		mu 0 4 84 85 13 14
		f 4 15 -10 16 17
		mu 0 4 45 77 39 74
		f 4 18 19 20 21
		mu 0 4 40 79 34 73
		f 4 22 23 24 25
		mu 0 4 20 21 26 82
		f 4 -14 26 27 28
		mu 0 4 16 17 31 80
		f 3 29 30 -27
		mu 0 3 10 56 30
		f 4 -21 31 32 33
		mu 0 4 36 75 50 71
		f 4 34 35 36 -9
		mu 0 4 58 59 54 38
		f 4 -17 -37 37 38
		mu 0 4 33 72 55 49
		f 4 -25 39 -19 40
		mu 0 4 24 83 42 78
		f 4 -28 41 -16 42
		mu 0 4 29 81 47 76
		f 4 43 -11 -42 -31
		mu 0 4 56 57 46 30
		f 4 -33 44 45 46
		mu 0 4 52 70 22 23
		f 4 47 -7 48 -36
		mu 0 4 59 60 9 54
		f 4 -38 -49 -6 49
		mu 0 4 69 68 18 19
		f 4 50 -44 51 -41
		mu 0 4 41 57 56 25
		f 4 52 -8 -51 -22
		mu 0 4 37 58 57 41
		f 4 53 -35 -53 -34
		mu 0 4 53 59 58 37
		f 4 54 -48 -54 -47
		mu 0 4 8 60 59 53
		f 4 55 -4 -55 -46
		mu 0 4 4 62 61 7
		f 4 56 -12 57 -23
		mu 0 4 12 66 65 15
		f 3 -52 58 -26
		mu 0 3 25 56 11
		f 4 -18 59 -2 60
		mu 0 4 44 32 86 111
		f 4 -15 61 62 63
		mu 0 4 64 1 87 101
		f 4 -29 64 65 -62
		mu 0 4 1 28 88 87
		f 4 -39 66 67 -60
		mu 0 4 32 48 89 86
		f 5 -43 -61 68 69 -65
		mu 0 5 28 44 111 214 88
		f 5 -50 70 71 72 -67
		mu 0 5 48 2 90 207 89
		f 4 -5 73 74 -71
		mu 0 4 2 63 91 90
		f 4 -56 75 76 -74
		mu 0 4 63 3 92 91
		f 5 -45 77 78 79 -76
		mu 0 5 3 51 93 203 92
		f 4 -32 80 81 -78
		mu 0 4 51 35 94 93
		f 4 -20 82 83 -81
		mu 0 4 35 43 95 94
		f 5 -40 84 85 86 -83
		mu 0 5 43 27 96 210 95
		f 4 -58 -64 87 88
		mu 0 4 0 64 101 98
		f 4 -24 -89 89 -85
		mu 0 4 27 0 98 96
		f 5 90 91 92 93 -87
		mu 0 5 105 215 216 217 218
		f 3 94 -75 95
		mu 0 3 97 103 104
		f 3 96 -96 -77
		mu 0 3 107 97 104
		f 3 97 98 -88
		mu 0 3 108 109 112
		f 3 99 -98 -63
		mu 0 3 114 109 108
		f 4 100 101 -97 -80
		mu 0 4 204 205 118 119
		f 4 102 -72 -95 -102
		mu 0 4 205 206 122 118
		f 4 103 104 105 106
		mu 0 4 123 124 125 126
		f 4 107 108 -104 109
		mu 0 4 127 128 129 130
		f 4 110 111 -105 -109
		mu 0 4 131 132 133 134
		f 4 112 113 -106 -112
		mu 0 4 135 136 137 138
		f 4 114 -110 -107 -114
		mu 0 4 139 140 141 142
		f 4 115 116 117 118
		mu 0 4 143 144 145 146
		f 4 119 120 -116 121
		mu 0 4 147 148 149 150
		f 4 122 123 -117 -121
		mu 0 4 151 152 153 154
		f 4 124 125 -118 -124
		mu 0 4 155 156 157 158
		f 4 126 -122 -119 -126
		mu 0 4 159 160 161 162
		f 4 127 128 129 130
		mu 0 4 163 164 165 166
		f 4 131 -131 132 133
		mu 0 4 167 168 169 170
		f 4 -133 -130 134 135
		mu 0 4 171 172 173 174
		f 4 -135 -129 136 137
		mu 0 4 175 176 177 178
		f 4 -137 -128 -132 138
		mu 0 4 179 180 181 182
		f 4 139 140 141 142
		mu 0 4 183 184 185 186
		f 4 143 -143 144 145
		mu 0 4 187 188 189 190
		f 4 -145 -142 146 147
		mu 0 4 191 192 193 194
		f 4 -147 -141 148 149
		mu 0 4 195 196 197 198
		f 4 -149 -140 -144 150
		mu 0 4 199 200 201 202
		f 5 151 -93 152 -101 -79
		mu 0 5 219 217 216 220 221
		f 3 153 -3 -68
		mu 0 3 121 209 102
		f 3 154 -152 -82
		mu 0 3 116 208 120
		f 3 -94 -155 -84
		mu 0 3 117 208 116
		f 5 155 -154 -73 -103 -153
		mu 0 5 216 222 223 224 220
		f 5 156 -69 -1 -156 -92
		mu 0 5 215 225 226 222 216
		f 4 157 158 -91 -86
		mu 0 4 115 106 212 211
		f 4 -70 -157 -159 159
		mu 0 4 99 213 212 106
		f 3 160 -30 -13
		mu 0 3 67 56 10
		f 3 -59 -161 -57
		mu 0 3 11 56 67
		f 3 -160 -100 -66
		mu 0 3 113 109 114
		f 3 -99 -158 -90
		mu 0 3 112 109 110;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Center" -p "mainCtrl";
	rename -uid "98BC38DF-4EEC-4C2A-75F6-11AB2CE130DF";
	setAttr ".rp" -type "double3" -0.0084625041856726379 1.1025398360678857 0 ;
	setAttr ".sp" -type "double3" -0.0084625041856726379 1.1025398360678857 0 ;
createNode nurbsCurve -n "CenterShape" -p "Center";
	rename -uid "A5B5E2BC-4878-7C7A-5F37-D1AD04E03277";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.50635662956179472 1.102539836067886 -0.51481913374746668
		-0.0084625041856727194 1.102539836067886 -0.72806420111483627
		-0.52328163793313964 1.102539836067886 -0.51481913374746691
		-0.7365267053005089 1.102539836067886 -2.1097479075492468e-016
		-0.52328163793313975 1.102539836067886 0.5148191337474668
		-0.0084625041856728565 1.102539836067886 0.72806420111483638
		0.50635662956179395 1.102539836067886 0.51481913374746702
		0.71960169692916343 1.102539836067886 3.9104470267870917e-016
		0.50635662956179472 1.102539836067886 -0.51481913374746668
		-0.0084625041856727194 1.102539836067886 -0.72806420111483627
		-0.52328163793313964 1.102539836067886 -0.51481913374746691
		;
createNode transform -n "Hips" -p "Center";
	rename -uid "06794DE9-457B-311A-8FEC-49A52DB4AEFF";
	setAttr ".rp" -type "double3" -0.0084625041856726379 1.1025398360678862 0 ;
	setAttr ".sp" -type "double3" -0.0084625041856726379 1.1025398360678862 0 ;
createNode nurbsCurve -n "HipsShape" -p "|mainCtrl|Center|Hips";
	rename -uid "87FF9ACC-4AD5-51F3-4FE2-918799E91E1F";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.50893786175346711 1.1025398360678864 -0.51740036593910232
		-0.0084625041857241904 1.1025398360678864 -0.51437721396163161
		-0.52586287012463695 1.1025398360678864 -0.51740036593905514
		-0.52283971814740715 1.1025398360678864 1.0028608060344278e-014
		-0.52586287012472255 1.1025398360678864 0.5174003659390457
		-0.008462504185708335 1.1025398360678864 0.51437721396167901
		0.50893786175343625 1.1025398360678864 0.51740036593905592
		0.50591470977592612 1.1025398360678864 5.8455839313393204e-014
		0.50893786175346711 1.1025398360678864 -0.51740036593910232
		-0.0084625041857241904 1.1025398360678864 -0.51437721396163161
		-0.52586287012463695 1.1025398360678864 -0.51740036593905514
		;
createNode transform -n "Spine" -p "Center";
	rename -uid "6BF7C7A3-44EE-077A-829A-00AEFB4B49A3";
	setAttr ".rp" -type "double3" -0.011880053952968079 1.5499473688316217 0 ;
	setAttr ".sp" -type "double3" -0.011880053952968079 1.5499473688316217 0 ;
createNode nurbsCurve -n "SpineShape" -p "Spine";
	rename -uid "9A6D879F-4B30-EDD5-B43D-198D0812C58D";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.45132462048896105 1.5499473688316217 -0.46320467444192853
		-0.011880053952968157 1.5499473688316217 -0.65507033275038984
		-0.47508472839489679 1.5499473688316217 -0.46320467444192875
		-0.66695038670335804 1.5499473688316217 -1.8982299386530864e-016
		-0.47508472839489696 1.5499473688316217 0.46320467444192864
		-0.011880053952968277 1.5499473688316217 0.65507033275039006
		0.45132462048896038 1.5499473688316217 0.46320467444192881
		0.64319027879742174 1.5499473688316217 3.5183955358851021e-016
		0.45132462048896105 1.5499473688316217 -0.46320467444192853
		-0.011880053952968157 1.5499473688316217 -0.65507033275038984
		-0.47508472839489679 1.5499473688316217 -0.46320467444192875
		;
createNode transform -n "Chest_Ctrl" -p "Spine";
	rename -uid "E696C010-4614-272A-F273-5EB6A439DEE4";
	setAttr ".rp" -type "double3" -0.013665898595399828 1.9547525304752105 0 ;
	setAttr ".sp" -type "double3" -0.01366589859539988 1.9547525304752122 0 ;
createNode nurbsCurve -n "Chest_CtrlShape" -p "Chest_Ctrl";
	rename -uid "36406DDD-4ACB-3C0B-6DC9-C6B33C1EF62D";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.44953877584652929 1.9547525304752122 -0.46320467444192853
		-0.013665898595399949 1.9547525304752122 -0.65507033275038984
		-0.47687057303732855 1.9547525304752122 -0.46320467444192875
		-0.66873623134578974 1.9547525304752122 -1.8982299386530864e-016
		-0.47687057303732872 1.9547525304752122 0.46320467444192864
		-0.013665898595400072 1.9547525304752122 0.65507033275039006
		0.44953877584652868 1.9547525304752122 0.46320467444192881
		0.64140443415498993 1.9547525304752122 3.5183955358851021e-016
		0.44953877584652929 1.9547525304752122 -0.46320467444192853
		-0.013665898595399949 1.9547525304752122 -0.65507033275038984
		-0.47687057303732855 1.9547525304752122 -0.46320467444192875
		;
createNode transform -n "HeadCtrl" -p "Chest_Ctrl";
	rename -uid "9834FBFE-4A0D-94B0-F0B8-2C8087D9867F";
	setAttr ".rp" -type "double3" -0.017194450050600343 2.1954951166504797 1.2110912251854579e-015 ;
	setAttr ".sp" -type "double3" -0.017194450050600343 2.1954951166504797 1.2110912251854579e-015 ;
createNode nurbsCurve -n "HeadCtrlShape" -p "HeadCtrl";
	rename -uid "990E457C-4E4B-8FE8-D8CE-5AB92EB8FF6F";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 12 2 no 3
		17 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
		15
		-0.21344766841042645 3.6888817617099772 -0.00030583903007655114
		-0.79493577639865909 3.6006906451894851 8.1951342314174756e-005
		-0.88107147573640654 3.0194360026961298 -2.1966339120804405e-005
		-0.88104689589835394 2.627314470881247 5.9140144645491361e-006
		-0.79500951591284297 2.045526578375974 -1.6897184249305989e-006
		-0.21317729019168968 1.959468461903211 8.4485921397916364e-007
		0.17878764524690649 1.9594824947982632 -1.6897183898089533e-006
		0.76062434002955148 2.0454844796908063 5.9140143864337519e-006
		0.84664384376915169 2.6274688327268034 -2.1966339209214068e-005
		0.84673545952925588 3.0188606539990324 8.1951342331735579e-005
		0.76034949274932306 3.6028376781322979 -0.00030583903008926758
		0.17979541860773587 3.6808689786358104 0.0011414047779902132
		-0.21344766841042645 3.6888817617099772 -0.00030583903007655114
		-0.79493577639865909 3.6006906451894851 8.1951342314174756e-005
		-0.88107147573640654 3.0194360026961298 -2.1966339120804405e-005
		;
createNode transform -n "ct_L_Foot" -p "mainCtrl";
	rename -uid "4710768F-4FC1-2139-9A89-76A9BCC66361";
	addAttr -ci true -sn "Heel_Lift" -ln "Heel_Lift" -min -10 -max 10 -at "double";
	setAttr ".rp" -type "double3" 0.23655353054188791 0.18701948746465408 -0.024560997684383083 ;
	setAttr ".sp" -type "double3" 0.23655353054188791 0.18701948746465408 -0.024560997684383083 ;
	setAttr -k on ".Heel_Lift";
createNode nurbsCurve -n "ct_L_FootShape" -p "ct_L_Foot";
	rename -uid "ADE93456-4B3A-F119-67BE-809095183453";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.418680937895129 0.18701948746465408 -0.26476447681293269
		0.23655353054188416 0.18701948746465408 -0.26912891178596582
		0.054426123188659761 0.18701948746465411 -0.26476447681293097
		0.038079271633283902 0.18701948746465411 -0.024560997684381903
		0.054426123188653384 0.18701948746465411 0.21564248144416168
		0.23655353054188549 0.18701948746465408 0.22000691641720319
		0.41868093789512645 0.18701948746465408 0.21564248144416354
		0.43502778945048359 0.18701948746465411 -0.024560997684378631
		0.418680937895129 0.18701948746465408 -0.26476447681293269
		0.23655353054188416 0.18701948746465408 -0.26912891178596582
		0.054426123188659761 0.18701948746465411 -0.26476447681293097
		;
createNode transform -n "ct_R_Foot" -p "mainCtrl";
	rename -uid "C776BE5D-4036-2E2C-64D7-D3BD158F67DB";
	setAttr ".rp" -type "double3" -0.2534786755096245 0.18795588840508209 -0.024515979330443208 ;
	setAttr ".sp" -type "double3" -0.2534786755096245 0.18795588840508209 -0.024515979330443208 ;
createNode nurbsCurve -n "ct_R_FootShape" -p "ct_R_Foot";
	rename -uid "C574D281-462B-C673-216C-8CBF5C077F56";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		-0.071351268156383263 0.18795588840508212 -0.26471945845899286
		-0.25347867550962816 0.18795588840508209 -0.26908389343202593
		-0.43560608286285257 0.18795588840508204 -0.26471945845899109
		-0.45195293441822837 0.18795588840508204 -0.024515979330442025
		-0.43560608286285901 0.18795588840508204 0.21568749979810142
		-0.25347867550962694 0.18795588840508209 0.2200519347711431
		-0.071351268156385844 0.18795588840508212 0.21568749979810348
		-0.055004416601028783 0.18795588840508212 -0.02451597933043875
		-0.071351268156383263 0.18795588840508212 -0.26471945845899286
		-0.25347867550962816 0.18795588840508209 -0.26908389343202593
		-0.43560608286285257 0.18795588840508204 -0.26471945845899109
		;
createNode transform -n "persp1";
	rename -uid "09001436-4DFF-6E72-9FFF-028ED74A409A";
	setAttr ".t" -type "double3" -401.97599507629656 126.5246423893922 -450.36487212752877 ;
	setAttr ".r" -type "double3" 1.4616472702908487 -858.19999999977199 0 ;
	setAttr ".rp" -type "double3" 0 -2.8421709430404007e-014 0 ;
	setAttr ".rpt" -type "double3" -1.9782071343340278e-015 1.1421724148145341e-016 
		1.6019205517361378e-015 ;
createNode camera -n "perspShape2" -p "persp1";
	rename -uid "EEC3C352-4916-D3CB-9D3D-F5BBF49C6D03";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 663.24582278120147;
	setAttr ".imn" -type "string" "persp1";
	setAttr ".den" -type "string" "persp1_depth";
	setAttr ".man" -type "string" "persp1_mask";
	setAttr ".tp" -type "double3" 12.419658728308534 181.87154664409937 36.417881977880207 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -n "pCube1";
	rename -uid "1608DA75-4884-0A4B-9D16-FCAEC44B171A";
	setAttr ".t" -type "double3" -53.471382925420045 209.78452728709726 0 ;
	setAttr ".s" -type "double3" 5.7174127903835217 5.7174127903835217 852.03959571277608 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "F57D06C2-4C41-783D-27D1-0590DA6F5C07";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "661E7A7E-402E-F926-56A4-2CA2A971BC05";
	setAttr ".t" -type "double3" -25.695099292516744 213.51527287389604 0 ;
	setAttr ".s" -type "double3" 5.7174127903835217 5.7174127903835217 852.03959571277608 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "361AE20F-41E2-5E2D-D85D-AAB256D5E2CF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "B8ADBDBF-4EAC-123D-1120-6A8B7E2E6B32";
	setAttr ".t" -type "double3" -48.701238549544719 0 189.66437960834455 ;
	setAttr ".r" -type "double3" 0 -53.173712415984014 0 ;
	setAttr ".s" -type "double3" 19.624400026377767 13.153038955734713 34.113245372617008 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "62FF0FAA-4E69-EDD3-8CEA-5D90EFF902B8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "bottom";
	rename -uid "7F1C28D8-44E2-CB42-CDA3-5FA7DB2B6299";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 13.325680686808568 -5003.7730251337007 274.87904213315619 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
createNode camera -n "bottomShape" -p "bottom";
	rename -uid "38CE369E-4BF9-0294-7829-6E85FD783FA1";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 5000.1000000000004;
	setAttr ".ow" 243.5293475054913;
	setAttr ".imn" -type "string" "bottom1";
	setAttr ".den" -type "string" "bottom1_depth";
	setAttr ".man" -type "string" "bottom1_mask";
	setAttr ".hc" -type "string" "viewSet -bo %camera";
	setAttr ".o" yes;
createNode transform -n "pCube4";
	rename -uid "3428EEEF-4202-EF86-D333-66B14552591A";
	setAttr ".t" -type "double3" 30.193422272174566 0 266.62049873467976 ;
	setAttr ".r" -type "double3" 0 39.80839079551847 0 ;
	setAttr ".s" -type "double3" 19.624400026377767 13.153038955734713 34.113245372617008 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "39B7DAD3-4098-A52B-995F-C0B3A065D595";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "back";
	rename -uid "CB22F05C-4C05-BF2D-C0E2-3085951028F7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.3857251998677995 51.312984838453488 -5000.1000000000004 ;
	setAttr ".r" -type "double3" 0 180 0 ;
createNode camera -n "backShape" -p "back";
	rename -uid "F2E14353-47C1-99A2-B7BB-C5A3A7BB9C17";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 5000.1000000000004;
	setAttr ".ow" 310.07077163065486;
	setAttr ".imn" -type "string" "back1";
	setAttr ".den" -type "string" "back1_depth";
	setAttr ".man" -type "string" "back1_mask";
	setAttr ".hc" -type "string" "viewSet -b %camera";
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DDF84051-4C72-94A9-ED74-6F957CD21B88";
	setAttr -s 5 ".lnk";
	setAttr -s 5 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "FCCDBA6A-478D-5831-976B-298F4C030720";
	setAttr ".cdl" 1;
	setAttr -s 9 ".dli[1:8]"  1 9 3 4 5 6 7 8;
	setAttr -s 9 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "1279D119-42DC-873E-68C0-1EB5BC192B30";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "6A84D915-4387-18AF-8E81-A1BAB13127FB";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "4F945100-4C40-188D-412D-F787D4CAC087";
	setAttr ".g" yes;
createNode phong -n "Default_Material";
	rename -uid "28D87FB7-4007-3CBA-FA84-7E858D87DB3D";
	setAttr ".dc" 1;
	setAttr ".ambc" -type "float3" 0.5 0.5 0.5 ;
	setAttr ".sc" -type "float3" 0 0 0 ;
	setAttr ".rfl" 1;
	setAttr ".cp" 2;
createNode shadingEngine -n "SF_Character_Knight_01SG";
	rename -uid "5C6F318B-44DA-D3C7-4574-40A8B20F364F";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "CC901524-4A22-363A-87A3-ACBA01048F96";
createNode file -n "file1";
	rename -uid "FF883820-4267-EBF2-FE69-E7BA33262100";
	setAttr ".ftn" -type "string" "E:/BackUp/Workspace/3D/ArtMotionStudios-BennyDz/AMS-AnimPj//scenes/Textures/Character_Knight01_White.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "B975B498-4738-F26B-3FF5-BE92479BE0E3";
createNode displayLayer -n "Ground";
	rename -uid "282F70EA-4D18-BEA5-2105-CDA5215349D4";
	setAttr ".dt" 1;
	setAttr ".do" 1;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "AA721AEB-4699-C68D-E6E8-39BBCA7E9DC9";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp1\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 706\n                -height 509\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp1\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n"
		+ "            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 706\n            -height 509\n            -sceneRenderFilter 0\n            $editorName;\n"
		+ "        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"left\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n"
		+ "                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"left\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"left\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"left\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 1\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 0\n                -imagePlane 1\n                -joints 1\n"
		+ "                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 707\n                -height 509\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 707\n            -height 509\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 1\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 1\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n"
		+ "                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -clipTime \"on\" \n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n"
		+ "                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n"
		+ "                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n"
		+ "                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 0.835\n"
		+ "                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 5\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"largeIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 0.835\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 5\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"largeIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"top3\\\" -ps 1 50 72 -ps 2 50 72 -ps 3 100 28 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 707\\n    -height 509\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 707\\n    -height 509\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp1\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 706\\n    -height 509\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"persp1\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 1\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 706\\n    -height 509\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Graph Editor\")) \n\t\t\t\t\t\"scriptedPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `scriptedPanel -unParent  -type \\\"graphEditor\\\" -l (localizedPanelLabel(\\\"Graph Editor\\\")) -mbv $menusOkayInPanels `;\\n\\n\\t\\t\\t$editorName = ($panelName+\\\"OutlineEd\\\");\\n            outlinerEditor -e \\n                -showShapes 1\\n                -showReferenceNodes 0\\n                -showReferenceMembers 0\\n                -showAttributes 1\\n                -showConnected 1\\n                -showAnimCurvesOnly 1\\n                -showMuteInfo 0\\n                -organizeByLayer 1\\n                -showAnimLayerWeight 1\\n                -autoExpandLayers 1\\n                -autoExpand 1\\n                -showDagOnly 0\\n                -showAssets 1\\n                -showContainedOnly 0\\n                -showPublishedAsConnected 0\\n                -showContainerContents 0\\n                -ignoreDagHierarchy 0\\n                -expandConnections 1\\n                -showUpstreamCurves 1\\n                -showUnitlessCurves 1\\n                -showCompounds 0\\n                -showLeafs 1\\n                -showNumericAttrsOnly 1\\n                -highlightActive 0\\n                -autoSelectNewObjects 1\\n                -doNotSelectNewObjects 0\\n                -dropIsParent 1\\n                -transmitFilters 1\\n                -setFilter \\\"0\\\" \\n                -showSetMembers 0\\n                -allowMultiSelection 1\\n                -alwaysToggleSelect 0\\n                -directSelect 0\\n                -displayMode \\\"DAG\\\" \\n                -expandObjects 0\\n                -setsIgnoreFilters 1\\n                -containersIgnoreFilters 0\\n                -editAttrName 0\\n                -showAttrValues 0\\n                -highlightSecondary 0\\n                -showUVAttrsOnly 0\\n                -showTextureNodesOnly 0\\n                -attrAlphaOrder \\\"default\\\" \\n                -animLayerFilterOptions \\\"allAffecting\\\" \\n                -sortOrder \\\"none\\\" \\n                -longNames 0\\n                -niceNames 1\\n                -showNamespace 1\\n                -showPinIcons 1\\n                -mapMotionTrails 1\\n                -ignoreHiddenAttribute 0\\n                -ignoreOutlinerColor 0\\n                $editorName;\\n\\n\\t\\t\\t$editorName = ($panelName+\\\"GraphEd\\\");\\n            animCurveEditor -e \\n                -displayKeys 1\\n                -displayTangents 0\\n                -displayActiveKeys 0\\n                -displayActiveKeyTangents 1\\n                -displayInfinities 0\\n                -autoFit 0\\n                -snapTime \\\"integer\\\" \\n                -snapValue \\\"none\\\" \\n                -showResults \\\"off\\\" \\n                -showBufferCurves \\\"off\\\" \\n                -smoothness \\\"fine\\\" \\n                -resultSamples 1\\n                -resultScreenSamples 0\\n                -resultUpdate \\\"delayed\\\" \\n                -showUpstreamCurves 1\\n                -clipTime \\\"on\\\" \\n                -stackedCurves 0\\n                -stackedCurvesMin -1\\n                -stackedCurvesMax 1\\n                -stackedCurvesSpace 0.2\\n                -displayNormalized 0\\n                -preSelectionHighlight 0\\n                -constrainDrag 0\\n                -classicMode 1\\n                $editorName\"\n"
		+ "\t\t\t\t\t\"scriptedPanel -edit -l (localizedPanelLabel(\\\"Graph Editor\\\")) -mbv $menusOkayInPanels  $panelName;\\n\\n\\t\\t\\t$editorName = ($panelName+\\\"OutlineEd\\\");\\n            outlinerEditor -e \\n                -showShapes 1\\n                -showReferenceNodes 0\\n                -showReferenceMembers 0\\n                -showAttributes 1\\n                -showConnected 1\\n                -showAnimCurvesOnly 1\\n                -showMuteInfo 0\\n                -organizeByLayer 1\\n                -showAnimLayerWeight 1\\n                -autoExpandLayers 1\\n                -autoExpand 1\\n                -showDagOnly 0\\n                -showAssets 1\\n                -showContainedOnly 0\\n                -showPublishedAsConnected 0\\n                -showContainerContents 0\\n                -ignoreDagHierarchy 0\\n                -expandConnections 1\\n                -showUpstreamCurves 1\\n                -showUnitlessCurves 1\\n                -showCompounds 0\\n                -showLeafs 1\\n                -showNumericAttrsOnly 1\\n                -highlightActive 0\\n                -autoSelectNewObjects 1\\n                -doNotSelectNewObjects 0\\n                -dropIsParent 1\\n                -transmitFilters 1\\n                -setFilter \\\"0\\\" \\n                -showSetMembers 0\\n                -allowMultiSelection 1\\n                -alwaysToggleSelect 0\\n                -directSelect 0\\n                -displayMode \\\"DAG\\\" \\n                -expandObjects 0\\n                -setsIgnoreFilters 1\\n                -containersIgnoreFilters 0\\n                -editAttrName 0\\n                -showAttrValues 0\\n                -highlightSecondary 0\\n                -showUVAttrsOnly 0\\n                -showTextureNodesOnly 0\\n                -attrAlphaOrder \\\"default\\\" \\n                -animLayerFilterOptions \\\"allAffecting\\\" \\n                -sortOrder \\\"none\\\" \\n                -longNames 0\\n                -niceNames 1\\n                -showNamespace 1\\n                -showPinIcons 1\\n                -mapMotionTrails 1\\n                -ignoreHiddenAttribute 0\\n                -ignoreOutlinerColor 0\\n                $editorName;\\n\\n\\t\\t\\t$editorName = ($panelName+\\\"GraphEd\\\");\\n            animCurveEditor -e \\n                -displayKeys 1\\n                -displayTangents 0\\n                -displayActiveKeys 0\\n                -displayActiveKeyTangents 1\\n                -displayInfinities 0\\n                -autoFit 0\\n                -snapTime \\\"integer\\\" \\n                -snapValue \\\"none\\\" \\n                -showResults \\\"off\\\" \\n                -showBufferCurves \\\"off\\\" \\n                -smoothness \\\"fine\\\" \\n                -resultSamples 1\\n                -resultScreenSamples 0\\n                -resultUpdate \\\"delayed\\\" \\n                -showUpstreamCurves 1\\n                -clipTime \\\"on\\\" \\n                -stackedCurves 0\\n                -stackedCurvesMin -1\\n                -stackedCurvesMax 1\\n                -stackedCurvesSpace 0.2\\n                -displayNormalized 0\\n                -preSelectionHighlight 0\\n                -constrainDrag 0\\n                -classicMode 1\\n                $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "777462A5-49B9-A139-87A3-B8B051811F17";
	setAttr ".b" -type "string" "playbackOptions -min 6 -max 64 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode displayLayer -n "Bones";
	rename -uid "39C4A91F-4A67-211F-CE60-8E89863D554A";
	setAttr ".do" 2;
createNode displayLayer -n "ModeloCompleto";
	rename -uid "1225D4EC-4C6D-C7EF-CF31-E1B0EAB2CA0A";
	setAttr ".c" 22;
	setAttr ".do" 3;
createNode ikRPsolver -n "ikRPsolver";
	rename -uid "33297725-44E0-7423-7427-E58A684CA0BF";
createNode displayLayer -n "Controls";
	rename -uid "9C1B484C-4AF5-14E8-EADD-8783962B8742";
	setAttr ".c" 18;
	setAttr ".do" 4;
createNode lambert -n "lambert2";
	rename -uid "A4FE9DF2-42B0-9129-CB46-A8B3B3879362";
	setAttr ".c" -type "float3" 0.5 0.44161603 0.24700001 ;
createNode shadingEngine -n "SF_Wep_Undead_Shield_06SG";
	rename -uid "2B56FB8C-4F3C-3B60-3E17-EC81461C1E98";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "CE1DE697-45EA-162C-8966-828ACED6B883";
createNode file -n "file148";
	rename -uid "82129692-4A76-7EF4-6E18-20949FABA966";
	setAttr ".ftn" -type "string" "U:/Dropbox/SyntyStudios/SimpleMedieval/_Working/Justin/Textures/Medieval_Texture.psd";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture2";
	rename -uid "0773273C-45E5-C496-3338-988768A7BD54";
createNode lambert -n "lambert3";
	rename -uid "94A955CE-41A7-CC3F-F508-CE8B10D4EF06";
	setAttr ".c" -type "float3" 0.19311 0.25801784 0.61500001 ;
createNode shadingEngine -n "SF_Wep_Undead_Longsword_01SG";
	rename -uid "C2282C64-4805-0E20-1B99-B094AD4F9827";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "634DD4B6-4C3A-F0B4-B4EB-A7B91ADE6754";
createNode file -n "file149";
	rename -uid "B9EC7B7C-449F-99B9-CD0E-7E922A919E29";
	setAttr ".ftn" -type "string" "U:/Dropbox/SyntyStudios/SimpleMedieval/_Working/Justin/Textures/Medieval_Texture.psd";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture3";
	rename -uid "08B26DF9-4E34-306E-AE00-D78B674736A3";
createNode displayLayer -n "SwordModel";
	rename -uid "9C7B92D8-43E6-EB45-062F-52BF0218875A";
	setAttr ".c" 17;
	setAttr ".do" 5;
createNode displayLayer -n "ShiedModel";
	rename -uid "09EB98C2-45E4-3496-44A2-62B69C9282A6";
	setAttr ".c" 22;
	setAttr ".do" 6;
createNode displayLayer -n "G";
	rename -uid "57DD26A8-48A6-49F9-1B0C-1DBED742248E";
	setAttr ".c" 16;
	setAttr ".do" 7;
createNode skinCluster -n "skinCluster1";
	rename -uid "1ED69726-4BF7-1B51-6645-7BA3FD3B526F";
	setAttr ".skm" 1;
	setAttr -s 250 ".wl";
	setAttr ".wl[0].w[3]"  1;
	setAttr ".wl[1].w[3]"  1;
	setAttr ".wl[2].w[3]"  1;
	setAttr ".wl[3].w[3]"  1;
	setAttr ".wl[4].w[3]"  1;
	setAttr ".wl[5].w[3]"  1;
	setAttr ".wl[6].w[3]"  1;
	setAttr ".wl[7].w[3]"  1;
	setAttr ".wl[8].w[3]"  1;
	setAttr ".wl[9].w[3]"  1;
	setAttr ".wl[10].w[3]"  1;
	setAttr ".wl[11].w[3]"  1;
	setAttr ".wl[12].w[3]"  1;
	setAttr ".wl[13].w[3]"  1;
	setAttr ".wl[14].w[3]"  1;
	setAttr ".wl[15].w[3]"  1;
	setAttr ".wl[16].w[9]"  1;
	setAttr ".wl[17].w[9]"  1;
	setAttr ".wl[18].w[9]"  1;
	setAttr ".wl[19].w[9]"  1;
	setAttr ".wl[20].w[8]"  1;
	setAttr ".wl[21].w[8]"  1;
	setAttr ".wl[22].w[9]"  1;
	setAttr ".wl[23].w[9]"  1;
	setAttr ".wl[24].w[8]"  1;
	setAttr ".wl[25].w[9]"  1;
	setAttr ".wl[26].w[9]"  1;
	setAttr ".wl[27].w[8]"  1;
	setAttr ".wl[28].w[8]"  1;
	setAttr ".wl[29].w[8]"  1;
	setAttr ".wl[30].w[8]"  1;
	setAttr ".wl[31].w[8]"  1;
	setAttr ".wl[32].w[7]"  1;
	setAttr ".wl[33].w[7]"  1;
	setAttr ".wl[34].w[8]"  1;
	setAttr ".wl[35].w[8]"  1;
	setAttr ".wl[36].w[8]"  1;
	setAttr ".wl[37].w[8]"  1;
	setAttr ".wl[38].w[7]"  1;
	setAttr ".wl[39].w[7]"  1;
	setAttr ".wl[40].w[7]"  1;
	setAttr ".wl[41].w[7]"  1;
	setAttr ".wl[42].w[7]"  1;
	setAttr ".wl[43].w[7]"  1;
	setAttr ".wl[44].w[7]"  1;
	setAttr ".wl[45].w[7]"  1;
	setAttr ".wl[46].w[7]"  1;
	setAttr ".wl[47].w[7]"  1;
	setAttr ".wl[48].w[12]"  1;
	setAttr ".wl[49].w[12]"  1;
	setAttr ".wl[50].w[1]"  1;
	setAttr ".wl[51].w[1]"  1;
	setAttr ".wl[52].w[1]"  1;
	setAttr ".wl[53].w[1]"  1;
	setAttr ".wl[54].w[12]"  1;
	setAttr ".wl[55].w[12]"  1;
	setAttr ".wl[56].w[16]"  1;
	setAttr ".wl[57].w[16]"  1;
	setAttr ".wl[58].w[16]"  1;
	setAttr ".wl[59].w[16]"  1;
	setAttr ".wl[60].w[16]"  1;
	setAttr ".wl[61].w[16]"  1;
	setAttr ".wl[62].w[16]"  1;
	setAttr ".wl[63].w[16]"  1;
	setAttr ".wl[64].w[12]"  1;
	setAttr ".wl[65].w[12]"  1;
	setAttr ".wl[66].w[12]"  1;
	setAttr ".wl[67].w[12]"  1;
	setAttr ".wl[68].w[17]"  1;
	setAttr ".wl[69].w[17]"  1;
	setAttr ".wl[70].w[17]"  1;
	setAttr ".wl[71].w[17]"  1;
	setAttr ".wl[72].w[17]"  1;
	setAttr ".wl[73].w[17]"  1;
	setAttr ".wl[74].w[17]"  1;
	setAttr ".wl[75].w[17]"  1;
	setAttr ".wl[76].w[17]"  1;
	setAttr ".wl[77].w[17]"  1;
	setAttr ".wl[78].w[17]"  1;
	setAttr ".wl[79].w[17]"  1;
	setAttr ".wl[80].w[18]"  1;
	setAttr ".wl[81].w[18]"  1;
	setAttr ".wl[82].w[18]"  1;
	setAttr ".wl[83].w[19]"  1;
	setAttr ".wl[84].w[18]"  1;
	setAttr ".wl[85].w[18]"  1;
	setAttr ".wl[86].w[18]"  1;
	setAttr ".wl[87].w[18]"  1;
	setAttr ".wl[88].w[18]"  1;
	setAttr ".wl[89].w[19]"  1;
	setAttr ".wl[90].w[19]"  1;
	setAttr ".wl[91].w[19]"  1;
	setAttr ".wl[92].w[18]"  1;
	setAttr ".wl[93].w[18]"  1;
	setAttr ".wl[94].w[12]"  1;
	setAttr ".wl[95].w[12]"  1;
	setAttr ".wl[96].w[12]"  1;
	setAttr ".wl[97].w[12]"  1;
	setAttr ".wl[98].w[12]"  1;
	setAttr ".wl[99].w[12]"  1;
	setAttr ".wl[100].w[12]"  1;
	setAttr ".wl[101].w[12]"  1;
	setAttr ".wl[102].w[12]"  1;
	setAttr ".wl[103].w[12]"  1;
	setAttr ".wl[104].w[12]"  1;
	setAttr ".wl[105].w[12]"  1;
	setAttr ".wl[106].w[10]"  1;
	setAttr ".wl[107].w[10]"  1;
	setAttr ".wl[108].w[10]"  1;
	setAttr ".wl[109].w[10]"  1;
	setAttr ".wl[110].w[10]"  1;
	setAttr ".wl[111].w[10]"  1;
	setAttr ".wl[112].w[10]"  1;
	setAttr ".wl[113].w[10]"  1;
	setAttr ".wl[114].w[10]"  1;
	setAttr ".wl[115].w[10]"  1;
	setAttr ".wl[116].w[10]"  1;
	setAttr ".wl[117].w[10]"  1;
	setAttr ".wl[118].w[10]"  1;
	setAttr ".wl[119].w[11]"  1;
	setAttr ".wl[120].w[11]"  1;
	setAttr ".wl[121].w[10]"  1;
	setAttr ".wl[122].w[10]"  1;
	setAttr ".wl[123].w[11]"  1;
	setAttr ".wl[124].w[11]"  1;
	setAttr ".wl[125].w[10]"  1;
	setAttr ".wl[126].w[11]"  1;
	setAttr ".wl[127].w[11]"  1;
	setAttr ".wl[128].w[11]"  1;
	setAttr ".wl[129].w[11]"  1;
	setAttr ".wl[130].w[14]"  1;
	setAttr ".wl[131].w[14]"  1;
	setAttr ".wl[132].w[14]"  1;
	setAttr ".wl[133].w[13]"  1;
	setAttr ".wl[134].w[14]"  1;
	setAttr ".wl[135].w[14]"  1;
	setAttr ".wl[136].w[14]"  1;
	setAttr ".wl[137].w[14]"  1;
	setAttr ".wl[138].w[14]"  1;
	setAttr ".wl[139].w[14]"  1;
	setAttr ".wl[140].w[13]"  1;
	setAttr ".wl[141].w[13]"  1;
	setAttr ".wl[142].w[13]"  1;
	setAttr ".wl[143].w[14]"  1;
	setAttr ".wl[144].w[5]"  1;
	setAttr ".wl[145].w[6]"  1;
	setAttr ".wl[146].w[6]"  1;
	setAttr ".wl[147].w[5]"  1;
	setAttr ".wl[148].w[5]"  1;
	setAttr ".wl[149].w[6]"  1;
	setAttr ".wl[150].w[6]"  1;
	setAttr ".wl[151].w[5]"  1;
	setAttr ".wl[152].w[6]"  1;
	setAttr ".wl[153].w[6]"  1;
	setAttr ".wl[154].w[6]"  1;
	setAttr ".wl[155].w[6]"  1;
	setAttr ".wl[156].w[4]"  1;
	setAttr ".wl[157].w[5]"  1;
	setAttr ".wl[158].w[5]"  1;
	setAttr ".wl[159].w[4]"  1;
	setAttr ".wl[160].w[4]"  1;
	setAttr ".wl[161].w[5]"  1;
	setAttr ".wl[162].w[5]"  1;
	setAttr ".wl[163].w[4]"  1;
	setAttr ".wl[164].w[5]"  1;
	setAttr ".wl[165].w[5]"  1;
	setAttr ".wl[166].w[5]"  1;
	setAttr ".wl[167].w[5]"  1;
	setAttr ".wl[168].w[4]"  1;
	setAttr ".wl[169].w[4]"  1;
	setAttr ".wl[170].w[4]"  1;
	setAttr ".wl[171].w[4]"  1;
	setAttr ".wl[172].w[4]"  1;
	setAttr ".wl[173].w[4]"  1;
	setAttr ".wl[174].w[4]"  1;
	setAttr ".wl[175].w[4]"  1;
	setAttr ".wl[176].w[3]"  1;
	setAttr ".wl[177].w[3]"  1;
	setAttr ".wl[178].w[3]"  1;
	setAttr ".wl[179].w[3]"  1;
	setAttr ".wl[180].w[3]"  1;
	setAttr ".wl[181].w[3]"  1;
	setAttr ".wl[182].w[3]"  1;
	setAttr ".wl[183].w[3]"  1;
	setAttr ".wl[184].w[3]"  1;
	setAttr ".wl[185].w[3]"  1;
	setAttr ".wl[186].w[3]"  1;
	setAttr ".wl[187].w[3]"  1;
	setAttr ".wl[188].w[3]"  1;
	setAttr ".wl[189].w[3]"  1;
	setAttr ".wl[190].w[3]"  1;
	setAttr ".wl[191].w[3]"  1;
	setAttr ".wl[192].w[3]"  1;
	setAttr ".wl[193].w[3]"  1;
	setAttr ".wl[194].w[3]"  1;
	setAttr ".wl[195].w[3]"  1;
	setAttr ".wl[196].w[3]"  1;
	setAttr ".wl[197].w[3]"  1;
	setAttr ".wl[198].w[3]"  1;
	setAttr ".wl[199].w[3]"  1;
	setAttr ".wl[200].w[3]"  1;
	setAttr ".wl[201].w[3]"  1;
	setAttr ".wl[202].w[3]"  1;
	setAttr ".wl[203].w[3]"  1;
	setAttr ".wl[204].w[3]"  1;
	setAttr ".wl[205].w[3]"  1;
	setAttr ".wl[206].w[3]"  1;
	setAttr ".wl[207].w[3]"  1;
	setAttr ".wl[208].w[3]"  1;
	setAttr ".wl[209].w[3]"  1;
	setAttr ".wl[210].w[3]"  1;
	setAttr ".wl[211].w[3]"  1;
	setAttr ".wl[212].w[3]"  1;
	setAttr ".wl[213].w[3]"  1;
	setAttr ".wl[214].w[3]"  1;
	setAttr ".wl[215].w[3]"  1;
	setAttr ".wl[216].w[3]"  1;
	setAttr ".wl[217].w[3]"  1;
	setAttr ".wl[218].w[3]"  1;
	setAttr ".wl[219].w[3]"  1;
	setAttr ".wl[220].w[3]"  1;
	setAttr ".wl[221].w[3]"  1;
	setAttr ".wl[222].w[3]"  1;
	setAttr ".wl[223].w[3]"  1;
	setAttr ".wl[224].w[3]"  1;
	setAttr ".wl[225].w[3]"  1;
	setAttr ".wl[226].w[3]"  1;
	setAttr ".wl[227].w[3]"  1;
	setAttr ".wl[228].w[3]"  1;
	setAttr ".wl[229].w[3]"  1;
	setAttr ".wl[230].w[3]"  1;
	setAttr ".wl[231].w[3]"  1;
	setAttr ".wl[232].w[3]"  1;
	setAttr ".wl[233].w[3]"  1;
	setAttr ".wl[234].w[1]"  1;
	setAttr ".wl[235].w[1]"  1;
	setAttr ".wl[236].w[1]"  1;
	setAttr ".wl[237].w[1]"  1;
	setAttr ".wl[238].w[13]"  1;
	setAttr ".wl[239].w[14]"  1;
	setAttr ".wl[240].w[14]"  1;
	setAttr ".wl[241].w[14]"  1;
	setAttr ".wl[242].w[14]"  1;
	setAttr ".wl[243].w[14]"  1;
	setAttr ".wl[244].w[18]"  1;
	setAttr ".wl[245].w[18]"  1;
	setAttr ".wl[246].w[18]"  1;
	setAttr ".wl[247].w[18]"  1;
	setAttr ".wl[248].w[18]"  1;
	setAttr ".wl[249].w[18]"  1;
	setAttr -s 21 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.007638338245241758 -0.99997082746890753 0 0 0.99997082746890753 -0.007638338245241758 0 0
		 0 0 1 0 -107.95978956858143 0.030247094490147404 0 1;
	setAttr ".pm[1]" -type "matrix" -0.0044115723650070132 -0.99999026896728782 0 0 0.99999026896728782 -0.0044115723650070132 0 0
		 0 0 1 0 -149.95910645016053 -0.45364634835505024 0 1;
	setAttr ".pm[2]" -type "matrix" -0.016561504548029009 -0.99986284887833798 0 0 0.99986284887833798 -0.016561504548029009 0 0
		 0 0 1 0 -187.95074355633469 1.8301609691688714 0 1;
	setAttr ".pm[3]" -type "matrix" -0.0081677587874703396 -0.99996664330186047 0 0 0.99996664330186047 -0.0081677587874703396 0 0
		 0 0 1 0 -207.95877979970339 0.084492744904494585 -1.1368683772161603e-013 1;
	setAttr ".pm[4]" -type "matrix" 0.99843415817990133 0.053275246310150355 -0.01705813325616513 0
		 -0.052218358912657782 0.99698791205170501 0.057344103578753206 0 0.020061773901156675 -0.056363564058483133 0.99820873261786514 0
		 -43.927369828490569 -202.561826144278 -7.3944853402106556 1;
	setAttr ".pm[5]" -type "matrix" 0.99843375824155278 0.053275246310150355 -0.017081526109860475 0
		 -0.052217015352050582 0.99698791205170501 0.057345327015421554 0 0.020085161472409178 -0.056363564058483133 0.99820826230564386 0
		 -90.927543052963117 -202.56182614427786 -7.3923549512471096 1;
	setAttr ".pm[6]" -type "matrix" 1 4.3368086899420177e-018 -6.9388939039072299e-018 0
		 -2.2334564753201394e-017 1 -4.1633363423443383e-017 0 1.0408340855860843e-017 4.1633363423443383e-017 1.0000000000000002 0
		 -148.37677365215077 -195.1734444879902 1.26769969974401 1;
	setAttr ".pm[7]" -type "matrix" 0.99942397863139298 0.021111148648948662 0.026571231422712574 0
		 0.033321858885667584 -0.758828724986776 -0.65043725281947651 0 0.0064315361307397678 0.65094798988680014 -0.75909508614226529 0
		 50.650652976922075 154.33392944144128 128.61960830504708 1;
	setAttr ".pm[8]" -type "matrix" 0.99942510665607243 0.021086972463653483 0.02654799008994254 0
		 0.033288378867448655 -0.75882934026302207 -0.65043824932760852 0 0.0064296203523820875 0.65094805625971697 -0.75909504545466544 0
		 97.657618921565387 154.33176468657098 128.61739048583428 1;
	setAttr ".pm[9]" -type "matrix" 1.0000000000000002 -3.4694469519536142e-018 -1.0408340855860847e-017 0
		 1.0408340855860849e-017 1 3.8857805861880484e-016 0 6.9388939039072284e-018 -5.5511151231257827e-017 1 0
		 151.24276603377965 -195.95306735842229 3.7586261909812619 1;
	setAttr ".pm[10]" -type "matrix" -0.00045566368752371919 0.9999883263750956 -0.0048103517687913128 0
		 -0.99890412464381706 -0.00023002935928889736 0.046802744108267 0 0.046801091228452454 0.0048264065337999245 0.99889256862777631 0
		 94.261999415483587 -22.183649117263826 -4.3092693450413515 1;
	setAttr ".pm[11]" -type "matrix" 1.2463983318025957e-006 0.99998832291154138 -0.0048326016812439828 0
		 -0.99954375931556338 -0.00014471741371849362 -0.030203514202515266 0 -0.030203860875024986 0.0048304344973550397 0.99953208737429189 0
		 56.977879748977969 -22.191603608410333 2.9571555750506455 1;
	setAttr ".pm[12]" -type "matrix" 1 -2.1285057050235423e-015 0 0 2.1285057050235423e-015 1 0 0
		 0 0 1 0 0.79438717750387966 -105.35528275315598 0 1;
	setAttr ".pm[13]" -type "matrix" 0.0095593241108671116 -0.99538150205078257 0.095521121735830661 0
		 -0.0049768146209814386 -0.095571662878439004 -0.99541011074374641 0 0.99994192363295342 0.0090400569565662076 -0.0058674297141302683 0
		 -4.2497543015566617 22.096103303503508 -0.56861854899634412 1;
	setAttr ".pm[14]" -type "matrix" 2.2481119358163517e-005 0.013951622601164899 -0.99990267112424214 0
		 -0.65568230538895267 0.75496361486015451 0.010519250786824544 0 0.7550368957173067 0.65561825208278224 0.0091648044981852 0
		 13.579152386759498 -12.429685820057554 22.034648418987754 1;
	setAttr ".pm[15]" -type "matrix" 0.00047470849924368465 -0.99937345097432584 -0.035390396147936952 0
		 0.99993156310529296 6.0683569672459313e-005 0.011698949666923286 0 -0.011689472085837607 -0.035393527729961217 0.99930508576569466 0
		 -1.0365150958602984 22.744596758290584 -19.490426806279043 1;
	setAttr ".pm[16]" -type "matrix" -0.99991019573185436 -0.013398117603920489 0.00030152306176988461 0
		 0.01318217098912587 -0.98734990237758491 -0.15800759678884957 0 0.002414713129692941 -0.15798943230370099 0.98743785041938514 0
		 -25.036071079746979 92.842912559471699 14.916031395788316 1;
	setAttr ".pm[17]" -type "matrix" -0.99981980869632492 0.018980464544707844 0.00030348659253690247 0
		 -0.018980975639472392 -0.99936408916469011 -0.030185093205147041 0 -0.00026963348923465845 -0.030185414585470742 0.999544280181766 0
		 -22.706304187100098 57.507327875993255 2.8551881992085755 1;
	setAttr ".pm[18]" -type "matrix" 0.99860817692359849 -0.020040068285179447 0.048786316159885441 0
		 -0.023013131573756243 -0.99786251875094278 0.061161993486097112 0 0.047456345797964011 -0.06219959272495082 -0.9969348553984626 0
		 24.288039023454502 17.48491757672344 -2.2431582598734834 1;
	setAttr ".pm[19]" -type "matrix" 0.99718063030407711 0.026234421635876696 -0.070303240805774866 0
		 0.027256695519254418 -0.99953566102067315 0.013621119533179614 0 -0.069913254077709597 -0.015498950590203146 -0.99743266411064957 0
		 23.848129896701007 2.2758871625206121 2.3040078850748622 1;
	setAttr ".pm[20]" -type "matrix" 0.99814936603838478 0.00057138928758492175 -0.06080720838397935 0
		 0.00018905693111230786 -0.99998017985447052 -0.006293183272199087 0 -0.060809599033766454 0.0062700408693022033 -0.99812969059779466 0
		 24.815656099909287 1.0990496697137035 18.754633391293449 1;
	setAttr ".gm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr -s 17 ".ma";
	setAttr -s 21 ".dpf[0:20]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4;
	setAttr -s 17 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".ucm" yes;
	setAttr -s 17 ".ifcl";
createNode tweak -n "tweak1";
	rename -uid "3D23D2E8-4975-224A-9AD7-0C9A4F5961EE";
createNode objectSet -n "skinCluster1Set";
	rename -uid "159E2662-447F-3FB1-B7DA-B1A81F706272";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	rename -uid "1CDCC4F5-4941-3F4F-03CC-79B26C70ED57";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	rename -uid "B2793C87-49BA-E544-CEF2-30AF589F8F15";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	rename -uid "57C5AD6B-4F57-5BF9-F4C5-D6ACC5B913AB";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	rename -uid "F6747034-444B-9A56-7130-BAB3EBCC2AD2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "2162FBDD-4A29-49C0-4731-70951C052DB8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose1";
	rename -uid "3020EC83-4AB4-3449-80C8-B1803EA49959";
	setAttr -s 23 ".wm";
	setAttr -s 24 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 -3.4694469519536142e-018 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.0083534669020751857 0.99996510918667358 
		0 0 0.012172469700649113 0.99992591274623277 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 1.7520707107365749e-016 0 0.030247094489068593
		 107.95978956858141 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.70324783129833657 0.71094478532034133 
		0 0 0.0054425683834455263 0.99998518911501455 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 -0.0032268258478793319 0 42.000000000000014
		 9.1038288019264856e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.70866479112659697 0.70554533080270365 
		0 0 -0.70866479112659753 0.7055453308027031 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0.012150675057767988 0 37.999999999999972
		 8.4376949871513917e-015 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.71293811251329131 0.70122695878437646 
		0 0 -0.7129381125132932 0.70122695878437458 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 -0.0083944121273693932 0 19.999999999999829
		 8.4376949871511897e-015 1.1368683772161603e-013 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.70998864736961487 0.70421312158058003 
		0 0 -0.70998864736961631 0.70421312158057858 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 11.200722895197259 -30.999999999999989
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.70710678118654746 0.70710678118654768 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 -0.053897001784612444 -0.056398479794354361
		 -0.068910774942598119 0 25.000000000000011 5.6843418860808015e-014 -3.1546056623016021 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.018209825542569637 0 0.99983418737994212 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 -7.1398700441590738e-017 0.059034783410405446
		 -4.8368148963476455e-015 0 46.999999999999993 -1.7053025658242404e-013 1.9984014443252818e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.02952481525834505 0 0.99956404761473916 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0.057385185200321168 0.017082356889388549
		 0.053308265025457817 0 47.000000000000128 5.6843418860808015e-014 -3.5527136788005009e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 11.673162207651302 30.825281453024701
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.70168503232178514 0.71248727386219002 4.296581644212406e-017 4.3627262968427731e-017 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 -0.7076831657946302 -0.029971383039035029
		 -0.03205702036683554 0 -25.000029629748283 -4.1318752380448132e-005 3.154610000000003 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0.018209825542568177 0 0.99983418737994212 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 1.1372508618138077e-006 0.059034952056434317
		 2.4224541892463753e-005 0 -47.000242032447105 -0.00023434786061216073 -8.5946018864024865e-006 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 -0.029524815258342448 0 0.99956404761473927 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 -2.4331294109063544 -0.026551109564631321
		 0.021095972122440566 0 -46.999405245692927 0.00090540455664722685 -1.6531703053601632e-005 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 -2.1094237467877974e-015 0 -2.6015124977278674
		 0.01987181211792555 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.70980220422496565 0.70440104406323834 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 -0.029070095405632445 -0.0052105179006055845
		 -0.00030983864720226622 0 22.999999999999996 -11 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 -0.70236586491900876 0.711816122180843 -0.0088404007764070584 -5.9075891502533714e-005 -0.0066820798555900672 0.99993859493118609 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 -0.017143374151054575 0.00032536112963406057
		 8.2618146948345691e-005 0 37.223624498579156 0.0030930882668313798 -2.8729275887621708 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.71378409405461773 0.70036580947004301 0.033597682651319169 0.032966086527290925 0.71299293730431734 0.6995895255174267 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0.048402604543672435 0.0038081942665240676
		 0.0024677833198834681 0 38.999999999999993 -1.4210854715202004e-014 1.7763568394002505e-015 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.22215817891715642 -0.68612840201229541 -0.23375113618066226 0.65209965942830861 
		-0.024332618097087005 0.010312596488144527 0.71371187684253068 0.69994066241655528 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0.0029372734218279248 4.860815703838637e-005
		 -0.00022753642119622153 0 15.635191375353306 -8.3175816148381845 0.00092829369432934072 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.54946622368685094 -0.44860757027074699 0.50880833888873334 0.48780343500264117 
		-0.2411289524345282 0.63418533990075532 0.2502539051707402 0.69067993014100559 1 1
		 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 16.223257891270887 0.21936079425606181
		 0.16055308274027125 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.0096720182709568066 0.0082911775026590431 0.70705817043268726 0.70704062971131043 
		-0.54585414480223282 0.4633215083074968 -0.50018156440076689 0.48702652409852465 1
		 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 2.9652580552077579 0.0026474062175611416
		 -3.1150900310583975 0 -23.000012822495929 -10.999966056766283 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0.9999385949311862 0.0066820798555872864 -5.9075891502484742e-005 0.0088404007764157737 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0.034302302257052476 -0.000651011669704743
		 -1.1342892527344645e-005 0 -0.49328325995498296 36.936009754266287 4.7725165370720752 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.047069626055869042 -0.0001320735593339184 0.016152065595960812 0.9987610042630416 1
		 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 -3.1308398955590762 -8.8079915101739836e-005
		 3.1010261198238203 0 0.74004397034043734 38.993020301891434 -2.5790303990191887e-006 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0096807664284571104 0.024590825751563152 0.00025288473821339833 0.99965069404243567 1
		 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0.0072712117178022843 0.00012181685127123865
		 -0.00056101239232728313 0 0.79556222184338665 16.120578815686603 -7.2892981461462689 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.027264680567314462 -0.059574857847917419 -0.02281269467968873 0.99759062468901238 1
		 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0 0 0 0 -1.0959105788523225 0.1307545171545561
		 -16.187996391774753 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.010021512526501799 0.0046237722864712382 0.013518089382303235 0.99984771404248163 1
		 1 1 yes;
	setAttr -s 22 ".m";
	setAttr -s 22 ".p";
	setAttr -s 24 ".g[0:23]" yes yes no yes no yes no no no yes no no no 
		no no no no no no no no no no no;
	setAttr ".bp" yes;
createNode animCurveTU -n "mainCtrl_visibility";
	rename -uid "FD5D7D1D-4AEC-FFDE-06A9-F89C8BC69857";
	setAttr ".tan" 5;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[0:27]"  2 2 2 9 2 9 9 9 
		1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 18 
		18 18 1;
	setAttr -s 28 ".kot[0:27]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 18 
		18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[22:27]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 28 ".koy[22:27]"  0 0 0 0 0 0;
createNode animCurveTL -n "mainCtrl_translateX";
	rename -uid "6C5B621F-4AB9-C51A-1B00-A0ADEEE0BA3D";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "mainCtrl_translateY";
	rename -uid "DB6FDD89-4466-7042-4BCB-19BC113F790C";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "mainCtrl_translateZ";
	rename -uid "85BE345A-40C8-461F-21F5-6F822175A90C";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "mainCtrl_rotateX";
	rename -uid "4299EE63-4599-70D8-3E52-CAB3B8F0774A";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "mainCtrl_rotateY";
	rename -uid "1A216B3E-4869-EDB2-3828-4E95801CE885";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "mainCtrl_rotateZ";
	rename -uid "EC1AC88A-49A4-F4A1-EF27-CF981EC713AC";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "mainCtrl_scaleX";
	rename -uid "F2C33937-4410-148D-605E-86A068E1815A";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 93.871407336971572 4 93.871407336971572
		 5 93.871407336971572 34 93.871407336971572 36 93.871407336971572 38 93.871407336971572
		 40 93.871407336971572 44 93.871407336971572 44.004 93.871407336971572 46 93.871407336971572
		 48 93.871407336971572 52 93.871407336971572 54 93.871407336971572 56 93.871407336971572
		 58 93.871407336971572 60 93.871407336971572 62 93.871407336971572 64 93.871407336971572
		 66 93.871407336971572 68 93.871407336971572 72 93.871407336971572 74 93.871407336971572
		 75 93.871407336971572 80 93.871407336971572 86 93.871407336971572 92 93.871407336971572
		 98 93.871407336971572 104 93.871407336971572;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "mainCtrl_scaleY";
	rename -uid "F53C65CF-441F-D28A-C605-EE9ECBED8D50";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 93.871407336971572 4 93.871407336971572
		 5 93.871407336971572 34 93.871407336971572 36 93.871407336971572 38 93.871407336971572
		 40 93.871407336971572 44 93.871407336971572 44.004 93.871407336971572 46 93.871407336971572
		 48 93.871407336971572 52 93.871407336971572 54 93.871407336971572 56 93.871407336971572
		 58 93.871407336971572 60 93.871407336971572 62 93.871407336971572 64 93.871407336971572
		 66 93.871407336971572 68 93.871407336971572 72 93.871407336971572 74 93.871407336971572
		 75 93.871407336971572 80 93.871407336971572 86 93.871407336971572 92 93.871407336971572
		 98 93.871407336971572 104 93.871407336971572;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "mainCtrl_scaleZ";
	rename -uid "83C3A171-4797-CCEB-926E-21826EF7ACBE";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 93.871407336971572 4 93.871407336971572
		 5 93.871407336971572 34 93.871407336971572 36 93.871407336971572 38 93.871407336971572
		 40 93.871407336971572 44 93.871407336971572 44.004 93.871407336971572 46 93.871407336971572
		 48 93.871407336971572 52 93.871407336971572 54 93.871407336971572 56 93.871407336971572
		 58 93.871407336971572 60 93.871407336971572 62 93.871407336971572 64 93.871407336971572
		 66 93.871407336971572 68 93.871407336971572 72 93.871407336971572 74 93.871407336971572
		 75 93.871407336971572 80 93.871407336971572 86 93.871407336971572 92 93.871407336971572
		 98 93.871407336971572 104 93.871407336971572;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_ElbowPoleV_visibility";
	rename -uid "9F40CA1F-4D9B-FEFD-4E89-A5BEFD26E7C8";
	setAttr ".tan" 5;
	setAttr -s 34 ".ktv[0:33]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 42 1 44 1 44.004 1 45 1 48 1 49 1 51 1 52 1 53 1 57 1 59 1 60 1 61 1 62 1 63 1
		 65 1 71 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 34 ".kit[0:33]"  2 2 2 9 9 9 9 2 
		9 9 9 9 1 2 2 2 9 9 9 9 9 9 9 9 9 
		9 9 9 1 18 18 18 18 1;
	setAttr -s 34 ".kot[0:33]"  2 2 2 5 5 5 5 2 
		5 5 5 5 5 2 2 2 5 5 5 5 5 5 5 5 5 
		5 5 5 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[28:33]"  0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 34 ".koy[28:33]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_ElbowPoleV_translateX";
	rename -uid "587B19C3-4F12-5B0A-6419-7BB9A0A02E66";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 0 4 0 5 0.094543820548990812 13 0.094543820548990812
		 21 0.094543820548990812 27 0.094543820548990812 34 0.094543820548990812 36 0.094543820548990812
		 38 -0.08573087046514162 40 -0.26600556147927407 42 -0.44628025249340603 44 -0.62655494350753893
		 44.004 -0.62655494350753893 45 -0.58854175611235049 48 -0.58854175611235049 49 -0.49399793556335969
		 51 -0.49399793556335969 52 -0.49399793556335969 53 -0.49399793556335969 57 -0.49399793556335969
		 59 -0.49399793556335969 60 -0.49399793556335969 61 -0.49399793556335969 62 -0.49399793556335969
		 63 -0.49399793556335969 65 -0.49399793556335969 71 -0.49399793556335969 74 -0.49399793556335969
		 75 0.094543820548990812 80 0.31063227033535745 86 0.18674057054411353 92 0.2874133124811491
		 98 0.18674057054411353 104 0.31063227033535745;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  -0.36054939031600952 0.038013186305761337 
		0 0.094543822109699249 0 0 0 0 0 0 0 0 0 0 0 0 0.094543822109699249 0 0 0 0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0.72109878063201904 0 0.094543822109699249 
		0 0 0 0 0 0 0 0 0 0 0 0 0.58854174613952637 0 0 0 0 0 0;
createNode animCurveTL -n "R_ElbowPoleV_translateY";
	rename -uid "25337F2A-426D-7DF0-E743-80B2AF7107D5";
	setAttr ".tan" 2;
	setAttr -s 32 ".ktv[0:31]"  1 0 4 0 5 -1.1910943157671323 13 -1.351161590075457
		 20 -1.2320593523053296 34 -1.1925671940512763 36 -1.191 38 -1.1909999999999998 40 -1.1909999999999998
		 42 -1.4327748792264474 44 -1.1909999999999998 44.004 -1.1909999999999998 45 -1.1511943968358382
		 48 -1.1511943968358382 49 -2.3422887126029703 51 -1.6721180894517929 52 -1.6035114313012302
		 53 -1.8304068770747202 57 -2.5023559869112955 59 -0.46460742778897302 60 0.052366548818519786
		 61 -0.27569997491529918 62 -0.18164582512227587 63 0.031511945341088053 64 -2.3832537491411676
		 74 -2.3832537491411676 75 -1.1910943157671323 80 -0.44459574482220465 86 -0.44459574482220465
		 92 -0.44459574482220465 98 -0.44459574482220465 104 -0.44459574482220465;
	setAttr -s 32 ".kit[3:31]"  18 1 2 2 2 2 2 2 
		1 2 2 18 18 2 2 2 2 2 2 2 2 1 2 1 18 
		18 18 18 1;
	setAttr -s 32 ".kot[2:31]"  1 18 1 2 2 2 2 2 
		2 1 2 2 18 18 2 2 2 2 2 2 2 2 1 2 1 
		18 18 18 18 1;
	setAttr -s 32 ".ktl[2:31]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 32 ".kix[4:31]"  0.29098191857337952 0.58333331346511841 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.083333373069763184 
		0.083333373069763184 0.041666626930236816 0.041499972343444824 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.29098191857337952 0.41666650772094727 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 32 ".kiy[4:31]"  0.15078897774219513 0.039492160081863403 
		0.0015671940054744482 0 0 -0.24177487194538116 0.24177487194538116 0 0.03980560228228569 
		0 0 0.4116387665271759 0.068606659770011902 -0.22689545154571533 -0.67194908857345581 
		2.0377485752105713 0.51697397232055664 -0.32806652784347534 0.094054147601127625 
		0.2131577730178833 0.15078897774219513 0 -1.1910942792892456 0 0 0 0 0;
	setAttr -s 32 ".kox[2:31]"  0.33224296569824219 0.29166662693023682 
		0.62353283166885376 0.083333373069763184 0.083333373069763184 0.083333253860473633 
		0.083333373069763184 0.083333373069763184 0.00016665458679199219 0.75000011920928955 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666746139526367 0.62353283166885376 
		0.041666746139526367 0.33224296569824219 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 32 ".koy[2:31]"  -0.32474195957183838 0 0.32311937212944031 
		0.0015671940054744482 0 0 -0.24177487194538116 0.24177487194538116 0 0 0 -1.1910942792892456 
		0 0.20581997931003571 -0.22689545154571533 -0.67194908857345581 2.0377485752105713 
		0.51697397232055664 -0.32806652784347534 0.094054147601127625 0.2131577730178833 
		-2.4147655963897705 0.32311937212944031 1.1921594142913818 -0.32474195957183838 0 
		0 0 0 0;
createNode animCurveTL -n "R_ElbowPoleV_translateZ";
	rename -uid "2E93B12F-4C78-3CE9-4F66-B0AF468EAC5A";
	setAttr ".tan" 2;
	setAttr -s 32 ".ktv[0:31]"  1 0 4 0 5 -0.70052734755677726 13 -0.59860399026471023
		 21 -0.46572839296178048 34 -0.68308022565701276 36 -0.701 38 -0.701 40 -0.701 42 -0.701
		 44 -0.701 44.004 -0.701 45 -0.69453261423384693 48 3.0800440643995466 49 2.9372043506632655
		 51 2.2493777092169465 52 1.5802232834246637 53 0.82277774641491963 57 -1.2931366044985571
		 59 0.65651163842036719 60 0.31862697483577118 61 -0.016450583840215804 62 -0.34661363339537993
		 63 0.92399753649337735 65 -1.1602610071956274 74 -1.1602610071956274 75 -0.70052734755677726
		 80 0 86 -0.32014966005340534 92 -0.71107924425579838 98 -0.32014966005340534 104 0;
	setAttr -s 32 ".kit[3:31]"  1 18 2 2 2 2 2 2 
		1 2 2 18 18 2 2 1 2 2 2 2 2 18 2 1 18 
		18 18 18 1;
	setAttr -s 32 ".kot[2:31]"  1 1 18 2 2 2 2 2 
		2 1 2 2 18 18 2 2 1 2 2 2 2 2 18 2 1 
		18 18 18 18 1;
	setAttr -s 32 ".ktl[2:31]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 32 ".kix[3:31]"  0.33098050951957703 0.33333331346511841 
		0.54166662693023682 0.083333373069763184 0.083333373069763184 0.083333253860473633 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.33098050951957703 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.375 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 32 ".kiy[3:31]"  0.17653581500053406 0 -0.21735183894634247 
		-0.017919775098562241 0 0 0 0 0 0.006467385683208704 3.7745766639709473 -0.27688932418823242 
		-0.90465319156646729 -0.66915440559387207 -0.75744551420211792 0.17653581500053406 
		1.9496482610702515 -0.33788466453552246 -0.33507755398750305 -0.33016306161880493 
		1.2706111669540405 0 0 -0.7005273699760437 0 -0.35553961992263794 0 0.35553979873657227 
		0;
	setAttr -s 32 ".kox[2:31]"  0.33365073800086975 0.3309803307056427 
		0.54166662693023682 0.083333373069763184 0.083333373069763184 0.083333253860473633 
		0.083333373069763184 0.083333373069763184 0.00016665458679199219 0.75000011920928955 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.3309803307056427 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.375 0.041666746139526367 
		0.33365073800086975 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 32 ".koy[2:31]"  0.050449151545763016 0.17653582990169525 
		0 -0.017919775098562241 0 0 0 0 0 0 3.7745766639709473 -0.142839714884758 -0.55377703905105591 
		-0.45232787728309631 -0.75744551420211792 -2.1159143447875977 0.17653582990169525 
		-0.33788466453552246 -0.33507755398750305 -0.33016306161880493 1.2706111669540405 
		-2.0842585563659668 0 0.45973366498947144 0.050449151545763016 0 -0.35553961992263794 
		0 0.35553944110870361 0;
createNode animCurveTA -n "R_ElbowPoleV_rotateX";
	rename -uid "B71B06AF-4B2E-3C8F-DA33-6596C95096FC";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 0 4 0 5 0 13 0 21 0 27 0 34 0 36 0 38 0
		 40 0 42 0 44 0 44.004 0 45 0 48 0 49 0 51 0 52 0 53 0 57 0 59 0 60 0 61 0 62 0 63 0
		 65 0 71 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTA -n "R_ElbowPoleV_rotateY";
	rename -uid "1F861046-4B29-5BC0-ED24-3183883C618B";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 0 4 0 5 0 13 0 21 0 27 0 34 0 36 0 38 0
		 40 0 42 0 44 0 44.004 0 45 0 48 0 49 0 51 0 52 0 53 0 57 0 59 0 60 0 61 0 62 0 63 0
		 65 0 71 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTA -n "R_ElbowPoleV_rotateZ";
	rename -uid "7E320084-4B0C-52C0-1A8D-88B502EFE802";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 0 4 0 5 0 13 0 21 0 27 0 34 0 36 0 38 0
		 40 0 42 0 44 0 44.004 0 45 0 48 0 49 0 51 0 52 0 53 0 57 0 59 0 60 0 61 0 62 0 63 0
		 65 0 71 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTU -n "R_ElbowPoleV_scaleX";
	rename -uid "9BEA60A2-4E09-B80C-D3B9-27B845019957";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 42 1 44 1 44.004 1 45 1 48 1 49 1 51 1 52 1 53 1 57 1 59 1 60 1 61 1 62 1 63 1
		 65 1 71 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTU -n "R_ElbowPoleV_scaleY";
	rename -uid "389B5DB2-43E9-7E85-6180-30A563AE7299";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 42 1 44 1 44.004 1 45 1 48 1 49 1 51 1 52 1 53 1 57 1 59 1 60 1 61 1 62 1 63 1
		 65 1 71 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTU -n "R_ElbowPoleV_scaleZ";
	rename -uid "8DA7DC9A-47E6-768B-6BB7-EE9A3AF94F18";
	setAttr ".tan" 2;
	setAttr -s 34 ".ktv[0:33]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 42 1 44 1 44.004 1 45 1 48 1 49 1 51 1 52 1 53 1 57 1 59 1 60 1 61 1 62 1 63 1
		 65 1 71 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 34 ".kit[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kot[12:33]"  1 2 2 2 2 2 2 2 
		2 2 2 2 2 2 2 2 1 18 18 18 18 1;
	setAttr -s 34 ".kix[12:33]"  0.041666626930236816 0.041499972343444824 
		0.125 0.041666746139526367 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 34 ".kiy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
	setAttr -s 34 ".kox[12:33]"  0.75000011920928955 0.125 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 0.041666507720947266 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.083333253860473633 0.25 0.125 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 34 ".koy[12:33]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0;
createNode animCurveTU -n "R_KneePoleV_visibility";
	rename -uid "0B2357A2-48E3-19D9-1218-BD92CA982DBF";
	setAttr ".tan" 5;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[0:28]"  2 2 2 9 2 9 9 9 
		1 9 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 
		18 18 18 1;
	setAttr -s 29 ".kot[0:28]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 
		18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[23:28]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 29 ".koy[23:28]"  0 0 0 0 0 0;
createNode animCurveTL -n "R_KneePoleV_translateX";
	rename -uid "9D9E5C30-4588-94BB-BDC4-5DA5A62EF881";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 -0.84027799018609051 34 -0.84027799018609051
		 36 -0.84027799018609051 38 -0.87653853480492994 40 -0.91279907942376948 44 -0.98532016866144834
		 44.004 -0.98532016866144834 45 -0.97706836983107581 46 -0.96444348836586602 48 -0.69685067936741474
		 52 -0.69685067936741474 54 -0.71745107191018298 56 -0.69685067936741474 58 -0.71745096526624319
		 60 -0.69685067936741474 62 -0.70539986217480011 64 -0.70602804056364354 66 -1.0635234915252749
		 68 -1.2780223970358411 72 -0.84027799018609051 74 -0.84027799018609051 75 -0.84027799018609051
		 80 0 86 0 92 0 98 -0.10490271806548494 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  -0.072521090507507324 0.0082517992705106735 
		0.012624881230294704 0.26759281754493713 0.28846949338912964 -0.020600393414497375 
		0.28846949338912964 -0.02060028538107872 0.28846949338912964 -0.0085491826757788658 
		-0.00062817841535434127 -0.35749545693397522 0 0 0 -0.84027796983718872 0 0 0 0 0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0.14504218101501465 0.012624881230294704 
		0.26759281754493713 0 -0.14342731237411499 0.020600393414497375 -0.14342731237411499 
		0.02060028538107872 -0.14342731237411499 -0.00062817841535434127 -0.35749545693397522 
		-0.21449890732765198 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_KneePoleV_translateY";
	rename -uid "2796C15E-46F7-AFFB-9E89-7EA394477EF6";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 45 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTL -n "R_KneePoleV_translateZ";
	rename -uid "946ACA0F-4471-F636-E48B-0FA57E276963";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 -1.2479578878558828 34 -1.2479578878558828
		 36 -1.2479578878558828 38 -0.90898932571055069 40 -0.57002076356521858 44 0.10791636072544573
		 44.004 0.10791636072544573 45 0.27067416537644018 46 0.46295242998827485 48 3.1751872519809727
		 52 3.1751872519809727 54 2.7921940218474512 56 3.1751872519809727 58 2.7921955470056168
		 60 3.1751872519809727 62 2.9115395747552943 64 2.8921672250639618 66 2.7584444137789457
		 68 2.5471567989873094 72 1.5816340382742986 74 1.5816340382742986 75 -1.2479578878558828
		 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0.67793715000152588 0.16275779902935028 
		0.19227826595306396 2.7122347354888916 3.0672709941864014 -0.38299322128295898 3.0672709941864014 
		-0.38299170136451721 3.0672709941864014 -0.26364767551422119 -0.019372349604964256 
		-0.13372281193733215 -0.39226976037025452 0 0 -1.2479579448699951 0 0 0 0 0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  -1.3558743000030518 0.19227826595306396 
		2.7122347354888916 0 -4.4231452941894531 0.38299322128295898 -4.4231452941894531 
		0.38299170136451721 -4.4231452941894531 -0.019372349604964256 -0.13372281193733215 
		-0.21128761768341064 -0.78454065322875977 0 -2.8295919895172119 0 0 0 0 0 0;
createNode animCurveTA -n "R_KneePoleV_rotateX";
	rename -uid "5FC7BE22-4356-2CF6-F0FC-21BF0F0730AF";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 45 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "R_KneePoleV_rotateY";
	rename -uid "18089AE9-4889-77B4-D196-70B9A1644644";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 45 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "R_KneePoleV_rotateZ";
	rename -uid "5A115C5C-4578-3191-D8CA-23B2635F3FD9";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 45 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "R_KneePoleV_scaleX";
	rename -uid "2B3F7A6E-430F-5198-A3C1-D1A7D93A757D";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "R_KneePoleV_scaleY";
	rename -uid "37F4BB83-48EB-0F2B-D7D8-07868825293F";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "R_KneePoleV_scaleZ";
	rename -uid "B5ED4E65-4C06-DF82-23AF-7DBC4B5717D2";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 2 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.083333253860473633 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "L_KneePoleV_visibility";
	rename -uid "942B7FBD-459E-5099-4DFC-B3A913D2173B";
	setAttr ".tan" 5;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[0:28]"  2 2 2 9 2 9 9 9 
		1 9 9 1 1 9 1 9 1 9 9 9 18 18 9 1 18 
		18 18 18 1;
	setAttr -s 29 ".kot[0:28]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 
		18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[23:28]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 29 ".koy[23:28]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_KneePoleV_translateX";
	rename -uid "5D9AF046-4050-D650-D876-389AE4048FD1";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0.097720710120814624 34 0.097720710120814624
		 36 0.097720710120814624 38 0.097720710120814624 40 0.097720710120814624 44 0.097720710120814624
		 44.004 0.097720710120814624 46 0.10589524030335283 48 0.27098531828508549 52 0.27098531828508549
		 53 0.27098531828508549 54 0.57913735160882474 56 0.38041770400536079 58 0.34894736135063997
		 60 0.17527266274551864 62 0.15550098646037974 64 -0.034951463221418108 66 -0.03218746760726432
		 68 -0.023895448776232078 72 0.19501154017745723 74 0.19501154017745723 75 0.097720710120814624
		 80 0 86 0.050067526388317234 92 0 98 0.050067526388317234 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0.0081745302304625511 0.16509008407592773 
		0.17326460778713226 0.17326460778713226 0.30815201997756958 0.17326460778713226 -0.031470343470573425 
		0.17326460778713226 -0.019771676510572433 -0.1904524564743042 0.0027639956679195166 
		0.024876056239008904 0 0 0.097720712423324585 0 0 0 0 0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0.16509008407592773 0 -0.17326460778713226 
		-0.17326460778713226 -0.19871965050697327 -0.17326460778713226 -0.17367470264434814 
		-0.17326460778713226 -0.1904524564743042 0.0027639956679195166 0.008292018435895443 
		0.049752183258533478 0 -0.097290828824043274 0 0 0 0 0 0;
createNode animCurveTL -n "L_KneePoleV_translateY";
	rename -uid "177021A0-4490-34ED-DCF9-FC819C3020E2";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 53 0 54 0 56 0 58 0 60 0 62 -0.0046844951393023027 64 -0.098777961588941365
		 66 -0.096720093419442751 68 -0.090546464789862946 72 0 74 0 75 0 80 0 86 0 92 0 98 0
		 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 -0.0046844952739775181 
		-0.094093464314937592 0.002057868055999279 0.018520886078476906 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 -0.094093464314937592 
		0.002057868055999279 0.0061736288480460644 0.03704182431101799 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_KneePoleV_translateZ";
	rename -uid "82E9B2D9-4AF3-02EF-DC89-498FC1DEF20A";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0.53908490080138893 34 0.53908490080138893
		 36 0.53908490080138893 38 0.53908490080138893 40 0.53908490080138893 44 0.53908490080138893
		 44.004 0.53908490080138893 46 0.52799398779925166 48 0.30400562048150315 52 1.8278290359043545
		 53 1.8278290359043545 54 1.9557955201921566 56 3.3407313394179661 58 3.3642055647969444
		 60 3.3407313394179661 62 3.3536020302413148 64 3.3317026098271789 66 3.1862541872650167
		 68 3.0989844674083638 72 3.5472010333216444 74 3.5472010333216444 75 0.53908490080138893
		 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 -0.011090912856161594 -0.2239883691072464 
		-0.23507927358150482 -0.23507927358150482 0.12796647846698761 -0.23507927358150482 
		0.023474225774407387 -0.23507927358150482 0.012870690785348415 -0.021899420768022537 
		-0.14544841647148132 0 0 0 0.53908491134643555 0 0 0 0 0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 -0.2239883691072464 1.5238233804702759 
		0.23507927358150482 0.23507927358150482 1.3849358558654785 0.23507927358150482 -0.023474225774407387 
		0.23507927358150482 -0.021899420768022537 -0.14544841647148132 -0.087269723415374756 
		0 0 -3.0081162452697754 0 0 0 0 0 0;
createNode animCurveTA -n "L_KneePoleV_rotateX";
	rename -uid "D157CECB-4D00-1D22-3845-D194D46A1B0F";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 53 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "L_KneePoleV_rotateY";
	rename -uid "833070D0-426D-BA53-0940-AF83DE3E619F";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 53 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "L_KneePoleV_rotateZ";
	rename -uid "77F735D8-42AA-0FE0-CAA6-B7ABB506017D";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 53 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "L_KneePoleV_scaleX";
	rename -uid "A2CE9ACC-4EA1-28A8-D5A9-EE805AE1CDFA";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "L_KneePoleV_scaleY";
	rename -uid "FE06F234-49B4-84D5-F059-D4A30BDF64DA";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "L_KneePoleV_scaleZ";
	rename -uid "F41CD0AB-4087-357F-7669-969AD5DB9436";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1
		 92 1 98 1 104 1;
	setAttr -s 29 ".kit[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[8:28]"  1 2 2 1 1 2 1 2 
		1 2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[8:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.041666626930236816 0.041666746139526367 
		0.041666626930236816 0.083333492279052734 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 29 ".kiy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
	setAttr -s 29 ".kox[8:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.66666674613952637 0.083333253860473633 0.66666674613952637 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[8:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTU -n "L_ElbowPoleV_visibility";
	rename -uid "2AD53901-4073-84B7-90CA-B7B78DD7D8F8";
	setAttr ".tan" 5;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[0:27]"  2 2 2 9 2 9 9 9 
		1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 18 
		18 18 1;
	setAttr -s 28 ".kot[0:27]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 18 
		18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[22:27]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 28 ".koy[22:27]"  0 0 0 0 0 0;
createNode animCurveTL -n "L_ElbowPoleV_translateX";
	rename -uid "C520F897-46B9-9EB0-B1CF-20A5F7B40A37";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 -0.20275941296019753 48 0 52 -0.73933662890512042 54 -0.5943237596202382 56 1.3602497159488567
		 58 1.3249238555405276 60 0.84877626238656023 62 0.8484571994513761 64 0.84204845404079465
		 66 0.82450582804783834 68 0.7718777495373953 72 0 74 0 75 0 80 -0.39700636883632934
		 86 -0.46319040739556816 92 -0.4519097944592726 98 -0.4912686936524211 104 -0.39700636883632934;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 -0.20275941491127014 0.20275941491127014 
		0 0.14501287043094635 0 -0.035325858741998672 0 -0.0003190629358869046 -0.0064087454229593277 
		-0.017542626708745956 -0.15788424015045166 0 0 0 -0.16546003520488739 0 0 0 -0.16546003520488739;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0.20275941491127014 -0.73933660984039307 
		0 1.9545735120773315 0 -0.47614759206771851 0 -0.0064087454229593277 -0.017542626708745956 
		-0.052628077566623688 -0.31576892733573914 0 0 0 -0.19855211675167084 0 0 0 -0.19855211675167084;
createNode animCurveTL -n "L_ElbowPoleV_translateY";
	rename -uid "EEB35424-43A7-3546-143E-E995F3D9E732";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 -1.257854540214822 34 -1.257854540214822
		 36 -1.257854540214822 38 -1.1172569636687628 40 -0.97665938712270339 44 -0.69546423403058488
		 44.004 -0.69546423403058488 46 -0.62617619995770879 48 -0.89316349766063041 52 -0.89316349766063041
		 54 -0.90523737459819631 56 -0.89316349766063041 58 -0.90523738518537156 60 -0.89316349766063041
		 62 -0.9075152573740628 64 -0.76075314762630475 66 -0.77110939693128433 68 -0.80217826367534772
		 72 -1.257854540214822 74 -1.257854540214822 75 -1.257854540214822 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.28119516372680664 0.069288037717342377 
		-0.26698729395866394 -0.19769926369190216 -0.012073877267539501 -0.19769926369190216 
		-0.012073887512087822 -0.19769926369190216 -0.014351760037243366 0.14676210284233093 
		-0.010356249287724495 -0.093206599354743958 0 0 -1.2578545808792114 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -0.56239032745361328 -0.26698729395866394 
		0 -0.36469104886054993 0.012073877267539501 -0.36469104886054993 0.012073887512087822 
		-0.36469104886054993 0.14676210284233093 -0.010356249287724495 -0.031068867072463036 
		-0.1864134669303894 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_ElbowPoleV_translateZ";
	rename -uid "17B96175-44F3-DC3C-DF21-CAAAF487CEA8";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 1.9374716585025544 34 1.9374716585025544
		 36 1.9374716585025544 38 1.700023931412133 40 1.4625762043217119 44 0.9876807501408692
		 44.004 0.9876807501408692 46 0.99139773033532896 48 0.057846606705316675 52 0.057846606705316675
		 54 0.20442869858937213 56 0.057846606705316675 58 0.34855929700714589 60 2.1446817966173213
		 62 2.2559349817315892 64 2.2484105065166555 66 2.2419326318821033 68 2.2224989358488139
		 72 1.9374716585025544 74 1.9374716585025544 75 1.9374716585025544 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  -0.47489544749259949 0.0037169801071286201 
		-0.9335511326789856 -0.92983412742614746 0.14658209681510925 -0.92983412742614746 
		0.29071268439292908 -0.92983412742614746 0.11125318706035614 -0.0075244749896228313 
		-0.0064778747037053108 -0.058301087468862534 0 0 1.9374716281890869 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0.94979089498519897 -0.9335511326789856 
		0 1.8796250820159912 -0.14658209681510925 1.8796250820159912 1.7961225509643555 1.8796250820159912 
		-0.0075244749896228313 -0.0064778747037053108 -0.019433695822954178 -0.1166023463010788 
		0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_ElbowPoleV_rotateX";
	rename -uid "B07FA113-4E2C-2F91-41BC-61A0D10619B9";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_ElbowPoleV_rotateY";
	rename -uid "27C4D108-46BB-EE79-EEA1-B7B7AA271C04";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_ElbowPoleV_rotateZ";
	rename -uid "188561B3-4285-37A7-22FE-BEBFA43AA383";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_ElbowPoleV_scaleX";
	rename -uid "4DB37910-4251-7412-3D30-EB807DF35B73";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_ElbowPoleV_scaleY";
	rename -uid "33023879-4A34-4920-AA9F-81B5E472DD94";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_ElbowPoleV_scaleZ";
	rename -uid "D4BD00F9-4572-513E-1CE1-95A50F1DFFA8";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Wrist_visibility";
	rename -uid "18AD2EDF-49A0-1D3B-973F-819E9616284D";
	setAttr ".tan" 5;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 41 1 42 1 43 1 44 1 44.004 1 45 1 45.004 1 46 1 48 1 50 1 51 1 54 1 56 1 58 1
		 60 1 62 1 64 1 66 1 68 1 70 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[0:37]"  2 2 2 9 9 9 9 2 
		9 9 9 9 9 9 1 9 1 9 9 9 9 9 1 9 1 
		9 9 9 18 9 18 9 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 5 5 5 5 2 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 18 5 18 5 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
	setAttr -s 38 ".kox[32:37]"  0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 38 ".koy[32:37]"  0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Wrist_translateX";
	rename -uid "07AC5CBC-4B39-031F-FEC9-7C87A926E304";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 0.78457115389934817 13 0.78457115389934817
		 21 0.78457115389934817 27 0.78457115389934817 34 0.78457115389934817 36 0.78457115389934817
		 38 0.65352994188528291 40 0.52248872987121764 41 0.52129238145141588 42 0.52009603303161411
		 43 0.69195751120981575 44 0.86381898938801649 44.004 0.86381898938801649 45 0.86381898938801649
		 45.004 0.86381898938801649 46 1.0021742612769211 48 2.444495927424986 50 2.0103092410113637
		 51 1.3963454630724006 54 0.92364202681359009 56 0.52355202049145777 58 0.38354020811522793
		 60 0.77656962943275565 62 0.68736093564415657 64 0.87559757913031588 66 0.87370120071904955
		 68 0.86801204369440188 70 0.82344685209785551 72 0.78457115389934817 74 0.78457115389934817
		 75 0.78457115389934817 80 0.89290254980400441 86 0.89290254980400441 92 0.89290254980400441
		 98 0.85721608888286716 104 0.89290254980400441;
	setAttr -s 38 ".kit[14:37]"  1 2 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[14:37]"  1 1 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0.34133026003837585 0 0.34133026003837585 
		0.13835527002811432 1.4423216581344604 -0.43418669700622559 -0.613963782787323 -0.47270342707633972 
		1.5806769132614136 -0.14001181721687317 1.5806769132614136 -0.089208692312240601 
		0.18823663890361786 -0.0018963784677907825 -0.017067471519112587 -0.044565193355083466 
		0 0 0.78457117080688477 0 0 0 0 0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  -0.079247832298278809 0 -0.079247832298278809 
		1.4423216581344604 -0.43418669700622559 -0.613963782787323 -0.47270342707633972 -0.40009000897407532 
		-1.6599247455596924 0.39302942156791687 -1.6599247455596924 0.18823663890361786 -0.0018963784677907825 
		-0.005689157173037529 -0.017067519947886467 -0.038875699043273926 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Wrist_translateY";
	rename -uid "020D1522-447C-DA8D-3B2C-07AF796738CB";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 -0.85762580154238688 13 -0.85762580154238688
		 21 -0.85762580154238688 27 -0.85762580154238688 34 -0.85762580154238688 36 -0.85762580154238688
		 38 -0.27886925083934377 40 0.29988729986369933 41 -0.044976082129371597 42 -0.38983946412244441
		 43 -0.6237326328324162 44 -0.85762580154238688 44.004 -0.85762580154238688 45 -0.85762580154238688
		 45.004 -0.85762580154238688 46 -0.81528998455518809 48 0.039710021133230279 50 -0.71579878612034398
		 51 -0.74730815230451264 54 -0.78069234495240381 56 -0.77019030246386266 58 -0.85481386496896006
		 60 -0.25168263562573945 62 -0.32487029890654673 64 -0.72451207412138718 66 -0.90550984027264358
		 68 -1.0141093313383385 70 -0.93586723066281507 72 -0.85762580154238688 74 -0.85762580154238688
		 75 -0.85762580154238688 80 -0.84573273524510506 86 -0.88580407740352318 92 -0.87523537545224861
		 98 -0.87248924272220496 104 -0.84573273524510506;
	setAttr -s 38 ".kit[14:37]"  1 2 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[14:37]"  1 1 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  -1.1575131416320801 0 -1.1575131416320801 
		0.042335815727710724 0.85500001907348633 -0.75550878047943115 -0.031509365886449814 
		-0.033384192734956741 0.89733582735061646 -0.084623560309410095 0.89733582735061646 
		-0.073187664151191711 -0.39964178204536438 -0.18099775910377502 0 0.078242100775241852 
		0 0 -0.85762578248977661 0 0 0.0066574141383171082 0.0082383984699845314 0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  0 0 0 0.85500001907348633 -0.75550878047943115 
		-0.031509365886449814 -0.033384192734956741 0.0105020422488451 -0.89733582735061646 
		0.60313123464584351 -0.89733582735061646 -0.39964178204536438 -0.18099775910377502 
		-0.10859949141740799 0 0.078241430222988129 0 0 0 0 0 0.0066574206575751305 0.0082383900880813599 
		0;
createNode animCurveTL -n "ct_R_Wrist_translateZ";
	rename -uid "F41A00DE-405A-3043-96D4-30B9A4AA45D9";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 -0.5357127506391639 13 -0.37400250913485333
		 20 -0.3010321445280873 27 -0.37170770199809922 34 -0.51468103664550369 36 -0.5357127506391639
		 38 -0.060997091880500509 40 0.41371856687816289 41 0.65107639625749325 42 0.88843422563682495
		 43 1.125792055016158 44 1.3631498843954897 44.004 1.3631498843954897 45 1.3631498843954897
		 45.004 1.3631498843954897 46 1.4267746249489173 48 1.729843200700661 50 2.5053429558851033
		 51 2.8185867947697614 54 3.0336093231726386 56 1.9786852199318279 58 1.9365829555918155
		 60 2.2929209873832699 62 2.244821901705139 64 3.9812392067816589 66 3.2588586859799467
		 68 2.6199145440860421 70 2.4398938917919795 72 2.1135804053413083 74 2.1135804053413083
		 75 -0.5357127506391639 80 0.52309738610570367 86 0.064978004850770765 92 -0.32253768486920342
		 98 0.5181233642310229 104 0.52309738610570367;
	setAttr -s 38 ".kit[3:37]"  18 18 18 2 2 2 2 2 
		2 2 2 1 2 1 2 2 2 2 2 1 2 1 2 2 2 
		18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[3:37]"  18 18 18 2 2 2 2 2 
		2 2 2 1 1 1 2 2 2 2 2 1 2 1 2 2 2 
		18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".ktl[5:37]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0.94943130016326904 0 0.94943130016326904 
		0.063624739646911621 0.30306857824325562 0.77549976110458374 0.31324383616447449 
		0.21502253413200378 0.36669331789016724 -0.042102266103029251 0.36669331789016724 
		-0.048099085688591003 1.736417293548584 -0.72238051891326904 -0.40948182344436646 
		-0.18002064526081085 0 0 -0.53571277856826782 0 -0.42281752824783325 0 0.014922079630196095 
		0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  -1.8988626003265381 0 -1.8988626003265381 
		0.30306857824325562 0.77549976110458374 0.31324383616447449 0.21502253413200378 -1.0549241304397583 
		-2.2655558586120605 0.3563380241394043 -2.2655558586120605 1.736417293548584 -0.72238051891326904 
		-0.63894414901733398 -0.40948298573493958 -0.32631349563598633 0 -2.6492931842803955 
		0.16171024739742279 0 -0.42281752824783325 0 0.014922065660357475 0;
createNode animCurveTA -n "ct_R_Wrist_rotateX";
	rename -uid "437DB339-4367-2285-4C82-20B0AB143D3F";
	setAttr ".tan" 2;
	setAttr -s 39 ".ktv[0:38]"  1 0 4 0 5 0 13 0 21 0 27 0 34 0 36 0 38 0
		 40 0 41 -58.223282393424292 42 0 43 -3.1241511421148891 44 0 44.004 0 45 0 45.004 0
		 46 0 48 0 50 -3.4905383688632927 51 -5.043973568811106 52 -6.2897327577531623 54 -6.2897327577531623
		 56 -6.2897327577531623 58 -6.2897327577531623 60 -6.2897327577531623 62 -6.2873683884032614
		 64 -6.2398773149971642 66 -6.1098802418053699 68 -5.7198875387474022 70 1.5647902928974988
		 72 0 74 0 75 0 80 -85.892173033639907 86 -85.892173033639907 92 -85.892173033639907
		 98 -85.892173033639907 104 -85.892173033639907;
	setAttr -s 39 ".kit[14:38]"  1 2 1 2 2 2 2 1 
		2 1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 39 ".kot[14:38]"  1 1 1 2 2 2 2 1 
		2 1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 39 ".kix[14:38]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 39 ".kiy[14:38]"  0 0 0 0 0 -0.060921385884284973 -0.027112558484077454 
		0 0 0 0 0 4.1266030166298151e-005 0.00082887557800859213 0.0022688768804073334 0.020419970154762268 
		0.12714160978794098 0 0 0 0 0 0 0 0;
	setAttr -s 39 ".kox[14:38]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.041666746139526367 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 39 ".koy[14:38]"  0 0 0 0 -0.060921385884284973 -0.027112558484077454 
		-0.021742599084973335 0 0 0 0 0 0.00082887557800859213 0.0022688768804073334 0.0068066567182540894 
		0.020420027896761894 -0.027310742065310478 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "ct_R_Wrist_rotateY";
	rename -uid "0FA30D88-4021-E3A2-1315-43AE3BBE4C43";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 0 13 0 21 0 27 0 34 0 36 0 38 0
		 40 0 41 3.0623044067270668 42 0 43 -38.263678620467147 44 0 44.004 0 45 0 45.004 0
		 46 0 48 0 50 0 51 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 70 1.7419896558173107
		 72 0 74 0 75 0 80 124.68724310098301 86 124.68724310098301 92 124.68724310098301
		 98 124.68724310098301 104 124.68724310098301;
	setAttr -s 38 ".kit[14:37]"  1 2 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[14:37]"  1 1 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.030403455719351768 
		0 0 0 0 0 0 0 0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.030403455719351768 
		0 0 0 0 0 0 0 0;
createNode animCurveTA -n "ct_R_Wrist_rotateZ";
	rename -uid "876F09B4-429E-5406-9960-C48E695F9359";
	setAttr ".tan" 2;
	setAttr -s 39 ".ktv[0:38]"  1 0 4 0 5 0 13 0 21 0 27 0 34 0 36 0 38 0
		 40 0 41 -0.53396637384953038 42 0 43 5.1899697217450518 44 0 44.004 0 45 0 45.004 0
		 46 0 48 0 50 7.362219465871533 51 10.638714316523869 52 13.26626108068233 54 13.26626108068233
		 56 13.26626108068233 58 13.26626108068233 60 13.26626108068233 62 13.261274169055822
		 64 13.161106322015982 66 12.655291863516021 68 12.064347461847987 70 11.005203560826766
		 72 0 74 0 75 0 80 -86.620379635507291 86 -86.620379635507291 92 -86.620379635507291
		 98 -86.620379635507291 104 -86.620379635507291;
	setAttr -s 39 ".kit[14:38]"  1 2 1 2 2 2 2 1 
		2 1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 39 ".kot[14:38]"  1 1 1 2 2 2 2 1 
		2 1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 39 ".kix[14:38]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 39 ".kiy[14:38]"  0 0 0 0 0 0.12849496304988861 0.057185623794794083 
		0 0 0 0 0 -8.7038024503272027e-005 -0.001748258713632822 -0.0088281277567148209 -0.014399716630578041 
		-0.018485547974705696 0 0 0 0 0 0 0 0;
	setAttr -s 39 ".kox[14:38]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.041666746139526367 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 39 ".koy[14:38]"  0 0 0 0 0.12849496304988861 0.057185623794794083 
		0.045859340578317642 0 0 0 0 0 -0.001748258713632822 -0.0088281277567148209 -0.010313925333321095 
		-0.014399757608771324 -0.19207704067230225 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Wrist_scaleX";
	rename -uid "62E15B0F-4270-E894-56C5-7CAD6D76D420";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 41 1 42 1 43 1 44 1 44.004 1 45 1 45.004 1 46 1 48 1 50 1 51 1 54 1 56 1 58 1
		 60 1 62 1 64 1 66 1 68 1 70 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[14:37]"  1 2 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[14:37]"  1 1 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTU -n "ct_R_Wrist_scaleY";
	rename -uid "1CC9DA8C-44B0-7B8C-7A0E-9A93D9B9681D";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 41 1 42 1 43 1 44 1 44.004 1 45 1 45.004 1 46 1 48 1 50 1 51 1 54 1 56 1 58 1
		 60 1 62 1 64 1 66 1 68 1 70 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[14:37]"  1 2 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[14:37]"  1 1 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTU -n "ct_R_Wrist_scaleZ";
	rename -uid "448F9A1F-46D4-FDDF-7776-3CAF20DCB78D";
	setAttr ".tan" 2;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 13 1 21 1 27 1 34 1 36 1 38 1
		 40 1 41 1 42 1 43 1 44 1 44.004 1 45 1 45.004 1 46 1 48 1 50 1 51 1 54 1 56 1 58 1
		 60 1 62 1 64 1 66 1 68 1 70 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[14:37]"  1 2 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[14:37]"  1 1 1 2 2 2 2 2 
		1 2 1 2 2 2 18 2 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[14:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041499972343444824 0.083333373069763184 0.083333253860473633 
		0.041666746139526367 0.125 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
	setAttr -s 38 ".kox[14:37]"  0.75000011920928955 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.083333253860473633 0.041666746139526367 
		0.125 0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.041666746139526367 0.33333337306976318 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[14:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0;
createNode animCurveTU -n "ct_R_Hand_visibility";
	rename -uid "3C1BA8F6-4AA4-8ED2-1820-D8BC766BC81C";
	setAttr ".tan" 5;
	setAttr -s 40 ".ktv[0:39]"  1 1 4 1 5 1 27 1 34 1 36 1 38 1 40 1 41 1
		 42 1 44 1 44.004 1 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 54 1 55 1 56 1 57 1 58 1
		 59 1 60 1 61 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 40 ".kit[0:39]"  2 2 2 9 9 2 9 9 
		9 9 9 1 9 9 9 9 9 9 9 1 9 9 1 9 9 
		9 1 9 9 9 9 18 18 9 1 18 18 18 18 1;
	setAttr -s 40 ".kot[0:39]"  2 2 2 5 5 2 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 18 18 5 1 18 18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 40 ".kox[34:39]"  0.91666668653488159 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 40 ".koy[34:39]"  0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Hand_translateX";
	rename -uid "643F8404-434F-1C32-E562-EDBFA05DC49A";
	setAttr ".tan" 2;
	setAttr -s 40 ".ktv[0:39]"  1 0 4 0 5 -0.1073594003791383 27 -0.1073594003791383
		 34 -0.1073594003791383 36 -0.1073594003791383 38 -0.1073594003791383 40 -0.1073594003791383
		 41 -0.1073594003791383 42 -0.1073594003791383 44 -0.1073594003791383 44.004 -0.1073594003791383
		 45 -0.1073594003791383 46 -0.1073594003791383 47 -0.1073594003791383 48 -0.1073594003791383
		 49 -0.1073594003791383 50 -0.1073594003791383 51 -0.1073594003791383 52 -0.1073594003791383
		 54 -0.1073594003791383 55 -0.1073594003791383 56 -0.1073594003791383 57 -0.1073594003791383
		 58 -0.1073594003791383 59 -0.1073594003791383 60 -0.1073594003791383 61 -0.1073594003791383
		 62 -0.1073594003791383 64 -0.1073594003791383 66 -0.1073594003791383 68 -0.1073594003791383
		 72 -0.1073594003791383 74 -0.1073594003791383 75 -0.1073594003791383 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 40 ".kit[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kot[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 -0.10735940188169479 0 0 0 0 0;
	setAttr -s 40 ".kox[11:39]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.66666674613952637 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.66666674613952637 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 40 ".koy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Hand_translateY";
	rename -uid "6B213A41-4276-B2DC-4DDB-EAB70E460618";
	setAttr ".tan" 2;
	setAttr -s 40 ".ktv[0:39]"  1 0 4 0 5 0 27 0 34 0 36 0 38 0 40 0 41 0
		 42 0 44 0 44.004 0 45 0 46 0 47 0 48 0 49 0 50 0 51 0 52 0 54 0 55 0 56 0 57 0 58 0
		 59 0 60 0 61 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 40 ".kit[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kot[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 40 ".kox[11:39]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.66666674613952637 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.66666674613952637 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 40 ".koy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Hand_translateZ";
	rename -uid "70A98FC1-4896-E733-3619-79B1B550BE2D";
	setAttr ".tan" 2;
	setAttr -s 40 ".ktv[0:39]"  1 0 4 0 5 0 27 0 34 0 36 0 38 0 40 0 41 0
		 42 0 44 0 44.004 0 45 0 46 0 47 0 48 0 49 0 50 0 51 0 52 0 54 0 55 0 56 0 57 0 58 0
		 59 0 60 0 61 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 40 ".kit[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kot[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 40 ".kox[11:39]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.66666674613952637 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.66666674613952637 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 40 ".koy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "ct_R_Hand_rotateX";
	rename -uid "266C7EEA-4949-13E2-7FBF-2A9E919BF136";
	setAttr ".tan" 2;
	setAttr -s 41 ".ktv[0:40]"  1 0 4 0 5 47.761654659122428 27 41.87135172784366
		 34 47.025366792712589 36 47.761654659122428 38 -12.067556727515218 40 -71.896768114152849
		 41 55.412809031388989 42 233.92058608893018 44 111.65392721248853 44.004 111.65392721248853
		 45 109.97041398319278 46 140.10104084059313 47 184.86543428102095 48 224.97589554456172
		 49 216.29020301666492 50 222.20067312529332 51 116.60171676474701 52 96.236967653502745
		 53 96.236967653502745 54 107.46874913942875 55 94.763682518554347 56 111.62159392821262
		 57 110.91654826771845 58 97.438104705293995 59 92.196702991291616 60 101.40226843408834
		 61 94.946191384215879 62 97.871497698293751 64 102.71418960063785 66 101.56934842577053
		 68 98.134811688844906 72 47.761654659122428 74 47.761654659122428 75 47.761654659122428
		 80 -20.980135110916638 86 -34.589388981297695 92 -43.997315927974832 98 -31.313592466978545
		 104 -20.980135110916638;
	setAttr -s 41 ".kit[11:40]"  1 2 2 2 2 2 2 18 
		18 18 18 2 1 2 2 2 1 2 2 2 2 18 18 2 1 
		18 18 18 18 1;
	setAttr -s 41 ".kot[11:40]"  1 2 2 2 2 2 2 18 
		18 18 18 2 1 2 2 2 1 2 2 2 2 18 18 2 1 
		18 18 18 18 1;
	setAttr -s 41 ".kix[11:40]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666746139526367 0.041666507720947266 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 41 ".kiy[11:40]"  3.203563928604126 -0.029382849112153053 
		0.5258786678314209 0.78128606081008911 0.70005959272384644 -0.15159393846988678 0.1031571626663208 
		-1.0662957429885864 0 0 0 -0.22174523770809174 1.9778414964675903 -0.012305367738008499 
		-0.23524321615695953 -0.091479718685150146 1.9778414964675903 -0.11267980188131332 
		0.051056228578090668 0.084520921111106873 -0.019981248304247856 -0.17983192205429077 
		0 0 0.83359813690185547 -0.59381550550460815 -0.20086279511451721 0 0.20086288452148438 
		-0.59381550550460815;
	setAttr -s 41 ".kox[11:40]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.041666507720947266 0.041666746139526367 
		0.041666746139526367 0.041666507720947266 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.66666674613952637 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 41 ".koy[11:40]"  -1.1151305437088013 0.5258786678314209 
		0.78128606081008911 0.70005959272384644 -0.15159393846988678 0.1031571626663208 -1.8430495262145996 
		-1.0662957429885864 0 0 0 0.29422605037689209 -3.0929720401763916 -0.23524321615695953 
		-0.091479718685150146 0.16066741943359375 -3.0929720401763916 0.051056228578090668 
		0.084520921111106873 -0.019981248304247856 -0.059943974018096924 -0.35966435074806213 
		0 0 -0.10280518233776093 -0.71257889270782471 -0.20086279511451721 0 0.20086270570755005 
		-0.71257889270782471;
createNode animCurveTA -n "ct_R_Hand_rotateY";
	rename -uid "AE8C0AC5-4AAE-DDAD-200A-099415ED6C1F";
	setAttr ".tan" 2;
	setAttr -s 39 ".ktv[0:38]"  1 0 4 0 5 35.652444131225238 27 32.605183302966587
		 34 35.271536527692909 36 35.652444131225238 38 42.427622436579341 40 49.202800741933451
		 41 80.610308068617428 42 58.085139417407589 44 -2.1035167485186292 44.004 -2.1035167485186292
		 45 -7.507791994261324 46 -18.062697148487068 47 16.473820562967379 48 -4.8016629307410117
		 49 23.519863886156553 50 62.043545999825241 52 11.816178306662943 54 -14.320789847763123
		 55 -11.653775751322261 56 -48.701003253708031 57 -54.575245103372744 58 -20.429974939979907
		 59 -17.418013550322787 60 -28.872378145805193 61 -25.735334229023493 62 -10.981515944147448
		 64 13.584190629101451 66 14.043944582084627 68 15.423211754278428 72 35.652444131225238
		 74 35.652444131225238 75 35.652444131225238 80 -1.565951566504022 86 -3.9553457888127301
		 92 -5.3423747800583152 98 -7.808175857826078 104 -1.565951566504022;
	setAttr -s 39 ".kit[11:38]"  1 2 2 2 2 2 18 18 
		18 18 1 2 2 2 1 2 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 39 ".kot[11:38]"  1 2 2 2 2 2 18 18 
		18 18 1 2 2 2 1 2 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 39 ".kix[11:38]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.083333492279052734 0.083333253860473633 0.041666746139526367 
		0.041666626930236816 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666626930236816 0.041666746139526367 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 39 ".kiy[11:38]"  -0.89546418190002441 -0.09432239830493927 
		-0.18421784043312073 0.6027759313583374 -0.37132725119590759 0.49430388212203979 
		0 -0.66640549898147583 0 0 -0.047091536223888397 -0.10252486169338226 0.59594738483428955 
		0.052568644285202026 -0.047091536223888397 0.054751746356487274 0.25750270485877991 
		0.42875245213508606 0.0080242203548550606 0.072218261659145355 0 0 0.62225252389907837 
		-0.10425695031881332 -0.032955508679151535 -0.033622268587350845 0 -0.10425695031881332;
	setAttr -s 39 ".kox[11:38]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.66666674613952637 0.041666746139526367 0.041666507720947266 0.041666746139526367 
		0.66666674613952637 0.041666507720947266 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 39 ".koy[11:38]"  0.6589658260345459 -0.18421784043312073 
		0.6027759313583374 -0.37132725119590759 0.49430388212203979 0.67236506938934326 0 
		-0.66640359163284302 0 0 0.70605736970901489 0.59594738483428955 0.052568644285202026 
		-0.19991637766361237 0.70605736970901489 0.25750270485877991 0.42875245213508606 
		0.0080242203548550606 0.024072753265500069 0.14443673193454742 0 0 -0.053184732794761658 
		-0.12510839104652405 -0.032955508679151535 -0.033622302114963531 0 -0.12510839104652405;
createNode animCurveTA -n "ct_R_Hand_rotateZ";
	rename -uid "72C44AFB-4F7D-1202-704A-94B6D057BD0C";
	setAttr ".tan" 2;
	setAttr -s 39 ".ktv[0:38]"  1 0 4 0 5 51.682170254896 27 55.964159315228379
		 34 52.217418887437546 36 51.682170254896 38 -25.094216887351926 40 -101.87060402959985
		 41 36.910647024890167 42 229.62681524734566 44 124.2598578025033 44.004 124.2598578025033
		 45 117.85194919450134 46 115.77073793201194 47 142.76609675633156 48 188.1637915195077
		 49 156.71156658039959 50 128.25073010138976 52 -10.431720245000095 54 -4.2509540817082687
		 55 -11.625292399728778 56 -23.655623562847033 57 -31.600719836956685 58 -9.7163212351590822
		 59 -4.5706114816683758 60 2.1120493064572612 61 -6.0393793311303261 62 -6.9631564766228751
		 64 5.0347052799275138 66 6.0065247199296099 68 8.9219940278415439 72 51.682170254896
		 74 51.682170254896 75 51.682170254896 80 4.4096732390311999 86 42.194072740126195
		 92 55.431005974608254 98 15.508909218059729 104 4.4096732390311999;
	setAttr -s 39 ".kit[11:38]"  1 2 2 2 2 2 18 18 
		18 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 39 ".kot[11:38]"  1 2 2 2 2 2 18 18 
		18 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 39 ".kix[11:38]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.083333492279052734 0.083333253860473633 0.041666746139526367 
		0.041666626930236816 0.041666746139526367 0.041666746139526367 0.041666507720947266 
		0.041666626930236816 0.041666746139526367 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 39 ".kiy[11:38]"  3.946721076965332 -0.11183910071849823 
		-0.036323990672826767 0.47115790843963623 0.7923392653465271 -0.54894489049911499 
		-0.97239649295806885 0 0 -0.12870648503303528 1.1153340339660645 -0.13866809010505676 
		0.38195481896400452 0.08980957418680191 1.1153340339660645 -0.14226926863193512 -0.016122952103614807 
		0.20940218865871429 0.016961449757218361 0.15265361964702606 0 0 0.9020240306854248 
		0 0.44524511694908142 0 -0.44524532556533813 0;
	setAttr -s 39 ".kox[11:38]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.041666746139526367 0.041666507720947266 
		0.66666674613952637 0.041666746139526367 0.041666507720947266 0.041666746139526367 
		0.66666674613952637 0.041666507720947266 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 39 ".koy[11:38]"  -1.2667195796966553 -0.036323990672826767 
		0.47115790843963623 0.7923392653465271 -0.54894489049911499 -0.49673530459403992 
		-1.9448041915893555 0 0 -0.20996889472007751 -2.3820536136627197 0.38195481896400452 
		0.08980957418680191 0.11663443595170975 -2.3820536136627197 -0.016122952103614807 
		0.20940218865871429 0.016961449757218361 0.050884537398815155 0.30530765652656555 
		0 0 0.074734807014465332 0 0.44524511694908142 0 -0.44524490833282471 0;
createNode animCurveTU -n "ct_R_Hand_scaleX";
	rename -uid "960756CF-413F-40CF-79E7-34BB122B14D5";
	setAttr ".tan" 2;
	setAttr -s 40 ".ktv[0:39]"  1 1 4 1 5 1 27 1 34 1 36 1 38 1 40 1 41 1
		 42 1 44 1 44.004 1 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 54 1 55 1 56 1 57 1 58 1
		 59 1 60 1 61 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 40 ".kit[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kot[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 40 ".kox[11:39]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.66666674613952637 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.66666674613952637 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 40 ".koy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Hand_scaleY";
	rename -uid "0024E591-48A3-3E1C-651A-1DA896BD0D91";
	setAttr ".tan" 2;
	setAttr -s 40 ".ktv[0:39]"  1 1 4 1 5 1 27 1 34 1 36 1 38 1 40 1 41 1
		 42 1 44 1 44.004 1 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 54 1 55 1 56 1 57 1 58 1
		 59 1 60 1 61 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 40 ".kit[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kot[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 40 ".kox[11:39]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.66666674613952637 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.66666674613952637 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 40 ".koy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Hand_scaleZ";
	rename -uid "6CA4AC3E-47EA-D653-B694-ACA3D7AA5EB0";
	setAttr ".tan" 2;
	setAttr -s 40 ".ktv[0:39]"  1 1 4 1 5 1 27 1 34 1 36 1 38 1 40 1 41 1
		 42 1 44 1 44.004 1 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 54 1 55 1 56 1 57 1 58 1
		 59 1 60 1 61 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 40 ".kit[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kot[11:39]"  1 2 2 2 2 2 2 2 
		1 2 2 1 2 2 2 1 2 2 2 2 18 18 2 1 18 
		18 18 18 1;
	setAttr -s 40 ".kix[11:39]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.041666746139526367 
		0.041666507720947266 0.041666746139526367 0.041666626930236816 0.083333253860473633 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666746139526367 
		0.041666507720947266 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 
		0.20833325386047363;
	setAttr -s 40 ".kiy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
	setAttr -s 40 ".kox[11:39]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.041666746139526367 0.66666674613952637 0.041666746139526367 
		0.041666507720947266 0.66666674613952637 0.041666746139526367 0.041666507720947266 
		0.041666746139526367 0.66666674613952637 0.041666507720947266 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666746139526367 0.91666668653488159 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 40 ".koy[11:39]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Wrist_visibility";
	rename -uid "6572E16C-4DED-F7CE-805C-57B304947248";
	setAttr ".tan" 5;
	setAttr -s 31 ".ktv[0:30]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1
		 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 31 ".kit[0:30]"  2 2 2 9 9 1 9 2 
		9 9 9 1 9 9 1 9 1 9 1 9 9 9 18 18 9 
		1 18 18 18 18 1;
	setAttr -s 31 ".kot[0:30]"  2 2 2 5 5 5 5 2 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 18 18 5 
		1 18 18 18 18 1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 31 ".kox[25:30]"  0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[25:30]"  0 0 0 0 0 0;
createNode animCurveTL -n "ct_L_Wrist_translateX";
	rename -uid "1DC60777-4D85-9ED9-4990-25B41200B6D9";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 0 4 0 5 -2.0040854441851956 13 -2.0977473174312928
		 20 -2.0316688416680524 27 -2.0977473174312928 34 -2.0160049022631439 36 -2.0040854441851956
		 38 -1.6881744006673598 40 -1.372263357149524 44 -0.74044127011385219 44.004 -0.74044127011385219
		 46 -0.91555619533061294 48 -1.2764586930466759 52 -0.85397927954895014 54 -0.90186186313502414
		 56 -1.2871916632941875 58 -1.3210464906546966 60 -1.5173022300207444 62 -1.5489751233092734
		 64 -1.3171881233015883 66 -1.2684798088202724 68 -1.2392545975088352 72 -2.0040854441851956
		 74 -2.0040854441851956 75 -2.0040854441851956 80 -0.77817364717615378 86 -0.73202877684416867
		 92 -0.83224670693449487 98 -0.73202877684416867 104 -0.77817364717615378;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  -0.093661874532699585 0.081742413341999054 
		0.011919458396732807 0.31591105461120605 0.31591105461120605 0.63182210922241211 
		0.63182210922241211 -0.17511492967605591 -0.36090248823165894 -0.53601741790771484 
		-0.047882582992315292 -0.53601741790771484 -0.033854827284812927 -0.53601741790771484 
		-0.031672894954681396 0.23178699612617493 0.048708315938711166 0 0 0 -2.0040855407714844 
		0.11536213010549545 0 0 0 0.11536213010549545;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0.06607847660779953 0.011919458396732807 
		0.31591105461120605 0.31591105461120605 0.63182210922241211 0 -1.2636442184448242 
		-0.36090248823165894 0.42247942090034485 -0.7276267409324646 -0.38532981276512146 
		-0.7276267409324646 -0.19625574350357056 -0.7276267409324646 0.23178699612617493 
		0.048708315938711166 0.029225211590528488 0 0 0 -0.093661874532699585 0.13843460381031036 
		0 0 0 0.13843460381031036;
createNode animCurveTL -n "ct_L_Wrist_translateY";
	rename -uid "C41258B3-44A9-183E-4603-A686BBA897EF";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 0 4 0 5 -0.35022374787204097 13 -0.32820090393898643
		 20 -0.31697507204851755 27 -0.32820090393898643 34 -0.34741223201769289 36 -0.35022374787204097
		 38 -0.34925491768740508 40 -0.34828608750276918 44 -0.34634842713349739 44.004 -0.34634842713349739
		 46 -0.37755989633449766 48 -0.35264496086270991 52 0.20482789440943056 54 0.16396771986070269
		 56 -0.39270034552545946 58 -0.39229082898583406 60 -0.39270034552545946 62 -0.40940003370076539
		 64 -0.74772066948416671 66 -0.73943950673465397 68 -0.71459592601648969 72 -0.35022374787204097
		 74 -0.35022374787204097 75 -0.35022374787204097 80 -0.84329863418082573 86 -0.86631935971782803
		 92 -0.68629554212241639 98 -0.80640118024611529 104 -0.84329863418082573;
	setAttr -s 31 ".kit[3:30]"  18 18 18 2 2 2 2 2 
		1 2 2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 31 ".kot[3:30]"  18 18 18 2 2 2 2 2 
		1 2 2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 31 ".kix[11:30]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[11:30]"  0.00193766038864851 -0.031211469322443008 
		0.024914935231208801 -0.0062965336255729198 -0.040860176086425781 -0.0062965336255729198 
		0.00040951653500087559 -0.0062965336255729198 -0.016699688509106636 -0.33832064270973206 
		0.0082811629399657249 0.074530743062496185 0 0 -0.35022374987602234 -0.057551790028810501 
		0 0 -0.078501582145690918 -0.057551790028810501;
	setAttr -s 31 ".kox[11:30]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 31 ".koy[11:30]"  -0.00387532077729702 0.024914935231208801 
		0.55747288465499878 0.0024212130811065435 -0.55666804313659668 0.0024212130811065435 
		-0.00040951653500087559 0.0024212130811065435 -0.33832064270973206 0.0082811629399657249 
		0.024843581020832062 0.14906169474124908 0 0 0.022022843360900879 -0.069062173366546631 
		0 0 -0.078501507639884949 -0.069062173366546631;
createNode animCurveTL -n "ct_L_Wrist_translateZ";
	rename -uid "A092EAF4-4AE0-B7FB-C5B0-17A247F4D560";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 0 4 0 5 0.99432726859175025 13 1.0636147551872497
		 20 0.93542318711660855 27 0.88226474431929458 34 0.97878731838436683 36 0.99432726859175025
		 38 0.96659375318105401 40 0.93886023777035765 44 0.88339320694896517 44.004 0.88339320694896517
		 46 0.68151472010846881 48 0.33927696722666356 52 1.5489964210818004 54 1.7176179848253597
		 56 3.1128604419670478 58 3.2221681797255992 60 3.8179508557267856 62 3.8496719528532837
		 64 3.7054283958025755 66 3.5981051265240001 68 3.533710672694951 72 3.7577776082650636
		 74 3.7577776082650636 75 0.99432726859175025 80 -0.036929837663977984 86 0.2598824808602318
		 92 0.36835602283680685 98 0.040075574822699217 104 -0.036929837663977984;
	setAttr -s 31 ".kit[3:30]"  18 18 18 2 2 2 2 2 
		1 2 2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 31 ".kot[3:30]"  18 18 18 2 2 2 2 2 
		1 2 2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 31 ".ktl[3:30]" no no no yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 31 ".kix[11:30]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[11:30]"  -0.055467031896114349 -0.20187848806381226 
		-0.34223774075508118 -0.54411625862121582 0.16862156987190247 -0.54411625862121582 
		0.10930773615837097 -0.54411625862121582 0.031721096485853195 -0.14424355328083038 
		-0.10732326656579971 0 0 0 0.99432724714279175 0 0.20264293253421783 0 -0.202643021941185 
		0;
	setAttr -s 31 ".kox[11:30]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 31 ".koy[11:30]"  0.1109340637922287 -0.34223774075508118 
		1.2097194194793701 0.65505027770996094 1.39524245262146 0.65505027770996094 0.59578269720077515 
		0.65505027770996094 -0.14424355328083038 -0.10732326656579971 -0.064394451677799225 
		0 0 -2.7634503841400146 0.069287486374378204 0 0.20264293253421783 0 -0.20264282822608948 
		0;
createNode animCurveTA -n "ct_L_Wrist_rotateX";
	rename -uid "7E0C4316-4B72-B207-E6AF-0D9F754C8D9B";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 0 4 0 5 0 13 0 20 0 27 0 34 0 36 0 38 0
		 40 0 44 0 44.004 0 46 0 48 0 52 0 54 0 56 0 58 -0.8507407242347591 60 -12.317641843136728
		 62 -12.313011526333545 64 -12.220006295802106 66 -11.965423577061387 68 -11.201672437818601
		 72 0 74 0 75 0 80 35.629770021476418 86 36.791113407349044 92 38.209211077293951
		 98 36.791113407349044 104 35.629770021476418;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 -0.014848226681351662 
		0 8.0814272223506123e-005 0.00162324751727283 0.0044433064758777618 0.039989914745092392 
		0 0 0 0.050673145800828934 0.022509869188070297 0 -0.022509880363941193 0.050673145800828934;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 -0.20013518631458282 
		0 0.00162324751727283 0.0044433064758777618 0.013329971581697464 0.079979948699474335 
		0 0 0 0.060807798057794571 0.022509869188070297 0 -0.022509858012199402 0.060807798057794571;
createNode animCurveTA -n "ct_L_Wrist_rotateY";
	rename -uid "FC68F772-40DE-9E32-15A4-6DAC4C8A1E4F";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 0 4 0 5 0 13 0 20 0 27 0 34 0 36 0 38 0
		 40 0 44 0 44.004 0 46 0 48 0 52 0 54 0 56 0 58 0.38290404365450748 60 5.5439627323206855
		 62 5.5418787048535769 64 5.5000186195260099 66 5.3854352319654799 68 5.0416837345655106
		 72 0 74 0 75 0 80 -50.711625394106647 86 -55.118794099736846 92 -60.500316695495371
		 98 -55.118794099736846 104 -50.711625394106647;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0.0066829361021518707 
		0 -3.6373141483636573e-005 -0.00073059630813077092 -0.0019998573698103428 -0.017998786643147469 
		0 0 0 -0.19229893386363983 -0.085422448813915253 0 0.085422486066818237 -0.19229893386363983;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0.090077467262744904 
		0 -0.00073059630813077092 -0.0019998573698103428 -0.0059995953924953938 -0.035997625440359116 
		0 0 0 -0.23075881600379944 -0.085422448813915253 0 0.085422404110431671 -0.23075881600379944;
createNode animCurveTA -n "ct_L_Wrist_rotateZ";
	rename -uid "FD51D447-4EE9-2BA8-8378-1B81BF09DE98";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 0 4 0 5 0 13 0 20 0 27 0 34 0 36 0 38 0
		 40 0 44 0 44.004 0 46 0 48 0 52 0 54 0 56 0 58 -0.0834673087674037 60 -1.208500293590834
		 62 -1.2080460070326458 64 -1.1989211395890462 66 -1.173943687615653 68 -1.0990110446232926
		 72 0 74 0 75 0 80 -42.800517893894622 86 -37.279526651869325 92 -30.537933328703001
		 98 -37.279526651869325 104 -42.800517893894622;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 -0.0014567793114110827 
		0 0 0.00015925898333080113 0.00043593876762315631 0.0039234641008079052 0 0 0 0 0.10701123625040054 
		0 -0.10701128840446472 0;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 -0.019635530188679695 
		0 0.00015925898333080113 0.00043593876762315631 0.0013078213669359684 0.0078469393774867058 
		0 0 0 0 0.10701123625040054 0 -0.10701118409633636 0;
createNode animCurveTU -n "ct_L_Wrist_scaleX";
	rename -uid "E09918A0-4BD8-6378-6B09-8AB2E6C52AA3";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1
		 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Wrist_scaleY";
	rename -uid "56E905DC-40D2-DF2B-F231-7D9E27C3F491";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1
		 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Wrist_scaleZ";
	rename -uid "6ED0DD25-41AA-D377-ACD0-BC896B6FCD66";
	setAttr ".tan" 2;
	setAttr -s 31 ".ktv[0:30]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1
		 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 31 ".kit[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kot[5:30]"  1 2 2 2 2 2 1 2 
		2 1 2 1 2 1 2 2 2 18 18 2 1 18 18 18 18 
		1;
	setAttr -s 31 ".kix[5:30]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 31 ".kiy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
	setAttr -s 31 ".kox[5:30]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.083333373069763184 0.16666674613952637 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 31 ".koy[5:30]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Hand_visibility1";
	rename -uid "B08009A5-4770-FA0D-458C-99BB24484971";
	setAttr ".tan" 5;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[0:27]"  2 2 2 9 2 9 9 9 
		1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 18 
		18 18 1;
	setAttr -s 28 ".kot[0:27]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 18 
		18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[22:27]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 28 ".koy[22:27]"  0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Hand_translateX1";
	rename -uid "FB7BD28B-4BF2-E39B-0DB8-E9A4CBCFCE72";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Hand_translateY1";
	rename -uid "7DF10434-4303-379F-13F5-F89DD91C5BC4";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Hand_translateZ1";
	rename -uid "D3CCB174-42D7-8F35-2FD9-F5A020DE41B2";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "ct_R_Hand_rotateX1";
	rename -uid "2A5D661A-4D3D-2C98-0B95-6C922DFC3615";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 -0.72233905677526011 34 -0.72233905677526011
		 36 -0.72233905677526011 38 -1.7681611086667028 40 -2.8139831605581453 44 -4.9056272643410308
		 44.004 -4.9056272643410308 46 -4.416875219478646 48 -4.1359585961635625 52 -12.641156474624088
		 54 -7.2734627049687068 56 62.761960657528881 58 62.921760560981319 60 62.761960657528881
		 62 56.196443500663015 64 -79.751558652056175 66 -79.417782369642438 68 -78.416449662397014
		 72 -0.72233905677526011 74 -0.72233905677526011 75 -0.72233905677526011 80 -44.864668561787838
		 86 -54.73526288718157 92 -50.995876425733428 98 -48.120965497120473 104 -44.864668561787838;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  -0.036506075412034988 0.0085303327068686485 
		0.0049029202200472355 0.013433252461254597 0.093683928251266479 0.013433252461254597 
		0.0027890345081686974 0.013433252461254597 -0.11458989232778549 -2.3727402687072754 
		0.0058254948817193508 0.052429657429456711 0 0 -0.012607194483280182 -0.4285014271736145 
		0 0.057720605283975601 0.053504906594753265 -0.4285014271736145;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0.073012150824069977 0.0049029202200472355 
		-0.14844371378421783 0.059578899294137955 1.222348690032959 0.059578899294137955 
		-0.0027890345081686974 0.059578899294137955 -2.3727402687072754 0.0058254948817193508 
		0.017476553097367287 0.10485946387052536 0 0 0 -0.51420193910598755 0 0.057720661163330078 
		0.053504858165979385 -0.51420193910598755;
createNode animCurveTA -n "ct_R_Hand_rotateY1";
	rename -uid "E3A2D1FC-4228-3473-9CFB-7F973CC41E05";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 -124.62242577504553 34 -124.62242577504553
		 36 -124.62242577504553 38 -100.31330235980127 40 -76.004178944556998 44 -27.385932114068467
		 44.004 -27.385932114068467 46 -23.246081255067384 48 32.728615602784167 52 20.518599523463401
		 54 1.799900000117806 56 -83.944261918343173 58 -95.447948781921824 60 -83.944261918343173
		 62 -94.559796889993763 64 -120.08365160129986 66 -109.99552654727003 68 -103.94260544265148
		 72 -124.62242577504553 74 -124.62242577504553 75 -124.62242577504553 80 -1.4085758414600738
		 86 8.117452873277534 92 -22.56558522961069 98 18.09594161160862 104 -1.4085758414600738;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.84854847192764282 0.07225402444601059 
		0.97694277763366699 1.0491968393325806 -0.32670295238494873 1.0491968393325806 -0.20077721774578094 
		1.0491968393325806 -0.18527603149414063 -0.44547531008720398 0.17607100307941437 
		0 0 0 -2.1750717163085937 0.41565126180648804 0 0 0 0.41565126180648804;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -1.6970969438552856 0.97694277763366699 
		-0.2131049782037735 -2.7462937831878662 -1.4965178966522217 -2.7462937831878662 0.20077721774578094 
		-2.7462937831878662 -0.44547531008720398 0.17607100307941437 0.10564339905977249 
		0 0 0 0 0.4987817108631134 0 0 0 0.4987817108631134;
createNode animCurveTA -n "ct_R_Hand_rotateZ1";
	rename -uid "56319EB6-4F0C-8201-FCE7-3BAA72538030";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 -5.7725618358986459 34 -5.7725618358986459
		 36 -5.7725618358986459 38 -3.0841110572020414 40 -0.39566027850543695 44 4.9812412788877722
		 44.004 4.9812412788877722 46 3.6032224803103508 48 -1.2202025460692509 52 37.047450411493791
		 54 29.067856660335231 56 -78.643789220618331 58 -78.63289665433426 60 -78.643789220618331
		 62 -71.335446701425568 64 80.891438263836093 66 80.399628182723546 68 78.924192274555551
		 72 -5.7725618358986459 74 -5.7725618358986459 75 -5.7725618358986459 80 -38.085323356354905
		 86 -41.812270920308578 92 -13.879515366780602 98 -51.274282097346408 104 -38.085323356354905;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.093844637274742126 -0.024050965905189514 
		-0.084184646606445313 -0.10823561251163483 -0.13927018642425537 -0.10823561251163483 
		0.00019011113909073174 -0.10823561251163483 0.12755464017391205 2.6568603515625 -0.0085837049409747124 
		-0.077253647148609161 0 0 -0.10075020790100098 -0.16261869668960571 0 0 0 -0.16261869668960571;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -0.18768927454948425 -0.084184646606445313 
		0.667896568775177 -0.079453662037849426 -1.8799228668212891 -0.079453662037849426 
		-0.00019011113909073174 -0.079453662037849426 2.6568603515625 -0.0085837049409747124 
		-0.025751214474439621 -0.15450750291347504 0 0 0 -0.19514252245426178 0 0 0 -0.19514252245426178;
createNode animCurveTU -n "ct_R_Hand_scaleX1";
	rename -uid "F2528512-4782-E097-CB7C-5F9B2B9B0F55";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Hand_scaleY1";
	rename -uid "06DE337F-482B-B9EF-8E83-B5A53FC6E184";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Hand_scaleZ1";
	rename -uid "5AFAA6E3-441D-5016-9A9E-E6BE0AAE72B2";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Hips_visibility";
	rename -uid "2D2F9670-426C-F429-4660-608E44DE5455";
	setAttr ".tan" 5;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[0:27]"  2 2 2 9 2 9 9 9 
		1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 18 
		18 18 1;
	setAttr -s 28 ".kot[0:27]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 18 
		18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[22:27]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 28 ".koy[22:27]"  0 0 0 0 0 0;
createNode animCurveTL -n "Hips_translateX";
	rename -uid "43CE0961-409B-B93A-C51D-03B3B8DDAA0F";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "Hips_translateY";
	rename -uid "1D09DDE3-48F1-2EFE-4333-7981B977D958";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "Hips_translateZ";
	rename -uid "9E59EBF9-4DBF-E2A4-E95F-E2928E80B5AE";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Hips_rotateX";
	rename -uid "A6D7FECE-4A02-C50E-05ED-16AAF5C30174";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Hips_rotateY";
	rename -uid "768C0A35-4BE6-AE2B-8647-F0BAAA71CB6D";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Hips_rotateZ";
	rename -uid "85C29216-4C92-292A-E49B-44AFDAA2244C";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Hips_scaleX";
	rename -uid "C283B2C1-43F3-47BA-709F-00811C0B67D1";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Hips_scaleY";
	rename -uid "D578144B-4F15-9993-037C-F286BC865D31";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Hips_scaleZ";
	rename -uid "31C91FB9-4F84-E6BA-29CF-DF8E5AC3D496";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Center_visibility";
	rename -uid "AD98D9E7-4AE8-4954-C49A-C8A018A2C8CA";
	setAttr ".tan" 5;
	setAttr -s 33 ".ktv[0:32]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 45 1 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1
		 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 33 ".kit[0:32]"  2 2 2 2 2 1 9 2 
		9 9 9 1 9 9 9 1 9 9 1 9 1 9 9 9 18 
		18 9 1 18 18 18 18 1;
	setAttr -s 33 ".kot[0:32]"  2 2 2 2 2 5 5 2 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 18 
		18 5 1 18 18 18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
	setAttr -s 33 ".kox[27:32]"  0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 33 ".koy[27:32]"  0 0 0 0 0 0;
createNode animCurveTL -n "Center_translateX";
	rename -uid "90393466-453D-D0E1-0A2B-D09CB2EC1A8B";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 0 4 0 5 -0.13902697571060366 13 -0.13902697571060366
		 20 -0.13902697571060366 27 -0.13902697571060366 34 -0.13902697571060366 36 -0.13902697571060366
		 38 -0.13902697571060366 40 -0.13902697571060366 44 -0.13902697571060366 44.004 -0.13902697571060366
		 45 -0.13902697571060366 46 -0.13902697571060366 48 -0.13902697571060366 52 -0.10974696857041792
		 53 -0.10974696857041787 54 -0.10974696857041792 56 -0.10974696857041792 58 -0.10974696857041792
		 60 -0.10974696857041792 62 -0.1097579751989493 64 -0.10997905597440749 66 -0.11058421923404983
		 68 -0.11239971595242383 72 -0.13902697571060366 74 -0.13902697571060366 75 -0.13902697571060366
		 80 0 86 0.038926568044177927 92 0 98 -0.039 104 0;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -1.1006628483301029e-005 
		-0.00022108077246230096 -0.000605163280852139 -0.005446490366011858 0 0 -0.13902696967124939 
		0.080887958407402039 0 -0.038963265717029572 0 0.080887958407402039;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0 0 0 0 0 0 0 0.029280006885528564 
		0 0 0 0 0 0 -0.00022108077246230096 -0.000605163280852139 -0.0018154967110604048 
		-0.01089299563318491 0 0 0 0.097065582871437073 0 -0.038963302969932556 0 0.097065582871437073;
createNode animCurveTL -n "Center_translateY";
	rename -uid "7DF0146F-4959-340B-A543-84AFD9FF4F96";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 0 4 0 5 -0.27324676159826339 13 -0.3202198084282879
		 20 -0.38285053753498766 27 -0.3202198084282879 34 -0.27896141775594296 36 -0.27324676159826339
		 38 -0.2578993454621778 40 -0.24255192932609224 44 -0.27324676159826339 44.004 -0.27324676159826339
		 45 -0.27320005830525707 46 -0.2722568578865151 48 -0.25226509368801919 52 -0.31987741907877948
		 53 -0.31345755345576554 54 -0.15947798147812187 56 -0.31987741907877948 58 -0.44234936610671638
		 60 -0.31987741907877948 62 -0.32537505767817426 64 -0.41077277268716478 66 -0.40790765549821517
		 68 -0.3993122717630897 72 -0.27324676159826339 74 -0.27324676159826339 75 -0.27324676159826339
		 80 -0.10855359669787663 86 -0.064588769660108875 92 -0.10855359669787663 98 -0.064588769660108875
		 104 -0.10855359669787663;
	setAttr -s 33 ".kit[3:32]"  18 18 18 2 2 2 2 2 
		1 2 2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 
		18 18 18 18 1;
	setAttr -s 33 ".kot[3:32]"  18 18 18 2 2 2 2 2 
		1 2 2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 
		18 18 18 18 1;
	setAttr -s 33 ".ktl[4:32]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes;
	setAttr -s 33 ".kix[11:32]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.041666507720947266 
		0.041666746139526367 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[11:32]"  -0.030694833025336266 4.6703291445737705e-005 
		0.00094320042990148067 0.019991764798760414 0.020981667563319206 0.0064198654145002365 
		0.15397956967353821 0.020981667563319206 -0.12247194349765778 0.020981667563319206 
		-0.0054976386018097401 -0.085397712886333466 0.0028651172760874033 0.025786152109503746 
		0 0 -0.27324676513671875 0.094844520092010498 0 0 0 0.094844520092010498;
	setAttr -s 33 ".kox[11:32]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.041666746139526367 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 33 ".koy[11:32]"  0 0.00094320042990148067 0.019991764798760414 
		-0.067612327635288239 -0.020981667563319206 0.15397956967353821 -0.16039943695068359 
		-0.020981667563319206 0.12247194349765778 -0.020981667563319206 -0.085397712886333466 
		0.0028651172760874033 0.0085953837260603905 0.051572374999523163 0 0 -0.046973045915365219 
		0.11381346732378006 0 0 0 0.11381346732378006;
createNode animCurveTL -n "Center_translateZ";
	rename -uid "680C2479-4101-D42F-4BD1-E68CD6B6F5AF";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 0 4 0 5 0 13 0.083507638808932555 20 0
		 27 -0.059507708208817067 34 -0.0082520615524334026 36 0 38 0.10216178966705371 40 0.20432357933410741
		 44 0.31158562750591617 44.004 0.31158562750591617 45 0.29673144078983438 46 0.32762540532039502
		 48 0.98244367494096363 52 1.4654533729962789 53 1.4526238678623116 54 1.6319908952639468
		 56 2.2249858731623777 58 2.0695438471034096 60 2.7608485474166153 62 2.7129479455398564
		 64 2.9227495451227958 66 2.4001692225840934 68 2.086618639071772 72 2.5106648754699501
		 74 2.5106648754699501 75 0 80 0 86 0.24152478473257549 92 0 98 0.24152478473257549
		 104 0;
	setAttr -s 33 ".kit[3:32]"  18 2 18 2 2 2 2 2 
		1 2 2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 
		18 18 18 18 1;
	setAttr -s 33 ".kot[3:32]"  18 2 18 2 2 2 2 2 
		1 2 2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 
		18 18 18 18 1;
	setAttr -s 33 ".kix[11:32]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.083333373069763184 0.041666626930236816 0.041666507720947266 
		0.041666746139526367 0.041666626930236816 0.083333492279052734 0.041666626930236816 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[11:32]"  0.10726204514503479 -0.014854187145829201 
		0.030893964692950249 0.65481829643249512 0.67085802555084229 -0.012829504907131195 
		0.17936702072620392 0.67085802555084229 -0.15544202923774719 0.67085802555084229 
		-0.047900602221488953 0.20980159938335419 -0.52258032560348511 0 0 0 0 0 0 0 0 0;
	setAttr -s 33 ".kox[11:32]"  0.75000011920928955 0.041666626930236816 
		0.083333373069763184 0.16666674613952637 0.66666674613952637 0.041666746139526367 
		0.083333253860473633 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.16666674613952637 
		0.083333253860473633 0.041666746139526367 0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 33 ".koy[11:32]"  -0.31158563494682312 0.030893964692950249 
		0.65481829643249512 0.4830096960067749 -0.98244369029998779 0.17936702072620392 0.59299498796463013 
		-0.98244369029998779 0.69130468368530273 -0.98244369029998779 0.20980159938335419 
		-0.52258032560348511 -0.31355059146881104 0 0 -2.5106649398803711 0.083507642149925232 
		0 0 0 0 0;
createNode animCurveTA -n "Center_rotateX";
	rename -uid "6A533562-4E2A-4247-27F9-E09AA09E9F73";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 0 4 0 5 7.0828009193283723 13 7.0828009193283723
		 20 7.0828009193283723 27 7.0828009193283723 34 7.0828009193283723 36 7.0828009193283723
		 38 8.7636528561356837 40 10.444504792942992 44 13.806208666557607 44.004 13.806208666557607
		 45 12.428183548881913 46 9.7050313790518459 48 8.2786404175390587 52 16.048994876728937
		 53 16.018686249852333 54 16.836398175397591 56 1.1539799063922018 58 1.7115935702411484
		 60 6.742840924624792 62 6.6721385862087539 64 6.6784972492790988 66 6.6869202178961693
		 68 6.7121892217832038 72 7.0828009193283723 74 7.0828009193283723 75 7.0828009193283723
		 80 10.879024433017799 86 10.871543179924112 92 10.862407942483161 98 10.871543179924112
		 104 10.879024433017799;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0.029336400330066681 0.029336400330066681 
		0.058672800660133362 0.058672800660133362 -0.024051075801253319 -0.047527972608804703 
		-0.024895219132304192 -0.096474267542362213 -0.00052898534340783954 0.014271765016019344 
		-0.096474267542362213 0.0097321942448616028 -0.096474267542362213 -0.0012339885579422116 
		0.00011097960668848827 0.00014700854080729187 0.0013230819022282958 0 0 0.12361819297075272 
		0 -0.00014500624092761427 0 0.00014500629913527519 0;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0.029336400330066681 0.029336400330066681 
		0.058672800660133362 0 -0.11734560132026672 -0.047527972608804703 -0.024895219132304192 
		0.13561826944351196 -0.02087133564054966 0.014271765016019344 -0.27370983362197876 
		-0.02087133564054966 0.087811835110187531 -0.02087133564054966 0.00011097960668848827 
		0.00014700854080729187 0.00044102731044404209 0.0026461677625775337 0 0 0 0 -0.00014500624092761427 
		0 0.00014500616816803813 0;
createNode animCurveTA -n "Center_rotateY";
	rename -uid "78374CF1-4FDB-F6D0-6D58-2D8BF54D54F3";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 0 4 0 5 -70.62156893923715 13 -70.62156893923715
		 20 -70.62156893923715 27 -70.62156893923715 34 -70.62156893923715 36 -70.62156893923715
		 38 -61.686267747165772 40 -52.750966555094386 44 -34.88036417095163 44.004 -34.88036417095163
		 45 -29.469078870907985 46 -6.2430627498552225 48 54.683711555180821 52 28.811744811397674
		 53 28.812000000000005 54 11.676321844005887 56 -30.249177418411296 58 -41.214691525069775
		 60 -30.249177418411296 62 -37.686244694727129 64 -37.593760966359064 66 -38.281838376525066
		 68 -40.346078297432229 72 -70.62156893923715 74 -70.62156893923715 75 -70.62156893923715
		 80 0.57233761547866424 86 -0.060206492174135519 92 -0.83259589578347526 98 -0.060206492174135519
		 104 0.57233761547866424;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0.15595042705535889 0.15595042705535889 
		0.31190085411071777 0.31190085411071777 0.094444744288921356 0.40537044405937195 
		1.0633728504180908 1.5631879568099976 0 -0.29907399415969849 1.5631879568099976 -0.19138433039188385 
		1.5631879568099976 -0.12980131804943085 0.0016141455853357911 -0.012009216472506523 
		-0.10808335244655609 0 0 -1.2325788736343384 0 -0.012260357849299908 0 0.012260363437235355 
		0;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0.15595042705535889 0.15595042705535889 
		0.31190085411071777 0 -0.62380170822143555 0.40537044405937195 1.0633728504180908 
		-0.45155099034309387 -2.1869897842407227 -0.29907399415969849 -0.73173803091049194 
		-2.1869897842407227 0.19138433039188385 -2.1869897842407227 0.0016141455853357911 
		-0.012009216472506523 -0.036027781665325165 -0.21616700291633606 0 0 0 0 -0.012260357849299908 
		0 0.01226035226136446 0;
createNode animCurveTA -n "Center_rotateZ";
	rename -uid "34995BED-4126-A220-07AA-5DADFEBF178A";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 0 4 0 5 -5.8442678141187452 13 -5.8442678141187452
		 20 -5.8442678141187452 27 -5.8442678141187452 34 -5.8442678141187452 36 -5.8442678141187452
		 38 -4.5818692991549232 40 -3.3194707841911 44 -0.79467375426345455 44.004 -0.79467375426345455
		 45 0.83223841445035385 46 5.5596914051724218 48 10.129444344475083 52 -0.10670599497908864
		 53 -0.77529515275300764 54 4.8500588888396878 56 -16.371770310851616 58 -17.748280975162054
		 60 -16.371770310851623 62 -17.313943407743487 64 -17.183805262903544 66 -16.947565566955461
		 68 -16.238843808838148 72 -5.8442678141187452 74 -5.8442678141187452 75 -5.8442678141187452
		 80 -2.9752513594010841 86 0.31403112305683689 92 4.3305208890524902 98 0.31403112305683689
		 104 -2.9752513594010841;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0.022033009678125381 0.022033009678125381 
		0.044066019356250763 0.044066019356250763 0.028394974768161774 0.082509621977806091 
		0.079757235944271088 0.19066183269023895 -0.011669082567095757 0.098180949687957764 
		0.19066183269023895 -0.024024643003940582 0.19066183269023895 -0.016444021835923195 
		0.002271339064463973 0.0041231606155633926 0.037108585238456726 0 0 -0.10200171917676926 
		0.048855714499950409 0.063754886388778687 0 -0.063754923641681671 0.048855714499950409;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0.022033009678125381 0.022033009678125381 
		0.044066019356250763 0 -0.088132038712501526 0.082509621977806091 0.079757235944271088 
		-0.17865452170372009 -0.27879387140274048 0.098180949687957764 -0.37039080262184143 
		-0.27879387140274048 0.024024643003940582 -0.27879387140274048 0.002271339064463973 
		0.0041231606155633926 0.012369528412818909 0.074217274785041809 0 0 0 0.058626879006624222 
		0.063754886388778687 0 -0.063754856586456299 0.058626879006624222;
createNode animCurveTU -n "Center_scaleX";
	rename -uid "8591CC31-4926-65E9-529B-F9A17731D4E5";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 45 1 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1
		 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Center_scaleY";
	rename -uid "DC28AFF8-4A71-138C-05F7-A6AAF4704330";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 45 1 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1
		 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Center_scaleZ";
	rename -uid "1D1C81EE-4160-895B-97C5-7E84B71EFBEC";
	setAttr ".tan" 2;
	setAttr -s 33 ".ktv[0:32]"  1 1 4 1 5 1 13 1 20 1 27 1 34 1 36 1 38 1
		 40 1 44 1 44.004 1 45 1 46 1 48 1 52 1 53 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1
		 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 33 ".kit[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kot[5:32]"  1 2 2 2 2 2 1 2 
		2 2 1 2 2 1 2 1 2 2 2 18 18 2 1 18 18 
		18 18 1;
	setAttr -s 33 ".kix[5:32]"  0.33333337306976318 0.29166662693023682 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.041499972343444824 0.041666626930236816 0.083333373069763184 
		0.041666626930236816 0.041666507720947266 0.041666746139526367 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 33 ".kiy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
	setAttr -s 33 ".kox[5:32]"  0.29166662693023682 0.083333373069763184 
		0.083333373069763184 0.083333253860473633 0.16666674613952637 0.00016665458679199219 
		0.75000011920928955 0.041666626930236816 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.041666746139526367 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 33 ".koy[5:32]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Spine_visibility";
	rename -uid "4052A279-4E74-6DC8-00A0-20B5D81936CD";
	setAttr ".tan" 5;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[0:27]"  2 2 2 9 2 9 9 9 
		1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 18 
		18 18 1;
	setAttr -s 28 ".kot[0:27]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 18 
		18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[22:27]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 28 ".koy[22:27]"  0 0 0 0 0 0;
createNode animCurveTL -n "Spine_translateX";
	rename -uid "1923353E-4F17-021D-9150-C8B71108F162";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "Spine_translateY";
	rename -uid "7A47D73D-4689-120F-3CD8-A09342B5B4F4";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "Spine_translateZ";
	rename -uid "51E50C57-4879-8E69-C914-4EA5854CEFD9";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Spine_rotateX";
	rename -uid "D14A83FA-4DD8-80EB-6E3A-B0A728B695F0";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 10.178645850566008 34 10.178645850566008
		 36 10.178645850566008 38 0.59655150987379713 40 -8.9855428308184138 44 7.180186909854382
		 44.004 7.180186909854382 46 6.9342152380140414 48 -1.217520645561075 52 -2.7625733206326237
		 54 -2.853960337349783 56 16.170937092355313 58 25.17433199961825 60 21.753366959397258
		 62 23.195615566314235 64 38.570544215572554 66 37.979047993228313 68 36.204552685155349
		 72 10.178645850566008 74 10.178645850566008 75 10.178645850566008 80 -3.0373756005162833
		 86 -3.0373756005162833 92 -3.0373756005162833 98 -3.0373756005162833 104 -3.0373756005162833;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.28214520215988159 -0.0042930156923830509 
		-0.1422746330499649 -0.14656764268875122 -0.0015950043452903628 -0.14656764268875122 
		0.1571388840675354 -0.14656764268875122 0.02517198771238327 0.26834312081336975 -0.010323556140065193 
		-0.092912353575229645 0 0 0.17765088379383087 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0.052332982420921326 -0.1422746330499649 
		-0.026966257020831108 0.19890062510967255 0.33204710483551025 0.19890062510967255 
		-0.059707105159759521 0.19890062510967255 0.26834312081336975 -0.010323556140065193 
		-0.030970785766839981 -0.18582497537136078 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Spine_rotateY";
	rename -uid "B2840CD3-4424-86DB-1E10-6D96CE9D4FA8";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 6.2956922923466685
		 40 12.591384584693337 44 25.182769169386688 44.004 25.182769169386688 46 23.856278523669982
		 48 23.809429362481879 52 16.573970868388489 54 14.902554827836539 56 14.755406974131803
		 58 7.0013400765056106 60 14.7554069741318 62 13.339616554024868 64 13.30369980760036
		 66 13.026540193965209 68 12.195058156967001 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.21976111829280853 -0.02315162867307663 
		-0.0008176721166819334 -0.023969301953911781 -0.029171712696552277 -0.023969301953911781 
		-0.13533399999141693 -0.023969301953911781 -0.02471020445227623 -0.0006268654833547771 
		-0.0048373476602137089 -0.043536297976970673 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -0.43952223658561707 -0.0008176721166819334 
		-0.12628257274627686 -0.41555294394493103 -0.0025682144332677126 -0.41555294394493103 
		0.13533399999141693 -0.41555294394493103 -0.0006268654833547771 -0.0048373476602137089 
		-0.014512099325656891 -0.087072722613811493 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Spine_rotateZ";
	rename -uid "DAE42578-4A38-22F9-91B5-5B9F741B5CC8";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 1.5412761423787027
		 40 3.0825522847574054 44 6.1651045695147841 44.004 6.1651045695147841 46 5.3450798181038754
		 48 -4.6689138000760764 52 -0.20529303714714905 54 -8.2567671198402302 56 6.7209933238267983
		 58 5.3984141681182791 60 6.7209933238267965 62 6.9950087928409017 64 6.9294572503706506
		 66 6.7850939766860785 68 6.3520024795064316 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.053800687193870544 -0.014312132261693478 
		-0.17477716505527496 -0.18908929824829102 -0.1405247300863266 -0.18908929824829102 
		-0.023083360865712166 -0.18908929824829102 0.0047824722714722157 -0.0011440902017056942 
		-0.0025196145288646221 -0.022676616907119751 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -0.10760137438774109 -0.17477716505527496 
		0.077904880046844482 0.08148791640996933 0.26141121983528137 0.08148791640996933 
		0.023083360865712166 0.08148791640996933 -0.0011440902017056942 -0.0025196145288646221 
		-0.0075588724575936794 -0.045353300869464874 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Spine_scaleX";
	rename -uid "F2F10640-4FBC-11F3-9D47-02895877232E";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Spine_scaleY";
	rename -uid "3FC0D944-404C-2499-2077-A6A278721115";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Spine_scaleZ";
	rename -uid "17472E00-4705-4603-73FC-909424EB8633";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Chest_Ctrl_visibility";
	rename -uid "B436A015-4A4E-5EB2-1F1D-B78E27A717FF";
	setAttr ".tan" 5;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[0:27]"  2 2 2 9 2 9 9 9 
		1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 18 
		18 18 1;
	setAttr -s 28 ".kot[0:27]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 18 
		18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[22:27]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 28 ".koy[22:27]"  0 0 0 0 0 0;
createNode animCurveTL -n "Chest_Ctrl_translateX";
	rename -uid "31321A26-4409-DD13-3CA5-9F88C75E9882";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "Chest_Ctrl_translateY";
	rename -uid "304288C4-4C52-ADBE-6C1A-EAA4288525F6";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "Chest_Ctrl_translateZ";
	rename -uid "CA16165C-4B40-1B57-EC15-E8BDB4711BC7";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0 86 0 92 0
		 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Chest_Ctrl_rotateX";
	rename -uid "C9819C87-43ED-8AE5-F142-5C8AFB0C70B7";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 8.4418854603926796 34 8.4418854603926796
		 36 8.4418854603926796 38 0.59876431067672498 40 -7.2443568390392299 44 5.8756666463775247
		 44.004 5.8756666463775247 46 6.0467883307249304 48 6.777542122453009 52 5.4012618918704867
		 54 5.7044739152123842 56 8.9933284738395365 58 16.652241338664346 60 20.463859024955418
		 62 20.557919692364365 64 20.461869623445029 66 20.211453985867724 68 19.460204276524006
		 72 8.4418854603926796 74 8.4418854603926796 75 8.4418854603926796 80 -7.9688213833253752
		 86 -7.9688213833253752 92 -7.9688213833253752 98 -7.9688213833253752 104 -7.9688213833253752;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.22898760437965393 0.0029866367112845182 
		0.01275405939668417 0.015740696340799332 0.0052920482121407986 0.015740696340799332 
		0.13367325067520142 0.015740696340799332 0.0016416683793067932 -0.0016763899475336075 
		-0.0043705771677196026 -0.039335343986749649 0 0 0.14733870327472687 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0.044788967818021774 0.01275405939668417 
		-0.024020621553063393 0.029048271477222443 0.057401340454816818 0.029048271477222443 
		0.066525280475616455 0.029048271477222443 -0.0016763899475336075 -0.0043705771677196026 
		-0.01311178132891655 -0.078670799732208252 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Chest_Ctrl_rotateY";
	rename -uid "F717C75F-4441-90B4-5334-A89D3AF8462F";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 4.2178577425224262
		 40 8.4357154850448524 44 16.871430970089619 44.004 16.871430970089619 46 16.190654021578759
		 48 20.358306256536594 52 20.484026201658992 54 19.120176521723344 56 19.597791644622937
		 58 16.201941040189798 60 5.8647011733397889 62 4.6566645388991645 64 4.6769348701849403
		 66 4.5794990063398391 68 4.2871902976695306 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.14723101258277893 -0.011881799437105656 
		0.072739250957965851 0.060857456177473068 -0.023803668096661568 0.060857456177473068 
		-0.059268772602081299 0.060857456177473068 -0.021084217354655266 0.0003537840093486011 
		-0.0017005766276270151 -0.015305248089134693 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -0.29446202516555786 0.072739250957965851 
		0.0021942269522696733 -0.35531947016716003 0.008335956372320652 -0.35531947016716003 
		-0.18041886389255524 -0.35531947016716003 0.0003537840093486011 -0.0017005766276270151 
		-0.0051017492078244686 -0.030610540881752968 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "Chest_Ctrl_rotateZ";
	rename -uid "3F8E4F07-41EB-17E1-6857-328820215551";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 0 4 0 5 0 34 0 36 0 38 0.42769005153861028
		 40 0.85538010307722057 44 1.7107602061544358 44.004 1.7107602061544358 46 1.6495020038966177
		 48 2.2290713276551353 52 0.87031655164501021 54 1.1958701225347803 56 7.7345109955743991
		 58 7.0734461980468053 60 5.5084111831498497 62 5.3743115790279603 64 5.3397878750735508
		 66 5.2285426074112378 68 4.8948055521507561 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0.01492919959127903 -0.0010691572679206729 
		0.010115392506122589 0.009046236053109169 0.005681981798261404 0.009046236053109169 
		-0.011537757702171803 0.009046236053109169 -0.0023404795210808516 -0.0006025523180142045 
		-0.001941596157848835 -0.017474431544542313 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  -0.02985839918255806 0.010115392506122589 
		-0.023714745417237282 -0.038904633373022079 0.11412081122398376 -0.038904633373022079 
		-0.027315013110637665 -0.038904633373022079 -0.0006025523180142045 -0.001941596157848835 
		-0.0058248103596270084 -0.034948911517858505 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Chest_Ctrl_scaleX";
	rename -uid "07E3562A-4DDE-51C8-2DDD-ADBB2E0813C4";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Chest_Ctrl_scaleY";
	rename -uid "5EA2344F-47B0-260F-C9B7-0E9FFE3782C1";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "Chest_Ctrl_scaleZ";
	rename -uid "1409F854-46F9-0488-4D09-03A1B9BDCFD8";
	setAttr ".tan" 2;
	setAttr -s 28 ".ktv[0:27]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1
		 98 1 104 1;
	setAttr -s 28 ".kit[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kot[8:27]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 28 ".kix[8:27]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 28 ".kiy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 28 ".kox[8:27]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 28 ".koy[8:27]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "HeadCtrl_visibility";
	rename -uid "8F10F454-41AF-A16B-3FA5-F8B122DBBEA3";
	setAttr ".tan" 5;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 13 1 34 1 36 1 38 1 40 1 44 1
		 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1
		 86 1 92 1 98 1 104 1;
	setAttr -s 29 ".kit[0:28]"  2 2 2 9 9 2 9 9 
		9 1 9 9 1 9 1 9 1 9 9 9 18 18 9 1 18 
		18 18 18 1;
	setAttr -s 29 ".kot[0:28]"  2 2 2 5 5 2 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 18 18 5 1 18 
		18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[23:28]"  0.33333337306976318 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 29 ".koy[23:28]"  0 0 0 0 0 0;
createNode animCurveTL -n "HeadCtrl_translateX";
	rename -uid "8424C050-4BF6-4DD0-942B-02A5D5294951";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 13 0 34 0 36 0 38 0 40 0 44 0
		 44.004 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0
		 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "HeadCtrl_translateY";
	rename -uid "2882A598-4087-756D-E98D-5A9D3528DB40";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 13 0 34 0 36 0 38 0 40 0 44 0
		 44.004 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0
		 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "HeadCtrl_translateZ";
	rename -uid "485E561D-44CC-E1DF-8960-E9BE25993690";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 0 13 0 34 0 36 0 38 0 40 0 44 0
		 44.004 0 46 0 48 0 52 0 54 0 56 0 58 0 60 0 62 0 64 0 66 0 68 0 72 0 74 0 75 0 80 0
		 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "HeadCtrl_rotateX";
	rename -uid "510EC2B3-4ACF-82D8-5AFF-D9B1257749BC";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 -28.222158196309024 20 -31.112222944807609
		 34 -28.426817988750273 36 -28.222158196309024 38 -17.975297824185837 40 -7.7284374520626535
		 44 -21.096884355002459 44.004 -21.096884355002459 46 -22.577090560860356 48 -44.904248555353924
		 52 -22.656440952905893 54 -22.358280647787396 56 -24.828799730831644 58 -25.57457502963107
		 60 -42.115991781220551 62 -34.908341501892927 64 -42.52670488402692 66 -46.715340272454654
		 68 -49.228540671490656 72 -28.222158196309024 74 -28.222158196309024 75 -28.222158196309024
		 80 -2.1029205585461632 86 -2.1029205585461632 92 -2.1029205585461632 98 -2.1029205585461632
		 104 -2.1029205585461632;
	setAttr -s 29 ".kit[2:28]"  1 18 2 2 2 2 2 1 
		2 2 1 2 18 18 18 18 2 2 18 18 2 1 18 18 18 
		18 1;
	setAttr -s 29 ".kot[2:28]"  1 18 2 2 2 2 2 1 
		2 2 1 2 18 18 18 18 2 2 18 18 2 1 18 18 18 
		18 1;
	setAttr -s 29 ".kix[2:28]"  0.041165892034769058 0.625 0.58333331346511841 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.041666626930236816 0.083166599273681641 0.083333373069763184 0.041666626930236816 
		0.083333253860473633 0.083333253860473633 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041165892034769058 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[2:28]"  -0.4971390962600708 0 0.046869158744812012 
		0.0035719871520996094 0.17884145677089691 0.17884145677089691 -0.23332341015338898 
		-0.23332341015338898 -0.025834470987319946 -0.38968241214752197 -0.1487908661365509 
		0.0052038789726793766 -0.028067423030734062 -0.039048701524734497 0 0 -0.13296552002429962 
		-0.073105476796627045 0 0 0 -0.4971390962600708 0 0 0 0 0;
	setAttr -s 29 ".kox[2:28]"  0.62450683116912842 0.58333331346511841 
		0.083333373069763184 0.083333373069763184 0.083333253860473633 0.16666674613952637 
		0.00016665458679199219 0.75000011920928955 0.083333373069763184 0.16666674613952637 
		0.66666674613952637 0.083333253860473633 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666746139526367 0.62450683116912842 
		0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[2:28]"  -0.08557254821062088 0 0.0035719871520996094 
		0.17884145677089691 0.17884145677089691 -0.23332341015338898 0 -0.12435948848724365 
		-0.38968241214752197 0.38829749822616577 0.024431373924016953 -0.043118692934513092 
		-0.028067503124475479 -0.039048589766025543 0 0 -0.073105476796627045 -0.043863620609045029 
		0 0 0 -0.08557254821062088 0 0 0 0 0;
createNode animCurveTA -n "HeadCtrl_rotateY";
	rename -uid "C0A2904A-42F2-A654-FCA6-D1818943A608";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 45.927701415460298 13 45.927701415460326
		 34 45.927701415460298 36 45.927701415460298 38 38.593531799683028 40 31.259362183905733
		 44 2.9384465165020979 44.004 2.9384465165020979 46 2.1967246662535973 48 -58.434720470046138
		 52 -45.230503805887068 54 -36.505270058579818 56 -18.528625001697989 58 -13.645179439638284
		 60 -47.450384372388257 62 -25.61141515935072 64 13.377980438593484 66 12.311485337764037
		 68 11.67158339586347 72 45.927701415460298 74 45.927701415460298 75 45.927701415460298
		 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 18 18 18 
		18 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 18 18 18 
		18 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  -0.494293212890625 -0.012945488095283508 
		-1.0582183599472046 -0.77761143445968628 0.15228405594825745 0.19949163496494293 
		0 0 0.5308268666267395 0.68049335479736328 -0.018613850697875023 0 0 0 0.80158960819244385 
		0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0.75030404329299927 -1.0582183599472046 
		0.23045705258846283 1.5279154777526855 0.31375163793563843 0.19949221611022949 0 
		0 0.53082835674285889 -0.018613850697875023 -0.011168396100401878 0 0 0 0 0 0 0 0 
		0;
createNode animCurveTA -n "HeadCtrl_rotateZ";
	rename -uid "E1B7243E-4EA1-F08D-CE1F-69AB9FCCAFF7";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 0 4 0 5 -17.587320948164937 13 -17.587320948164948
		 34 -17.587320948164937 36 -17.587320948164937 38 -10.23772355121902 40 -2.888126154273102
		 44 -6.5890094888171928 44.004 -6.5890094888171928 46 -5.148468937404127 48 35.623718770540755
		 52 5.8584148126404214 54 2.5032684693800586 56 -1.0909579490499559 58 -2.0351931186519652
		 60 26.866376652511146 62 9.70498133523982 64 -18.249669018367907 66 -21.00889476775351
		 68 -22.664442835991132 72 -17.587320948164937 74 -17.587320948164937 75 -17.587320948164937
		 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 18 
		18 18 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 18 
		18 18 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  -0.064592599868774414 0.025142176076769829 
		0.71160894632339478 0.3937811553478241 -0.058558352291584015 0.3937811553478241 -0.016480011865496635 
		0 -0.39371120929718018 -0.14447313547134399 -0.048157572746276855 0 0 0 -0.30695664882659912 
		0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.083333253860473633 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  -0.19195674359798431 0.71160894632339478 
		-0.51950258016586304 -0.58573788404464722 -0.062731087207794189 -0.58573788404464722 
		0.50442755222320557 0 -0.39371234178543091 -0.14447271823883057 -0.028894765302538872 
		0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "HeadCtrl_scaleX";
	rename -uid "DCD2EAF4-43E1-8587-FE50-EE96069252C6";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 13 1 34 1 36 1 38 1 40 1 44 1
		 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1
		 86 1 92 1 98 1 104 1;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "HeadCtrl_scaleY";
	rename -uid "D0200BE5-428B-D227-37F7-AFACCAB09AAC";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 13 1 34 1 36 1 38 1 40 1 44 1
		 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1
		 86 1 92 1 98 1 104 1;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "HeadCtrl_scaleZ";
	rename -uid "49767FF5-4677-EFC4-2B08-1D9612BBFA90";
	setAttr ".tan" 2;
	setAttr -s 29 ".ktv[0:28]"  1 1 4 1 5 1 13 1 34 1 36 1 38 1 40 1 44 1
		 44.004 1 46 1 48 1 52 1 54 1 56 1 58 1 60 1 62 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1
		 86 1 92 1 98 1 104 1;
	setAttr -s 29 ".kit[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kot[9:28]"  1 2 2 1 2 1 2 1 
		2 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 29 ".kix[9:28]"  0.041666626930236816 0.083166599273681641 
		0.083333373069763184 0.041666626930236816 0.083333253860473633 0.041666626930236816 
		0.083333492279052734 0.041666626930236816 0.083333253860473633 0.083333492279052734 
		0.083333253860473633 0.083333253860473633 0.16666674613952637 0.083333253860473633 
		0.041666656732559204 0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 29 ".kiy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 29 ".kox[9:28]"  0.75000011920928955 0.083333373069763184 
		0.16666674613952637 0.66666674613952637 0.083333253860473633 0.66666674613952637 
		0.083333253860473633 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		0.33333337306976318 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 29 ".koy[9:28]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Foot_visibility";
	rename -uid "A52EE1A9-407C-90E0-22CC-F6B23DD0116D";
	setAttr ".tan" 5;
	setAttr -s 35 ".ktv[0:34]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 49 1 50 1 51 1 52 1 54 1 56 1 57 1 58 1 59 1 60 1 62 1 64 1 66 1 68 1 70 1
		 71 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 35 ".kit[0:34]"  2 2 2 9 2 9 9 9 
		1 9 1 1 1 1 1 9 1 1 9 1 1 9 9 9 18 
		9 9 18 9 1 18 18 18 18 1;
	setAttr -s 35 ".kot[0:34]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 18 
		5 5 18 5 1 18 18 18 18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
	setAttr -s 35 ".kox[29:34]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[29:34]"  0 0 0 0 0 0;
createNode animCurveTL -n "ct_L_Foot_translateX";
	rename -uid "C01DD8FF-4396-1C0E-10B1-C3AB877EF581";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 -0.055653054312164309 34 -0.055653054312164309
		 36 -0.055653054312164309 38 -0.055653054312164309 40 -0.055653054312164309 44 -0.055653054312164309
		 44.004 -0.055653054312164309 46 -0.055653054312164309 48 -0.055653054312164309 49 -0.055653054312164309
		 50 -0.055653054312164309 51 -0.055653054312164309 52 -0.055653054312164309 54 -0.045611092443684977
		 56 0.089740703625163468 57 0.089740703625163468 58 0.089740703625163468 59 0.089740703625163468
		 60 0.089740703625163468 62 0.08968604875301317 64 0.088588242855195079 66 0.085583224592908888
		 68 0.076568134757915171 70 0.0036960811169208241 71 -0.033397022293485415 72 -0.055653054312164309
		 74 -0.055653054312164309 75 -0.055653054312164309 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0.010041961446404457 0 0 
		0 0 0 -5.4654872656101361e-005 -0.0010978059144690633 -0.0030050182249397039 -0.027045270428061485 
		-0.072872050106525421 -0.037093102931976318 0 0 -0.055653054267168045 0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 0.13535179197788239 0 0 0 
		0 0 -0.0010978059144690633 -0.0030050182249397039 -0.0090150898322463036 -0.027045346796512604 
		-0.037093102931976318 -0.022256031632423401 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "ct_L_Foot_translateY";
	rename -uid "0D36F7C9-410E-BCC0-5152-E09301CD8A69";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 49 0 50 0 51 0 52 0 54 0.18926614581075568 56 0 57 0 58 0 59 0 60 0 62 0
		 64 0 66 0 68 0 70 0.091581083125463716 71 0.075274222262855725 72 0 74 0 75 0 80 0.018087203192318147
		 86 0.051192925846657397 92 0.091617789611378175 98 0.25234158882992025 104 0.018087203192318147;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0.1892661452293396 0 0 0 
		0 0 0 0 0 0 0.091581083834171295 -0.016306860372424126 0 0 0 0.023269506171345711 
		0.036765292286872864 0.10057428479194641 0 0.023269506171345711;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 -0.1892661452293396 0 0 0 
		0 0 0 0 0 0 -0.016306860372424126 -0.07527422159910202 0 0 0 0.027923418208956718 
		0.036765292286872864 0.10057438164949417 0 0.027923418208956718;
createNode animCurveTL -n "ct_L_Foot_translateZ";
	rename -uid "723E5940-493D-2780-C405-C4ADD4FA89BC";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 0.59804341805047534 34 0.59804341805047534
		 36 0.59804341805047534 38 0.59804341805047534 40 0.59804341805047534 44 0.59804341805047534
		 44.004 0.59804341805047534 46 0.59804341805047534 48 0.7373215094558293 49 0.7373215094558293
		 50 0.7373215094558293 51 0.7373215094558293 52 0.7373215094558293 54 1.2328320266372321
		 56 2.7992563473535088 57 2.7992563473535088 58 2.7992563473535088 59 2.9224768776710777
		 60 2.9224768776710777 62 2.9274627550095187 64 3.0276098260473834 66 2.9010702712549867
		 68 2.8251459587143084 70 2.966927430346602 71 3.0555402169307695 72 3.1087082935204249
		 74 3.1087082935204249 75 0.59804341805047534 80 0.36187922892948682 86 0.025304381943705767
		 92 -0.3856817329976206 98 -0.014365500478306179 104 0.36187922892948682;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0.4955105185508728 0.052585955709218979 
		0.052585955709218979 0 0 0 0.0049858773127198219 0.1001470685005188 -0.12653955817222595 
		0 0.14178146421909332 0.088612787425518036 0 0 0.59804344177246094 -0.26033586263656616 
		-0.37378048896789551 0 0.37378066778182983 -0.26033586263656616;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0.13927808403968811 0 0 0 0 0 1.5664243698120117 
		0.1232205331325531 0.1232205331325531 0.1232205331325531 0 0 0.1001470685005188 -0.12653955817222595 
		-0.075924314558506012 0 0.088612787425518036 0.053168077021837234 0 -2.5106649398803711 
		0 -0.31240317225456238 -0.37378048896789551 0 0.37378031015396118 -0.31240317225456238;
createNode animCurveTA -n "ct_L_Foot_rotateX";
	rename -uid "9F973855-48F0-A289-DF64-65A94B959489";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 49 0 50 0 51 0 52 0 54 57.473826896735488 56 0 57 0 58 0 59 0 60 0 62 0
		 64 0 66 0 68 0 70 28.990791086916087 71 14.266710349466392 72 0 74 0 75 0 80 -13.274104874580392
		 86 11.961068145488127 92 42.775330744939851 98 71.500799085118274 104 -13.274104874580392;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 1.0031075477600098 0 0 0 
		0 0 0 0 0 0 0.50598478317260742 -0.25698369741439819 0 0 0 0 0.48912358283996582 
		0.51958191394805908 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 -1.0031075477600098 0 0 0 
		0 0 0 0 0 0 -0.25698369741439819 -0.24900107085704803 0 0 0 0 0.48912358283996582 
		0.51958239078521729 0 0;
createNode animCurveTA -n "ct_L_Foot_rotateY";
	rename -uid "A2A23A79-461A-0534-0725-B4BF8292DA1F";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 9.4281679113262857 34 9.4281679113262857
		 36 9.4281679113262857 38 16.569657803247424 40 23.711147695168556 44 37.994127479010828
		 44.004 37.994127479010828 46 39.081826813721719 48 80.222614923042698 49 80.222614923042698
		 50 70.832938685584907 51 61.626277481280795 52 47.894287484693862 54 37.685315315610957
		 56 13.161260705357678 57 13.161260705357678 58 13.161260705357678 59 20.108612173062358
		 60 20.108612173062358 62 16.511980901398516 64 42.033081243990068 66 41.353814114927467
		 68 39.316005132934762 70 19.673051040384632 71 12.321479876019016 72 9.4281679113262857
		 74 9.4281679113262857 75 9.4281679113262857 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0.249285027384758 0.018983934074640274 
		0.93182706832885742 0.93182706832885742 0.93182706832885742 0.93182706832885742 0.93182706832885742 
		-0.17818017303943634 -0.034583266824483871 -0.034583266824483871 0 0.93182706832885742 
		0.93182706832885742 -0.062773056328296661 0.44542723894119263 -0.011855447664856911 
		-0.10669942945241928 -0.3428342342376709 -0.12830911576747894 0 0 0.16455256938934326 
		0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  -0.49857005476951599 0.71804219484329224 
		-1.4303971529006958 -1.4303971529006958 -1.4303971529006958 -1.4303971529006958 -1.4303971529006958 
		-0.42802548408508301 0.12125416100025177 0.12125416100025177 0.12125416100025177 
		-1.4303971529006958 -1.4303971529006958 0.44542723894119263 -0.011855447664856911 
		-0.035566475242376328 -0.10669973492622375 -0.12830911576747894 -0.050497818738222122 
		0 0 0 0 0 0 0 0;
createNode animCurveTA -n "ct_L_Foot_rotateZ";
	rename -uid "DAAAB416-4FEE-A790-F7E3-70A185D5D352";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 49 0 50 0 51 0 52 0 54 8.6026894507087466 56 0 57 0 58 0 59 0 60 0 62 0
		 64 0 66 0 68 0 70 20.458974938611963 71 12.490874752893628 72 0 74 0 75 0 80 0 86 0
		 92 0 98 0 104 0;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0.1501452624797821 0 0 0 
		0 0 0 0 0 0 0.35707646608352661 -0.13906958699226379 0 0 0 0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 -0.1501452624797821 0 0 0 
		0 0 0 0 0 0 -0.13906958699226379 -0.21800689399242401 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Foot_scaleX";
	rename -uid "C5FE6BC3-405C-6B52-9437-119F72D46D22";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 49 1 50 1 51 1 52 1 54 1 56 1 57 1 58 1 59 1 60 1 62 1 64 1 66 1 68 1 70 1
		 71 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Foot_scaleY";
	rename -uid "C56A774D-4CEE-98CC-1A6D-7D824A7EDB7C";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 49 1 50 1 51 1 52 1 54 1 56 1 57 1 58 1 59 1 60 1 62 1 64 1 66 1 68 1 70 1
		 71 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Foot_scaleZ";
	rename -uid "BB1D8C9B-4B73-3BC8-0FEF-519AD7D6BBB5";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 46 1 48 1 49 1 50 1 51 1 52 1 54 1 56 1 57 1 58 1 59 1 60 1 62 1 64 1 66 1 68 1 70 1
		 71 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_L_Foot_Heel_Lift";
	rename -uid "16911BD4-4CA1-F6A1-61AD-A2BE4D16BC0A";
	setAttr ".tan" 2;
	setAttr -s 35 ".ktv[0:34]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0 44.004 0
		 46 0 48 0 49 0 50 0 51 0 52 0 54 0 56 0 57 0 58 0 59 0 60 0 62 0 64 0 66 0 68 0 70 0
		 71 0 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 35 ".kit[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kot[8:34]"  1 2 1 1 1 1 1 2 
		1 1 2 1 1 2 2 2 18 2 2 18 2 1 18 18 18 
		18 1;
	setAttr -s 35 ".kix[8:34]"  0.041666626930236816 0.083166599273681641 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.083333253860473633 0.041666746139526367 0.041666746139526367 
		0.041666746139526367 0.041666626930236816 0.041666626930236816 0.083333253860473633 
		0.083333492279052734 0.083333253860473633 0.083333253860473633 0.083333492279052734 
		0.041666507720947266 0.041666746139526367 0.083333253860473633 0.041666656732559204 
		0.20833325386047363 0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 35 ".kiy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
	setAttr -s 35 ".kox[8:34]"  0.75000011920928955 0.083333373069763184 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.083333253860473633 0.041666507720947266 0.041666507720947266 0.041666507720947266 
		0.66666674613952637 0.66666674613952637 0.083333492279052734 0.083333253860473633 
		0.083333253860473633 0.083333492279052734 0.041666507720947266 0.041666746139526367 
		0.083333253860473633 0.041666746139526367 1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 35 ".koy[8:34]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Foot_visibility";
	rename -uid "E1B66096-409D-D633-A314-43A844697AC9";
	setAttr ".tan" 5;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 53 1 54 1 55 1 56 1 57 1 58 1 60 1 61 1 62 1
		 63 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[0:37]"  2 2 2 9 2 9 9 9 
		1 9 9 9 9 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 9 9 18 18 9 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 5 2 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 18 18 5 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 38 ".kox[32:37]"  1.2083332538604736 0.25 0.25 0.2500002384185791 
		0.25 0.25;
	setAttr -s 38 ".koy[32:37]"  0 0 0 0 0 0;
createNode animCurveTL -n "ct_R_Foot_translateX";
	rename -uid "1885AAC9-4E0D-C9AB-C8A5-729EF0A96D60";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 -0.22832018751365585 34 -0.22832018751365585
		 36 -0.22832018751365585 38 -0.22513880617102353 40 -0.2219574248283912 44 -0.21559466214312653
		 44.004 -0.21559466214312653 45 -0.21513120934245289 46 -0.19225785542509827 47 -0.21702000647770545
		 48 -0.24178215753031276 49 -0.24178215753031276 50 -0.24178215753031276 51 -0.24178215753031276
		 52 -0.24178215753031276 53 -0.24178215753031276 54 -0.24178215753031276 55 -0.24178215753031276
		 56 -0.24178215753031276 57 -0.24178215753031276 58 -0.24178215753031276 60 -0.24178215753031276
		 61 -0.24178215753031276 62 -0.24178215753031276 63 -0.24178215753031276 64 -0.24092077793823707
		 66 -0.24065826638982563 68 -0.23987072873618864 72 -0.22832018751365585 74 -0.22832018751365585
		 75 -0.22832018751365585 80 0 86 0.045168650820048256 92 0 98 -0.043595187543054047
		 104 0;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0.0063627627678215504 0.00046345280134119093 
		0.022873353213071823 -0.024762151762843132 -0.024762151762843132 -0.049524303525686264 
		-0.049524303525686264 -0.049524303525686264 -0.02618749625980854 -0.02618749625980854 
		-0.02618749625980854 -0.02618749625980854 -0.02618749625980854 -0.02618749625980854 
		-0.02618749625980854 -0.02618749625980854 -0.02618749625980854 -0.02618749625980854 
		-0.02618749625980854 0.00086137960897758603 0.00026251154486089945 0.0023626128677278757 
		0 0 -0.22832018136978149 0.1129215806722641 0 -0.044381897896528244 0 0.1129215806722641;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  -0.012725525535643101 0.022873353213071823 
		-0.024762151762843132 -0.024762151762843132 0 0 0 0 0.013461969792842865 0.013461969792842865 
		0.013461969792842865 0.013461969792842865 0.013461969792842865 0.013461969792842865 
		0.013461969792842865 0.013461969792842865 0.013461969792842865 0.013461969792842865 
		0.013461969792842865 0.00026251154486089945 0.00078753766138106585 0.004725232720375061 
		0 0 0 0.13550595939159393 0 -0.044381938874721527 0 0.13550595939159393;
createNode animCurveTL -n "ct_R_Foot_translateY";
	rename -uid "6F4E4744-4A55-4971-0F85-118E85302E1D";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 0.061326776639800351
		 44.004 0 45 0.043556663894970087 46 0.097097930601035679 47 0.071259146467736095
		 48 0 49 0 50 0 51 0 52 0.0049775941358439465 53 0 54 0 55 0 56 0 57 0 58 0 60 0.0047234668394575997
		 61 0.0047234668394575997 62 0.0047234668394575997 63 0.0047234668394575997 64 0.0046860263724429786
		 66 0.0045884011034516359 68 0.0042955241747393985 72 0 74 0 75 0 80 0.084057137826563738
		 86 0.26709718527967374 92 0.010526551407504303 98 0.055580454772307254 104 0.084057137826563738;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 0.043556664139032364 0.053541265428066254 
		-0.025838784873485565 -0.071259148418903351 -0.14336511492729187 -0.14336511492729187 
		-0.14336511492729187 0 0 0 0 0 0 0 0 0 0 0 -3.7440466257976368e-005 -9.762527042767033e-005 
		-0.00087863078806549311 0 0 0 0.12140778452157974 0 0 0.036765310913324356 0.12140778452157974;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  0 0.053541265428066254 -0.025838784873485565 
		-0.071259148418903351 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -9.762527042767033e-005 -0.00029287693905644119 
		-0.0017572641372680664 0 0 0 0.14568939805030823 0 0 0.03676527738571167 0.14568939805030823;
createNode animCurveTL -n "ct_R_Foot_translateZ";
	rename -uid "D1226EFF-4434-BEB2-F0D2-0E837147CC0F";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 -0.43238600117343429 34 -0.43238600117343429
		 36 -0.43238600117343429 38 -0.39488423495578495 40 -0.35738246873813562 44 -0.282378936302837
		 44.004 -0.282378936302837 45 -0.04425895661639466 46 0.0021996944376589276 47 0.7387961046121031
		 48 1.4753925147865514 49 1.4753925147865514 50 1.4753925147865514 51 1.4753925147865514
		 52 1.8409977408100777 53 1.8409977408100777 54 1.8409977408100777 55 1.8409977408100777
		 56 1.8409977408100777 57 1.8409977408100777 58 1.8409977408100777 60 2.2468072339435348
		 61 2.2468072339435348 62 2.2468072339435348 63 2.2468072339435348 64 2.2421787572606773
		 66 2.1028789083760699 68 2.0192983607245019 72 2.0782788742965166 74 2.0782788742965166
		 75 -0.43238600117343429 80 -0.42497914542678428 86 -0.0083250072920222316 92 0.35934710970985417
		 98 0.021304038624475691 104 -0.42497914542678428;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0.075003534555435181 0.23811997473239899 
		0.046458650380373001 0.73659640550613403 0.73659640550613403 1.3832976818084717 1.3832976818084717 
		1.3832976818084717 1.7577714920043945 1.7577714920043945 1.7577714920043945 1.7577714920043945 
		1.7577714920043945 1.7577714920043945 1.7577714920043945 1.7577714920043945 1.7577714920043945 
		1.7577714920043945 1.7577714920043945 -0.0046284766867756844 -0.13929985463619232 
		0 0 0 -0.43238601088523865 0.022220566868782043 0.39216312766075134 0 -0.39216330647468567 
		0.022220566868782043;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  -0.15000706911087036 0.046458650380373001 
		0.73659640550613403 0.73659640550613403 0 0.36560523509979248 0.36560523509979248 
		0.36560523509979248 -1.9077785015106201 -1.9077785015106201 -1.9077785015106201 -1.9077785015106201 
		-1.9077785015106201 -1.9077785015106201 -1.9077785015106201 -1.9077785015106201 -1.9077785015106201 
		-1.9077785015106201 -1.9077785015106201 -0.13929985463619232 -0.083580546081066132 
		0 0 -2.5106649398803711 0 0.026664691045880318 0.39216312766075134 0 -0.39216294884681702 
		0.026664691045880318;
createNode animCurveTA -n "ct_R_Foot_rotateX";
	rename -uid "53A4E386-44A3-A064-1A3B-40AB4A4AC67C";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 13.92894112557107
		 44.004 0 45 77.76275063387385 46 74.753889838563325 47 48.05057350187127 48 0 49 0
		 50 0 51 0 52 9.4259989412391985 53 0 54 0 55 0 56 0 57 0 58 0 60 0 61 0 62 0 63 0
		 64 0 66 0 68 0 72 0 74 0 75 0 80 33.116404723912218 86 69.731791768329842 92 -14.191954945326115
		 98 12.88315922295287 104 33.116404723912218;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 1.3572160005569458 -0.05251452699303627 
		-0.46606078743934631 -0.83864068984985352 -1.0092689990997314 -1.0092689990997314 
		-1.0092689990997314 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.55320411920547485 0 0 0.41284352540969849 
		0.55320411920547485;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  0 -0.05251452699303627 -0.46606078743934631 
		-0.83864068984985352 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.6638452410697937 
		0 0 0.41284313797950745 0.6638452410697937;
createNode animCurveTA -n "ct_R_Foot_rotateY";
	rename -uid "F4F6EFC1-4659-830F-08A4-6FAE4721E5DB";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 -120.16119031139206 34 -120.16119031139206
		 36 -120.16119031139206 38 -99.879392343654487 40 -79.597594375916927 44 -39.033998440441806
		 44.004 -39.033998440441806 45 -40.454924243405443 46 -43.426416501927953 47 -16.27044244913721
		 48 10.885531603653684 49 10.885531603653684 50 10.885531603653684 51 10.885531603653684
		 52 -9.1053820631480953 53 -11.492373167631222 54 -9.5598120730484428 55 0.80940840956999704
		 56 -3.7095007782885516 57 -3.7095007782885516 58 -9.7103357736905771 60 -15.337884652126442
		 61 -15.337884652126442 62 -15.337884652126442 63 -15.337884652126442 64 -23.515200523595006
		 66 -35.663602942185101 68 -52.617299078477863 72 -120.16119031139206 74 -120.16119031139206
		 75 -120.16119031139206 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".ktl[2:37]" no yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes yes 
		yes yes yes yes;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0.70796829462051392 -0.024799833074212074 
		-0.051862321794033051 0.47396114468574524 0.47396114468574524 0.94792228937149048 
		0.94792228937149048 0.94792228937149048 0.87126016616821289 0.87126016616821289 0.87126016616821289 
		0.87126016616821289 0.87126016616821289 0.87126016616821289 0.87126016616821289 0.87126016616821289 
		0.87126016616821289 0.87126016616821289 0.87126016616821289 -0.14272108674049377 
		-0.21202962100505829 -0.49158656597137451 0 0 -2.0972084999084473 0 0 0 0 0;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  -1.4159365892410278 -0.051862321794033051 
		0.47396114468574524 0.47396114468574524 0 -0.049520526081323624 -0.049520526081323624 
		-0.049520526081323624 -2.2871968746185303 -2.2871968746185303 -2.2871968746185303 
		-2.2871968746185303 -2.2871968746185303 -2.2871968746185303 -2.2871968746185303 -2.2871968746185303 
		-2.2871968746185303 -2.2871968746185303 -2.2871968746185303 -0.21202962100505829 
		-0.29589781165122986 -0.98317456245422363 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "ct_R_Foot_rotateZ";
	rename -uid "0A52127B-466E-747D-931E-27AAD46571D0";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 0 4 0 5 0 34 0 36 0 38 0 40 0 44 -1.0236443453948413e-015
		 44.004 0 45 -31.634146889918831 46 -5.3611391646306323 47 -2.6805695823153304 48 0
		 49 0 50 0 51 0 52 0 53 0 54 0 55 0 56 0 57 0 58 0 60 0 61 0 62 0 63 0 64 0 66 0 68 0
		 72 0 74 0 75 0 80 0 86 0 92 0 98 0 104 0;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 -0.55212002992630005 0.45855048298835754 
		0.046784766018390656 0.046784766018390656 0.093569532036781311 0.093569532036781311 
		0.093569532036781311 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  0 0.45855048298835754 0.046784766018390656 
		0.046784766018390656 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Foot_scaleX";
	rename -uid "57A4B0BC-4268-9445-B173-529A08AE99D0";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 53 1 54 1 55 1 56 1 57 1 58 1 60 1 61 1 62 1
		 63 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Foot_scaleY";
	rename -uid "65C143B2-4906-3392-39B5-BEA95DD761C0";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 53 1 54 1 55 1 56 1 57 1 58 1 60 1 61 1 62 1
		 63 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "ct_R_Foot_scaleZ";
	rename -uid "3B7C2BD9-4FA4-F5F8-5135-CFBD1E4E8BE9";
	setAttr ".tan" 1;
	setAttr -s 38 ".ktv[0:37]"  1 1 4 1 5 1 34 1 36 1 38 1 40 1 44 1 44.004 1
		 45 1 46 1 47 1 48 1 49 1 50 1 51 1 52 1 53 1 54 1 55 1 56 1 57 1 58 1 60 1 61 1 62 1
		 63 1 64 1 66 1 68 1 72 1 74 1 75 1 80 1 86 1 92 1 98 1 104 1;
	setAttr -s 38 ".kit[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kot[0:37]"  2 2 2 2 2 2 2 2 
		1 2 2 2 2 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 2 2 18 18 2 1 18 18 18 18 1;
	setAttr -s 38 ".kix[8:37]"  0.041666626930236816 0.041499972343444824 
		0.041666626930236816 0.041666746139526367 0.041666626930236816 0.083333373069763184 
		0.083333373069763184 0.083333373069763184 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666626930236816 0.041666626930236816 0.041666626930236816 
		0.041666626930236816 0.041666746139526367 0.083333253860473633 0.083333253860473633 
		0.16666674613952637 0.083333253860473633 0.041666656732559204 0.20833325386047363 
		0.25 0.25 0.2500002384185791 0.20833325386047363;
	setAttr -s 38 ".kiy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
	setAttr -s 38 ".kox[8:37]"  0.75000011920928955 0.041666626930236816 
		0.041666746139526367 0.041666626930236816 0.041666746139526367 0.16666674613952637 
		0.16666674613952637 0.16666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 0.66666674613952637 
		0.66666674613952637 0.66666674613952637 0.66666674613952637 0.083333253860473633 
		0.083333253860473633 0.16666674613952637 0.083333253860473633 0.041666746139526367 
		1.2083332538604736 0.25 0.25 0.2500002384185791 0.25 0.25;
	setAttr -s 38 ".koy[8:37]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
createNode displayLayer -n "layer1";
	rename -uid "37C87D59-42DE-980F-D65A-9A84C082F6FF";
	setAttr ".v" no;
	setAttr ".do" 8;
createNode polyCube -n "polyCube1";
	rename -uid "4A834629-4BFD-7BEC-EA77-C199E91D645E";
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube2";
	rename -uid "BB2A3376-434D-85AD-0988-42856EED7668";
	setAttr ".cuv" 4;
createNode gameFbxExporter -n "gameExporterPreset1";
	rename -uid "6B4F1313-45AA-C700-E0D5-769A2633EDB0";
	setAttr ".pn" -type "string" "amt_kk_mainknight_anim";
	setAttr ".opv" 1;
	setAttr ".ils" yes;
	setAttr ".vfr" yes;
	setAttr ".esi" 2;
	setAttr ".smm" yes;
	setAttr ".rac" yes;
	setAttr ".tri" yes;
	setAttr ".cns" -type "string" "NURBS";
	setAttr ".ean" yes;
	setAttr ".qim" -type "string" "resample";
	setAttr ".bas" 1;
	setAttr ".bae" 100;
	setAttr ".bst" 1;
	setAttr ".ral" yes;
	setAttr ".dm" yes;
	setAttr ".rtp" 9.9999997473787516e-005;
	setAttr ".rrp" 0.008999999612569809;
	setAttr ".rsp" 0.0040000001899898052;
	setAttr ".rop" 0.008999999612569809;
	setAttr ".ato" yes;
	setAttr ".sd" yes;
	setAttr ".ebm" yes;
	setAttr ".inc" yes;
	setAttr ".asf" yes;
	setAttr ".uc" -type "string" "Centimeters";
	setAttr ".gld" yes;
	setAttr ".ft" 1;
	setAttr ".fv" -type "string" "FBX201600";
	setAttr ".exp" -type "string" "E:/BackUp/Workspace/3D/ArtMotionStudios-BennyDz/_AnimationTEST-Unity/unity_animation/_ArtAssests/artmotion_kingdom_art/main_char_knight/_result-anims";
	setAttr ".exf" -type "string" "kk_all";
createNode gameFbxExporter -n "gameExporterPreset2";
	rename -uid "61E196A7-4D56-8AE8-4B68-FD95E5791360";
	setAttr ".pn" -type "string" "Anim Default";
	setAttr ".ils" yes;
	setAttr ".ilu" yes;
	setAttr ".vfr" yes;
	setAttr ".eti" 2;
	setAttr ".esi" 2;
	setAttr -s 4 ".ac";
	setAttr ".ac[0].acn" -type "string" "idle";
	setAttr ".ac[0].acs" 6;
	setAttr ".ac[0].ace" 34;
	setAttr ".ac[1].acn" -type "string" "1stAttack";
	setAttr ".ac[1].acs" 36;
	setAttr ".ac[1].ace" 46;
	setAttr ".ac[2].acn" -type "string" "2ndAttack";
	setAttr ".ac[2].acs" 47;
	setAttr ".ac[2].ace" 58;
	setAttr ".ac[3].acn" -type "string" "3rdAttack";
	setAttr ".ac[3].acs" 59;
	setAttr ".ac[3].ace" 64;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201600";
	setAttr ".exp" -type "string" "E:/BackUp/Workspace/3D/ArtMotionStudios-BennyDz/_AnimationTEST-Unity/unity_animation/Assets/Models";
	setAttr ".exf" -type "string" "kk_anims";
createNode gameFbxExporter -n "gameExporterPreset3";
	rename -uid "0714B82D-4F77-451F-06A1-9DBF2E215814";
	setAttr ".pn" -type "string" "Anim_Walk";
	setAttr ".eti" 2;
	setAttr ".esi" 2;
	setAttr ".ac[0].acn" -type "string" "kk_WalkClip";
	setAttr ".ac[0].acs" 80;
	setAttr ".ac[0].ace" 104;
	setAttr ".spt" 2;
	setAttr ".ic" no;
	setAttr ".ebm" yes;
	setAttr ".fv" -type "string" "FBX201600";
	setAttr ".exp" -type "string" "E:/BackUp/Workspace/3D/ArtMotionStudios-BennyDz/_AnimationTEST-Unity/unity_animation/Assets/Models";
	setAttr ".exf" -type "string" "kk_walkClip";
select -ne :time1;
	setAttr ".o" 97;
	setAttr ".unw" 97;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".aoon" yes;
	setAttr ".laa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
	setAttr -s 3 ".tx";
select -ne :initialShadingGroup;
	setAttr -s 9 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".ovt" no;
	setAttr ".povt" no;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr ".gsn" no;
	setAttr -s 4 ".sol";
connectAttr "Ground.di" "ground.do";
connectAttr "R_Humerus.msg" "ik_R_wrist.hsj";
connectAttr "effector2.hp" "ik_R_wrist.hee";
connectAttr "ikRPsolver.msg" "ik_R_wrist.hsv";
connectAttr "ik_R_wrist_poleVectorConstraint1.ctx" "ik_R_wrist.pvx";
connectAttr "ik_R_wrist_poleVectorConstraint1.cty" "ik_R_wrist.pvy";
connectAttr "ik_R_wrist_poleVectorConstraint1.ctz" "ik_R_wrist.pvz";
connectAttr "ik_R_wrist_parentConstraint1.ctx" "ik_R_wrist.tx";
connectAttr "ik_R_wrist_parentConstraint1.cty" "ik_R_wrist.ty";
connectAttr "ik_R_wrist_parentConstraint1.ctz" "ik_R_wrist.tz";
connectAttr "ik_R_wrist_parentConstraint1.crx" "ik_R_wrist.rx";
connectAttr "ik_R_wrist_parentConstraint1.cry" "ik_R_wrist.ry";
connectAttr "ik_R_wrist_parentConstraint1.crz" "ik_R_wrist.rz";
connectAttr "Bones.di" "ik_R_wrist.do";
connectAttr "ik_R_wrist.pim" "ik_R_wrist_poleVectorConstraint1.cpim";
connectAttr "R_Humerus.pm" "ik_R_wrist_poleVectorConstraint1.ps";
connectAttr "R_Humerus.t" "ik_R_wrist_poleVectorConstraint1.crp";
connectAttr "loc_R_ElbowPoleV.t" "ik_R_wrist_poleVectorConstraint1.tg[0].tt";
connectAttr "loc_R_ElbowPoleV.rp" "ik_R_wrist_poleVectorConstraint1.tg[0].trp";
connectAttr "loc_R_ElbowPoleV.rpt" "ik_R_wrist_poleVectorConstraint1.tg[0].trt";
connectAttr "loc_R_ElbowPoleV.pm" "ik_R_wrist_poleVectorConstraint1.tg[0].tpm";
connectAttr "ik_R_wrist_poleVectorConstraint1.w0" "ik_R_wrist_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "ik_R_wrist.ro" "ik_R_wrist_parentConstraint1.cro";
connectAttr "ik_R_wrist.pim" "ik_R_wrist_parentConstraint1.cpim";
connectAttr "ik_R_wrist.rp" "ik_R_wrist_parentConstraint1.crp";
connectAttr "ik_R_wrist.rpt" "ik_R_wrist_parentConstraint1.crt";
connectAttr "ct_R_Wrist.t" "ik_R_wrist_parentConstraint1.tg[0].tt";
connectAttr "ct_R_Wrist.rp" "ik_R_wrist_parentConstraint1.tg[0].trp";
connectAttr "ct_R_Wrist.rpt" "ik_R_wrist_parentConstraint1.tg[0].trt";
connectAttr "ct_R_Wrist.r" "ik_R_wrist_parentConstraint1.tg[0].tr";
connectAttr "ct_R_Wrist.ro" "ik_R_wrist_parentConstraint1.tg[0].tro";
connectAttr "ct_R_Wrist.s" "ik_R_wrist_parentConstraint1.tg[0].ts";
connectAttr "ct_R_Wrist.pm" "ik_R_wrist_parentConstraint1.tg[0].tpm";
connectAttr "ik_R_wrist_parentConstraint1.w0" "ik_R_wrist_parentConstraint1.tg[0].tw"
		;
connectAttr "L_Humerus.msg" "ik_L_wrist.hsj";
connectAttr "effector1.hp" "ik_L_wrist.hee";
connectAttr "ikRPsolver.msg" "ik_L_wrist.hsv";
connectAttr "ikHandle1_poleVectorConstraint1.ctx" "ik_L_wrist.pvx";
connectAttr "ikHandle1_poleVectorConstraint1.cty" "ik_L_wrist.pvy";
connectAttr "ikHandle1_poleVectorConstraint1.ctz" "ik_L_wrist.pvz";
connectAttr "ikHandle1_parentConstraint1.ctx" "ik_L_wrist.tx";
connectAttr "ikHandle1_parentConstraint1.cty" "ik_L_wrist.ty";
connectAttr "ikHandle1_parentConstraint1.ctz" "ik_L_wrist.tz";
connectAttr "ikHandle1_parentConstraint1.crx" "ik_L_wrist.rx";
connectAttr "ikHandle1_parentConstraint1.cry" "ik_L_wrist.ry";
connectAttr "ikHandle1_parentConstraint1.crz" "ik_L_wrist.rz";
connectAttr "Bones.di" "ik_L_wrist.do";
connectAttr "ik_L_wrist.pim" "ikHandle1_poleVectorConstraint1.cpim";
connectAttr "L_Humerus.pm" "ikHandle1_poleVectorConstraint1.ps";
connectAttr "L_Humerus.t" "ikHandle1_poleVectorConstraint1.crp";
connectAttr "loc_L_ElbowPoleV.t" "ikHandle1_poleVectorConstraint1.tg[0].tt";
connectAttr "loc_L_ElbowPoleV.rp" "ikHandle1_poleVectorConstraint1.tg[0].trp";
connectAttr "loc_L_ElbowPoleV.rpt" "ikHandle1_poleVectorConstraint1.tg[0].trt";
connectAttr "loc_L_ElbowPoleV.pm" "ikHandle1_poleVectorConstraint1.tg[0].tpm";
connectAttr "ikHandle1_poleVectorConstraint1.w0" "ikHandle1_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "ik_L_wrist.ro" "ikHandle1_parentConstraint1.cro";
connectAttr "ik_L_wrist.pim" "ikHandle1_parentConstraint1.cpim";
connectAttr "ik_L_wrist.rp" "ikHandle1_parentConstraint1.crp";
connectAttr "ik_L_wrist.rpt" "ikHandle1_parentConstraint1.crt";
connectAttr "ct_L_Wrist.t" "ikHandle1_parentConstraint1.tg[0].tt";
connectAttr "ct_L_Wrist.rp" "ikHandle1_parentConstraint1.tg[0].trp";
connectAttr "ct_L_Wrist.rpt" "ikHandle1_parentConstraint1.tg[0].trt";
connectAttr "ct_L_Wrist.r" "ikHandle1_parentConstraint1.tg[0].tr";
connectAttr "ct_L_Wrist.ro" "ikHandle1_parentConstraint1.tg[0].tro";
connectAttr "ct_L_Wrist.s" "ikHandle1_parentConstraint1.tg[0].ts";
connectAttr "ct_L_Wrist.pm" "ikHandle1_parentConstraint1.tg[0].tpm";
connectAttr "ikHandle1_parentConstraint1.w0" "ikHandle1_parentConstraint1.tg[0].tw"
		;
connectAttr "loc_L_ElbowPoleV_parentConstraint1.ctx" "loc_L_ElbowPoleV.tx";
connectAttr "loc_L_ElbowPoleV_parentConstraint1.cty" "loc_L_ElbowPoleV.ty";
connectAttr "loc_L_ElbowPoleV_parentConstraint1.ctz" "loc_L_ElbowPoleV.tz";
connectAttr "loc_L_ElbowPoleV_parentConstraint1.crx" "loc_L_ElbowPoleV.rx";
connectAttr "loc_L_ElbowPoleV_parentConstraint1.cry" "loc_L_ElbowPoleV.ry";
connectAttr "loc_L_ElbowPoleV_parentConstraint1.crz" "loc_L_ElbowPoleV.rz";
connectAttr "Bones.di" "loc_L_ElbowPoleV.do";
connectAttr "loc_L_ElbowPoleV.ro" "loc_L_ElbowPoleV_parentConstraint1.cro";
connectAttr "loc_L_ElbowPoleV.pim" "loc_L_ElbowPoleV_parentConstraint1.cpim";
connectAttr "loc_L_ElbowPoleV.rp" "loc_L_ElbowPoleV_parentConstraint1.crp";
connectAttr "loc_L_ElbowPoleV.rpt" "loc_L_ElbowPoleV_parentConstraint1.crt";
connectAttr "L_ElbowPoleV.t" "loc_L_ElbowPoleV_parentConstraint1.tg[0].tt";
connectAttr "L_ElbowPoleV.rp" "loc_L_ElbowPoleV_parentConstraint1.tg[0].trp";
connectAttr "L_ElbowPoleV.rpt" "loc_L_ElbowPoleV_parentConstraint1.tg[0].trt";
connectAttr "L_ElbowPoleV.r" "loc_L_ElbowPoleV_parentConstraint1.tg[0].tr";
connectAttr "L_ElbowPoleV.ro" "loc_L_ElbowPoleV_parentConstraint1.tg[0].tro";
connectAttr "L_ElbowPoleV.s" "loc_L_ElbowPoleV_parentConstraint1.tg[0].ts";
connectAttr "L_ElbowPoleV.pm" "loc_L_ElbowPoleV_parentConstraint1.tg[0].tpm";
connectAttr "loc_L_ElbowPoleV_parentConstraint1.w0" "loc_L_ElbowPoleV_parentConstraint1.tg[0].tw"
		;
connectAttr "loc_R_ElbowPoleV_parentConstraint1.ctx" "loc_R_ElbowPoleV.tx";
connectAttr "loc_R_ElbowPoleV_parentConstraint1.cty" "loc_R_ElbowPoleV.ty";
connectAttr "loc_R_ElbowPoleV_parentConstraint1.ctz" "loc_R_ElbowPoleV.tz";
connectAttr "loc_R_ElbowPoleV_parentConstraint1.crx" "loc_R_ElbowPoleV.rx";
connectAttr "loc_R_ElbowPoleV_parentConstraint1.cry" "loc_R_ElbowPoleV.ry";
connectAttr "loc_R_ElbowPoleV_parentConstraint1.crz" "loc_R_ElbowPoleV.rz";
connectAttr "Bones.di" "loc_R_ElbowPoleV.do";
connectAttr "loc_R_ElbowPoleV.ro" "loc_R_ElbowPoleV_parentConstraint1.cro";
connectAttr "loc_R_ElbowPoleV.pim" "loc_R_ElbowPoleV_parentConstraint1.cpim";
connectAttr "loc_R_ElbowPoleV.rp" "loc_R_ElbowPoleV_parentConstraint1.crp";
connectAttr "loc_R_ElbowPoleV.rpt" "loc_R_ElbowPoleV_parentConstraint1.crt";
connectAttr "R_ElbowPoleV.t" "loc_R_ElbowPoleV_parentConstraint1.tg[0].tt";
connectAttr "R_ElbowPoleV.rp" "loc_R_ElbowPoleV_parentConstraint1.tg[0].trp";
connectAttr "R_ElbowPoleV.rpt" "loc_R_ElbowPoleV_parentConstraint1.tg[0].trt";
connectAttr "R_ElbowPoleV.r" "loc_R_ElbowPoleV_parentConstraint1.tg[0].tr";
connectAttr "R_ElbowPoleV.ro" "loc_R_ElbowPoleV_parentConstraint1.tg[0].tro";
connectAttr "R_ElbowPoleV.s" "loc_R_ElbowPoleV_parentConstraint1.tg[0].ts";
connectAttr "R_ElbowPoleV.pm" "loc_R_ElbowPoleV_parentConstraint1.tg[0].tpm";
connectAttr "loc_R_ElbowPoleV_parentConstraint1.w0" "loc_R_ElbowPoleV_parentConstraint1.tg[0].tw"
		;
connectAttr "L_ikGroup_parentConstraint1.ctx" "L_ikGroup.tx";
connectAttr "L_ikGroup_parentConstraint1.cty" "L_ikGroup.ty";
connectAttr "L_ikGroup_parentConstraint1.ctz" "L_ikGroup.tz";
connectAttr "L_ikGroup_parentConstraint1.crx" "L_ikGroup.rx";
connectAttr "L_ikGroup_parentConstraint1.cry" "L_ikGroup.ry";
connectAttr "L_ikGroup_parentConstraint1.crz" "L_ikGroup.rz";
connectAttr "Bones.di" "L_ikGroup.do";
connectAttr "L_Femur.msg" "ik_L_knee.hsj";
connectAttr "effector3.hp" "ik_L_knee.hee";
connectAttr "ikRPsolver.msg" "ik_L_knee.hsv";
connectAttr "ik_L_knee_poleVectorConstraint1.ctx" "ik_L_knee.pvx";
connectAttr "ik_L_knee_poleVectorConstraint1.cty" "ik_L_knee.pvy";
connectAttr "ik_L_knee_poleVectorConstraint1.ctz" "ik_L_knee.pvz";
connectAttr "ik_L_knee.pim" "ik_L_knee_poleVectorConstraint1.cpim";
connectAttr "L_Femur.pm" "ik_L_knee_poleVectorConstraint1.ps";
connectAttr "L_Femur.t" "ik_L_knee_poleVectorConstraint1.crp";
connectAttr "L_KneePoleV.t" "ik_L_knee_poleVectorConstraint1.tg[0].tt";
connectAttr "L_KneePoleV.rp" "ik_L_knee_poleVectorConstraint1.tg[0].trp";
connectAttr "L_KneePoleV.rpt" "ik_L_knee_poleVectorConstraint1.tg[0].trt";
connectAttr "L_KneePoleV.pm" "ik_L_knee_poleVectorConstraint1.tg[0].tpm";
connectAttr "ik_L_knee_poleVectorConstraint1.w0" "ik_L_knee_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "L_Ball.msg" "ik_L_Toe.hsj";
connectAttr "effector6.hp" "ik_L_Toe.hee";
connectAttr "ikRPsolver.msg" "ik_L_Toe.hsv";
connectAttr "L_Ankle.msg" "ik_L_Ball.hsj";
connectAttr "effector5.hp" "ik_L_Ball.hee";
connectAttr "ikRPsolver.msg" "ik_L_Ball.hsv";
connectAttr "L_ikGroup.ro" "L_ikGroup_parentConstraint1.cro";
connectAttr "L_ikGroup.pim" "L_ikGroup_parentConstraint1.cpim";
connectAttr "L_ikGroup.rp" "L_ikGroup_parentConstraint1.crp";
connectAttr "L_ikGroup.rpt" "L_ikGroup_parentConstraint1.crt";
connectAttr "ct_L_Foot.t" "L_ikGroup_parentConstraint1.tg[0].tt";
connectAttr "ct_L_Foot.rp" "L_ikGroup_parentConstraint1.tg[0].trp";
connectAttr "ct_L_Foot.rpt" "L_ikGroup_parentConstraint1.tg[0].trt";
connectAttr "ct_L_Foot.r" "L_ikGroup_parentConstraint1.tg[0].tr";
connectAttr "ct_L_Foot.ro" "L_ikGroup_parentConstraint1.tg[0].tro";
connectAttr "ct_L_Foot.s" "L_ikGroup_parentConstraint1.tg[0].ts";
connectAttr "ct_L_Foot.pm" "L_ikGroup_parentConstraint1.tg[0].tpm";
connectAttr "L_ikGroup_parentConstraint1.w0" "L_ikGroup_parentConstraint1.tg[0].tw"
		;
connectAttr "R_ikGroup_parentConstraint1.ctx" "R_ikGroup.tx";
connectAttr "R_ikGroup_parentConstraint1.cty" "R_ikGroup.ty";
connectAttr "R_ikGroup_parentConstraint1.ctz" "R_ikGroup.tz";
connectAttr "R_ikGroup_parentConstraint1.crx" "R_ikGroup.rx";
connectAttr "R_ikGroup_parentConstraint1.cry" "R_ikGroup.ry";
connectAttr "R_ikGroup_parentConstraint1.crz" "R_ikGroup.rz";
connectAttr "Bones.di" "R_ikGroup.do";
connectAttr "R_Femur.msg" "ik_R_knee.hsj";
connectAttr "effector4.hp" "ik_R_knee.hee";
connectAttr "ikRPsolver.msg" "ik_R_knee.hsv";
connectAttr "ik_R_knee_poleVectorConstraint1.ctx" "ik_R_knee.pvx";
connectAttr "ik_R_knee_poleVectorConstraint1.cty" "ik_R_knee.pvy";
connectAttr "ik_R_knee_poleVectorConstraint1.ctz" "ik_R_knee.pvz";
connectAttr "ik_R_knee.pim" "ik_R_knee_poleVectorConstraint1.cpim";
connectAttr "R_Femur.pm" "ik_R_knee_poleVectorConstraint1.ps";
connectAttr "R_Femur.t" "ik_R_knee_poleVectorConstraint1.crp";
connectAttr "R_KneePoleV.t" "ik_R_knee_poleVectorConstraint1.tg[0].tt";
connectAttr "R_KneePoleV.rp" "ik_R_knee_poleVectorConstraint1.tg[0].trp";
connectAttr "R_KneePoleV.rpt" "ik_R_knee_poleVectorConstraint1.tg[0].trt";
connectAttr "R_KneePoleV.pm" "ik_R_knee_poleVectorConstraint1.tg[0].tpm";
connectAttr "ik_R_knee_poleVectorConstraint1.w0" "ik_R_knee_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "R_Ankle.msg" "ik_R_Ball.hsj";
connectAttr "effector7.hp" "ik_R_Ball.hee";
connectAttr "ikRPsolver.msg" "ik_R_Ball.hsv";
connectAttr "R_Ball.msg" "ik_R_Toe.hsj";
connectAttr "effector8.hp" "ik_R_Toe.hee";
connectAttr "ikRPsolver.msg" "ik_R_Toe.hsv";
connectAttr "R_ikGroup.ro" "R_ikGroup_parentConstraint1.cro";
connectAttr "R_ikGroup.pim" "R_ikGroup_parentConstraint1.cpim";
connectAttr "R_ikGroup.rp" "R_ikGroup_parentConstraint1.crp";
connectAttr "R_ikGroup.rpt" "R_ikGroup_parentConstraint1.crt";
connectAttr "ct_R_Foot.t" "R_ikGroup_parentConstraint1.tg[0].tt";
connectAttr "ct_R_Foot.rp" "R_ikGroup_parentConstraint1.tg[0].trp";
connectAttr "ct_R_Foot.rpt" "R_ikGroup_parentConstraint1.tg[0].trt";
connectAttr "ct_R_Foot.r" "R_ikGroup_parentConstraint1.tg[0].tr";
connectAttr "ct_R_Foot.ro" "R_ikGroup_parentConstraint1.tg[0].tro";
connectAttr "ct_R_Foot.s" "R_ikGroup_parentConstraint1.tg[0].ts";
connectAttr "ct_R_Foot.pm" "R_ikGroup_parentConstraint1.tg[0].tpm";
connectAttr "R_ikGroup_parentConstraint1.w0" "R_ikGroup_parentConstraint1.tg[0].tw"
		;
connectAttr "reference_parentConstraint1.ctx" "reference.tx";
connectAttr "reference_parentConstraint1.cty" "reference.ty";
connectAttr "reference_parentConstraint1.ctz" "reference.tz";
connectAttr "reference_parentConstraint1.crx" "reference.rx";
connectAttr "reference_parentConstraint1.cry" "reference.ry";
connectAttr "reference_parentConstraint1.crz" "reference.rz";
connectAttr "Bones.di" "reference.do";
connectAttr "reference.s" "|reference|Hips.is";
connectAttr "Bones.di" "|reference|Hips.do";
connectAttr "Hips_parentConstraint1.ctx" "|reference|Hips.tx";
connectAttr "Hips_parentConstraint1.cty" "|reference|Hips.ty";
connectAttr "Hips_parentConstraint1.ctz" "|reference|Hips.tz";
connectAttr "Hips_parentConstraint1.crx" "|reference|Hips.rx";
connectAttr "Hips_parentConstraint1.cry" "|reference|Hips.ry";
connectAttr "Hips_parentConstraint1.crz" "|reference|Hips.rz";
connectAttr "|reference|Hips.s" "Spine01.is";
connectAttr "Spine01_orientConstraint1.crx" "Spine01.rx";
connectAttr "Spine01_orientConstraint1.cry" "Spine01.ry";
connectAttr "Spine01_orientConstraint1.crz" "Spine01.rz";
connectAttr "Spine01.s" "Spine02.is";
connectAttr "Spine02_orientConstraint1.crx" "Spine02.rx";
connectAttr "Spine02_orientConstraint1.cry" "Spine02.ry";
connectAttr "Spine02_orientConstraint1.crz" "Spine02.rz";
connectAttr "Spine02.s" "HeadBase.is";
connectAttr "HeadBase_orientConstraint1.crx" "HeadBase.rx";
connectAttr "HeadBase_orientConstraint1.cry" "HeadBase.ry";
connectAttr "HeadBase_orientConstraint1.crz" "HeadBase.rz";
connectAttr "HeadBase.s" "HeadEnd.is";
connectAttr "HeadBase.s" "Plume01.is";
connectAttr "Plume01.s" "Plume02.is";
connectAttr "Plume02.s" "Plume03.is";
connectAttr "Plume03.s" "PlumeEnd.is";
connectAttr "HeadBase.s" "HelmetBase.is";
connectAttr "HelmetBase.s" "HelmetEnd.is";
connectAttr "HeadBase.ro" "HeadBase_orientConstraint1.cro";
connectAttr "HeadBase.pim" "HeadBase_orientConstraint1.cpim";
connectAttr "HeadBase.jo" "HeadBase_orientConstraint1.cjo";
connectAttr "HeadBase.is" "HeadBase_orientConstraint1.is";
connectAttr "HeadCtrl.r" "HeadBase_orientConstraint1.tg[0].tr";
connectAttr "HeadCtrl.ro" "HeadBase_orientConstraint1.tg[0].tro";
connectAttr "HeadCtrl.pm" "HeadBase_orientConstraint1.tg[0].tpm";
connectAttr "HeadBase_orientConstraint1.w0" "HeadBase_orientConstraint1.tg[0].tw"
		;
connectAttr "Spine02.s" "L_Clavicle.is";
connectAttr "L_Clavicle.s" "L_Humerus.is";
connectAttr "L_Humerus.s" "L_ForeArm.is";
connectAttr "L_ForeArm.s" "L_Hand.is";
connectAttr "L_Hand_orientConstraint1.crx" "L_Hand.rx";
connectAttr "L_Hand_orientConstraint1.cry" "L_Hand.ry";
connectAttr "L_Hand_orientConstraint1.crz" "L_Hand.rz";
connectAttr "L_Hand.s" "L_EndHand.is";
connectAttr "L_Hand.ro" "L_Hand_orientConstraint1.cro";
connectAttr "L_Hand.pim" "L_Hand_orientConstraint1.cpim";
connectAttr "L_Hand.jo" "L_Hand_orientConstraint1.cjo";
connectAttr "L_Hand.is" "L_Hand_orientConstraint1.is";
connectAttr "|mainCtrl|ct_L_Wrist|ct_R_Hand.r" "L_Hand_orientConstraint1.tg[0].tr"
		;
connectAttr "|mainCtrl|ct_L_Wrist|ct_R_Hand.ro" "L_Hand_orientConstraint1.tg[0].tro"
		;
connectAttr "|mainCtrl|ct_L_Wrist|ct_R_Hand.pm" "L_Hand_orientConstraint1.tg[0].tpm"
		;
connectAttr "L_Hand_orientConstraint1.w0" "L_Hand_orientConstraint1.tg[0].tw";
connectAttr "L_Hand.tx" "effector1.tx";
connectAttr "L_Hand.ty" "effector1.ty";
connectAttr "L_Hand.tz" "effector1.tz";
connectAttr "Spine02.s" "R_Clavicle.is";
connectAttr "R_Clavicle.s" "R_Humerus.is";
connectAttr "R_Humerus.s" "R_ForeArm.is";
connectAttr "R_ForeArm.s" "R_Hand.is";
connectAttr "R_Hand_orientConstraint1.crx" "R_Hand.rx";
connectAttr "R_Hand_orientConstraint1.cry" "R_Hand.ry";
connectAttr "R_Hand_orientConstraint1.crz" "R_Hand.rz";
connectAttr "R_Hand.s" "R_EndHand.is";
connectAttr "R_Hand.ro" "R_Hand_orientConstraint1.cro";
connectAttr "R_Hand.pim" "R_Hand_orientConstraint1.cpim";
connectAttr "R_Hand.jo" "R_Hand_orientConstraint1.cjo";
connectAttr "R_Hand.is" "R_Hand_orientConstraint1.is";
connectAttr "|mainCtrl|ct_R_Wrist|ct_R_Hand.r" "R_Hand_orientConstraint1.tg[0].tr"
		;
connectAttr "|mainCtrl|ct_R_Wrist|ct_R_Hand.ro" "R_Hand_orientConstraint1.tg[0].tro"
		;
connectAttr "|mainCtrl|ct_R_Wrist|ct_R_Hand.pm" "R_Hand_orientConstraint1.tg[0].tpm"
		;
connectAttr "R_Hand_orientConstraint1.w0" "R_Hand_orientConstraint1.tg[0].tw";
connectAttr "R_Hand.tx" "effector2.tx";
connectAttr "R_Hand.ty" "effector2.ty";
connectAttr "R_Hand.tz" "effector2.tz";
connectAttr "Spine02.ro" "Spine02_orientConstraint1.cro";
connectAttr "Spine02.pim" "Spine02_orientConstraint1.cpim";
connectAttr "Spine02.jo" "Spine02_orientConstraint1.cjo";
connectAttr "Spine02.is" "Spine02_orientConstraint1.is";
connectAttr "Chest_Ctrl.r" "Spine02_orientConstraint1.tg[0].tr";
connectAttr "Chest_Ctrl.ro" "Spine02_orientConstraint1.tg[0].tro";
connectAttr "Chest_Ctrl.pm" "Spine02_orientConstraint1.tg[0].tpm";
connectAttr "Spine02_orientConstraint1.w0" "Spine02_orientConstraint1.tg[0].tw";
connectAttr "Spine01.ro" "Spine01_orientConstraint1.cro";
connectAttr "Spine01.pim" "Spine01_orientConstraint1.cpim";
connectAttr "Spine01.jo" "Spine01_orientConstraint1.cjo";
connectAttr "Spine01.is" "Spine01_orientConstraint1.is";
connectAttr "Spine.r" "Spine01_orientConstraint1.tg[0].tr";
connectAttr "Spine.ro" "Spine01_orientConstraint1.tg[0].tro";
connectAttr "Spine.pm" "Spine01_orientConstraint1.tg[0].tpm";
connectAttr "Spine01_orientConstraint1.w0" "Spine01_orientConstraint1.tg[0].tw";
connectAttr "Bones.di" "HipsSway.do";
connectAttr "|reference|Hips.s" "HipsSway.is";
connectAttr "HipsSway_orientConstraint1.crx" "HipsSway.rx";
connectAttr "HipsSway_orientConstraint1.cry" "HipsSway.ry";
connectAttr "HipsSway_orientConstraint1.crz" "HipsSway.rz";
connectAttr "HipsSway.s" "L_Femur.is";
connectAttr "L_Femur.s" "L_Knee.is";
connectAttr "L_Knee.s" "L_Ankle.is";
connectAttr "L_Ankle.s" "L_Ball.is";
connectAttr "L_Ball.s" "L_Toe.is";
connectAttr "L_Toe.tx" "effector6.tx";
connectAttr "L_Toe.ty" "effector6.ty";
connectAttr "L_Toe.tz" "effector6.tz";
connectAttr "L_Ball.tx" "effector5.tx";
connectAttr "L_Ball.ty" "effector5.ty";
connectAttr "L_Ball.tz" "effector5.tz";
connectAttr "L_Ankle.tx" "effector3.tx";
connectAttr "L_Ankle.ty" "effector3.ty";
connectAttr "L_Ankle.tz" "effector3.tz";
connectAttr "HipsSway.s" "R_Femur.is";
connectAttr "R_Femur.s" "R_Knee.is";
connectAttr "R_Knee.s" "R_Ankle.is";
connectAttr "R_Ankle.s" "R_Ball.is";
connectAttr "R_Ball.s" "R_Toe.is";
connectAttr "R_Toe.tx" "effector8.tx";
connectAttr "R_Toe.ty" "effector8.ty";
connectAttr "R_Toe.tz" "effector8.tz";
connectAttr "R_Ball.tx" "effector7.tx";
connectAttr "R_Ball.ty" "effector7.ty";
connectAttr "R_Ball.tz" "effector7.tz";
connectAttr "R_Ankle.tx" "effector4.tx";
connectAttr "R_Ankle.ty" "effector4.ty";
connectAttr "R_Ankle.tz" "effector4.tz";
connectAttr "HipsSway.ro" "HipsSway_orientConstraint1.cro";
connectAttr "HipsSway.pim" "HipsSway_orientConstraint1.cpim";
connectAttr "HipsSway.jo" "HipsSway_orientConstraint1.cjo";
connectAttr "HipsSway.is" "HipsSway_orientConstraint1.is";
connectAttr "|mainCtrl|Center|Hips.r" "HipsSway_orientConstraint1.tg[0].tr";
connectAttr "|mainCtrl|Center|Hips.ro" "HipsSway_orientConstraint1.tg[0].tro";
connectAttr "|mainCtrl|Center|Hips.pm" "HipsSway_orientConstraint1.tg[0].tpm";
connectAttr "HipsSway_orientConstraint1.w0" "HipsSway_orientConstraint1.tg[0].tw"
		;
connectAttr "|reference|Hips.ro" "Hips_parentConstraint1.cro";
connectAttr "|reference|Hips.pim" "Hips_parentConstraint1.cpim";
connectAttr "|reference|Hips.rp" "Hips_parentConstraint1.crp";
connectAttr "|reference|Hips.rpt" "Hips_parentConstraint1.crt";
connectAttr "|reference|Hips.jo" "Hips_parentConstraint1.cjo";
connectAttr "Center.t" "Hips_parentConstraint1.tg[0].tt";
connectAttr "Center.rp" "Hips_parentConstraint1.tg[0].trp";
connectAttr "Center.rpt" "Hips_parentConstraint1.tg[0].trt";
connectAttr "Center.r" "Hips_parentConstraint1.tg[0].tr";
connectAttr "Center.ro" "Hips_parentConstraint1.tg[0].tro";
connectAttr "Center.s" "Hips_parentConstraint1.tg[0].ts";
connectAttr "Center.pm" "Hips_parentConstraint1.tg[0].tpm";
connectAttr "Hips_parentConstraint1.w0" "Hips_parentConstraint1.tg[0].tw";
connectAttr "reference.ro" "reference_parentConstraint1.cro";
connectAttr "reference.pim" "reference_parentConstraint1.cpim";
connectAttr "reference.rp" "reference_parentConstraint1.crp";
connectAttr "reference.rpt" "reference_parentConstraint1.crt";
connectAttr "reference.jo" "reference_parentConstraint1.cjo";
connectAttr "mainCtrl.t" "reference_parentConstraint1.tg[0].tt";
connectAttr "mainCtrl.rp" "reference_parentConstraint1.tg[0].trp";
connectAttr "mainCtrl.rpt" "reference_parentConstraint1.tg[0].trt";
connectAttr "mainCtrl.r" "reference_parentConstraint1.tg[0].tr";
connectAttr "mainCtrl.ro" "reference_parentConstraint1.tg[0].tro";
connectAttr "mainCtrl.s" "reference_parentConstraint1.tg[0].ts";
connectAttr "mainCtrl.pm" "reference_parentConstraint1.tg[0].tpm";
connectAttr "reference_parentConstraint1.w0" "reference_parentConstraint1.tg[0].tw"
		;
connectAttr "ModeloCompleto.di" "SF_Character_Knight.do";
connectAttr "skinCluster1GroupId.id" "SF_Character_KnightShape.iog.og[0].gid";
connectAttr "skinCluster1Set.mwc" "SF_Character_KnightShape.iog.og[0].gco";
connectAttr "groupId2.id" "SF_Character_KnightShape.iog.og[1].gid";
connectAttr "tweakSet1.mwc" "SF_Character_KnightShape.iog.og[1].gco";
connectAttr "skinCluster1.og[0]" "SF_Character_KnightShape.i";
connectAttr "tweak1.vl[0].vt[0]" "SF_Character_KnightShape.twl";
connectAttr "mainCtrl_translateX.o" "mainCtrl.tx";
connectAttr "mainCtrl_translateY.o" "mainCtrl.ty";
connectAttr "mainCtrl_translateZ.o" "mainCtrl.tz";
connectAttr "mainCtrl_rotateX.o" "mainCtrl.rx";
connectAttr "mainCtrl_rotateY.o" "mainCtrl.ry";
connectAttr "mainCtrl_rotateZ.o" "mainCtrl.rz";
connectAttr "mainCtrl_scaleX.o" "mainCtrl.sx";
connectAttr "mainCtrl_scaleY.o" "mainCtrl.sy";
connectAttr "mainCtrl_scaleZ.o" "mainCtrl.sz";
connectAttr "Controls.di" "mainCtrl.do";
connectAttr "mainCtrl_visibility.o" "mainCtrl.v";
connectAttr "R_ElbowPoleV_translateX.o" "R_ElbowPoleV.tx";
connectAttr "R_ElbowPoleV_translateY.o" "R_ElbowPoleV.ty";
connectAttr "R_ElbowPoleV_translateZ.o" "R_ElbowPoleV.tz";
connectAttr "R_ElbowPoleV_rotateX.o" "R_ElbowPoleV.rx";
connectAttr "R_ElbowPoleV_rotateY.o" "R_ElbowPoleV.ry";
connectAttr "R_ElbowPoleV_rotateZ.o" "R_ElbowPoleV.rz";
connectAttr "R_ElbowPoleV_scaleX.o" "R_ElbowPoleV.sx";
connectAttr "R_ElbowPoleV_scaleY.o" "R_ElbowPoleV.sy";
connectAttr "R_ElbowPoleV_scaleZ.o" "R_ElbowPoleV.sz";
connectAttr "Controls.di" "R_ElbowPoleV.do";
connectAttr "R_ElbowPoleV_visibility.o" "R_ElbowPoleV.v";
connectAttr "R_KneePoleV_translateX.o" "R_KneePoleV.tx";
connectAttr "R_KneePoleV_translateY.o" "R_KneePoleV.ty";
connectAttr "R_KneePoleV_translateZ.o" "R_KneePoleV.tz";
connectAttr "Controls.di" "R_KneePoleV.do";
connectAttr "R_KneePoleV_visibility.o" "R_KneePoleV.v";
connectAttr "R_KneePoleV_rotateX.o" "R_KneePoleV.rx";
connectAttr "R_KneePoleV_rotateY.o" "R_KneePoleV.ry";
connectAttr "R_KneePoleV_rotateZ.o" "R_KneePoleV.rz";
connectAttr "R_KneePoleV_scaleX.o" "R_KneePoleV.sx";
connectAttr "R_KneePoleV_scaleY.o" "R_KneePoleV.sy";
connectAttr "R_KneePoleV_scaleZ.o" "R_KneePoleV.sz";
connectAttr "L_KneePoleV_translateX.o" "L_KneePoleV.tx";
connectAttr "L_KneePoleV_translateY.o" "L_KneePoleV.ty";
connectAttr "L_KneePoleV_translateZ.o" "L_KneePoleV.tz";
connectAttr "Controls.di" "L_KneePoleV.do";
connectAttr "L_KneePoleV_visibility.o" "L_KneePoleV.v";
connectAttr "L_KneePoleV_rotateX.o" "L_KneePoleV.rx";
connectAttr "L_KneePoleV_rotateY.o" "L_KneePoleV.ry";
connectAttr "L_KneePoleV_rotateZ.o" "L_KneePoleV.rz";
connectAttr "L_KneePoleV_scaleX.o" "L_KneePoleV.sx";
connectAttr "L_KneePoleV_scaleY.o" "L_KneePoleV.sy";
connectAttr "L_KneePoleV_scaleZ.o" "L_KneePoleV.sz";
connectAttr "L_ElbowPoleV_translateX.o" "L_ElbowPoleV.tx";
connectAttr "L_ElbowPoleV_translateY.o" "L_ElbowPoleV.ty";
connectAttr "L_ElbowPoleV_translateZ.o" "L_ElbowPoleV.tz";
connectAttr "L_ElbowPoleV_rotateX.o" "L_ElbowPoleV.rx";
connectAttr "L_ElbowPoleV_rotateY.o" "L_ElbowPoleV.ry";
connectAttr "L_ElbowPoleV_rotateZ.o" "L_ElbowPoleV.rz";
connectAttr "L_ElbowPoleV_scaleX.o" "L_ElbowPoleV.sx";
connectAttr "L_ElbowPoleV_scaleY.o" "L_ElbowPoleV.sy";
connectAttr "L_ElbowPoleV_scaleZ.o" "L_ElbowPoleV.sz";
connectAttr "Controls.di" "L_ElbowPoleV.do";
connectAttr "L_ElbowPoleV_visibility.o" "L_ElbowPoleV.v";
connectAttr "ct_R_Wrist_translateX.o" "ct_R_Wrist.tx";
connectAttr "ct_R_Wrist_translateY.o" "ct_R_Wrist.ty";
connectAttr "ct_R_Wrist_translateZ.o" "ct_R_Wrist.tz";
connectAttr "ct_R_Wrist_rotateX.o" "ct_R_Wrist.rx";
connectAttr "ct_R_Wrist_rotateY.o" "ct_R_Wrist.ry";
connectAttr "ct_R_Wrist_rotateZ.o" "ct_R_Wrist.rz";
connectAttr "ct_R_Wrist_scaleX.o" "ct_R_Wrist.sx";
connectAttr "ct_R_Wrist_scaleY.o" "ct_R_Wrist.sy";
connectAttr "ct_R_Wrist_scaleZ.o" "ct_R_Wrist.sz";
connectAttr "Controls.di" "ct_R_Wrist.do";
connectAttr "ct_R_Wrist_visibility.o" "ct_R_Wrist.v";
connectAttr "ct_R_Hand_rotateX.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.rx";
connectAttr "ct_R_Hand_rotateY.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.ry";
connectAttr "ct_R_Hand_rotateZ.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.rz";
connectAttr "Controls.di" "|mainCtrl|ct_R_Wrist|ct_R_Hand.do";
connectAttr "ct_R_Hand_visibility.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.v";
connectAttr "ct_R_Hand_translateX.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.tx";
connectAttr "ct_R_Hand_translateY.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.ty";
connectAttr "ct_R_Hand_translateZ.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.tz";
connectAttr "ct_R_Hand_scaleX.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.sx";
connectAttr "ct_R_Hand_scaleY.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.sy";
connectAttr "ct_R_Hand_scaleZ.o" "|mainCtrl|ct_R_Wrist|ct_R_Hand.sz";
connectAttr "SwordModel.di" "SF_Wep_Undead_Longsword_01.do";
connectAttr "ct_L_Wrist_translateX.o" "ct_L_Wrist.tx";
connectAttr "ct_L_Wrist_translateY.o" "ct_L_Wrist.ty";
connectAttr "ct_L_Wrist_translateZ.o" "ct_L_Wrist.tz";
connectAttr "ct_L_Wrist_rotateX.o" "ct_L_Wrist.rx";
connectAttr "ct_L_Wrist_rotateY.o" "ct_L_Wrist.ry";
connectAttr "ct_L_Wrist_rotateZ.o" "ct_L_Wrist.rz";
connectAttr "ct_L_Wrist_scaleX.o" "ct_L_Wrist.sx";
connectAttr "ct_L_Wrist_scaleY.o" "ct_L_Wrist.sy";
connectAttr "ct_L_Wrist_scaleZ.o" "ct_L_Wrist.sz";
connectAttr "Controls.di" "ct_L_Wrist.do";
connectAttr "ct_L_Wrist_visibility.o" "ct_L_Wrist.v";
connectAttr "ct_R_Hand_rotateX1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.rx";
connectAttr "ct_R_Hand_rotateY1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.ry";
connectAttr "ct_R_Hand_rotateZ1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.rz";
connectAttr "Controls.di" "|mainCtrl|ct_L_Wrist|ct_R_Hand.do";
connectAttr "ct_R_Hand_visibility1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.v";
connectAttr "ct_R_Hand_translateX1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.tx";
connectAttr "ct_R_Hand_translateY1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.ty";
connectAttr "ct_R_Hand_translateZ1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.tz";
connectAttr "ct_R_Hand_scaleX1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.sx";
connectAttr "ct_R_Hand_scaleY1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.sy";
connectAttr "ct_R_Hand_scaleZ1.o" "|mainCtrl|ct_L_Wrist|ct_R_Hand.sz";
connectAttr "SwordModel.di" "SF_Wep_Undead_Shield_06.do";
connectAttr "Center_translateX.o" "Center.tx";
connectAttr "Center_translateY.o" "Center.ty";
connectAttr "Center_translateZ.o" "Center.tz";
connectAttr "Center_rotateX.o" "Center.rx";
connectAttr "Center_rotateY.o" "Center.ry";
connectAttr "Center_rotateZ.o" "Center.rz";
connectAttr "Center_scaleX.o" "Center.sx";
connectAttr "Center_scaleY.o" "Center.sy";
connectAttr "Center_scaleZ.o" "Center.sz";
connectAttr "Controls.di" "Center.do";
connectAttr "Center_visibility.o" "Center.v";
connectAttr "Hips_rotateX.o" "|mainCtrl|Center|Hips.rx";
connectAttr "Hips_rotateY.o" "|mainCtrl|Center|Hips.ry";
connectAttr "Hips_rotateZ.o" "|mainCtrl|Center|Hips.rz";
connectAttr "Controls.di" "|mainCtrl|Center|Hips.do";
connectAttr "Hips_visibility.o" "|mainCtrl|Center|Hips.v";
connectAttr "Hips_translateX.o" "|mainCtrl|Center|Hips.tx";
connectAttr "Hips_translateY.o" "|mainCtrl|Center|Hips.ty";
connectAttr "Hips_translateZ.o" "|mainCtrl|Center|Hips.tz";
connectAttr "Hips_scaleX.o" "|mainCtrl|Center|Hips.sx";
connectAttr "Hips_scaleY.o" "|mainCtrl|Center|Hips.sy";
connectAttr "Hips_scaleZ.o" "|mainCtrl|Center|Hips.sz";
connectAttr "Spine_rotateX.o" "Spine.rx";
connectAttr "Spine_rotateY.o" "Spine.ry";
connectAttr "Spine_rotateZ.o" "Spine.rz";
connectAttr "Controls.di" "Spine.do";
connectAttr "Spine_visibility.o" "Spine.v";
connectAttr "Spine_translateX.o" "Spine.tx";
connectAttr "Spine_translateY.o" "Spine.ty";
connectAttr "Spine_translateZ.o" "Spine.tz";
connectAttr "Spine_scaleX.o" "Spine.sx";
connectAttr "Spine_scaleY.o" "Spine.sy";
connectAttr "Spine_scaleZ.o" "Spine.sz";
connectAttr "Chest_Ctrl_rotateX.o" "Chest_Ctrl.rx";
connectAttr "Chest_Ctrl_rotateY.o" "Chest_Ctrl.ry";
connectAttr "Chest_Ctrl_rotateZ.o" "Chest_Ctrl.rz";
connectAttr "Controls.di" "Chest_Ctrl.do";
connectAttr "Chest_Ctrl_visibility.o" "Chest_Ctrl.v";
connectAttr "Chest_Ctrl_translateX.o" "Chest_Ctrl.tx";
connectAttr "Chest_Ctrl_translateY.o" "Chest_Ctrl.ty";
connectAttr "Chest_Ctrl_translateZ.o" "Chest_Ctrl.tz";
connectAttr "Chest_Ctrl_scaleX.o" "Chest_Ctrl.sx";
connectAttr "Chest_Ctrl_scaleY.o" "Chest_Ctrl.sy";
connectAttr "Chest_Ctrl_scaleZ.o" "Chest_Ctrl.sz";
connectAttr "HeadCtrl_rotateX.o" "HeadCtrl.rx";
connectAttr "HeadCtrl_rotateY.o" "HeadCtrl.ry";
connectAttr "HeadCtrl_rotateZ.o" "HeadCtrl.rz";
connectAttr "Controls.di" "HeadCtrl.do";
connectAttr "HeadCtrl_visibility.o" "HeadCtrl.v";
connectAttr "HeadCtrl_translateX.o" "HeadCtrl.tx";
connectAttr "HeadCtrl_translateY.o" "HeadCtrl.ty";
connectAttr "HeadCtrl_translateZ.o" "HeadCtrl.tz";
connectAttr "HeadCtrl_scaleX.o" "HeadCtrl.sx";
connectAttr "HeadCtrl_scaleY.o" "HeadCtrl.sy";
connectAttr "HeadCtrl_scaleZ.o" "HeadCtrl.sz";
connectAttr "ct_L_Foot_Heel_Lift.o" "ct_L_Foot.Heel_Lift";
connectAttr "ct_L_Foot_translateX.o" "ct_L_Foot.tx";
connectAttr "ct_L_Foot_translateY.o" "ct_L_Foot.ty";
connectAttr "ct_L_Foot_translateZ.o" "ct_L_Foot.tz";
connectAttr "ct_L_Foot_rotateX.o" "ct_L_Foot.rx";
connectAttr "ct_L_Foot_rotateY.o" "ct_L_Foot.ry";
connectAttr "ct_L_Foot_rotateZ.o" "ct_L_Foot.rz";
connectAttr "ct_L_Foot_scaleX.o" "ct_L_Foot.sx";
connectAttr "ct_L_Foot_scaleY.o" "ct_L_Foot.sy";
connectAttr "ct_L_Foot_scaleZ.o" "ct_L_Foot.sz";
connectAttr "Controls.di" "ct_L_Foot.do";
connectAttr "ct_L_Foot_visibility.o" "ct_L_Foot.v";
connectAttr "ct_R_Foot_translateX.o" "ct_R_Foot.tx";
connectAttr "ct_R_Foot_translateY.o" "ct_R_Foot.ty";
connectAttr "ct_R_Foot_translateZ.o" "ct_R_Foot.tz";
connectAttr "ct_R_Foot_rotateX.o" "ct_R_Foot.rx";
connectAttr "ct_R_Foot_rotateY.o" "ct_R_Foot.ry";
connectAttr "ct_R_Foot_rotateZ.o" "ct_R_Foot.rz";
connectAttr "ct_R_Foot_scaleX.o" "ct_R_Foot.sx";
connectAttr "ct_R_Foot_scaleY.o" "ct_R_Foot.sy";
connectAttr "ct_R_Foot_scaleZ.o" "ct_R_Foot.sz";
connectAttr "Controls.di" "ct_R_Foot.do";
connectAttr "ct_R_Foot_visibility.o" "ct_R_Foot.v";
connectAttr "layer1.di" "pCube1.do";
connectAttr "polyCube1.out" "pCubeShape1.i";
connectAttr "layer1.di" "pCube2.do";
connectAttr "layer1.di" "pCube3.do";
connectAttr "polyCube2.out" "pCubeShape3.i";
connectAttr "layer1.di" "pCube4.do";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "SF_Character_Knight_01SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "SF_Wep_Undead_Shield_06SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "SF_Wep_Undead_Longsword_01SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "SF_Character_Knight_01SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "SF_Wep_Undead_Shield_06SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "SF_Wep_Undead_Longsword_01SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "file1.oc" "Default_Material.c";
connectAttr "Default_Material.oc" "SF_Character_Knight_01SG.ss";
connectAttr "SF_Character_KnightShape.iog" "SF_Character_Knight_01SG.dsm" -na;
connectAttr "SF_Character_Knight_01SG.msg" "materialInfo1.sg";
connectAttr "Default_Material.msg" "materialInfo1.m";
connectAttr "file1.msg" "materialInfo1.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "layerManager.dli[1]" "Ground.id";
connectAttr "layerManager.dli[3]" "Bones.id";
connectAttr "layerManager.dli[4]" "ModeloCompleto.id";
connectAttr "layerManager.dli[5]" "Controls.id";
connectAttr "lambert2.oc" "SF_Wep_Undead_Shield_06SG.ss";
connectAttr "SF_Wep_Undead_Shield_06Shape.iog" "SF_Wep_Undead_Shield_06SG.dsm" -na
		;
connectAttr "SF_Wep_Undead_Shield_06SG.msg" "materialInfo2.sg";
connectAttr "lambert2.msg" "materialInfo2.m";
connectAttr "place2dTexture2.o" "file148.uv";
connectAttr "place2dTexture2.ofu" "file148.ofu";
connectAttr "place2dTexture2.ofv" "file148.ofv";
connectAttr "place2dTexture2.rf" "file148.rf";
connectAttr "place2dTexture2.reu" "file148.reu";
connectAttr "place2dTexture2.rev" "file148.rev";
connectAttr "place2dTexture2.vt1" "file148.vt1";
connectAttr "place2dTexture2.vt2" "file148.vt2";
connectAttr "place2dTexture2.vt3" "file148.vt3";
connectAttr "place2dTexture2.vc1" "file148.vc1";
connectAttr "place2dTexture2.ofs" "file148.fs";
connectAttr ":defaultColorMgtGlobals.cme" "file148.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file148.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file148.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file148.ws";
connectAttr "lambert3.oc" "SF_Wep_Undead_Longsword_01SG.ss";
connectAttr "SF_Wep_Undead_Longsword_01Shape.iog" "SF_Wep_Undead_Longsword_01SG.dsm"
		 -na;
connectAttr "SF_Wep_Undead_Longsword_01SG.msg" "materialInfo3.sg";
connectAttr "lambert3.msg" "materialInfo3.m";
connectAttr "place2dTexture3.o" "file149.uv";
connectAttr "place2dTexture3.ofu" "file149.ofu";
connectAttr "place2dTexture3.ofv" "file149.ofv";
connectAttr "place2dTexture3.rf" "file149.rf";
connectAttr "place2dTexture3.reu" "file149.reu";
connectAttr "place2dTexture3.rev" "file149.rev";
connectAttr "place2dTexture3.vt1" "file149.vt1";
connectAttr "place2dTexture3.vt2" "file149.vt2";
connectAttr "place2dTexture3.vt3" "file149.vt3";
connectAttr "place2dTexture3.vc1" "file149.vc1";
connectAttr "place2dTexture3.ofs" "file149.fs";
connectAttr ":defaultColorMgtGlobals.cme" "file149.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file149.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file149.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file149.ws";
connectAttr "layerManager.dli[6]" "SwordModel.id";
connectAttr "layerManager.dli[7]" "ShiedModel.id";
connectAttr "layerManager.dli[8]" "G.id";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "Spine01.wm" "skinCluster1.ma[1]";
connectAttr "HeadBase.wm" "skinCluster1.ma[3]";
connectAttr "L_Humerus.wm" "skinCluster1.ma[4]";
connectAttr "L_ForeArm.wm" "skinCluster1.ma[5]";
connectAttr "L_Hand.wm" "skinCluster1.ma[6]";
connectAttr "R_Humerus.wm" "skinCluster1.ma[7]";
connectAttr "R_ForeArm.wm" "skinCluster1.ma[8]";
connectAttr "R_Hand.wm" "skinCluster1.ma[9]";
connectAttr "L_Femur.wm" "skinCluster1.ma[10]";
connectAttr "L_Knee.wm" "skinCluster1.ma[11]";
connectAttr "HipsSway.wm" "skinCluster1.ma[12]";
connectAttr "L_Ball.wm" "skinCluster1.ma[13]";
connectAttr "L_Ankle.wm" "skinCluster1.ma[14]";
connectAttr "R_Femur.wm" "skinCluster1.ma[16]";
connectAttr "R_Knee.wm" "skinCluster1.ma[17]";
connectAttr "R_Ankle.wm" "skinCluster1.ma[18]";
connectAttr "R_Ball.wm" "skinCluster1.ma[19]";
connectAttr "Spine01.liw" "skinCluster1.lw[1]";
connectAttr "HeadBase.liw" "skinCluster1.lw[3]";
connectAttr "L_Humerus.liw" "skinCluster1.lw[4]";
connectAttr "L_ForeArm.liw" "skinCluster1.lw[5]";
connectAttr "L_Hand.liw" "skinCluster1.lw[6]";
connectAttr "R_Humerus.liw" "skinCluster1.lw[7]";
connectAttr "R_ForeArm.liw" "skinCluster1.lw[8]";
connectAttr "R_Hand.liw" "skinCluster1.lw[9]";
connectAttr "L_Femur.liw" "skinCluster1.lw[10]";
connectAttr "L_Knee.liw" "skinCluster1.lw[11]";
connectAttr "HipsSway.liw" "skinCluster1.lw[12]";
connectAttr "L_Ball.liw" "skinCluster1.lw[13]";
connectAttr "L_Ankle.liw" "skinCluster1.lw[14]";
connectAttr "R_Femur.liw" "skinCluster1.lw[16]";
connectAttr "R_Knee.liw" "skinCluster1.lw[17]";
connectAttr "R_Ankle.liw" "skinCluster1.lw[18]";
connectAttr "R_Ball.liw" "skinCluster1.lw[19]";
connectAttr "Spine01.obcc" "skinCluster1.ifcl[1]";
connectAttr "HeadBase.obcc" "skinCluster1.ifcl[3]";
connectAttr "L_Humerus.obcc" "skinCluster1.ifcl[4]";
connectAttr "L_ForeArm.obcc" "skinCluster1.ifcl[5]";
connectAttr "L_Hand.obcc" "skinCluster1.ifcl[6]";
connectAttr "R_Humerus.obcc" "skinCluster1.ifcl[7]";
connectAttr "R_ForeArm.obcc" "skinCluster1.ifcl[8]";
connectAttr "R_Hand.obcc" "skinCluster1.ifcl[9]";
connectAttr "L_Femur.obcc" "skinCluster1.ifcl[10]";
connectAttr "L_Knee.obcc" "skinCluster1.ifcl[11]";
connectAttr "HipsSway.obcc" "skinCluster1.ifcl[12]";
connectAttr "L_Ball.obcc" "skinCluster1.ifcl[13]";
connectAttr "L_Ankle.obcc" "skinCluster1.ifcl[14]";
connectAttr "R_Femur.obcc" "skinCluster1.ifcl[16]";
connectAttr "R_Knee.obcc" "skinCluster1.ifcl[17]";
connectAttr "R_Ankle.obcc" "skinCluster1.ifcl[18]";
connectAttr "R_Ball.obcc" "skinCluster1.ifcl[19]";
connectAttr "HipsSway.msg" "skinCluster1.ptt";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "SF_Character_KnightShape.iog.og[0]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "SF_Character_KnightShape.iog.og[1]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "SF_Character_KnightShapeOrig1.w" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "reference.msg" "bindPose1.m[0]";
connectAttr "|reference|Hips.msg" "bindPose1.m[1]";
connectAttr "Spine01.msg" "bindPose1.m[2]";
connectAttr "Spine02.msg" "bindPose1.m[3]";
connectAttr "HeadBase.msg" "bindPose1.m[4]";
connectAttr "L_Clavicle.msg" "bindPose1.m[5]";
connectAttr "L_Humerus.msg" "bindPose1.m[6]";
connectAttr "L_ForeArm.msg" "bindPose1.m[7]";
connectAttr "L_Hand.msg" "bindPose1.m[8]";
connectAttr "R_Clavicle.msg" "bindPose1.m[9]";
connectAttr "R_Humerus.msg" "bindPose1.m[10]";
connectAttr "R_ForeArm.msg" "bindPose1.m[11]";
connectAttr "R_Hand.msg" "bindPose1.m[12]";
connectAttr "HipsSway.msg" "bindPose1.m[13]";
connectAttr "L_Femur.msg" "bindPose1.m[14]";
connectAttr "L_Knee.msg" "bindPose1.m[15]";
connectAttr "L_Ankle.msg" "bindPose1.m[16]";
connectAttr "L_Ball.msg" "bindPose1.m[17]";
connectAttr "R_Femur.msg" "bindPose1.m[19]";
connectAttr "R_Knee.msg" "bindPose1.m[20]";
connectAttr "R_Ankle.msg" "bindPose1.m[21]";
connectAttr "R_Ball.msg" "bindPose1.m[22]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[2]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[3]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[6]" "bindPose1.p[7]";
connectAttr "bindPose1.m[7]" "bindPose1.p[8]";
connectAttr "bindPose1.m[3]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[11]" "bindPose1.p[12]";
connectAttr "bindPose1.m[1]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[14]" "bindPose1.p[15]";
connectAttr "bindPose1.m[15]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[13]" "bindPose1.p[19]";
connectAttr "bindPose1.m[19]" "bindPose1.p[20]";
connectAttr "bindPose1.m[20]" "bindPose1.p[21]";
connectAttr "bindPose1.m[21]" "bindPose1.p[22]";
connectAttr "reference.bps" "bindPose1.wm[0]";
connectAttr "|reference|Hips.bps" "bindPose1.wm[1]";
connectAttr "Spine01.bps" "bindPose1.wm[2]";
connectAttr "Spine02.bps" "bindPose1.wm[3]";
connectAttr "HeadBase.bps" "bindPose1.wm[4]";
connectAttr "L_Clavicle.bps" "bindPose1.wm[5]";
connectAttr "L_Humerus.bps" "bindPose1.wm[6]";
connectAttr "L_ForeArm.bps" "bindPose1.wm[7]";
connectAttr "L_Hand.bps" "bindPose1.wm[8]";
connectAttr "R_Clavicle.bps" "bindPose1.wm[9]";
connectAttr "R_Humerus.bps" "bindPose1.wm[10]";
connectAttr "R_ForeArm.bps" "bindPose1.wm[11]";
connectAttr "R_Hand.bps" "bindPose1.wm[12]";
connectAttr "HipsSway.bps" "bindPose1.wm[13]";
connectAttr "L_Femur.bps" "bindPose1.wm[14]";
connectAttr "L_Knee.bps" "bindPose1.wm[15]";
connectAttr "L_Ankle.bps" "bindPose1.wm[16]";
connectAttr "L_Ball.bps" "bindPose1.wm[17]";
connectAttr "R_Femur.bps" "bindPose1.wm[19]";
connectAttr "R_Knee.bps" "bindPose1.wm[20]";
connectAttr "R_Ankle.bps" "bindPose1.wm[21]";
connectAttr "R_Ball.bps" "bindPose1.wm[22]";
connectAttr "layerManager.dli[2]" "layer1.id";
connectAttr "SF_Character_Knight_01SG.pa" ":renderPartition.st" -na;
connectAttr "SF_Wep_Undead_Shield_06SG.pa" ":renderPartition.st" -na;
connectAttr "SF_Wep_Undead_Longsword_01SG.pa" ":renderPartition.st" -na;
connectAttr "Default_Material.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert3.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file148.msg" ":defaultTextureList1.tx" -na;
connectAttr "file149.msg" ":defaultTextureList1.tx" -na;
connectAttr "groundShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "L_ElbowPoleVShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "R_ElbowPoleVShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "R_KneePoleVShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "L_KneePoleVShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of ams_kk_mainCharKnight-animations_states.ma
