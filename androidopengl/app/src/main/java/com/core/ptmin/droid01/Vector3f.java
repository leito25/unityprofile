package com.core.ptmin.droid01;

/**
 * Created by ptmind on 22/03/18.
 */

//Clase Vector 3

public class Vector3f {

    public float x;
    public float y;
    public float z;

    public Vector3f(float _x, float _y, float _z )
    {

        x = _x;
        y = _y;
        z = _z;

    }

    public void Adicionar(Vector3f v)
    {
        //Suma del vectores
        x += v.x;
        y += v.y;
        z += v.z;
    }

    public void Restar(Vector3f v)
    {
        //Resta de vectores
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }

    public void Multip(float f)
    {
        //Multiplicacion
        x *= f;
        y *= f;
        z *= f;
    }

    //Función para normalizar el vector
    //y encontrar la longitud
    // (la longitud es como el valor absoluto pero en un vector)
    public  void Normalizar()
    {
        //Ecuación para sacar la longitud
        //longitud = raziCuadrada(VecA a la 2 + VecB a la 2 . ...  VecN a la 2);
        float longitudVector = (float) Math.sqrt(x*x + y*y + z*z);

        //resultado de la normalización
        x /= longitudVector;
        y /= longitudVector;
        z /= longitudVector;
    }

    //Calcular el dotProduct
    public float dotProduct(Vector3f v)
    {
        //La ecuación es
        //segun dos vectores A, B
        //Ax Ay Az seria el valor del vector local
        //Bx By Bz seria el valor que viene como parametro
        //al momento de ejecutar esta función.
        //entonces el dotProduct es un scalar resultado de
        //dotProduct = (Ax * Bx) + (Ay * By) + (Az * Bz)


        float _x = x * v.x;
        float _y = y * v.y;
        float _z = z * v.z;

        return _x + _y + _z;

    }

    //Calcular el crossProduct
    public Vector3f crossProduct(Vector3f v)
    {
        float _x = y * v.z - z * v.y;
        float _y = z * v.x - x * v.z;
        float _z = x * v.y - y * v.x;

        Vector3f cP = new Vector3f(_x , _y, _z);
        return cP;
    }



}
