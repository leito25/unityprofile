package com.core.ptmin.droid01;

import android.opengl.GLES20;
import android.util.Log;

import static android.opengl.GLES20.*;

/**
 * Created by ptmind on 20/03/18.
 *
 * Clase para manipular los shaders
 * TODO - Hacer que los shaders se puedan cargar local o desde intenet
 */


//Clase para manipular los shaders
    //

public class managerShaders {

    public static int shaderGlobal;

    //método para el vertex
    public static int compiladorVertex(String sombreado)
    {
        //
        return compilarShader(GL_VERTEX_SHADER, sombreado);
    }


    //método para el fragment
    public static int compiladorFragment(String sombreado)
    {
        //
        return compilarShader(GL_FRAGMENT_SHADER, sombreado);
    }

    //método compilador
    public static int compilarShader(int tipo, String some_shader)
    //Tipo es el Tipo de shader
    //PerFragment o PerVertex

    //someShader es el código fuente del shader


    {
        //compilación del shader
        final int ObjetoSombreado = glCreateShader(tipo);
        shaderGlobal = ObjetoSombreado;

        //catch error
        if (ObjetoSombreado == 0)
        {
            return 0;
        }

        glShaderSource(ObjetoSombreado, some_shader);
        glCompileShader(ObjetoSombreado);

        final int[] compilarStatus = new int[1];
        glGetShaderiv(ObjetoSombreado, GL_COMPILE_STATUS, compilarStatus, 0);

        if(compilarStatus[0] == 0)
        {
            glDeleteShader(ObjetoSombreado);

            return 0;

        }

        return ObjetoSombreado;


    }

    public static int enlacePrograma(int vertexShader, int fragmentShader){

        int programaObjeto = glCreateProgram();

        if(programaObjeto == 0)
        {
            return 0;
        }

        glAttachShader(programaObjeto, vertexShader);
        glAttachShader(programaObjeto, fragmentShader);

        glLinkProgram(programaObjeto);

        final int[] linkStatus = new int[1];

        glGetProgramiv(programaObjeto, GL_LINK_STATUS, linkStatus, 0);

        //String error  = glGetShaderInfoLog(shaderGlobal);

        //Log.d("ShaderOk", (error));

        if (linkStatus[0] == 0)
        {
            glDeleteProgram(programaObjeto);
            return 0;
        }

        return programaObjeto;

    }

}
