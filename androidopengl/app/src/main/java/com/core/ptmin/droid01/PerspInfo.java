package com.core.ptmin.droid01;

/**
 * Created by innovativeeducation on 30/03/18.
 */

public class PerspInfo {

    public float FOV;
    public float Ancho;
    public float Alto;
    public float zCercano;
    public float zLejano;

    public PerspInfo(float f, float w, float h, float zn, float zf)
    {
        //Este es el contructor de la clase Persepctiva
        FOV = f;
        Ancho = w;
        Alto = h;
        zCercano = zn;
        zLejano = zf;
    }

}
