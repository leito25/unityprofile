package com.core.ptmin.droid01;

/**
 * Created by ptmind on 30/03/18.
 */

public class Pipeline {

    public Vector3f mScale;
    public Vector3f mPos;
    public Vector3f mRot;

    public Vector3f mCamPos;
    public Vector3f mCamTarget;
    public Vector3f mCamUp;

    public PerspInfo mPersp;

    public Matrizx4f mWorldTrans; //World transformation matrix
    public Matrizx4f mWVPTrans; //view transformation matrix

    public Pipeline()
    {
        mScale = new Vector3f(1.0f, 1.0f, 1.0f);
        mPos = new Vector3f(0.0f, 0.0f, 0.0f);
        mRot = new Vector3f(0.0f, 0.0f, 0.0f);

    }

    public void Scale(Vector3f sc)
    {
        mScale = sc;
    }

    public void WorldPos(Vector3f pos)
    {
        mPos = pos;
    }

    public void Rotate(Vector3f rot)
    {
        mRot = rot;
    }

    public void SetPersp(PerspInfo pr)
    {
        mPersp = pr;
    }

    public void SetCamera(Vector3f pos, Vector3f target, Vector3f up)
    {
        mCamPos = pos;
        mCamTarget = target;
        mCamUp = up;
    }

    public Matrizx4f getWorldTrans()//transforaciones del mundo
    {
        Matrizx4f scale = new Matrizx4f();
        Matrizx4f rot = new Matrizx4f();
        Matrizx4f pos = new Matrizx4f();

        scale.escalaTrans(mScale);
        rot.rotateTransform(mRot);
        pos.translacionTrans(mPos);

        Matrizx4f sc = pos.mult(rot);
        mWorldTrans = sc.mult(pos);

        return mWorldTrans;
    }

    public Matrizx4f getWVPTrans()//transformaciones de la camara
    {
        getWorldTrans();

        Matrizx4f cameraTrans = new Matrizx4f();
        Matrizx4f cameraRot = new Matrizx4f();
        Matrizx4f persp = new Matrizx4f();

        cameraTrans.translacionTrans(mCamPos);
        cameraRot.cameraTransform(mCamTarget, mCamUp);
        persp.perspProjMatrix(mPersp);

        //Multiplicacion de matrices de la camara
        Matrizx4f p_cr = persp.mult(cameraRot);
        Matrizx4f p_cr_cp = p_cr.mult(cameraTrans);
        mWVPTrans = p_cr_cp.mult(mWorldTrans);

        return mWVPTrans;//vector de resultado

    }







}
