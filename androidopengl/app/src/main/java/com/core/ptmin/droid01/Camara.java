package com.core.ptmin.droid01;

/**
 * Created by innovativeeducation on 30/03/18.
 */

public class Camara {

    public Vector3f mPos;
    public Vector3f mTarget;
    public Vector3f mSideUp;

    public Camara(Vector3f pos, Vector3f targ, Vector3f up)
    {
        mPos = pos;
        mTarget = targ;
        mSideUp = up;
    }


}
