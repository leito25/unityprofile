package com.core.ptmin.droid01;

/**
 * Created by ptmind on 22/03/18.
 */

//Clase para tipo matriz de 4dimensiones

public class Matrizx4f {

    // Declararacion de la matriz
    public float m[][] = new float[4][4];

    // Matriz de identidad
    public void InitIdentidad()
    {
        m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
        m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
        m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
        m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
    }


    //Matrix de Proyección
    public void perspProjMatrix(PerspInfo p)
    {
        //
        float ar = p.Ancho / p.Alto;
        float zRange = p.zCercano - p.zLejano;
        float tanHalfFOV = (float) Math.tan(Math.toRadians(p.FOV/2.0));

        m[0][0] = 1.0f/(tanHalfFOV * ar); m[0][1] = 0.0f;            m[0][2] = 0.0f;            				m[0][3] = 0.0f;
        m[1][0] = 0.0f;                   m[1][1] = 1.0f/tanHalfFOV; m[1][2] = 0.0f;            				m[1][3] = 0.0f;
        m[2][0] = 0.0f;                   m[2][1] = 0.0f;            m[2][2] = (-p.zCercano - p.zLejano)/zRange ; 	m[2][3] = 2f * p.zLejano * (p.zCercano / zRange);
        m[3][0] = 0.0f;                   m[3][1] = 0.0f;            m[3][2] = 1.0f; 							m[3][3] = 0.0f;

    }

    public void cameraTransform(Vector3f targ, Vector3f up)
    {
        //creación de la U y V

        //normalizo el target vector-point
        Vector3f n = targ;
        n.Normalizar();

        //Normalizo el upside vector
        Vector3f u = up;
        u.Normalizar();

        //cross product para sacar el vector derecho guia
        u = u.crossProduct(n); //priemero up contra n
        Vector3f v = n.crossProduct(u); //despues n contra up

        //matriz de proyexion en 3 dimensiones
        m[0][0] = u.x;   m[0][1] = u.y;   m[0][2] = u.z;   m[0][3] = 0.0f;
        m[1][0] = v.x;   m[1][1] = v.y;   m[1][2] = v.z;   m[1][3] = 0.0f;
        m[2][0] = n.x;   m[2][1] = n.y;   m[2][2] = n.z;   m[2][3] = 0.0f;
        m[3][0] = 0.0f;  m[3][1] = 0.0f;  m[3][2] = 0.0f;  m[3][3] = 1.0f;

    }

    //Matrices de transformación
    // Matriz de escalamiento
    public void escalaTrans(Vector3f escala) {
        m[0][0] = escala.x; m[0][1] = 0.0f;    m[0][2] = 0.0f;    m[0][3] = 0.0f;
        m[1][0] = 0.0f;    m[1][1] = escala.y; m[1][2] = 0.0f;    m[1][3] = 0.0f;
        m[2][0] = 0.0f;    m[2][1] = 0.0f;    m[2][2] = escala.z; m[2][3] = 0.0f;
        m[3][0] = 0.0f;    m[3][1] = 0.0f;    m[3][2] = 0.0f;    m[3][3] = 1.0f;

    }

    // Create a translate matrix
    public void translacionTrans(Vector3f trans) {
        m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = trans.x;
        m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = trans.y;
        m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = trans.z;
        m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;

    }

    //multiplicación de dos matrices4
    public Matrizx4f mult(Matrizx4f r) {
        Matrizx4f ret = new Matrizx4f();

        for(int i=0; i<4; i++) {
            for(int j=0; j<4; j++) {
                ret.m[i][j] = m[i][0] * r.m[0][j] +
                        m[i][1] * r.m[1][j] +
                        m[i][2] * r.m[2][j] +
                        m[i][3] * r.m[3][j];
            }
        }

        return ret;
    }

    //función de rotación la cual se
    //debe hacer en los 3 ejes
    public void rotateTransform(Vector3f rot) {
        Matrizx4f rx = new Matrizx4f();
        Matrizx4f ry = new Matrizx4f();
        Matrizx4f rz = new Matrizx4f();

        // Convert the angles to radians
        float x = (float) Math.toRadians(rot.x);
        float y = (float) Math.toRadians(rot.y);
        float z = (float) Math.toRadians(rot.z);

        // Create the X-Rotation matrix
        rx.m[0][0] = 1.0f; rx.m[0][1] = 0.0f;	 			 rx.m[0][2] = 0.0f; 				rx.m[0][3] = 0.0f;
        rx.m[1][0] = 0.0f; rx.m[1][1] = (float) Math.cos(x); rx.m[1][2] = (float) -Math.sin(x); rx.m[1][3] = 0.0f;
        rx.m[2][0] = 0.0f; rx.m[2][1] = (float) Math.sin(x); rx.m[2][2] = (float) Math.cos(x) ; rx.m[2][3] = 0.0f;
        rx.m[3][0] = 0.0f; rx.m[3][1] = 0.0f; 				 rx.m[3][2] = 0.0f;		 			rx.m[3][3] = 1.0f;

        // Create the Y-Rotation matrix
        ry.m[0][0] = (float) Math.cos(y); ry.m[0][1] = 0.0f; ry.m[0][2] = (float) -Math.sin(y); ry.m[0][3] = 0.0f;
        ry.m[1][0] = 0.0f; 				  ry.m[1][1] = 1.0f; ry.m[1][2] = 0.0f; 				ry.m[1][3] = 0.0f;
        ry.m[2][0] = (float) Math.sin(y); ry.m[2][1] = 0.0f; ry.m[2][2] = (float) Math.cos(y) ; ry.m[2][3] = 0.0f;
        ry.m[3][0] = 0.0f; 				  ry.m[3][1] = 0.0f; ry.m[3][2] = 0.0f; 				ry.m[3][3] = 1.0f;

        // Create the Z-Rotation matrix
        rz.m[0][0] = (float) Math.cos(z); rz.m[0][1] = (float) -Math.sin(z); rz.m[0][2] = 0.0f; rz.m[0][3] = 0.0f;
        rz.m[1][0] = (float) Math.sin(z); rz.m[1][1] = (float) Math.cos(z) ; rz.m[1][2] = 0.0f; rz.m[1][3] = 0.0f;
        rz.m[2][0] = 0.0f; 				  rz.m[2][1] = 0.0f; 				 rz.m[2][2] = 1.0f; rz.m[2][3] = 0.0f;
        rz.m[3][0] = 0.0f; 				  rz.m[3][1] = 0.0f; 				 rz.m[3][2] = 0.0f; rz.m[3][3] = 1.0f;

        // Multiply all the matrices together
        Matrizx4f rzy = rz.mult(ry);
        Matrizx4f r = rzy.mult(rx);

        m = r.m;

    }




}
