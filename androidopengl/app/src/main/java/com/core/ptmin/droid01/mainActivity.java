package com.core.ptmin.droid01;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Build;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


public class mainActivity extends Activity {

    //Vars para el OpenGL
    private GLSurfaceView glSurfaceVista;
    private boolean rendererSet = false;
    private TextView greetingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        //CONSTRUCCIÓN DE UNA ACTIVIDAD OPENGL
        glSurfaceVista = new GLSurfaceView(this);

        //TOMAR LA INFORMACIÓN DEL DISPOSITIVO
        final ActivityManager actividadManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configInformacion = actividadManager.getDeviceConfigurationInfo();

        //VERIABLES DE CONTROL
        final boolean supportsEs2 = configInformacion.reqGlEsVersion >= 0x20000
                || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                && (Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")));

        //condicion para la variable de control
        if (supportsEs2)
        {
            Log.d("myTag", ("This is my message" + supportsEs2));
            glSurfaceVista.setEGLContextClientVersion(2);

            glSurfaceVista.setRenderer(new OpenGLTutorialRenderizador(this));
            rendererSet = true;

        } else {
            Toast.makeText(this, "Este dispositivo no soporta ES", Toast.LENGTH_LONG).show();
            return;
        }


        //Aqui se llama un layout normal.
        //setContentView(R.layout.mainlayout);


        //aqui se llama un layout para openGL
        //setContentView(R.layout.activity_open_gl);

        //Este es un textico para mostrar los cambios
        //greetingView = (TextView) findViewById(R.id.Greeting);

        //Poner la view como OpenGL
        setContentView(glSurfaceVista);

    }

    //on pause - on resume


    @Override
    protected void onPause() {
        super.onPause();
        if(rendererSet){
            glSurfaceVista.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(rendererSet){
            glSurfaceVista.onResume();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        //despliega el menu, se puede utilizar para agregar opciones
        getMenuInflater().inflate(R.menu.activite_main , menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_goodbye)
        {
            // TODO: say good bye

            greetingView.setText(R.string.aurevoir);

            return true;

        }

        if (item.getItemId() == R.id.close)
        {

            System.exit(0);

        }



        return super.onOptionsItemSelected(item);
    }
}
