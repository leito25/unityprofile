package com.core.ptmin.droid01;


import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glViewport;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glUniform4f;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDrawArrays;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

class OpenGLTutorialRenderizador implements Renderer {

    private final Context context;

    //Formato para pasar los datos
    private static final int FLOAT_BYTES = 4;
    private final FloatBuffer verticesData;

    //Variables de los shader
    private static final String U_COLOUR = "u_Color";
    private int uColourLocation;

    private static final String A_POS = "a_Pos";
    private int aPositionLocation;


    //Formato para el código de los shaders
    //VERTEX SHADER CODE
    String vertexShader_Code =
            "attribute vec4 a_Pos; 		\n"

                    + 	"void main() 				\n "
                    + 	"{ 							\n"
                    + 	"gl_Position = a_Pos;		\n"
                    + 	"}";


    //FRAGMENTE SHADER CODE
    String fragmentShader_Code =
            "precision mediump float; 		\n"

                    +	"uniform vec4 u_Color; 		\n"

                    +	"void main() 					\n"
                    +	"{ 								\n"
                    +		"gl_FragColor = u_Color;	\n"
                    +	"}";



    //creación de un arreglo con los vertices
    //figura de ejemplo
    //TODO - Crear clases para los tipos y para los shapes
    //TODO - Editor de shaders a partir de commands

    float[] VerticeCuadrado = {
            -0.5f, -0.5f,
            0.5f, -0.5f,
            -0.5f, 0.5f,
            -0.5f, 0.5f,
            0.5f, -0.5f,
            0.5f, 0.5f
    };

    private int programa_run;


    public OpenGLTutorialRenderizador(Context context){

        //declaración del contexto
        this.context = context;


        //contrucción de los datos serializados
        //Se declara un tamaño
        verticesData = ByteBuffer.allocateDirect(VerticeCuadrado.length * FLOAT_BYTES)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        //Se asignan los datos
        verticesData.put(VerticeCuadrado);



    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

        int verShader = managerShaders.compiladorVertex(vertexShader_Code);
        int fragShader = managerShaders.compiladorFragment(fragmentShader_Code);

        programa_run = managerShaders.enlacePrograma(verShader, fragShader);

        glUseProgram(programa_run);


        //
        uColourLocation = glGetUniformLocation(programa_run, U_COLOUR);
        aPositionLocation = glGetAttribLocation(programa_run, A_POS);

        verticesData.position(0);
        glVertexAttribPointer(aPositionLocation, 2, GL_FLOAT, false, 0, verticesData);

        glEnableVertexAttribArray(aPositionLocation);

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        glViewport(0,0, width, height);

    }

    @Override
    public void onDrawFrame(GL10 arg0) {
        glClear(GL_COLOR_BUFFER_BIT);



        glUniform4f(uColourLocation, 1.0f, 0.0f, 0.0f, 1.0f);
        glDrawArrays(GL_TRIANGLES, 0, 6);

    }
}
